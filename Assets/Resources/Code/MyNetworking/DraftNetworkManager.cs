using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class DraftNetworkManager : NetworkManager{

	private int port = 7777;
	private string ipAddress = "localhost";
	
	public GameObject thePlayerPrefab;
	public bool started = false;

	private GameObject draftModelObject;	
	List<NetworkConnection> conns = new List<NetworkConnection>();
	List<short> pcIds = new List<short>();

	void Start()
	{
		DontDestroyOnLoad(this.gameObject);
	}

	void OnGUI()
	{
		if (!started)
		{
			if (GUI.Button(new Rect(100, 100, 250, 100), "Start as Host"))
				StartHost();
			
			if (GUI.Button(new Rect(100, 250, 250, 100), "Start as Client"))
				JoinGame();
		}
	}

	private void init()
	{
		UnityEngine.Networking.NetworkManager.singleton.networkAddress = ipAddress;
		UnityEngine.Networking.NetworkManager.singleton.networkPort = 7777;
	}

	public void StartHost()
	{
		init();
		UnityEngine.Networking.NetworkManager.singleton.StartHost();
		started = true;
	}

	public void JoinGame()
	{
		init();
		UnityEngine.Networking.NetworkManager.singleton.StartClient();
		started = true;
	}

	
	public override void OnServerConnect(NetworkConnection conn)
	{
		base.OnServerConnect(conn);
		MyLogger.log("SERVER CONNECTED");
		
		if (draftModelObject == null)
		{
			//if (isServer)
			draftModelObject = Instantiate (Resources.Load("DraftModelPrefab")) as GameObject;
			ClientScene.RegisterPrefab(draftModelObject);

        	NetworkServer.Spawn(draftModelObject);	
			

		}
		//SpawnPlayer(conn);
	}

	// called when connected to a server
	public override void OnClientConnect(NetworkConnection conn)
	{
		base.OnClientConnect(conn);
		MyLogger.log("I CONNECTED AS " + numPlayers + " th player!");

		int team = numPlayers;
		//numPlayers++;
        ClientScene.AddPlayer(conn, (short)team);
       
	}

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
    	
    	GameObject serverObj = GameObject.FindWithTag("ServerData");
		ServerData serverDataScript = serverObj.GetComponent<ServerData>();
		serverDataScript.addConnection(conn, playerControllerId);
		
        GameObject player = getSpawnPlayer();

        NetworkServer.Spawn(player);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        player.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
        PlayerDraftController vc = player.GetComponentInChildren<PlayerDraftController> ();
		int team = numPlayers;
		numPlayers++;
		vc.setTeam(team);


    	if (numPlayers == 2)
    	{
    		draftModelObject.GetComponent<DraftModel>().CmdInit(2, Constants.DRAFT_NUM_ROUNDS, CardSet.SET1);
    	}

		//List<GameObject> userGameObjects = new List<GameObject>();
   		//draftModelObject.GetComponent<DraftModel>().addPlayer(vc);
    }

    int numPlayers = 0;

    public GameObject getSpawnPlayer()
    {
    	MyLogger.log("SPAWNED1");
		GameObject obj = Instantiate<GameObject>(thePlayerPrefab);
		return obj;
    }

}
