using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkManagerCustom : NetworkManager {

	private int port = 7777;
	private string ipAddress = "localhost";
	
	public GameObject thePlayerPrefab;
	public string otherSceneName;
	public bool started = false;

	private GameObject mainBoardModelObject;

	GameObject optsServer;
	GameObject gameOptionsObject;
	Dropdown deckDropDown;
	Dropdown playerTypeDropDown;
	List<string> playerDropDownValues = new List<string>{ "PLAYER", "AI"};


	void Start()
	{

		gameOptionsObject = GameObject.FindWithTag("GameOptions");
		
		
		Dropdown[] dropDowns = gameOptionsObject.GetComponentsInChildren<Dropdown>();
		foreach (Dropdown dropDown in dropDowns)
		{
			if (dropDown.name.Equals("DeckDropDown"))
			{
				dropDown.options.Clear();
				List<DeckInfo> decks = DeckParser.getInstance().getAllDecks();
				foreach (DeckInfo deck in decks)
				{
					dropDown.options.Add(new Dropdown.OptionData(deck.getName()));
				}
				dropDown.value = 1;
				dropDown.value = 0;
				deckDropDown = dropDown;
			}
			else if (dropDown.name.Equals("PlayerType"))
			{
				dropDown.options.Clear();
				foreach (string str in playerDropDownValues)
				{
					dropDown.options.Add(new Dropdown.OptionData(str));
				}
				dropDown.value = 1;
				dropDown.value = 0;
				playerTypeDropDown = dropDown;
			}
		}
	}

	private bool isAIGame()
	{
		return playerTypeDropDown.value == 1;
	}

	//List<MainBoardModel> models = new List<MainBoardModel>();
	void OnGUI()
	{
		if (!started)
		{
			if (GUI.Button(new Rect(100, 100, 250, 100), "Start as Host"))
				StartHost();
			
			if (GUI.Button(new Rect(100, 250, 250, 100), "Start as Client"))
				JoinGame();
				//RefreshHostList();

			if (GUI.Button(new Rect(100, 400, 250, 100), "OTherScene"))
				GoToDeckScene();

			if (GUI.Button(new Rect(100, 550, 250, 100), "CHECK IMAGE GEN"))
				checkImageGen();
		}
	}

	private void init()
	{
		UnityEngine.Networking.NetworkManager.singleton.networkAddress = ipAddress;
		UnityEngine.Networking.NetworkManager.singleton.networkPort = 7777;
	}

	public void StartHost()
	{
		init();
		UnityEngine.Networking.NetworkManager.singleton.StartHost();
		started = true;
	}

	public void JoinGame()
	{
		init();
		UnityEngine.Networking.NetworkManager.singleton.StartClient();
		started = true;
	}

	void GoToDeckScene()
	{
		AutoFade.LoadLevel(otherSceneName ,3,1,Color.black);
	}

	void checkImageGen()
	{
		ImageGenHelper.checkImages();
	}

	public override void OnServerConnect(NetworkConnection conn)
	{
		base.OnServerConnect(conn);
		MyLogger.log("SERVER CONNECTED");
		if (optsServer == null)
		{

			/*optsServer = Instantiate (Resources.Load("GameOptionsServer")) as GameObject;
			ClientScene.RegisterPrefab(optsServer);

	    	NetworkServer.Spawn(optsServer);
			*/
		}
		/*if (mainBoardModelObject == null )
		{
			if (isAIGame())
			{
				createGame();
			}
			else
			{
				//waitingForClient = true;
			}
		}*/
	}

	private void createMultiplayerGame()
	{
		//if (isServer)




		

		mainBoardModelObject = Instantiate (Resources.Load("GameMainBoardModel")) as GameObject;
		ClientScene.RegisterPrefab(mainBoardModelObject);

    	NetworkServer.Spawn(mainBoardModelObject);
    	MainBoardVC[] vcs = new MainBoardVC[2];


    	GameObject serverObj = GameObject.FindWithTag("ServerData");
		ServerData serverDataScript = serverObj.GetComponent<ServerData>();
		List<NetworkConnection> conns = serverDataScript.getConns();
		List<short> pcIds = serverDataScript.getIds();

		for (int i = 0; i < conns.Count; i++)
		{
			NetworkConnection conn = conns[i];
			short playerControllerId = pcIds[i];
			
			GameObject player = serverDataScript.getSpawnPlayer();
	        NetworkServer.Spawn(player);
	        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
	        player.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
	        vcs[i] = player.GetComponentInChildren<MainBoardVC> ();
			
		}
		vcs[0].initVC(0);
		//vcs[0].shouldSetDeck();
		vcs[1].initVC(1);
		//vcs[1].shouldSetDeck();

		optsServer = Instantiate (Resources.Load("GameOptionsServer")) as GameObject;
		ClientScene.RegisterPrefab(optsServer);

    	NetworkServer.Spawn(optsServer);
		//Destroy(gameOptionsObject);	
	}

	private void createAIGame()
	{
		//if (isServer)




		

		mainBoardModelObject = Instantiate (Resources.Load("GameMainBoardModel")) as GameObject;
		ClientScene.RegisterPrefab(mainBoardModelObject);

    	NetworkServer.Spawn(mainBoardModelObject);
    	MainBoardVC[] vcs = new MainBoardVC[1];

    	GameObject serverObj = GameObject.FindWithTag("ServerData");
		ServerData serverDataScript = serverObj.GetComponent<ServerData>();
		List<NetworkConnection> conns = serverDataScript.getConns();
		List<short> pcIds = serverDataScript.getIds();

		NetworkConnection conn = conns[0];
		short playerControllerId = pcIds[0];
		
		GameObject player = serverDataScript.getSpawnPlayer();
        NetworkServer.Spawn(player);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        player.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
        vcs[0] = player.GetComponentInChildren<MainBoardVC> ();
	
		vcs[0].initVC(0);

		optsServer = Instantiate (Resources.Load("GameOptionsServer")) as GameObject;
		ClientScene.RegisterPrefab(optsServer);

    	NetworkServer.Spawn(optsServer);
		GameOptionsServer optServerScript = optsServer.GetComponent<GameOptionsServer>();
		optServerScript.isAIGame = true;
    	optServerScript.setDeckId(0, deckDropDown.value);
    	optServerScript.setDeckId(1, deckDropDown.value);
		//Destroy(gameOptionsObject);	
	}

	// called when connected to a server
	public override void OnClientConnect(NetworkConnection conn)
	{
		base.OnClientConnect(conn);
		MyLogger.log("I CONNECTED!");
		
		short team = 0;
		if (hasP1)
		{
			team = 1;
			//createGame();
			MyLogger.log("Connected");
        }
        ClientScene.AddPlayer(conn, team);
	}


    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {

		
		GameObject serverObj = GameObject.FindWithTag("ServerData");
		ServerData serverDataScript = serverObj.GetComponent<ServerData>();
		serverDataScript.addConnection(conn, playerControllerId);

    	MyLogger.log("JOINED");
    	hasP1 = true;

    	if (isAIGame())
		{
			createAIGame();
		}
		else
		{
			if (serverDataScript.getConns().Count == 2)
			{
				createMultiplayerGame();
			}
		}
    }

    bool hasP1 = false;

}
