using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ServerData : NetworkBehaviour {

	List<NetworkConnection> conns = new List<NetworkConnection>();
	List<short> pcIds = new List<short>();
	public GameObject thePlayerPrefab;

	public void addConnection(NetworkConnection conn, short pid)
	{
		conns.Add(conn);
		pcIds.Add(pid);
	}

	public GameObject getSpawnPlayer()
    {
    	//MyLogger.log("SPAWNED1");
		GameObject obj = Instantiate<GameObject>(thePlayerPrefab);
		return obj;
    }

    public List<NetworkConnection> getConns()
    {
    	return conns;
    }

    public List<short> getIds()
    {
    	return pcIds;
    }

}
