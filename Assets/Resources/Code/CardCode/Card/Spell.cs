using UnityEngine;
using System;

public class Spell : Card
{

	
	public Spell():base()
	{
		playabasePhase = PlayablePhase.defaultSpellPlayablePhase();
	}
	
	public Spell(Spell c):base(c)
	{
	}
	
	//@Override
	override public int getCardType()
	{
		return CardType.SPELL;
	}
	
	//@Override
	/*
	public TargetType getTargetType()
	{
		return passives[0].getTargetType();
	}
	*/
	//@Override
	override public bool isDead()
	{
		return false;
	}
	
	//@Override
	override public string getDesc()
	{
		string s = base.toQuickString();
		s = s + base.getDesc();
		return s;
	}


	override public string getNameTypeStr()
	{
		string str = base.getNameTypeStr();
		if (str == null || str.Length == 0)
			return "CAST SPELL";
		return str;//base.getNameTypeStr();
		//return "Cast" + base.getNameTypeStr ();
	}
}