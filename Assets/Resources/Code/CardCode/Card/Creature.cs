using UnityEngine;
using System.Collections.Generic;
using System;

public class Creature : CombatCard
{

	/*protected int attack = 0;
	protected int health;
	protected int tempHealth;
	protected int tempAttack;
	protected int maxHealth;
	protected bool dead = false;
*/
	public Creature():base()
	{
		
	}
	
	public Creature(Creature c):base(c)
	{
	/*	
		this.attack = c.attack;
		this.health = c.health;
		this.maxHealth = c.maxHealth;
		tempHealth = 0;
		tempAttack = 0;
*/
	}

/*
	public int getAttack()
	{
		return attack;
	}

	public void setAttack(int attack)
	{
		this.attack = attack;
	}

	public int getHealth()
	{
		return health;
	}

	public void setHealth(int health)
	{
		this.health = health;
		this.maxHealth = health;
	}

	public int getTempHealth()
	{
		return tempHealth;
	}

	public void setTempHealth(int tempHealth)
	{
		this.tempHealth = tempHealth;
	}

	public int getTempAttack()
	{
		return tempAttack;
	}

	public void setTempAttack(int tempAttack)
	{
		this.tempAttack = tempAttack;
	}

	public int getMaxHealth()
	{
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth)
	{
		this.maxHealth = maxHealth;
	}

	public void addAttack(int attack, Card source)
	{
		this.attack += attack;
	}

	public void addHealth(int health, Card source)
	{
		this.health += health;
	}

	public void addTempHealth(int tempHealth, Card source)
	{
		this.tempHealth += tempHealth;
	}

	public void addTempAttack(int tempAttack, Card source)
	{
		this.tempAttack += tempAttack;
	}

*/
	//Override
	override public int getCardType()
	{
		return CardType.CREATURE;
	}

	//Override
	override public string getDesc()
	{
		string s = base.getDesc ();//.toQuickString();
		//s = s + " Attack: " + attack + " Health: " + health + "/" +  maxHealth;
		//s = s + base.getDesc();
		return s;
	}

	//Override
	override public string toString()
	{
		string s = base.toString();
		//s = s + " Attack: " + attack + " Health: " + health + "/" +  maxHealth;

		return s;
	}

	//Override
	override public string toQuickString()
	{
		string s = base.toQuickString();
		s = s + " Attack: " + attack + " Health: " + health + "/" +  maxHealth;
		return s;
	}


	//Override
	override public int attackCard(CombatCard defender, int[] overflow)//Source : this
	{
		MyLogger.log(this.name + " Attacks " + defender.getName());
		//if (defender is Structure && this.hasStatusEffect(Status.RAZE))
		//	return defender.takeDamage((attack + tempAttack) *2, this, overflow);// this);
		//else
		return defender.takeDamage(attack + tempAttack, this, overflow);// this);

	}

/*
	public int getTotalHealth()
	{
		return tempHealth + health;
	}

	public int getTotalAttack()
	{
		return tempAttack + attack;
	}

	public void kill()
	{
		this.health = 0;
		this.tempHealth = 0;
	}
	*/



	public string getBoardText()
	{
		string s = getDesc();
		return s;

	}

	override public string getNameTypeStr()
	{
		return base.getNameTypeStr();//"Call" + base.getNameTypeStr ();
	}
	
	//Sacrifice
	override public bool canSalvage()
	{
		if (this.hasStatusEffect(Status.CURSED))
		{
			return false;
		}
		//CANNOT SAC
		return true;
	}

	override public bool canDeclareAttack()
	{
		return (!this.isDead() && !this.isDizzy() && this.getState() == CardState.BOARD);
	}
}
