using UnityEngine;
using System;

public class Land : Card
{
	public Land():base()
	{
	}
	
	public Land(Land c):base(c)
	{

		//this.targetType = new TargetType();
		//this.requiresTarget = getTargetType().requiresTarget();
	}
	
	//@Override
	override public int getCardType()
	{
		return CardType.LAND;
	}

	//@Override
	override public string getDesc()
	{
		string s = base.toQuickString();
		s = s + base.getDesc();
		return s;
	}
	
	override public string getNameTypeStr()
	{
				string str = base.getNameTypeStr();
		if (str == null || str.Length == 0)
			return "LAND";
		return str;//base.getNameTypeStr();
		//return "Cast" + base.getNameTypeStr ();
	}
}
