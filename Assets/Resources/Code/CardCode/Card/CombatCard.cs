using UnityEngine;
using System.Collections.Generic;
using System;

public abstract class CombatCard : Card {

	protected int[] salvageValue;
	protected int attack = 0;
	protected int health;
	protected int tempHealth;
	protected int tempAttack;
	protected int maxHealth;
	protected bool dead = false;
	protected List<string> uniqueNameType = new List<string>();

	protected CombatCard() : base()
	{
		salvageValue = new int[Constants.NUM_REZ];
	}
	
	protected CombatCard(CombatCard c) : base(c)
	{
		this.salvageValue = ((CombatCard)c).salvageValue;
		this.attack = c.attack;
		this.health = c.health;
		this.maxHealth = c.maxHealth;
		tempHealth = 0;
		tempAttack = 0;
		this.uniqueNameType = new List<string>();
		for (int i = 0; i < c.uniqueNameType.Count; i++)
		{
			uniqueNameType.Add(c.uniqueNameType[i]);
		}
	}

	override public List<string> getNameType()
	{
		//Join two lists.
		List<string> all = new List<string>();
		all.AddRange (nameType);
		all.AddRange (uniqueNameType);

		return all;
	}

	public List<string> getUniqueNameType()
	{
		return uniqueNameType;
	}

	public void setUniqueNameType(List<string> uniqueNameType)
	{
		this.uniqueNameType = uniqueNameType;
	}

	public bool isUnique()
	{
		return uniqueNameType.Count > 0;
	}

	public int getAttack()
	{
		return attack;
	}

	public void setAttack(int attack)
	{
		this.attack = attack;
	}

	public int getHealth()
	{
		return health;
	}

	virtual public void setHealth(int health)
	{
		this.health = health;
		this.maxHealth = health;
	}

	public int getTempHealth()
	{
		return tempHealth;
	}

	public void setTempHealth(int tempHealth)
	{
		this.tempHealth = tempHealth;
	}

	public int getTempAttack()
	{
		return tempAttack;
	}

	public void setTempAttack(int tempAttack)
	{
		this.tempAttack = tempAttack;
	}

	public int getMaxHealth()
	{
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth)
	{
		this.maxHealth = maxHealth;
	}

	public void addAttack(int attack, Card source)
	{
		this.attack += attack;
	}

	virtual public void addHealth(int health, Card source)
	{
		this.health += health;
		maxHealth += health;
	}

	virtual public void regenHealth(int health, Card source)
	{
		this.health += health;
		if (this.health > maxHealth)
		{
			this.health = maxHealth;
		}
	}

	public void addTempHealth(int tempHealth, Card source)
	{
		this.tempHealth += tempHealth;
	}

	public void addTempAttack(int tempAttack, Card source)
	{
		this.tempAttack += tempAttack;
	}

	public int getTotalHealth()
	{
		return tempHealth + health;
	}

	public int getTotalAttack()
	{
		return tempAttack + attack;
	}

		////Override
	override public bool isDead()
	{
		return dead;
	}

	public void setDead(bool dead)
	{
		this.dead = dead;
	}

	public void kill()
	{
		this.health = 0;
		this.tempHealth = 0;
	}

	virtual public string getSacStr()
	{
		string s = "";
		if (salvageValue[0] > 0)
			s = s + salvageValue[0] + "C ";
		if (salvageValue[1] > 0)
			s = s + salvageValue[1] + "R ";
		if (salvageValue[2] > 0)
			s = s + salvageValue[2] + "G ";
		if (salvageValue[3] > 0)
			s = s + salvageValue[3] + "B ";
		if (s.Equals (""))
			return "Zero";
		return s;
	}

	//Override
	public abstract int attackCard(CombatCard defender, int[] overflow);
	

	//Override
	// res will be residual damage overflow from temporary health., temp health is reduced first
	virtual public int takeDamage(int dmg, Card source, int[] overflow)
	{
		dmg = factorDamageMultiplier(source, dmg);

//TODO MAYBE REMOVE COUNTER ONLY, NOT WHOLE EFF.
		if(this.hasStatusEffect(Status.PROTECTION) && dmg > 0)
		{
			dmg = 0;
			List<StatusEffect>pro = this.getStatusEffects(Status.PROTECTION);
			if(pro[0].removeCounter())
			{
				this.removeStatusEffect(pro[0]);
			}
		}
		
		if (overflow != null)
			overflow[0] = 0;
		if (dmg <= 0)
		{
			return 0;
		}

		tempHealth = tempHealth - dmg;
		if (tempHealth < 0)
		{
			health = health + tempHealth;
			tempHealth = 0;
		}

		MyLogger.log(this.name + " takes " + dmg + " damage from " + source.getName());
		MyLogger.log(this.name + ": " + (health+tempHealth) + "/" + maxHealth);
		if (health < 0)
		{
			if (overflow != null)
			{
				overflow[0] = -health;
				MyLogger.log("Overflow " + overflow[0]);
			}
		}
		return dmg;
	}

	virtual protected int factorDamageMultiplier(Card source, int dmg)
	{
		//bool factored = false;
		if (this.hasStatusEffect(Status.DAMAGE_MULTIPLY_RED) && Faction.hasRed(source.getFactions()))
		{
			List<StatusEffect>effs = this.getStatusEffects(Status.DAMAGE_MULTIPLY_RED);
			StatusEffect eff = effs[0];
			dmg *= eff.getCounter();
			dmg /= 100;
		}

		if (this.hasStatusEffect(Status.DAMAGE_MULTIPLY_BLUE) && Faction.hasBlue(source.getFactions()))
		{
			List<StatusEffect>effs = this.getStatusEffects(Status.DAMAGE_MULTIPLY_BLUE);
			StatusEffect eff = effs[0];
			dmg *= eff.getCounter();
			dmg /= 100;
		}

		if (this.hasStatusEffect(Status.DAMAGE_MULTIPLY_GREEN) && Faction.hasGreen(source.getFactions()))
		{
			List<StatusEffect>effs = this.getStatusEffects(Status.DAMAGE_MULTIPLY_GREEN);
			StatusEffect eff = effs[0];
			dmg *= eff.getCounter();
			dmg /= 100;
		}

		if (this.hasStatusEffect(Status.DAMAGE_MULTIPLY_COLOURLESS) && Faction.hasColourless(source.getFactions()))
		{
			List<StatusEffect>effs = this.getStatusEffects(Status.DAMAGE_MULTIPLY_COLOURLESS);
			StatusEffect eff = effs[0];
			dmg *= eff.getCounter();
			dmg /= 100;
		}
		return dmg;
	}


	public abstract bool canDeclareAttack();

	public abstract bool canSalvage();
	public int[] getSalvageValue()
	{
		return salvageValue;
	}
	public void setSalvageValue(int[] sacValue)
	{
		//MyLogger.log(sacValue.ToString() + " gfsg "  + " " + this.getID() + " " + sacValue[0]);
		this.salvageValue = sacValue;
	}
	
}