using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public abstract class Card
{
	//inits
	private const string DEFAULT_ARTIST = "DEFAULT ARTIST";
	private const int DEFAULT_RARITY = -111;
	protected int rarity;
	protected string artistName;
	protected CardSet cardSet;

	protected string name;
	protected int ID;
	protected bool token;
	protected int factions;

	protected int[] cost;

	protected List<TargetListObject> extraCostTargetTypes;
	protected List<PreAbilityObject> extraCostEffects;

	protected string desc = "";
	protected string flavour = "";

	protected int team = TargetTeam.TEAM_NONE;
	
	protected List<string> nameType = new List<string>();
	
	protected int state = CardState.NONE;
	protected Dizzy dizzy = new Dizzy();
	protected bool sick = false; // TODO

	protected PlayablePhase playabasePhase;

	//ABILITY
	protected List<PassiveAbility> passives = new List<PassiveAbility>();
	protected List<ActiveAbility> actives = new List<ActiveAbility>();
	protected List<AltAbility> alts = new List<AltAbility>();
	protected List<GraveAbility> graveAbilities = new List<GraveAbility>();

	protected List<Counter> counters = new List<Counter>();



	// STATUSES
	protected StatusEffect surpress = null;
	protected List<StatusEffect> statuses = new List<StatusEffect>();



	protected Card()
	{
		playabasePhase = PlayablePhase.defaultNonSpellPlayablePhase();
		cost = new int[Constants.NUM_REZ];
		token = false;
		this.rarity = DEFAULT_RARITY;
		this.artistName = DEFAULT_ARTIST;
		this.cardSet = CardSet.DEFAULT_SET;

		extraCostEffects = new List<PreAbilityObject>();
		extraCostTargetTypes = new List<TargetListObject>();
	}

	protected Card(Card c)
	{
		playabasePhase = c.playabasePhase;
		this.name = c.name;
		this.ID = c.ID;
		this.token = c.token;
		this.cost = c.cost;
		this.desc = c.desc;
		this.flavour = c.flavour;
		this.team = c.team;
		//this.listener = c.listener;
		this.factions = c.factions;
		
		this.rarity = c.rarity;
		this.artistName = c.artistName;
		this.cardSet = c.cardSet;
		extraCostEffects = new List<PreAbilityObject>();
		foreach (PreAbilityObject eff in c.extraCostEffects)
		{
			extraCostEffects.Add(new PreAbilityObject(eff));
		}

		this.extraCostTargetTypes = new List<TargetListObject>();
		foreach (TargetListObject tt in c.extraCostTargetTypes)
		{
			extraCostTargetTypes.Add(new TargetListObject(tt));
		}
		this.passives = new List<PassiveAbility>();
		for (int i = 0; i < c.passives.Count; i++)
		{
			passives.Add(((PassiveAbility)c.passives[i].deepCopy()));
		}
		
		this.actives = new List<ActiveAbility>();
		for (int i = 0; i < c.actives.Count; i++)
		{
			actives.Add(((ActiveAbility)c.actives[i].deepCopy()));
		}

		this.graveAbilities = new List<GraveAbility>();
		for (int i = 0; i < c.graveAbilities.Count; i++)
		{
			graveAbilities.Add(((GraveAbility)c.graveAbilities[i].deepCopy()));
		}

		this.alts = new List<AltAbility>();
		for (int i = 0; i < c.alts.Count; i++)
		{
			alts.Add(((AltAbility)c.alts[i].deepCopy()));
		}
		
		this.nameType = new List<string>();
		for (int i = 0; i < c.nameType.Count; i++)
		{
			nameType.Add(c.nameType[i]);
		}

		counters = new List<Counter>();
		for (int i = 0; i < c.counters.Count; i++)
		{
			counters.Add(c.counters[i]);
		}
		
		// this.targets = new List<Card>();
		// this.extraTargets = new List<Card>();
		this.dizzy = new Dizzy(c.dizzy);
		// this.extraCardTypes = (List<CardType>) c.extraCardTypes.clone();
		
		this.state = c.state;
		
		this.statuses = new List<StatusEffect>();
		for (int i = 0; i < c.statuses.Count; i++)
		{
			statuses.Add(new StatusEffect(c.statuses[i]));
		}
	}

	public void setPlayablePhase(PlayablePhase pp)
	{
		this.playabasePhase = pp;
	}

	public PlayablePhase getPlayablePhase()
	{
		return playabasePhase;
	}

	public List<PreAbilityObject> getExtraCostEffects()
	{
		return extraCostEffects;
	}

	public void setExtraCostEffects(List<PreAbilityObject> effs)
	{
		extraCostEffects = effs;
	}
	public List<TargetListObject> getExtraCostTargetTypes()
	{
		return extraCostTargetTypes;
	}

	public void setExtraCostTargetTypes(List<TargetListObject> tts)
	{
		extraCostTargetTypes = tts;
	}

	public int getRarity()
	{
		return rarity;
	}

	public string getArtistName()
	{
		return artistName;
	}

	public CardSet getCardSet()
	{
		return cardSet;
	}

	public void setRarity(int rarity)
	{
		this.rarity = rarity;
	}

	public void setArtistName(string artistName)
	{
		this.artistName = artistName;
	}

	public void setCardSet(CardSet cardSet)
	{
		this.cardSet = cardSet;
	}

	public void setFactions(int factions)
	{
		this.factions = factions;
	}
	virtual public int getFactions()
	{
		return factions;
	}

	public int getTeam()
	{
		return team;
	}

	public void setTeam(int t)
	{
		team = t;
	}
	public void setToken(bool b)
	{
		this.token = b;
	}

	public bool isToken()
	{
		return token;
	}

	public void setName(string name)
	{
		this.name = name;
	}

	public void setID(long iD)
	{
		ID = (int) iD;
	}

	public void setID(int iD)
	{
		ID = iD;
	}

	public string getFlavour()
	{
		return flavour;
	}

	public void setFlavour(string flavour)
	{
		this.flavour = flavour;
	}

	
	public int getState()
	{
		return state;
	}
	
	public void setState(int state)
	{
		this.state = state;
	}

	public void setCost(int[] cost)
	{
		this.cost = cost;
	}


	public int[] getCost()
	{
		return cost;
	}

	public string getName()
	{
		return name;
	}

	public void undizzy()
	{
		dizzy.modifyDizzy (Dizzy.DizzyFactor.F_UNDIZZY);
	}
	
	public void moreDizzy()
	{
		dizzy.modifyDizzy (Dizzy.DizzyFactor.F_DIZZY);
	}
	
	public void setDizzy(int diz)
	{

		dizzy.setDizzy(diz);
	}
	
	public Dizzy getDizzy()
	{
		return dizzy;
	}

	public bool isDizzy()
	{
		if (dizzy.getDizzy() == Dizzy.UNDIZZY)
			return false;
		return true;
	}

	/*
	public int getFactions()
	{
		return factions;
	}
	*/

	public int getID()
	{
		return ID;
	}

	virtual public string getDesc()
	{
		string str = desc;
		foreach (StatusEffect e in statuses)
		{
			str = str + "S - " + e.getDesc();
		}
		
		foreach (PassiveAbility abil in passives)
		{
			str = str + "P - " + abil.getDesc();
		}
		
		foreach (ActiveAbility abil in actives)
		{
			str = str + "A - " + abil.getDesc();
		}

		foreach (AltAbility abil in alts)
		{
			str = str + "ALT - " + abil.getDesc();
		}

		foreach (GraveAbility abil in graveAbilities)
		{
			str = str + "GA - " + abil.getDesc();
		}

		return str;
	}

	void initData(int ID, string name, int[] cost)
	{
		this.ID = ID;
		this.cost = cost;
		this.name = name;
	}

	//@Override
	virtual public string toString()
	{
		string s = getName ();
		//string s = "Temp error Card";
		//string s = string.format("Card ID:%d Type: %s Name: %s ", ID,
		//		CardType.convertTostring(getCardType()), name);



		// string s = string.format("Card ID:%d Type: %s Name: %s ", ID,
		// CardType.convertTostring(getCardType()), name);
		/*
		 * for (int i = 0; i < factions.size(); i++) { s = s +
		 * string.format(" %s", factions[i).tostring()); }
		 */
		// s = s + string.format(" Description: %s", desc);
		return s;
	}

	virtual public string toQuickString()
	{
		string s = getName ();
		//string s = "Temp error Card";
		//string s = string.format("Name: %s ", name);
		// string s = string.format("%s %s ",
		// CardType.convertTostring(getCardType()), name);
		/*
		 * for (int i = 0; i < factions.size(); i++) { s = s +
		 * string.format(" %s ", factions[i).tostring()); }
		 */

		return s;
	}
	virtual public string getCostStr()
	{
		string s = "";
		if (cost[0] > 0)
			s = s + cost[0] + "C ";
		if (cost[1] > 0)
			s = s + cost[1] + "R ";
		if (cost[2] > 0)
			s = s + cost[2] + "G ";
		if (cost[3] > 0)
			s = s + cost[3] + "B ";
		if (s.Equals (""))
			return "Zero";
		return s;
	}

	virtual public string getNameTypeStr()
	{
		string s = "";
		foreach (string type in nameType)
		{
			s = s + " " + type;
		}
		return s;
	}


	virtual public bool isDead()
	{
		return true;
	}


	virtual public void setDesc(string str)
	{
		this.desc = str;
	}
	
	/**
	 * Abstracts ------
	 * 
	 */
	public abstract int getCardType();



	/*
	 * 
	 * STATUSES
	*/

	public void addStatusEffect(StatusEffect newEffect)
	{
		if (newEffect.getDuration() <= 0)
		{
			MyLogger.log("Cannot add effect with no duration");
			return;
		}

		if (newEffect.getStatus() == Status.SURPRESS)
		{
			surpress = newEffect;
		}
		else
		{
			if (Status.isJoinableStatus(newEffect.getStatus()))
			{
				foreach (StatusEffect effect in statuses)
				{
					if (effect.isSameEffect(newEffect.getStatus()))
					{
						effect.join(newEffect);
						return;
					}
				}
				
			}
			statuses.Add(newEffect);

		}
		
	}
	
	
	//TODO clean this up somehow
	public List<int> decayStatusEffects()
	{
		List<StatusEffect> remove = new List<StatusEffect>();
		List<int> removedStatuses = new List<int>();
		foreach (StatusEffect effect in statuses)
		{
			bool decayed = effect.decay();
			//TODO FIX THIS
			if (decayed && effect.getStatus() != Status.FLEETING)
			{
				removedStatuses.Add(effect.getStatus());
				remove.Add(effect);
			}
		}
		
		if (surpress != null)
		{
			bool decayed = surpress.decay();
			if (decayed)
				surpress = null;
		}
		statuses = statuses.Except(remove).ToList();
		return removedStatuses;
	}
	
	public void removeStatus(int eff)
	{
		List<StatusEffect> remove = new List<StatusEffect>();
		if (eff == Status.SURPRESS)
		{
			surpress = null;
		}

		
		foreach (StatusEffect effect in statuses)
		{
			if (effect.isSameEffect(eff))
			{
				remove.Add(effect);
				//TODO HMMM THIS WAY WE ONLY REMOVE ONE, I THINK IT IS BETTER THIS WAY ZZZ.
				break;
			}
			
		}
		
		statuses = statuses.Except(remove).ToList();
		


	}
	
	public void removeStatusEffect(StatusEffect eff)
	{
		removeStatus(eff.getStatus());
	}
	
	
	/**
	 * DOES NOT WORK IF SURPRESSED!
	 */
	public bool hasStatusEffect(int status)
	{
		// If searching for surpress, it exists
		bool surpress = isSurpressed();
		if (status == Status.SURPRESS)
		{
			return surpress;
		}
		else
		{
			if (!surpress)
			{
				// IF NOT, then look for it.
				foreach (StatusEffect effect in statuses)
				{
					if (effect.isSameEffect(status))
					{
						return true;
					}
					
				}
			}
			
		}
		return false;
	}
	
	public bool isSurpressed()
	{
		return surpress != null;
	}
	
	public List<StatusEffect> getStatusEffects(int status)
	{
		
		List<StatusEffect> foundStatuses = new List<StatusEffect>();
		
		if (isSurpressed())
		{
			foundStatuses.Add(surpress);
			return foundStatuses;
		}
		
		foreach (StatusEffect effect in statuses)
		{
			if (effect.isSameEffect(status))
			{
				foundStatuses.Add(effect);
			}
		}
		return foundStatuses;
	}

	/**
	 * RANDOM JUNK CLEAN UP TODO
	 */
	public bool containsAbility(PassiveAbility p)
	{
		if (passives.Contains(p))
			return true;
		return false;
	}
	
	public void setNameType(List<string> typeAr)
	{
		nameType = typeAr;
	}

	public void addNameType(string nt)
	{
		if (!nameType.Contains(nt))
			nameType.Add(nt);
	}


	public void removeNameType(string nt)
	{
		nameType.Remove(nt);
	}

	virtual public List<string> getNameType()
	{
		return nameType;
	}

	public bool hasNameType(string nt)
	{
		List<string> theTypes = getNameType();
		return theTypes.Contains(nt);
	}

	public List<StatusEffect> getStatuses()
	{
		return statuses;
	}
	
	public void setStatuses(List<StatusEffect> statuses)
	{
		this.statuses = statuses;
	}
	
	public void purge()
	{
		statuses = new List<StatusEffect>();
		passives = new List<PassiveAbility>();
		actives = new List<ActiveAbility>();
		alts = new List<AltAbility>();
		graveAbilities = new List<GraveAbility>();
	}

	/**
	 * EXTRACOST
	 * 
	 */
	
	public bool needsExtraCostTargets()
	{
		return extraCostTargetTypes.Count > 0;
	}

	/*
	 * Extra abilites
	 * 
	 */

	public void addAbilityDuringCreation(Ability abil)
	{
		if (abil is AltAbility)
		{
			addAlt((AltAbility)abil);
		}
		else if (abil is ActiveAbility)
		{
			addActive((ActiveAbility)abil);
		}
		else if (abil is PassiveAbility)
		{
			addPassive((PassiveAbility)abil);
		}
	}

	public void addPassive(PassiveAbility abil)
	{
		passives.Add(abil);
	}
	
	public void addActive(ActiveAbility abil)
	{
		actives.Add(abil);
		
	}

	public void addActive(GraveAbility abil)
	{
		graveAbilities.Add(abil);
		
	}
	public void addAlt(AltAbility abil)
	{
		alts.Add(abil);
		
	}

	public void addGraveAbility(GraveAbility abil)
	{
		graveAbilities.Add(abil);
		
	}

	public List<PassiveAbility> getPassives()
	{
		return passives;
	}
	
	public void setPassives(List<PassiveAbility> passives)
	{
		this.passives = passives;
	}
	
	public List<ActiveAbility> getActives()
	{
		return actives;
	}
	
	public void setActives(List<ActiveAbility> actives)
	{
		this.actives = actives;
	}

	public void setGraveAbilities(List<GraveAbility> graveAbilities)
	{
		this.graveAbilities = graveAbilities;
	}

	public List<GraveAbility> getGraveAbilities()
	{
		return graveAbilities;
	}
	
		public List<AltAbility> getAlts()
	{
		return alts;
	}
	
	public void setAlts(List<AltAbility> alts)
	{
		this.alts = alts;
	}

	public List<Ability> getAllAbilities()
	{
		List<Ability> abils = new List<Ability>();
		abils.AddRange(passives.ConvertAll(x => (Ability)x));
		abils.AddRange(actives.ConvertAll(x => (Ability)x));
		abils.AddRange(alts.ConvertAll(x => (Ability)x));
		abils.AddRange(graveAbilities.ConvertAll(x => (Ability)x));
		return abils;
	}
	/**
	 * 
	 * COUNTERS
	 * TODO string version of all of them -_-... 
	 CounterManager().getInstance().getCounterType(counterName)
	 */

	public bool canPayCounter(List<CounterCost> counterCosts, Card source)
	{
		if (counterCosts == null || counterCosts.Count == 0)
		{
			return true;
		}
		
		foreach (CounterCost cCost in counterCosts)
		{
			Counter marker = getCounterOfType(cCost.name);
		
			if (marker == null || !marker.canPay(cCost.cost))
			{
				return false;
			}
			
		}
		return true;
	}


	private void printCounters()
	{
		MyLogger.log("PRINTING COUNTERS FOR " + getName());
		foreach (Counter counter in counters)
		{
			MyLogger.log("COUNTER IS " + counter.getName() + " amt " + counter.getCount());
		}
	}
	public void payCounter(List<CounterCost> counterCosts, Card source)
	{
		//printCounters();
		List<Counter> deleteCounters = new List<Counter>();
		foreach (CounterCost cCost in counterCosts)
		{
			Counter marker = getCounterOfType(cCost.name);
			MyLogger.log(cCost.name + " " + cCost.cost);

			if (marker == null)
			{
				MyLogger.log("MARKER IS NULL?");
			}
			else if (marker.pay(cCost.cost))
			{
				MyLogger.log("PAY COST! ");
				if (marker.getCount() <= 0)
				{
					deleteCounters.Add(marker);
				}
			}
			else
			{
				MyLogger.log("REAL ERROR HAPPENIGN HERE");
			}

		}
		counters = counters.Except (deleteCounters).ToList();
	}

		
	private Counter getCounterOfType(string counterName)
	{
		foreach (Counter c in counters)
		{
			if (c.getName().Equals(counterName))
			{
				return c;
			}
		}
		return null;
	}

	public void refillCounter(string counterName, int amount, Card source, int maxAmount) //NOTE MAY BE USED FOR DECAY AS WELL!!!!
	{

		Counter marker = getCounterOfType(counterName);
		
		if (marker == null)
		{
			Counter counter = new Counter(counterName, maxAmount);
			counter.refill(amount);
			counters.Add(counter);
		}
		else
		{
			marker.refill(amount);
		}
	}

	public void refillAllCounter(string counterName, Card source, int maxAmount)
	{
		Counter marker = getCounterOfType(counterName);
		
		if (marker == null)
		{
			//????
			MyLogger.log("ERROR REFILL ALL OF COUNTER NON_EXISTANT");
		}
		else
		{
			marker.refillAll();
		}
	}

	public List<Counter> getCounters()
	{
		return counters;
	}

	public void setCounters(List<Counter> cnts)
	{
		counters = cnts;
	}

	public void directSetCounter(string counterName, int amount, Card source, int maxAmount)
	{
		Counter marker = getCounterOfType(counterName);
		
		if (marker == null)
		{
			Counter counter = new Counter(counterName, maxAmount);
			counter.setCount(amount);
			counters.Add(counter);
		}
		else
		{
			marker.setCount(amount);
		}
	}

	//TODO POST COUNTER EVENTS
	/*
	model.eventOccurred() //OR!!! USE CARD LISTENER
listener.postEvent()

(GameEventType.COUNTER_SET, source);
(GameEventType.COUNTER_PAY), source;
(GameEventType.COUNTER_OVERFLOW, source);
(GameEventType.COUNTER_REFILL, source);
*/
}