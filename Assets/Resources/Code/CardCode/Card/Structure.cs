using UnityEngine;
using System.Collections.Generic;
using System;

public class Structure : CombatCard
{

	
	public Structure():base()
	{
		
	}
	
	public Structure(Structure c):base(c)
	{
		
		//this.attack = c.attack;

		//dizzy = Dizzy.DIZZY;
	}

/*
	public int getAttack()
	{
		return 0;
	}

	public void setAttack(int attack)
	{
		this.attack = attack;
	}

	public int getHealth()
	{
		return health;
	}

	public void setHealth(int health)
	{
		this.health = health;
		this.maxHealth = health;
	}

	public int getTempHealth()
	{
		return tempHealth;
	}

	public void setTempHealth(int tempHealth)
	{
		this.tempHealth = tempHealth;
	}

	public int getTempAttack()
	{
		return tempAttack;
	}

	public void setTempAttack(int tempAttack)
	{
		this.tempAttack = tempAttack;
	}

	public int getMaxHealth()
	{
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth)
	{
		this.maxHealth = maxHealth;
	}

	public void addAttack(int attack, Card source)
	{
		this.attack += attack;
	}

	public void addHealth(int health, Card source)
	{
		this.health += health;
	}

	public void addTempHealth(int tempHealth, Card source)
	{
		this.tempHealth += tempHealth;
	}

	public void addTempAttack(int tempAttack, Card source)
	{
		this.tempAttack += tempAttack;
	}
*/

	//@Override
	override public string getDesc()
	{
		string s = base.toQuickString();
		s = s + " Attack: " + attack + " Health: " + health + "/" +  maxHealth;
		s = s + base.getDesc();
		return s;
	}

	//@Override
	override public string toString()
	{
		string s = base.toString();
		s = s + " Health: " + health + "/" +  maxHealth;
		return s;
	}

	//@Override
	override public string toQuickString()
	{
		string s = base.toQuickString();
		s = s + " Health: " + health + "/" +  maxHealth;
		return s;
	}

	//@Override
	override public int getCardType()
	{
		return CardType.STRUCTURE;
	}


	//@Override
	override public int attackCard(CombatCard defender, int[] overflow)//Source : this
	{
		if (overflow != null)
			overflow[0] = 0;
		MyLogger.log("why structure attack");
		return Constants.ERROR_DMG;
	}

	override protected int factorDamageMultiplier(Card source, int dmg)
	{
		if (source.hasStatusEffect(Status.RAZE))
		{
			dmg = dmg * 2;
		}
		return base.factorDamageMultiplier(source, dmg);
	}
/*
	public int getTotalHealth()
	{
		return tempHealth + health;
	}

	public void kill()
	{
		this.health = 0;
		this.tempHealth = 0;
	}

	//@Override
	override public bool isDead()
	{
		return dead;
	}

	public void setDead(bool dead)
	{
		this.dead = dead;
	}
*/
	override public string getNameTypeStr()
	{
		return base.getNameTypeStr();
		//return "Call" + base.getNameTypeStr ();
	}

	//Sacrifice
	override public bool canSalvage()
	{
		if (this.hasStatusEffect(Status.CURSED))
		{
			return false;
		}
		//CANNOT SAC
		return true;
	}


	override public bool canDeclareAttack()
	{
		return false;
	}
}