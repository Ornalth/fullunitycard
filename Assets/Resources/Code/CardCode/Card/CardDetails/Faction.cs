using System.Collections.Generic;
public class Faction
{
	
	public static int FACTION_NONE = 0;
	public static int FACTION_RED = 1;
	public static int FACTION_GREEN = 1<<1;
	public static int FACTION_BLUE = 1<<2;
	public static int FACTION_COLOURLESS = 1<<3;

	public static int FACTION_ANY = FACTION_RED | FACTION_GREEN | FACTION_BLUE | FACTION_COLOURLESS;
    
    private static Dictionary <int, string> dict = new Dictionary<int, string> (){
        { FACTION_RED, "F_R"},
        { FACTION_GREEN, "F_G"},
        { FACTION_BLUE, "F_B"},
        { FACTION_COLOURLESS, "F_C"}

    };

    private static Dictionary <string, int> reverseDict = new Dictionary<string, int> (){
        { "F_R", FACTION_RED},
        { "F_G", FACTION_GREEN},
        { "F_B", FACTION_BLUE},
        { "F_C", FACTION_COLOURLESS}
        
    };

    public static int stringToFaction(string s)
    {
    	if (s.Equals("F_R"))
    	{
    		return FACTION_RED;
    	}
    	else if (s.Equals("F_G"))
    	{
    		return FACTION_GREEN;
    	}
    	else if (s.Equals("F_B"))
    	{
    		return FACTION_BLUE;
    	}
		else if (s.Equals("F_C"))
		{
			return FACTION_COLOURLESS;
		}
		return FACTION_NONE;
    }

    public string toString(){
       return "temp broken faction";
    }

    public static List<string> factionToStringArray(int factions)
    {
        List<string> array = new List<string>();

        foreach (int key in dict.Keys)
        {
            if ((factions & key) != 0 )
            {
                array.Add(dict[key]);  
            }
        }
        if (array.Count == 0)
        {
            array.Add(dict[FACTION_COLOURLESS]);
        }
        return array;
    }

    public static bool hasRed(int factions)
    {
        return HelperUtils.testBits(factions, FACTION_RED);
    }

    public static bool hasBlue(int factions)
    {
        return HelperUtils.testBits(factions, FACTION_BLUE);
    }

    public static bool hasGreen(int factions)
    {
        return HelperUtils.testBits(factions, FACTION_GREEN);
    }

    public static bool hasColourless(int factions)
    {
        return HelperUtils.testBits(factions, FACTION_COLOURLESS);
    }

}
