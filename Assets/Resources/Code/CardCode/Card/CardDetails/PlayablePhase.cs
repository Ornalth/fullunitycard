using UnityEngine;
using System.Collections;

public class PlayablePhase {
	public int targetTeam = TargetTeam.TEAM_NONE;
	public bool canPlayOnStack = false;
	public int phases = Phase.ERROR;

	private PlayablePhase()
	{

	}

	public static PlayablePhase defaultSpellPlayablePhase()
	{
		PlayablePhase pp = new PlayablePhase();
		pp.targetTeam = TargetTeam.TEAM_BOTH;
		pp.canPlayOnStack = true;
		pp.phases = Phase.DEFAULT_SPELL_PHASES;
		return pp;
	}

	public static PlayablePhase defaultNonSpellPlayablePhase()
	{
		PlayablePhase pp = new PlayablePhase();
		pp.targetTeam = TargetTeam.TEAM_ALLY;
		pp.canPlayOnStack = false;
		pp.phases = Phase.DEFAULT_CREATURE_PHASES;
		return pp;
	}

	public bool canPlayInThisPhase(int phase, int targTeam, int stackSize)
	{
		if (!((phase & phases) == phase))
		{
			MyLogger.log("PHASE ERROR" + phase + " " + " " + phases);
		}
		if (!((targTeam & targetTeam) == targTeam))
		{
			MyLogger.log("TEAM ERROR " + targTeam + " " + targetTeam);
		}

		if (!((stackSize > 0) ? canPlayOnStack : true))
		{
			MyLogger.log("STACK ERROR");
		}
		return ((phase & phases) == phase) 
		&& ((targTeam & targetTeam) == targTeam)
		&& ((stackSize > 0) ? canPlayOnStack : true);
	}
}
