﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CardSet
{
	SET1,
	DEFAULT_SET
};

public class CardSetConverter
{
	private static Dictionary<string, CardSet> dict = new Dictionary<string, CardSet> {
		{ "SET1", CardSet.SET1 },
		{ "DEFAULT_SET", CardSet.DEFAULT_SET }

	};

	private static Dictionary<CardSet, string> reverseDict = new Dictionary<CardSet, string> {
		{ CardSet.SET1, "SET1" },
		{ CardSet.DEFAULT_SET, "DEFAULT_SET" }
	};


	public static CardSet cardSetFromString(string s)
	{
		return dict [s];
	}

	public static string cardSetToString(CardSet ty)
	{
		return reverseDict[ty];
	}

}
