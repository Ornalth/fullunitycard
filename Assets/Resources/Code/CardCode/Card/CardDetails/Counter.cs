
//Different types of counters.
//NEW RESPONDS TO EVENT -- COUNTER REACHED
//COUNTER IN ABILITY DATA HELPER PRASE -EX COUTNER NUMBER OF COUNTERS ON ALL CARDS OF TYPE!!!!
//MAYBE TARGETTYPE - TARGET MUST HAVE COUNTER~!!
public class Counter
{
	public static readonly int INFINITE = 90909;
	public static readonly string DEFAULT_COUNTER_NAME = "GENERIC_COUNTER_NAME";
	protected int maxCount;
	protected int currentCount;
	protected string name;
	protected string image;
	//protected int decayRate;
	
	public Counter()
	{
		image = "";
		name =  DEFAULT_COUNTER_NAME;
		maxCount = INFINITE;
		currentCount = 0;
	}

	public Counter(string counterName)
	{
		image = "";
		name = counterName;
		maxCount = INFINITE;
		currentCount = 0;
	}

	public Counter(string counterName, int max)
	{
		image = "";
		name = counterName;
		maxCount = max;
		currentCount = 0;
	}

	public Counter(Counter c)
	{
		maxCount = c.maxCount;
		name = c.name;
		currentCount = c.currentCount;
	}

	virtual public void setMaxCount(int c)
	{
		maxCount = c;
		if (maxCount <= 0)
		{
			maxCount = 99999;
		}
	}

	virtual public void setCount(int c)
	{
		currentCount = c;
	}

	virtual public int getCount()
	{
		return currentCount;
	}
	virtual public int getMaxCount()
	{
		return maxCount;
	}
	virtual public void refill(int amount)
	{
		currentCount += amount;
		if (hasOverflowed())
		{
			//POST EVENT
			currentCount = maxCount;
		}
		else if (currentCount < 0)
		{
			//POST EVENT
			currentCount = 0;
		}
		//POST EVENT
	}

	virtual public void refillAll()
	{
		currentCount = maxCount;
		//POST EVENT
	}

	protected bool hasOverflowed()
	{
		return currentCount > maxCount;

		//if (currentCount > maxCount)

	}

	//Return success T/F
	virtual public bool pay(int count)
	{
		if (canPay(count))
		{
			if (count == CounterCost.ALL_COST)
			{
				currentCount = 0;
			}
			else
			{
				currentCount -= count;
			}
			//post event.
			return true;
		}
		return false;
	}

	virtual public bool canPay(int count)
	{
		if (count > currentCount)
		{
			return false;
		}
		return true;
	}

	//TODO THIS MAY BREAK STUFF.
	//public static implicit operator Counter(string s)
	//{
	//   return (Counter)Activator.CreateInstance(CounterManager.getInstance().getCounterType(s));
	//}

	virtual public string getName()
	{
		return name;
	} //NEVER COLLIDE PLEEZUH.

	public void setName(string tempName)
	{
		this.name = tempName;
	}

	public void setImage(string image)
	{
		this.image = image;
	}

	public string getImage()
	{
		return "TEMP AERROR";
	}
}


/*
//TODO REDO THIS.
public class CounterManager
{
	private Dictionary<string, Type> counterDictionary;
	private static CounterManager instance;

	private CounterManager()
	{
		counterDictionary = new Dictionary<string, Type>();
	}
	public static CounterManager getInstance ()
	{
		if (instance == null)
		{
			instance = new CounterManager();
		}
		return instance;
	}

	public void setCounterDictionary(Dictionary<string, Type> dict)
	{
		counterDictionary = dict;
	}

	public Type getCounterType(string s)
	{
		Type type = null;
		if (counterDictionary.TryGetValue(s, out type))
		{

		}
		return type;
	}
}

public class GenericCounter
{
	override public string getName()
	{
		return "GenericCounter";
	}

	override public string getImage()
	{
		return "GenericCounter";
	}
}
*/

//TODO add this also in ability eff manager && the thing.
