using UnityEngine;
using System.Collections.Generic;

public class Dizzy
{


	public static readonly int UNDIZZY = 1 << 1;
	public static readonly int DIZZY = 1 << 2;
	public static readonly int DIZZY_LOCK = 1 << 3;
	public static readonly int DIZZY_ANY = UNDIZZY | DIZZY | DIZZY_LOCK;

	 private static Dictionary <int, string> dict = new Dictionary<int, string> (){
		{ UNDIZZY, "UNDIZZY"},
		{ DIZZY, "DIZZY"},
		{ DIZZY_LOCK, "DIZZY_LOCK"},
		{ DIZZY_ANY, "DIZZY_ANY"}
	};

	public class DizzyFactor
	{
		public static int F_UNDIZZY = -1;
		public static int F_UNDIZZY_LOCK = -2;
		public static int F_NONE = 0;
		public static int F_DIZZY = 1; // WILL ONLY GO TO DIZZY, IF ALREADY DIZZY, NO LOCK
		public static int F_DIZZY_ADD = 3; // WILL DIZZY LOCK IF ALREADY DIZZY, else just DIZZY
		public static int F_DIZZY_LOCK = 2;
	}

	public int getDizzy()
	{
		return dizzy;
	}

	public void setDizzy(int dizzy)
	{
		this.dizzy = dizzy;
	}

	public static string dizzyFactorToString(int factor)
	{
		if (factor == DizzyFactor.F_UNDIZZY)
		{
			return "UNDIZZY";
		}
		else if (factor == DizzyFactor.F_UNDIZZY_LOCK)
		{
			return "UNDIZZY_LOCK";
		}
		else if (factor == DizzyFactor.F_NONE)
		{
			return "NONE";
		}
		else if (factor == DizzyFactor.F_DIZZY)
		{
			return "DIZZY";
		}
		else if (factor == DizzyFactor.F_DIZZY_LOCK)
		{
			return "DIZZY_LOCK";
		}
		else if (factor == DizzyFactor.F_DIZZY_ADD)
		{
			return "DIZZY_ADD";
		}

//BROKEFDSFSDFSD
		return null;
	}

	public static int dizzyFactorFromString(string str)
	{
		if (str.Equals("UNDIZZY"))
		{
			return DizzyFactor.F_UNDIZZY;
		}
		else if (str.Equals("UNDIZZY_LOCK"))
		{
			return DizzyFactor.F_UNDIZZY_LOCK;
		}
		else if (str.Equals("NONE"))
		{
			return DizzyFactor.F_NONE;
		}
		else if (str.Equals("DIZZY"))
		{
			return DizzyFactor.F_DIZZY;
		}
		else if (str.Equals("DIZZY_ADD"))
		{
			return DizzyFactor.F_DIZZY_ADD;
		}
		else if (str.Equals("DIZZY_LOCK"))
		{
			return DizzyFactor.F_DIZZY_LOCK;
		}
		MyLogger.log("ERROR IN DIZZYFACTOR FROM STRING");
		return Constants.ERROR_GENERAL;
	}
	
    public static int dizzyFromString(string name){

		if (name.Equals ("UNDIZZY"))
			return UNDIZZY;
		if (name.Equals ("DIZZY"))
			return DIZZY;
		if (name.Equals ("DIZZY_LOCK"))
			return DIZZY_LOCK;
		if (name.Equals ("DIZZY_ANY"))
			return DIZZY_ANY;


        return Constants.ERROR_GENERAL;
    }
    
    public string toString(){
       return dict[dizzy];
    }
    
    public void modifyDizzy(int dizzyFactor)
    { 
    	if (dizzyFactor == DizzyFactor.F_UNDIZZY_LOCK)
    	{
    		dizzy = Dizzy.UNDIZZY;
    	}
    	
		else if (dizzyFactor == DizzyFactor.F_DIZZY_LOCK)
    	{
    		dizzy = Dizzy.DIZZY_LOCK;
    	}
    	
    	else if (dizzyFactor == DizzyFactor.F_UNDIZZY)
    	{
			if (dizzy == Dizzy.DIZZY || dizzy == Dizzy.UNDIZZY)
				dizzy = Dizzy.UNDIZZY;
			else if (dizzy == Dizzy.DIZZY_LOCK)
				dizzy = Dizzy.DIZZY;
    	}
    	else if (dizzyFactor == DizzyFactor.F_DIZZY_ADD)
    	{
    		if (dizzy == Dizzy.UNDIZZY)
    		{
    			dizzy = Dizzy.DIZZY;
    		}
			else if (dizzy == Dizzy.DIZZY_LOCK || dizzy == Dizzy.DIZZY )
    		{
    			dizzy = Dizzy.DIZZY_LOCK;
    		}
    	}
    	else if (dizzyFactor == DizzyFactor.F_DIZZY)
    	{
    		if (dizzy == Dizzy.UNDIZZY)
    		{
    			dizzy = Dizzy.DIZZY;
    		}
    	}
    	
    	else if (dizzyFactor == DizzyFactor.F_NONE)
    	{
    	}
    	//MyLogger.log("ERROR MODIFY DIZZY");
    	//return null;
    }

	private int dizzy = UNDIZZY;
	
	public Dizzy()
	{
		dizzy = UNDIZZY;
	}

	public Dizzy(Dizzy diz)
	{
		dizzy = diz.dizzy;
	}
}
