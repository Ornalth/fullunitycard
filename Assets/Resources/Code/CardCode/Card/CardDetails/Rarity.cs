﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Rarity
{
	COMMON,
	UNCOMMON,
	RARE,
	VERY_RARE
};

public class RarityConverter
{
	private static int UNCOMMON_THRESHOLD = 50;
	private static int RARE_THRESHOLD = 80;
	private static int VERY_RARE_THRESHOLD = 95;
	public static Rarity convertRarity(int rarity)
	{
		if (rarity > VERY_RARE_THRESHOLD)
		{
			return Rarity.VERY_RARE;
		}
		if (rarity > RARE_THRESHOLD)
		{
			return Rarity.RARE;
		}

		if (rarity > UNCOMMON_THRESHOLD)
		{
			return Rarity.UNCOMMON;
		}
		return Rarity.COMMON;
	}

	public static string rarityToString(Rarity rarity)
	{
		switch (rarity)
		{
			case Rarity.COMMON:
				return "common";
			case Rarity.UNCOMMON:
				return "uncommon";
			case Rarity.RARE:
				return "rare";
			case Rarity.VERY_RARE:
				return "veryrare";
		}
		return "error";
	}
}