using System.Collections.Generic;

public class CardType
{
	public const int NONE = 0;
	public const int CREATURE = 1;
	public const int STRUCTURE = 1<<1; // 2
	public const int SPELL = 1<<2; // 4
	public const int PLAYER = 1<<3; // 8
	public const int LAND = 1 << 4; // 16
	public const int BOARD_TYPE = CREATURE | STRUCTURE | LAND;
	public const int ANY = CREATURE | STRUCTURE | SPELL | PLAYER | LAND;

	 private static Dictionary <int, string> dict = new Dictionary<int, string> (){
	        { NONE, "NONE"},
	        { CREATURE, "CREATURE"},
	        { STRUCTURE, "STRUCTURE"},
	        { SPELL, "SPELL"},
	        { PLAYER, "PLAYER"},
	        { LAND, "LAND"},
	        { ANY, "ANY"}//,
	       // { BOARD_TYPE, "BOARD_TYPE"}
	    };

	 private static Dictionary <string, int> reverseDict = new Dictionary<string, int> (){
	        { "NONE", NONE},
	        { "CREATURE", CREATURE},
	        { "STRUCTURE", STRUCTURE},
	        { "SPELL", SPELL},
	        { "PLAYER", PLAYER},
	        { "LAND", LAND},
	        { "ANY", ANY}//,
	       // { BOARD_TYPE, "BOARD_TYPE"}
	    };


	public static List<string> cardTypeToArray(int cardType)
	{

		List<string> cardTypes = new List<string>();

		foreach (int key in dict.Keys)
		{
			if (key == ANY)
				continue;
			if ((key & cardType) != 0)
			{
				cardTypes.Add(dict[key]);
			}
		}	
		return cardTypes;
	}

    public static int cardTypeFromString(string name){
    	return reverseDict[name];
    }



	public static string cardTypeToString(int cardType)
	{
		return dict[cardType];
	}

}
