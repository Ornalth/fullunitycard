using System.Collections.Generic;

public class CardState
{
	public static int NONE = 0;
	public static int GRAVE = 1;
	public static int EXILE = 1<<1;
	public static int DECK = 1<<2;
	public static int STACK = 1<<3;
	public static int BOARD = 1<<4;
	public static int PLAYER = 1<<5;
	public static int HAND = 1<<6;
	public static int ERROR = 879;
	public static int ANY = HAND | GRAVE | BOARD | EXILE | DECK | STACK | PLAYER;
	public static int ANY_NOT_BOARD = GRAVE | EXILE | DECK;
	private static Dictionary <int, string> dict = new Dictionary<int, string> (){
	        { NONE, "NONE"},
	        { GRAVE, "GRAVE"},
	        { EXILE, "EXILE"},
	        { DECK, "DECK"},
	        { STACK, "STACK"},
	        { BOARD, "BOARD"},
	        { PLAYER, "PLAYER"},
	        { HAND, "HAND"},
	        { ANY, "ANY"}
	    };

	private static List<string> possibleCardStates = new List<string> (){
	        "GRAVE",
	        "EXILE",
	        "DECK",
	        "STACK",
	        "BOARD",
	        "PLAYER",
	        "HAND",
	        "ANY"
	    };


	public static List<string> cardStateToArray(int state)
	{

		List<string> states = new List<string>();

		foreach (int key in dict.Keys)
		{
			if ((key & state) != 0)
			{
				states.Add(dict[key]);
			}
		}	
		return states;
	}

	public static string cardStateToString(int state)
	{
		return dict[state];
	}

	public static int cardStateFromString(string s)
	{
		if (s.Equals("GRAVE"))
		{
			return GRAVE;
		}
		else if (s.Equals("EXILE"))
		{
			return EXILE;
		}
		else if (s.Equals("DECK"))
		{
			return DECK;
		}
		else if (s.Equals("STACK"))
		{
			return STACK;
		}
		else if (s.Equals("BOARD"))
		{
			return BOARD;
		}
		else if (s.Equals("HAND"))
		{
			return HAND;
		}
		else if (s.Equals("ANY"))
		{
			return ANY;
		}
		else if (s.Equals("PLAYER"))
		{
			return PLAYER;
		}
		return ERROR;
	}

	public static List<string> getPossibleCardStateList()
	{
		return possibleCardStates;
	}
}
