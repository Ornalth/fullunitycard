using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;


public class Player : CombatCard
{

	List<Card> deck = new List<Card>();
	List<Card> hand = new List<Card>();
	List<Card> grave = new List<Card>();
	List<Card> exiled = new List<Card>();
	//int health;
	//int attack = 0;
	int[] resources = new int[Constants.NUM_REZ];
	int[] genRate = new int[Constants.NUM_REZ];
	Board board;
	MainBoardModel model;

	public Player(MainBoardModel m) : base()
	{
		model = m;
		//super();
		// 0;
		// super();
		//model = m;
		board = new Board(this);
		genRate[0] = 0;
		genRate[1] = Constants.BASE_COLOUR_GEN_RATE;
		genRate[2] = Constants.BASE_COLOUR_GEN_RATE;
		genRate[3] = Constants.BASE_COLOUR_GEN_RATE;
	}

	public void init(string name, int team, List<Card> deck)
	{
		setName(name);
		setTeam(team);
		this.setState(CardState.PLAYER);
		health = Constants.PLAYER_STARTING_HEALTH;
		maxHealth = health;
		this.deck = deck;
		shuffle(deck);
		for (int i = 0; i < Constants.INITIAL_HAND_SIZE; i++)
			drawCardSilent();

		for (int i = 0; i < Constants.NUM_REZ; i++)
		{
			resources[i] = Constants.INITIAL_REZ_AMT;
		}
	}
	
	override
	public int getFactions()
	{
		return Faction.FACTION_ANY;
	}

	public void requestDiscard(Card origin, int number, TargetType type,
			bool strict, int pChoice)
	{
		model.requestDiscard(this, origin, number, type, strict, false, pChoice);
	}

	public void discardRandomly(Card origin, int number, TargetType type)
	{
		List<Card> discardable = new List<Card>();
		if (type == null)
		{
			foreach (Card c in hand)
				discardable.Add(c);
		}
		else
		{
			foreach (Card c in hand)
			{
				//Maybe dontneed targetetam.
				if (TargetType.satisfies(c, type,
							TargetTeam.TEAM_BOTH))
				{
					discardable.Add(c);
				}
			}
		}

		for (int i = 0; i < number && discardable.Count > 0; i++)
		{
			int x = UnityEngine.Random.Range(0, discardable.Count);
			Card c = discardable[x];

			int handIndex = hand.IndexOf(c); 
			discard(hand[handIndex]);
			discardable.RemoveAt(x);
		}
	}

	public void discard(List<Card> targets)
	{
		foreach (Card target in targets)
		{
			discard(target);
		}
	}

	public void discard(Card target)
	{
		MyLogger.log(target.getName() + " discarded.");
		hand.Remove(target);
		grave.Add(CardLibrary.getCardFromID(target.getID(), false, this,
				CardState.GRAVE));
		postEvent(GameEventType.ENTER_GRAVE, target);
	}

	//Assume cards exist in hand + deck has enough
	public void mulligan(List<int> handIndices)
	{
		List<Card> cards = new List<Card>();
		foreach (int index in handIndices)
		{
			cards.Add(hand[index]);
		}
		mulligan(cards);
	}
	//Assume cards exist in hand + deck has enough
	public void mulligan(List<Card> cards)
	{
		foreach (Card c in cards)
		{
			hand.Remove(c);
			drawCardSilent();
		}
		deck.AddRange(cards);
		shuffle(deck);
	}

	//Assume cards exist in hand + deck has enough
	public void mulliganHand()
	{
		mulligan(new List<Card>(hand));
	}


	public void requestSacrifice(Card tempSource, int sacNum,
			TargetType sacTargType, bool sacStrict, bool sacRandom)
	{
		if (sacRandom)
		{
			List<Card> cards = board.getAllCards();
			cards = TargetType.filter(cards, sacTargType);
			for (int i = 0; i < sacNum && cards.Count > 0; i++)
			{
				int x = UnityEngine.Random.Range(0, cards.Count);
				sacrifice(cards[x]);
			}
		}
		else
		{
			model.requestSacrifice(this, tempSource, sacNum, sacTargType, sacStrict, false);
		}


	}

	public void sacrifice(List<Card> targets)
	{
		board.sacrifice(targets);
	}

	public void sacrifice(Card target)
	{
		board.sacrifice(target);
	}
	
	public void salvageCard(Card target)
	{

		addRez(((CombatCard)target).getSalvageValue());
		board.sacrifice(target);
	}

	public void mill(int number)
	{
		for (int i = 0; i < number && deck.Count > 0; i++)
		{
			Card target = deck[0];
			deck.RemoveAt(0);
			grave.Add(CardLibrary.getCardFromID(target.getID(), false, this,
					CardState.GRAVE));
			postEvent(GameEventType.ENTER_GRAVE, target);
		}
	}

	public void moveFromLibrary(List<Card> targets, int destinationState, bool shuff)
	{
		if (shuff)
		{
			shuffle(deck);
		}

		foreach (Card target in targets)
		{
			moveFromLibrary(target, destinationState);
		}

		//deck.removeAll(targets);

	}
	private void shuffle(List<Card> cards)
	{
		for (int i = cards.Count - 1; i >= 0; i--)
		{

			int slot = UnityEngine.Random.Range (0, i);
			Card temp = cards[slot];
			cards[slot] = cards[i];
			cards[i] = temp;
		}
	}
	private void moveFromLibrary(Card target, int destinationState)
	{
		deck.Remove(target);
		if (destinationState == CardState.BOARD)
		{
			board.playCard(target, null);
		}
		else if (destinationState == CardState.EXILE)
		{
			target.setState(CardState.EXILE);
			exiled.Add(target);
		}
		else if (destinationState == CardState.HAND)
		{
			addCardToHand(target);
		}
		else if (destinationState == CardState.GRAVE)
		{
			target.setState(CardState.GRAVE);
			grave.Add(target);
			postEvent(GameEventType.ENTER_GRAVE, target);
		}
		else if (destinationState == CardState.DECK)
		{
			//topdeck

			deck.Insert(0, target);
		}
		else
		{
			MyLogger.log("errorrrr state in movefromdeck");
		}
	}

	public void topDeck(List<Card> cardsToTopDeck, bool shouldShuffle)
	{
		deck = deck.Except (cardsToTopDeck).ToList();
		if (shouldShuffle)
		{
			shuffle(deck);
		}
		cardsToTopDeck.AddRange(deck);
		deck =  cardsToTopDeck;
	}

	public int getDeckSize()
	{
		return deck.Count;
	}

	public int getHandSize()
	{
		return hand.Count;
	}
//
	public bool drawCard()
	{
		// TODO post to MODEL if lost by draw empty
		if (deck.Count > 0)
		{

			Card c = deck[0];
			deck.RemoveAt(0);
			addCardToHand(c);
			postEvent(GameEventType.CARD_DRAWN, this);
			return true;
		}
		LossDetails detail = new LossDetails();
		detail.msg = "DECKED";
		model.setLoss(this, detail);
		return false;
	}

	public void drawCardSilent()
	{
		if (deck.Count > 0)
		{
			Card c = deck[0];
			deck.RemoveAt(0);
			addCardToHand(c);
		}
	}


//
	public void drawXCards(int numDraw)
	{
		for (int i = 0; i < numDraw; i++)
		{
			drawCard();

		}
	}

	public void setResources(int[] resources)
	{
		//ASSERT IS CORRECT NUMBER
		this.resources = resources;
}
//
	/**
	 * 
	 * @param Card
	 *            to be played.
	 * @param Extra
	 *            cost targets for sac etc.
	 * @return
	 */
	public int playCard(Card c, List<List<Card>> extraCostTargets,
			Dictionary<int, List<Card>> targetDict)
	{

		// TODO $$$SKEW to make sure we check for targets before extracost ---->
		// MAYBE CHANGE LATER?

		int result = CardPlayResult.NONE;

		if (c is Creature)
		{
			if (board.creatures.Count >= Constants.MAX_BOARD)
			{
				result |= CardPlayResult.FAILURE_FULL;
			}
		}
		else if (c is Structure)
		{
			if (board.structures.Count >= Constants.MAX_BOARD)
			{
				result |= CardPlayResult.FAILURE_FULL;
			}
		}
		else if (c is Spell)
		{

			PassiveAbility ability = c.getPassives()[0];

			TargetListObject targetObj = model.fillTargetsForAbility(c, ability, targetDict);
			if (targetObj != null)
			{
				result |= CardPlayResult.FAILURE_TARGET;
			}
/*
			TargetType targType = abil.getTargetType();
			if (abil.requiresRequestGenTargets())
			{
				if (targets == null)
				{
					result |= CardPlayResult.FAILURE_TARGET;
				}
				else
				{
					// TODO clean up this junk


					// Verify all targets all targets legal
					int numTargets = TargetType.filter(targets, targType)
							.Count;
					if (targets.Count != numTargets)
					{
						result |= CardPlayResult.FAILURE_TARGET;
					}
					else if ( !TargetType.satisfiesStrict(targType.numTargets, numTargets, targType.strict))
					{
						result |= CardPlayResult.FAILURE_TARGET;
					}
				}
			}
			else
			{
				targets = model.getTargetsForCard(c, targType);
			}
*/
		}

		if (c is CombatCard)
		{
			CombatCard cc = (CombatCard)c;
			if (cc.isUnique())
			{
				if (hasUniqueCollided(cc))
				{
					result |= CardPlayResult.FAILURE_UNIQUE_COLLISION;
				}
			}
		}

		//SKEW check extra cost later.
		result |= canPayExtraCost(c, extraCostTargets);
		if (result != CardPlayResult.SUCCESS)
		{
			return result; //FAILURE
		}
		//MyLogger.log("SUCCESS!!");
		// Land always success
		playSelectedCard(c, extraCostTargets, targetDict);

		return CardPlayResult.SUCCESS;

	}

	protected bool hasUniqueCollided(CombatCard cc)
	{
		List<string> unique = cc.getUniqueNameType();
		List<Card> cards = board.getAllCards();
		foreach(Card card in cards)
		{
			List<string> boardUniques = ((CombatCard)card).getUniqueNameType();
			foreach(string s in unique)
			{
				if (boardUniques.Contains(s))
				{
					return true;
				}
			}
		}

		return false;
	}



	public Card getCard(int index)
	{
		if (index < 0 || index >= hand.Count)
		{
			MyLogger.log("invalid card hand index");
			return null;
		}
		return hand[index];
	}

	public int canPayExtraCost(Card c, List<List<Card>> extraCostTargets)
	{
		int result = CardPlayResult.NONE;
		int[] cost = c.getCost();
		for (int i = 0; i < cost.Length; i++)
		{
			if (resources[i] - cost[i] < 0)
				result |= CardPlayResult.FAILURE_COST;
		}

		if (c.needsExtraCostTargets())
		{
			List<TargetListObject> targetList = c.getExtraCostTargetTypes();
			int targetListSize = extraCostTargets.Count;
			MyLogger.log("EXTRA TARGET LIST REQUIRED " + targetList.Count + " TARGETS REQ'd " + extraCostTargets.Count );

			for (int i = targetListSize; i < targetList.Count; i++)
			{
				TargetListObject targetObj = targetList[i];
				if (targetObj.targetGen == TargetGeneration.NEEDS_REQUEST)
				{	
					result |= CardPlayResult.FAILURE_EXTRA_COST;
					break;
				}
				else if (targetObj.targetGen == TargetGeneration.MODEL_GEN)
				{
					List<Card> newExtraCostTargets = model.getTargetsForCard(c, targetObj.targetType);
					extraCostTargets.Add(newExtraCostTargets);
				}
				else
				{
					MyLogger.log("DAFUQ IS THIS EXTRA HAS NO CHANGE, THAT MAKES NO SENSE?");
				}
			}
		}
		else
		{
//			MyLogger.log("THE FK EXTRA COST NO EXISDT");
		}

		if (result != CardPlayResult.SUCCESS)
		{
			return result; //FAILURE
		}

		return CardPlayResult.SUCCESS;

	}
/*
	public int canPay(int index)
	{
		if (index < 0 || index >= hand.Count)
		{
			return CardPlayResult.FAILURE_INVALID;
		}

		return canPay(hand[index], null);
	}
*/

	private bool payCardExtraCost(Card c, List<List<Card>> extraCostTargets)
	{
		// NOTICE I CHECK AGAIN now! to make sure that it is success, maybe
		// board state changed
		if (CardPlayResult.SUCCESS == canPayExtraCost(c, extraCostTargets))
		{
			//MyLogger.log("PAY1");
			int[] result = c.getCost();
			for (int i = 0; i < result.Length; i++)
			{
				resources[i] = resources[i] - result[i];
				//MyLogger.log("Rez " + i + ": " + resources[i]);
			}

			if (c.needsExtraCostTargets())
			{
				List<PreAbilityObject> effectObjs = c.getExtraCostEffects();
				List<TargetListObject> targetList = c.getExtraCostTargetTypes();
				if (effectObjs.Count != targetList.Count)
				{
					MyLogger.log("THE SIZES DONT MATCH? " + effectObjs.Count + " " + targetList.Count);
					return false;
				}

				for (int i = 0; i < effectObjs.Count; i++)
				{
					PreAbilityObject abilityObj = effectObjs[i];
					List<Card> targets = extraCostTargets[abilityObj.targetListNum];
					List<AbilityEffect> effs = abilityObj.effects;
					foreach (AbilityEffect effect in effs)
					{
						if (!(effect is IPreAbilityEffect))
						{
							MyLogger.log("Player -- ERROR! WHY IS A EFF NOT IPREABILITYEFFECT IN PREEFFECTS");
							return false;
						}
						IPreAbilityEffect preEffect = (IPreAbilityEffect) effect;
						bool success = preEffect.canSatisfy(c, targets, model);
						if (!success)
						{
							MyLogger.log("FAILED TO PAY EXTRA!");
							return false;
						}
					}
				}

				for (int i = 0; i < effectObjs.Count; i++)
				{
					PreAbilityObject abilityObj = effectObjs[i];
					List<Card> targets = extraCostTargets[i];
					List<AbilityEffect> effs = abilityObj.effects;
					foreach (AbilityEffect effect in effs)
					{
						IPreAbilityEffect preEffect = (IPreAbilityEffect) effect;
						preEffect.preEffectAct(c, targets, model);
					}
				}
			}
			else
			{
//				MyLogger.log("THE FK EXTRA COST NO EXISDT");
			}


			return true;
		}
		MyLogger.log("CANT PAY EXTRA COST?");
		return false;
	}

	private void playSelectedCard(Card c, List<List<Card>> extraCostTargets,
			Dictionary<int, List<Card>> targetDict)
	{
		if (payCardExtraCost(c, extraCostTargets))
		{
			board.playCard(c, targetDict);
			hand.Remove(c);
		}
		else
		{
			MyLogger.log("failed to play selected card... errors? in player");
			MyLogger.log("CARD IS " + c.getName());
			MyLogger.log( "TARGETS EXTRA " + extraCostTargets.Count);
			//MyLogger.log( "DF" +  targets.Count );
		}

	}

	public void printDeck()
	{
		MyLogger.log("Deck == ");
		for (int i = 0; i < deck.Count; i++)
		{
			MyLogger.log(deck[i].toString());
		}
	}

	public void printHand()
	{
		MyLogger.log("Hand == ");
		for (int i = 0; i < hand.Count; i++)
		{
			MyLogger.log(hand[i].toString());
		}
	}

	public void printGrave()
	{
		MyLogger.log("Grave == ");
		for (int i = 0; i < grave.Count; i++)
		{
			MyLogger.log(grave[i].toString());
		}

	}

//	/**
//	 * Asks model to broadcast to all players
//	 * 
//	 */
	public void postEvent(int ev, Card c)
	{
		model.eventOccurred(ev, c, this);
	}
//
//	/**
//	 * Responds to a broadcast.
//	 * 
//	 */
//
	public void printBoard()
	{
		MyLogger.log(board.toString());
	}
//
	public Card getCreature(int index)
	{
		return board.getCreature(index);
		// return null;
	}

//	/**
//	 * Passive of the card Origin is the card that owns the ability targets
//	 * PARAM DIRECT -> means add directly to stack
//	 */
//
//
	public void addAbilitiesToStack(PassiveAbility i, Card origin,
			List<Card> targets, int targetGen)
	{
		model.addAbilitiesToStack(i, origin, targets, targetGen);
	}

	public void addAbilitiesToStack(PassiveAbility i, Card origin,
			Dictionary<int, List<Card>> targetDict)
	{
		model.addAbilitiesToStack(i, origin, targetDict);
	}

	public Card getStructure(int index)
	{
		return board.getStructure(index);
	}

	public void eotCleanup(int curTurn)
	{
		bool shouldDecay = curTurn == team;
		board.eotCleanup(shouldDecay);
	}

	public void clearBoard()
	{
		board.clearBoard();

	}
//
	public void generate(int turn)
	{
		resources[0] = turn * 100;
		for (int i = 1; i < Constants.NUM_REZ; i++)
		{
			resources[i] += genRate[i];
		}
	}

	public void undizzyAll()
	{
		board.undizzyAll();

	}

	public void specialSummon(Card card)
	{
		board.addCardToBoard(card);
	}

	public void specialSummonSilent(Card card)
	{
		board.addCardToBoardSilently(card);
	}
//
	public bool canPayResourceCost(int[] cost)
	{
		for (int i = 0; i < cost.Length; i++)
		{
			if (resources[i] - cost[i] < 0)
				return false;
		}
		return true;

	}

	private void addCardToHand(Card c)
	{
		if (!c.isToken())
		{
			c.setState(CardState.HAND);
			hand.Add(c);
		}
		else
		{
			MyLogger.log("CANT ADD CARD IT IS A TOKEN " + c.getName());
		}
	}
//
	public void payCost(int[] cost)
	{
		for (int i = 0; i < cost.Length; i++)
		{
			resources[i] -= cost[i];
		}
	}
//
	public void kill(Card c, bool silent)
	{
		board.kill(c, silent);
	}

	public ActiveAbility getAbility(int cardType, int slot, int abilityNumber)
	{

		return board.getAbility(cardType, slot, abilityNumber);
	}

	public Card getCard(int cardType, int slot)
	{
		return board.getCard(cardType, slot);
	}

	public List<Card> getTargetsFor(Card c, TargetType type)
	{
		//MyLogger.log(type.toString());
		List<Card> targets = new List<Card>();
		if (type.containsState(CardState.BOARD))
		{
			targets.AddRange(board.getTargetsFor(c, type));
		}

		if (type.containsState(CardState.HAND))
		{
			targets.AddRange(hand);
		}

		if (type.containsState(CardState.GRAVE))
		{
			targets.AddRange(grave);
		}

		if (type.containsState(CardState.PLAYER))
		{
			MyLogger.log("THIS");
			targets.Add(this);
		}
		return targets;
	}
/*
	private void finishAddingToStack()
	{
		model.finishedAddingAbilities(this);
	}
*//*
	public void receivePhaseChange(int phase, int teamOpp)
	{
		board.respondToPhaseChange(phase, teamOpp);
		finishAddingToStack();
	}
*/

	public void respondToEvent(ExecutedGameEvent eev)
	{
		//board.respondToEvent(eev);

		if (eev.origin.getTeam() == this.getTeam())
		{
			switch (eev.gameEventType)
			{
				case GameEventType.ATTACK:
					break;
			case GameEventType.COMBAT_DAMAGE:
					break;
			case GameEventType.DAMAGE:
					break;
			case GameEventType.DEATH:
					if (!eev.origin.isToken())
					{
						if (eev.origin.hasStatusEffect(Status.SECOND_LIFE))
						{
							Card sLifeCard = CardLibrary.getCardFromID(eev.origin.getID(), false,
									this, CardState.HAND);
							addCardToHand(sLifeCard);
						}
						else
						{
							Card graveCard = CardLibrary.getCardFromID(eev.origin.getID(), false,
									this, CardState.GRAVE);
							grave.Add(graveCard);
							postEvent(GameEventType.ENTER_GRAVE, eev.origin);
						}
							
					}

					break;
			case GameEventType.ENTER:
					if (eev.origin is Spell && !eev.origin.isToken())
					{

						Card graveCard = CardLibrary.getCardFromID(eev.origin.getID(), false,
								this, CardState.GRAVE);
						grave.Add(graveCard);
						postEvent(GameEventType.ENTER_GRAVE, eev.origin);
					}
					break;
			case GameEventType.ENTER_GRAVE:
					eev.origin.setState(CardState.GRAVE);
					break;
			case GameEventType.EXILE:
					break;
			case GameEventType.LEAVE:
					if (eev.origin is Land && !eev.origin.isToken())
					{
						Card graveCard = CardLibrary.getCardFromID(eev.origin.getID(), false,
								this, CardState.GRAVE);
						graveCard.setTeam(this.team);
						grave.Add(graveCard);
						postEvent(GameEventType.ENTER_GRAVE, eev.origin);
					}
					break;
				default:
					break;

			}
		}
		//finishAddingToStack();

	}

	override public void addHealth(int health, Card source)
	{
		this.health += health;
		MyLogger.log("Gained " + health + " health.");
	}

	override public void setHealth(int health)
	{
		base.setHealth(health);
		
		if (health <= 0)
		{
			LossDetails detail = new LossDetails();
			detail.msg = "<= 0hp set";
			model.setLoss(this, detail);
		}
	}
	//Override
	// res will be residual damage overflow from temporary health., temp health is reduced first
	override public int takeDamage(int dmg, Card source, int[] overflow)
	{
		base.takeDamage(dmg,source,overflow);

		if (health <= 0)
		{
			LossDetails detail = new LossDetails();
			detail.msg = "<= 0hp from " + source.getName();
			detail.cardName = source.getName();
			model.setLoss(this, detail);
		}
		return dmg;
	}

//
	override
		public int attackCard(CombatCard defender, int[] overflow)// Source : this
	{
		if (overflow != null)
			overflow[0] = 0;
		MyLogger.log("why player attack");
		return Constants.ERROR_DMG;
	}

//
	public List<Card> getBoardCards()
	{
		return board.getAllCards();
	}
//
	public List<Card> getHand()
	{
		return hand;
	}

	//EXTRA JUNK
	public Card getGraveCard(int index)
	{
		if (index < 0 || index >= grave.Count)
		{
			return grave[index];
		}
		return null;
	}
//
	public void addRez(int[] reclaimGain)
	{
		for (int i = 0; i < Constants.NUM_REZ; i++)
		{
			resources[i] += reclaimGain[i];
			if (resources[i] < 0)
			{
				resources[i] = 0;
			}
		}
	}

	public void bounce(Card target)
	{
		if (target.getState() == CardState.GRAVE)
		{
			grave.Remove(target);
			addCardToHand(target);
			postEvent(GameEventType.LEAVE, target);
		}
		else if (board.bounce(target) && !target.isToken())
		{
			target.setState(CardState.NONE);

			Card c = CardLibrary.getCardFromID(target.getID(), false, this,
					CardState.HAND);
			addCardToHand(c);
			postEvent(GameEventType.LEAVE, target);
		}
		else
		{
			MyLogger.log("ERROR IN BOUNCE");
		}
	}

	public void exile(Card target)
	{
		if (target.getState() == CardState.GRAVE)
		{
			grave.Remove(target);
			exiled.Add(target);
			target.setState(CardState.EXILE);
			postEvent(GameEventType.EXILE, target);

		}
		else if (board.exile(target) && !target.isToken())
		{
			target.setState(CardState.NONE);
			//exile.Add(target);
			Card c = CardLibrary.getCardFromID(target.getID(), false, this,
					CardState.EXILE);
			exiled.Add(c);

			postEvent(GameEventType.EXILE, target);
		}
		else
		{
			MyLogger.log("ERROR IN EXILE");
		}
	}
//
	public List<Card> getGrave()
	{
		return grave;
	}

	public int getGraveSize()
	{
		return grave.Count;
	}
//
	public List<Card> getCreatures()
	{
		return board.getCreatures();
	}

	public List<Card> getStructures()
	{
		return board.getStructures();
	}
//
	override
	public string getDesc()
	{
		string s = base.toQuickString();
		s = s + base.getDesc();
		return s;
	}
//
	public List<Card> getLibrary()
	{
		return deck;
	}
//
	public List<Card> getLibrary(int libTopX)
	{
		if (libTopX > deck.Count)
			return new List<Card>(deck.GetRange(0, libTopX));
		return deck;
	}

	public void startTurnStatuses()
	{
		board.startTurnStatuses();

	}
	public void replaceCard(Card curCard, Card newCard)
	{
		board.replaceCard(curCard, newCard);
	}
	public void removeCard(Card target)
	{
		if (target.getState() == CardState.GRAVE)
		{
			grave.Remove(target);
		}
		else if (target.getState() == CardState.EXILE)
		{
			exiled.Remove(target);

		}
		else if (target.getState() == CardState.DECK)
		{
			deck.Remove(target);
		}
		else if (target.getState() == CardState.BOARD)
		{
			board.bounce(target);
		}
		else if (target.getState() == CardState.HAND)
		{
			hand.Remove(target);
		}
	}
	
	public void addCard(Card target, int zone)
	{
		bool silent = true;
		if (target.getState() != CardState.BOARD)
		{
			silent = false;
		}
		target.setTeam(getTeam());
		target.setState(zone);
		MyLogger.log(zone);
		if (target.getState() == CardState.GRAVE)
		{
			grave.Add(target);
		}
		else if (target.getState() == CardState.EXILE)
		{
			exiled.Add(target);

		}
		else if (target.getState() == CardState.DECK)
		{
			deck.Add(target);
			shuffle(deck);
		}
		else if (target.getState() == CardState.BOARD)
		{
			if (silent)
			{
				board.addCardToBoardSilently(target);
			}
			else
			{
				board.addCardToBoard(target);
			}
		}
		else if (target.getState() == CardState.HAND)
		{
			addCardToHand(target);
		}
	}

	public int[] getResources()
	{
		return resources;
	}
	override public int getCardType()
	{
		return CardType.PLAYER;
	}

	public bool hasBoardCards()
	{
		return board.hasCards ();
	}

	//Sacrifice
	override public bool canSalvage()
	{
		//CANNOT SAC
		return false;
	}

	override public bool canDeclareAttack()
	{
		return false;
	}
}
