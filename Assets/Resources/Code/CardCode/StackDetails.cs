using System.Collections.Generic;

public class StackDetails
{
	public Card origin;
	//public List<Card> targets;
	public Ability ev;
	//public TargetType extraDetailTargetType;
	//public List<Card> extraDetailTargets;
	public EventResult result;
	public List<Card> lastActionCards;


	public Dictionary<int, List<Card>> targetDict;
	
	//TODO FIXME
	public EventResult executeEvent(MainBoardModel m)
	{
		if (result != null && !result.done)
		{
			result.lastActionCards = lastActionCards;
		}
		result = ev.theEvent(origin, targetDict, m, null, null, result);
		return result; 
		//	return ev.theEvent(origin,targets, extraDetailTargets,m);
	}
	
	
	public void counterThisAbility()
	{
		ev.counterThis();
	}
	
	//Lower Number === FASTER!!! 0 fasgter than 10.
	public int getPriority(int turn)
	{
		//If not equal to own player's turn it is slower!
		if (turn != origin.getTeam())
		{
			return ev.getAbilitySpeed() * 2 + 1;
		}
		return ev.getAbilitySpeed() * 2;
	}
	
}

