using System.Collections.Generic;

public class Phase
{
	public const int ERROR = -1;
	public const int DRAW = 1;
	public const int SACRIFICE = 2;
	public const int MAIN_ALLY = 4;
	public const int MAIN_ENEMY = 8;
	public const int ATTACKS_ALLY = 16;
	public const int ATTACKS_ENEMY = 32;
	public const int BLOCKS_ENEMY = 64;
	public const int BLOCKS_ALLY = 128;
	public const int MAIN_2ND_ALLY = 256;
	public const int MAIN_2ND_ENEMY = 512;
	public const int END_ALLY = 1024;
	public const int END_ENEMY = 2048;
	public const int CLEANUP = 4096;
	public static readonly int ALL_PHASES = DRAW | SACRIFICE | MAIN_ALLY | MAIN_ENEMY | ATTACKS_ALLY
									| ATTACKS_ENEMY | BLOCKS_ENEMY | BLOCKS_ALLY | MAIN_2ND_ALLY
									| MAIN_2ND_ENEMY | END_ALLY | END_ENEMY | CLEANUP;

	public static readonly int DEFAULT_SPELL_PHASES = MAIN_ALLY | MAIN_ENEMY | ATTACKS_ALLY
									| ATTACKS_ENEMY | BLOCKS_ENEMY | BLOCKS_ALLY | MAIN_2ND_ALLY
									| MAIN_2ND_ENEMY | END_ALLY | END_ENEMY;

	public static readonly int DEFAULT_CREATURE_PHASES = MAIN_ALLY | MAIN_2ND_ALLY;

	private static Dictionary <int, string> dict = new Dictionary<int, string> (){
		{ DRAW, "DRAW"},
		{ SACRIFICE, "SACRIFICE"},
		{ MAIN_ALLY, "MAIN_ALLY"},
		{ MAIN_ENEMY, "MAIN_ENEMY"},
		{ ATTACKS_ALLY, "ATTACKS_ALLY"},
		{ ATTACKS_ENEMY, "ATTACKS_ENEMY"},
		{ BLOCKS_ENEMY, "BLOCKS_ENEMY"},
		{ BLOCKS_ALLY, "BLOCKS_ALLY"},
		{ MAIN_2ND_ALLY, "MAIN_2ND_ALLY"},
		{ MAIN_2ND_ENEMY, "MAIN_2ND_ENEMY"},
		{ END_ALLY, "END_ALLY"},
		{ END_ENEMY, "END_ENEMY"},
		{ CLEANUP, "CLEAN_UP"}

	};

	private static Dictionary <string, int> reverseDict = new Dictionary<string, int> (){
		{ "DRAW", DRAW},
		{ "SACRIFICE", SACRIFICE},
		{ "MAIN_ALLY", MAIN_ALLY},
		{ "MAIN_ENEMY", MAIN_ENEMY},
		{ "ATTACKS_ALLY", ATTACKS_ALLY},
		{ "ATTACKS_ENEMY", ATTACKS_ENEMY},
		{ "BLOCKS_ENEMY", BLOCKS_ENEMY},
		{ "BLOCKS_ALLY", BLOCKS_ALLY},
		{ "MAIN_2ND_ALLY", MAIN_2ND_ALLY},
		{ "MAIN_2ND_ENEMY", MAIN_2ND_ENEMY},
		{ "END_ALLY", END_ALLY},
		{ "END_ENEMY", END_ENEMY},
		{ "CLEAN_UP", CLEANUP}
		
	};

	private const int ROLLOVER_PHASE = CLEANUP * 2;
	int phase = DRAW;

	
	//@Override
	public string toString(){
		return dict[phase];
	}

	public void nextPhase()
	{
		phase *= 2;
		if (phase == ROLLOVER_PHASE)
			phase = DRAW;
	}
    
    public int getPhase()
	{
		return phase;
	}

	public bool allowCreature()
	{
		if (phase == Phase.MAIN_ALLY || phase == Phase.MAIN_2ND_ALLY)
			return true;
		return false;
	}

	public static int phaseFromString(string s)
	{
		return reverseDict[s];
	}

	public static string phaseToString(int ph)
	{
		return dict[ph];
	}


	public int getStartPriority()
	{
		switch (phase)
		{
		case DRAW:
		case SACRIFICE:
		case MAIN_ALLY:
		case ATTACKS_ALLY:
		case BLOCKS_ALLY:
		case MAIN_2ND_ALLY:
		case END_ALLY:
			return 0;
		default :
			break;
		}
		return 1;

	}

	public static List<int> getAllPhases()
	{
		return new List<int>(dict.Keys);
	}

	public void skipCombatPhase()
	{
		phase = MAIN_2ND_ALLY;
	}
}
