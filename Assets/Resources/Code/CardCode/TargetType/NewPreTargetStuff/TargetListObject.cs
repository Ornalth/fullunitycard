using System.Collections.Generic;

public class TargetListObject
{
	public TargetType targetType;
	public int targetGen;
	public string desc;	
	public int id;

	public static readonly int PROPERTIES_NONE = 0;
	public static readonly int PROPERTIES_NO_SATISFY = 1;
	public static readonly int PROPERTIES_REMOVE_SOURCE = 2;

	public int properties = PROPERTIES_NONE; //TODO


	private static Dictionary<string, int> dict = new Dictionary<string, int> {
		{ "NONE", PROPERTIES_NONE },
		{ "NO_SATISFY", PROPERTIES_NO_SATISFY },
		{ "REMOVE_SOURCE", PROPERTIES_REMOVE_SOURCE }

	};

	private static Dictionary<int, string> reverseDict = new Dictionary<int, string> {
		{ PROPERTIES_NONE, "NONE" },
		{ PROPERTIES_NO_SATISFY, "NO_SATISFY" },
		{ PROPERTIES_REMOVE_SOURCE, "REMOVE_SOURCE" }
	};

	
	public TargetListObject()
	{
		id = 0;
		targetType = new TargetType();
		targetGen = TargetGeneration.NO_CHANGE;
		desc = "NO DESCRIPTION";
		properties = PROPERTIES_NONE;
	}
	
	public TargetListObject(TargetListObject tlo)
	{
		id = tlo.id;
		targetType = new TargetType(tlo.targetType);
		targetGen = tlo.targetGen;
		desc = string.Copy(tlo.desc);
		properties = tlo.properties;
	}

	public bool isSatisfied(Card origin, Dictionary<int, List<Card>> targetDict)
	{
		if (!targetDict.ContainsKey(id))
		{
			return false;
		}
		List<Card> targets = targetDict[id];
		foreach (Card card in targets)
		{
			if (!TargetType.satisfies(card, targetType, TargetTeam.getTargetTeamFromCards(origin ,card)))
				return false;
		}
		return true;
	}

	public bool removesSource()
	{
		return (properties & PROPERTIES_REMOVE_SOURCE) == PROPERTIES_REMOVE_SOURCE;
	}

	public static int propertiesFromString(string pop)
	{
		return dict[pop];
	}
}