using System.Collections.Generic;

public class PreAbilityObject
{
	public List<AbilityEffect> effects;
	public int targetListNum;
	public string desc;	
	
	public PreAbilityObject()
	{
		effects = new List<AbilityEffect>();
		targetListNum = 0;
		desc = "NO DESCRIPTION";
	}
	
	public PreAbilityObject(PreAbilityObject ptao)
	{
		effects = new List<AbilityEffect>();
		foreach (AbilityEffect eff in ptao.effects)
		{
			effects.Add(eff.deepCopy());
		}
		targetListNum = ptao.targetListNum;
		desc = string.Copy(ptao.desc);
	}

}