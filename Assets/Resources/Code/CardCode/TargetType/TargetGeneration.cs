﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetGeneration {
	public const int NO_CHANGE = 0;
	public const int MODEL_GEN = 1;
	public const int NEEDS_REQUEST = 2;

	private static Dictionary<string, int> dict = new Dictionary<string, int> {
		{ "NO_CHANGE", NO_CHANGE },
		{ "MODEL_GEN", MODEL_GEN },
		{ "NEEDS_REQUEST", NEEDS_REQUEST }
	};

	private static Dictionary<int, string> reverseDict = new Dictionary<int, string> {
		{ NO_CHANGE, "NO_CHANGE" },
		{ MODEL_GEN, "MODEL_GEN" },
		{ NEEDS_REQUEST, "NEEDS_REQUEST" }
	};


	public static int targetGenerationFromString(string s)
	{
		return dict [s];
	}

	public static string targetGenerationToString(int ty)
	{
		return reverseDict[ty];
	}

	public static bool isModelGeneratesTargets(int gen)
	{
		return gen == MODEL_GEN;
	}

	public static bool isRequiresRequestForTargets(int gen)
	{
		return gen == NEEDS_REQUEST;
	}
}
