﻿using UnityEngine;
using System.Collections.Generic;

public class TargetExtras  {
	public static int NONE = 0;
	public static int CURRENT_TURN = 1;
	public static int CURRENT_PRIORITY = 1 << 1;

	public static int EXCLUDE_SELF = 1 << 2; //TODO
	public static int EXCLUDE_TARGET_TYPE = 1 << 31;

	private static Dictionary <int, string> dict = new Dictionary<int, string> (){
	        { NONE, "NONE"},
	        { CURRENT_TURN, "CURRENT_TURN"},
	        { CURRENT_PRIORITY, "CURRENT_PRIORITY"},
	    //    { EXCLUDE_SELF, "EXCLUDE_SELF"}
	    	{ EXCLUDE_TARGET_TYPE, "EXCLUDE_TARGET_TYPE"}
	    };

	private static Dictionary <string, int> reverseDict = new Dictionary<string, int> (){
	        { "NONE", NONE},
	        { "CURRENT_TURN", CURRENT_TURN},
	        { "CURRENT_PRIORITY", CURRENT_PRIORITY},
	    //    { "EXCLUDE_SELF", EXCLUDE_SELF}
	    	{ "EXCLUDE_TARGET_TYPE", EXCLUDE_TARGET_TYPE}
	    };

	public static int extrasFromString(string s)
	{
		return reverseDict[s];
	}

	public static string extrasToString(int s)
	{
		return dict[s];
	}

	public static List<string> extrasToArray(int state)
	{

		List<string> extras = new List<string>();

		foreach (int key in dict.Keys)
		{
			if ((key & state) != 0)
			{
				extras.Add(dict[key]);
			}
		}	
		return extras;
	}

	public static bool isCurrentTurn(int s)
	{
		if ((s & CURRENT_TURN) != 0)
			return true;
		return false;
	}

	public static bool isCurrentPriority(int s)
	{
		if ((s & CURRENT_PRIORITY) != 0)
			return true;
		return false;
	}

	public static bool isExcludeTargetType(int s)
	{
		if ((s & EXCLUDE_TARGET_TYPE) != 0)
			return true;
		return false;
	}

}
