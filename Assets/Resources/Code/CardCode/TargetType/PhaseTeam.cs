public class PhaseTeam
{
	public static int TEAM_NONE = 0;
	public static int TEAM_ALLY = 1;
	public static int TEAM_OPP = 2;
	public static int TEAM_BOTH = TEAM_ALLY | TEAM_OPP;
	public static int getPhaseTeamFromCards(Card origin, Card target)
	{
		if (origin.getTeam() == target.getTeam())
			return TEAM_ALLY;
		return TEAM_OPP;
	}
	
	public static int phaseTeamFromString(string s)
	{
		if (s.Equals("TEAM_BOTH"))
		{
			return TEAM_BOTH;
		}
		else if (s.Equals("TEAM_ALLY"))
		{
			return TEAM_ALLY;
		}
		else if (s.Equals("TEAM_OPP"))
		{
			return TEAM_OPP;
		}
		return TEAM_NONE;
	}

	public static string phaseTeamToString(int phT)
	{
		if (phT == TEAM_BOTH)
		{
			return "TEAM_BOTH";
		}
		else if (phT == TEAM_ALLY)
		{
			return "TEAM_ALLY";
		}
		else if  (phT == TEAM_OPP)
		{
			return "TEAM_OPP";
		}
		return null;
	}
}
