using System.Collections.Generic;
using UnityEngine;

public class TargetType
{
	public static int DEFAULT_CARD_TYPE = CardType.ANY;
	public static int DEFAULT_NUM_TARGETS = 0;
	public static int DEFAULT_STRICT = StrictType.EXACT;
	public static int DEFAULT_FACTIONS = Faction.FACTION_ANY;
	public static int DEFAULT_CARD_STATES = CardState.BOARD;
	public static int DEFAULT_TOKEN = TargetTokenType.ANY;
	public static int DEFAULT_TARGET_TEAM = TargetTeam.TEAM_BOTH;
	//public static int DEFAULT_TARGET_UNIT = TargetUnitType.TARGET_ALL;
	public static int DEFAULT_EXTRAS = TargetExtras.NONE;

	//TODO add static locks to prevent some targettypes from resolving! (in satisfies)
	public int cardTypes = DEFAULT_CARD_TYPE;

	public List<string> nameTypes = new List<string>();
	public int numTargets = DEFAULT_NUM_TARGETS;

	public List<int> targetSpecificIDs = new List<int>();
	public int targetTeam = DEFAULT_TARGET_TEAM;
	//public int targetUnitType = DEFAULT_TARGET_UNIT; // cardtype
	public int cardStates = DEFAULT_CARD_STATES;
	public int targetToken = DEFAULT_TOKEN;
	public int targetFaction = DEFAULT_FACTIONS;
	public int strict = DEFAULT_STRICT;
	public int extras = DEFAULT_EXTRAS;
	public TargetType excludeTargetType = null;
	public List<int> statuses = new List<int>();
	public int dizzyStates = Dizzy.DIZZY_ANY;
	//bool isSelf = false;

	public string toString()
	{
		return "TARGET TEAM" + targetTeam + " CARDSTATES " + cardStates + " CARDTYPES " + cardTypes;
	}
	public TargetType()
	{

	}

	public TargetType(TargetType t)
	{
		if (t != null)
		{
			//this.isSelf = t.isSelf;
			this.numTargets = t.numTargets;
			this.nameTypes = new List<string>(t.nameTypes);
			cardTypes = t.cardTypes;
			cardStates = t.cardStates;
			targetTeam = t.targetTeam;
			targetFaction = t.targetFaction;
			targetToken = t.targetToken;
			extras = t.extras;
			strict = t.strict;

			dizzyStates = t.dizzyStates;

			statuses = new List<int>();
			foreach (int i in t.statuses)
			{
				statuses.Add(i);
			}

			targetSpecificIDs = new List<int>();
			foreach (int i in t.targetSpecificIDs)
			{
				targetSpecificIDs.Add(i);
			}

			if (t.excludeTargetType != null)
			{
				excludeTargetType = new TargetType(t.excludeTargetType);
			}
		}

	}

	public bool isDefault()
	{
		if (cardTypes == DEFAULT_CARD_TYPE &&
			nameTypes.Count == 0 && numTargets == DEFAULT_NUM_TARGETS
			&& targetSpecificIDs.Count == 0 && targetTeam == DEFAULT_TARGET_TEAM && cardStates == DEFAULT_CARD_STATES
			&& targetToken == DEFAULT_TOKEN && strict == DEFAULT_STRICT && statuses.Count == 0)
			return true;
		return false;
	}

//TODO FIXME
	/*public static TargetType constructSelfTarget(Card c)
	{
		TargetType type = new TargetType();
		type.numTargets = 1;
		targetUnitType = TargetUnitType.TARGET_UNIT;
		//type.isSelf = true;
		//return type;
		return null;
	}*/
//TODO FIXME
	public static TargetType constructOppPlayerTargetType()
	{
		TargetType type = new TargetType();
		type.cardStates = CardState.PLAYER;
		type.targetTeam = TargetTeam.TEAM_OPP;
		return  type;
	}


	/*
	public bool targetsSelf()
	{
		System.out.println("TargetsSElf in targettype should not be used anymore");
		return false;
		//return (targetUnitType & TargetUnitType.TARGET_SELF) != 0;
	}*/

	public bool containsCardType(int cardType)
	{
		//if (cardTypes == CardType.UNKNOWN)
		//return true;
		return (cardType & cardTypes) != 0;
	}

	public bool satisfiesExtras(Card card, int targetTeam)
	{
		MainBoardModel model = MainBoardModel.getInstance();
		if (TargetExtras.isCurrentTurn(extras))
		{
			if (card.getTeam() != model.currentPlayer())
			{
				return false;
			}
		}

		if (TargetExtras.isCurrentPriority(extras))
		{
			if (card.getTeam() != model.getPriority())
			{
				return false;
			}
		}

		if (TargetExtras.isExcludeTargetType(extras))
		{
			if (excludeTargetType != null)
			{
				MyLogger.log("EXCLUDE TARGET TYPE1");
				return !TargetType.satisfies(card, excludeTargetType, targetTeam);
			}
			else
			{
				MyLogger.log("exclude target type is null but has extra flag");
			}
		}
		return true;

	}

	public bool containsNames (List<string> names)
	{
		if (nameTypes.Count == 0)
		{
			return true;
		}
		else
		{
	
			for (int i = 0; i < nameTypes.Count; i++)
			{

				if (names.Contains(nameTypes[i]))
				{
					/*MyLogger.log("NAME MATCHED!!!! -> " + nameTypes[i]);
					foreach (string name in names)
					{
						MyLogger.log("NAME IS " + name);
					}*/
					return true;
				}
			}
		}
		return false;
	}

	public bool containsDizzy(int dizzy)
	{
		return (dizzyStates & dizzy) != 0;
	}
	public bool containsTeam (int team)
	{
		//if (targetTeam == TargetTeam.TEAM_NONE)
		//return true;
		return (team & targetTeam) != 0;
	}

	public bool containsState(int cardState)
	{
		//if (cardStates == CardState.UNKNOWN)
		//return true;
		//MyLogger.log("MY STATE " + cardState + " TARGET STATE " + cardStates);
		return (cardState & cardStates) != 0;
	}

	public bool containsToken(bool token)
	{
		if (token && ((targetToken & TargetTokenType.TOKEN) != 0))
		{
			return true;
		}
		else if (!token && ((targetToken & TargetTokenType.NOT_TOKEN) != 0))
		{
			return true;
		}
		return false;
	}

	public bool containsFaction(int faction)
	{
		if ((targetFaction & faction) != 0)
			return true;
		return false;
	}
	
	public bool containsSpecificID(int id)
	{
		if (targetSpecificIDs.Count == 0)
			return true;
		else 
		{
			MyLogger.log("ID IS " + id);
			return targetSpecificIDs.Contains(id);
		}
	}

	public bool containsStatus(Card card)
	{
		foreach (int status in statuses)
		{
			if (!card.hasStatusEffect(status))
			{
				MyLogger.log("NOSTATUS " + status);
				return false;
			}
		}
		return true;
	}

	public void setCardState(int cardstate)
	{
		this.cardStates = cardstate;

	}

	public void setTargetTeam(int targteam)
	{
		this.targetTeam = targteam;
	}

	
	

	/** Note target team is always ALLY!
	 * Filters all that satisfies targetType
	 */
	public static List<Card> filter(List<Card> cards,
			TargetType targetType)
			{
		List<Card> ans = new List<Card>();
		int targetTeam = TargetTeam.TEAM_ALLY;
		foreach (Card card in cards)
		{
			if (TargetType.satisfies(card, targetType, targetTeam))
			{
				ans.Add(card);
			}
		}
		return ans;
			}

	public static bool satisfies(Card card, TargetType type, int targetTeam)
	{
		if (type == null)
		{
			MyLogger.log("SATISFY - NO TARGET TYPE");
			return false;
		}
		if (!type.containsCardType(card.getCardType()))
		{
			MyLogger.log("Satisfy - ERROR CARD TYPE");
			return false;
		}

		if (!type.containsTeam(targetTeam))
		{
			MyLogger.log("CARD AM" + card.getName());
			MyLogger.log("THE TEAM " + targetTeam);
			MyLogger.log("Satisfy - ERROR TEAM");
			return false;
		}
		if (!type.containsNames(card.getNameType()))
		{
			MyLogger.log("Satisfy - NAME TYPE");
			return false;
		}
		if (!type.containsState(card.getState()))
		{
			MyLogger.log("THIS STAE " + type.cardStates + " CARD Gstaes" + card.getState());
			MyLogger.log("Satisfy - STATE");
			return false;
		}
		if (!type.containsFaction(card.getFactions()))
		{
			MyLogger.log("Card name : + " + card.getName());
			MyLogger.log("TARGET FACTION " + type.targetFaction);
			MyLogger.log("CARD FACTIONS " + card.getFactions());
			MyLogger.log("Satisfy - FACTIONS");
			return false;
		}

		if (!type.containsDizzy(card.getDizzy().getDizzy()))
		{
			MyLogger.log("Satisfy - ERROR DIZZY");
			return false;
		}

		if (!type.containsToken(card.isToken()))
		{
			MyLogger.log("Satisfy - ERROR TOPKLEN");
			return false;
		}

		if (!type.containsSpecificID(card.getID()))
		{
			MyLogger.log("Satisfy - ERROR ID");
			return false;
		}

		if (!type.satisfiesExtras(card, targetTeam))
		{
			MyLogger.log("Satisfy - ERROR EXTRAS");
			return false;
		}



		if (!type.containsStatus(card))
		{
			MyLogger.log("Satisfy - ERROR STATUS");
			return false;
		}
		
		return true;
		
		/*
		if (type.containsCardType(card.getCardType())
				&& type.containsTeam(targetTeam)
				&& type.containsNames(card.getNameType()) && type.containsState(card.getState())
				&& type.containsFaction(card.getFactions())
				&& type.containsToken(card.isToken()))
		{
			return true;
		}
		return false;
		*/
	}

	/**
	 * 
	 * @param reqSize
	 * @param targetSize
	 * @param strictType
	 * @return
	 */
	public static bool satisfiesStrict(int reqSize, int targetSize, int strictType)
	{
		if (targetSize == reqSize)
		{
			return true;
		}
		else if (targetSize <= reqSize && strictType == StrictType.ALLOW_LESS)
		{
			return true;
		}
		else if (targetSize >= reqSize && strictType == StrictType.ALLOW_MORE)
		{
			return true;
		}
		return false;
	}

	public static List<Card> filterUntargettable(List<Card> targets)
	{
		List<Card> newTargets = new List<Card>();
		foreach (Card c in targets)
		{
			if (!c.hasStatusEffect(Status.SHROUD))
			{
				newTargets.Add(c);
			}
		}
		return newTargets;
	}

}