using UnityEngine;

public class StrictType
{
	public static int EXACT = 0;
	public static int ALLOW_LESS = 1;
	public static int ALLOW_MORE = 2;

	public static string strictToString(int strict)
	{
		if (strict == ALLOW_LESS)
			return "LESS";
		else if (strict == ALLOW_MORE)
			return "MORE";
		return "EXACT";
	}
	public static int typeFromString(string str)
	{
		if (str.Equals("LESS"))
		{
			return ALLOW_LESS;
		}
		else if (str.Equals("MORE"))
		{
			return ALLOW_MORE;
		}
		else if (str.Equals("EXACT"))
		{
			return EXACT;
		}
		MyLogger.log("ERROR IN CONVERT STRICT TYPE? RETURN EXACT");
		return EXACT;
	}
}
