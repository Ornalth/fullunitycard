using System.Collections.Generic;
using UnityEngine;

public class Request
{

	public interface RequestListener
	{
		void requestDone(Request r, Card origin, List<Card> targets);

		void requestCancelled(Request r);

		void requestNotEnoughTargets(Request r, Card origin,
				List<Card> targets);
	}



	bool progress = false;
	public static int MODE_NONE = 0;
	public static int MODE_DISCARD = 1;
	public static int MODE_SAC = 2;
	public static int MODE_TARGET = 3;
	//public static int MODE_LIBRARY = 4;
	public static int MODE_SEARCH_LIBRARY = 4;

	private RequestListener listener;
	//private MainBoardScript model;
	private int mode = MODE_NONE;
	private Player player = null;
	private int strict = StrictType.EXACT;
	private int num = 0;
	private TargetType targetType = null;
	private List<Card> targets = new List<Card>();
	private List<Card> selectedTargets = new List<Card>();
	private bool reqCancellable = false;
	private Card card;
	Player playerChoice = null;
	private string reason;
	/**
	 * Returns options that are possible.
	 * 
	 * @param player
	 * @param numberoftargets
	 * @param targettype
	 * @param MUST
	 *            be exact number (or may be greater)
	 * @param cancellable
	 *            .
	 */
	public void requestDiscard(Player player, Card origin, int number,
			TargetType type, int strict, bool cancel, Player playerChoice)
	{
		reset();
		this.playerChoice = playerChoice;
		progress = true;
		mode = MODE_DISCARD;
		this.player = player;
		num = number;
		targetType = type;
		targets = new List<Card>();
		List<Card> hand = player.getHand();
		targets = TargetType.filter(hand, targetType);
		selectedTargets = new List<Card>();
		this.strict = strict;
		this.reqCancellable = cancel;
		card = origin;
		
		if (checkPreemptFinish())
		{
			foreach (Card c in targets)
			{
				selectedTargets.Add(c);
			}
			MyLogger.log("Lack of targets'");
			preemptFinish();
		}
		printMessage();
	}

	/**
	 * Returns options that are possible.
	 * 
	 * @param player
	 * @param allPossibleTargets
	 * @param numberoftargets
	 * @param targettype
	 * @param MUST
	 *            be exact number (or may be greater)
	 * @param cancellable
	 *            .
	 */
	public void requestTargets(Player player, Card origin, int number,
			TargetType type, int strict, bool cancel, List<Card> allPossibleTargets)
	{
		reset();

		MyLogger.log("RPGORES TRUE");
		//this.playerChoice = playerChoice;
		progress = true;
		mode = MODE_TARGET;
		this.player = player;
		num = number;
		targetType = type;
		
		targets = allPossibleTargets;
		targets = TargetType.filterUntargettable(targets);
		selectedTargets = new List<Card>();
		this.strict = strict;
		this.reqCancellable = cancel;
		card = origin;
		if (checkPreemptFinish())
		{
			foreach (Card c in targets)
			{
				selectedTargets.Add(c);
			}
			MyLogger.log("Lack of targets'" + strict + " numtargets " + targets.Count + " numexpected " + num);
			preemptFinish();
		}
		printMessage();
	}

	/**
	 * Returns options that are possible.
	 * 
	 * @param player
	 * @param card
	 *            origin - may be null, doesnt matter. tbh
	 * @param numberoftargets
	 * @param targettype
	 * @param MUST
	 *            be exact number (or may be greater)
	 * @param cancellable
	 *            .
	 */
	public void requestSac(Player player, Card origin, int number,
			TargetType type, int strict, bool cancel)
	{
		reset();
		
		MyLogger.log("RPGORES TRUE");
		//this.playerChoice = playerChoice;
		progress = true;
		mode = MODE_SAC;
		this.player = player;
		num = number;
		targetType = type;
		targets = new List<Card>();
		List<Card> board = player.getBoardCards();
		targets = TargetType.filter(board, targetType);
		selectedTargets = new List<Card>();
		this.strict = strict;
		this.reqCancellable = cancel;
		card = origin;

		if (checkPreemptFinish())
		{
			foreach (Card c in targets)
			{
				selectedTargets.Add(c);
			}
			MyLogger.log("Lack of targets'");
			preemptFinish();
		}
		printMessage();
	}

/**
	 * 
	 * @param model
	 * @param origin
	 * @param number
	 * @param type
	 * @param strict
	 * @param cancel
	 * @param libTopX
	 * @param destinationState
	 */
	public void searchDeck(Player player, Card origin, int number,
			TargetType type, int strict, bool cancel, Player playerChoice)
	{
		reset();
		this.playerChoice = playerChoice;
		progress = true;
		mode = MODE_SEARCH_LIBRARY;
		this.player = player;
		num = number;
		targetType = type;
		targets = new List<Card>();
		List<Card> cards = player.getLibrary(number);
		targets = TargetType.filter(cards, targetType);
		selectedTargets = new List<Card>();
		this.strict = strict;
		this.reqCancellable = cancel;
		card = origin;

		if (checkPreemptFinish())
		{
			foreach (Card c in targets)
			{
				selectedTargets.Add(c);
			}
			MyLogger.log("Lack of targets'");
			preemptFinish();
		}
		printMessage();

	}
	/**
	 * returns succes/failure of add
	 * 
	 */
	public bool addTarget(int index, int playerTeam)
	{
		if (playerTeam != getControllingTeam())
		{
			MyLogger.log("Not controlling player in Request.addTarget");
			return false;
		}
		if (index < 0 || index >= targets.Count)
		{
			MyLogger.log("Outside of index Request.addTarget");
			return false;
		}
		return addTarget(targets[index]);
	}

	public int getTargetIndex(Card c)
	{
		return targets.IndexOf(c);
	}

	public void setRequestListener(RequestListener l)
	{
		this.listener = l;
	}

	public bool addTarget(Card card)
	{
		if (targets.Contains(card))
		{
			if (!selectedTargets.Contains(card))
			{
				if (strict != StrictType.ALLOW_MORE && selectedTargets.Count == num)
				{
					return false;
				}
				selectedTargets.Add(card);
				return true;
			}
			else
			{
				selectedTargets.Remove(card);
				MyLogger.log("REMOVED TARGETTED CARD!!!");
				return false;
			}
		}
		return false;
	}


	public bool removeTarget(Card c)
	{
		return selectedTargets.Remove(c);
	}

	public bool removeTarget(int index)
	{
		if (index < 0 || index >= selectedTargets.Count)
		{
			return false;
		}
		selectedTargets.RemoveAt(index);
		return true;

	}

	/**
	 * Returns success of resolve, if not . ened to continue picking targets.!
	 * 
	 */
	public bool resolve()
	{

		// Check if we satisfy strict.
		if (!TargetType.satisfiesStrict(num, selectedTargets.Count, strict))
		{
			MyLogger.log("StrictType error " + StrictType.strictToString(strict) + " numSelect " + selectedTargets.Count + " numExpected " + num);
			return false;
		}
		weakReset();
		listener.requestDone(this, card, selectedTargets);
		//reset();

		return true;
		// return ans;
	}

	//NOT ENOUGH TARGETS AVAILABLE.
	private bool checkPreemptFinish()
	{
		return targets.Count < num && (strict == StrictType.ALLOW_MORE || strict == StrictType.EXACT);
	}

	/**
	 * Returns success of resolve, if not . ened to continue picking targets.!
	 * 
	 */
	private void preemptFinish()
	{

		weakReset();
		listener.requestNotEnoughTargets(this, card, selectedTargets);
		reset();
		// return ans;
	}

	private void weakReset()
	{
//		MyLogger.log("PROGRESS = FALSE");
		//mode = MODE_NONE;
		progress = false;
	}

	private void reset()
	{
		weakReset();
		mode = MODE_NONE;
		player = null;
		this.strict = StrictType.EXACT;
		num = 0;
		targetType = null;
		targets = new List<Card>();
		selectedTargets = new List<Card>();
		reqCancellable = false;
		card = null;
		playerChoice = null;
		reason = null;
	}

	public bool cancelRequest()
	{
		if (reqCancellable)
		{
			// reset();
			//mode = MODE_NONE;
			weakReset();
			if (listener != null)
			{
				listener.requestCancelled(this);
			}
			//reset();
			return true;
		}
		return false;

	}

	public bool inProgress()
	{
		return progress;
	}

	public int getMode()
	{
		// TODO Auto-generated method stub
		return mode;
	}

	public Player getPlayer()
	{
		return player;
	}

	public Player getPlayerChoice()
	{
		// TODO Auto-generated method stub
		return playerChoice;
	}

    public int getControllingTeam()
    {
		if (playerChoice != null)
		{
			return playerChoice.getTeam();
		}        
		return player.getTeam();
    }

	public void clear()
	{
		reset();
	}

	public void printMessage()
	{
		if (mode == MODE_DISCARD)
		{
			MyLogger.log("Select cards to discard! Player: "
					+ player.getTeam());
			int index = 0;
			foreach (Card c in targets)
			{
				MyLogger.log("Choice " + index + ": " + c.getName());
				index++;
			}
		}
		else if (mode == MODE_SAC)
		{
			MyLogger.log("Select cards to sac! Player: "
					+ player.getTeam());
			int index = 0;
			foreach (Card c in targets)
			{
				MyLogger.log("Choice " + index + ": " + c.getName());
				index++;
			}
		}
		else if (mode == MODE_TARGET)
		{
			MyLogger.log("Select targets: Player: "
					+ player.getTeam());
			int index = 0;
			foreach (Card c in targets)
			{
				MyLogger.log("Choice " + index + ": " + c.getName());
				index++;
			}
		}
		else
		{
			MyLogger.log("No request active.");
		}
	}

	public void setReason(string reason)
	{
		this.reason = reason;
	}

	public string getReason()
	{
		return reason;
	}

	public List<Card> getSelectedTargets()
	{
		return selectedTargets;
	}

	public List<Card> getTargets()
	{
		return targets;
	}

	public TargetType getTargetType()
	{
		return targetType;
	}

	public string getDetailsStr()
	{
		return "WHY IS THIS REQUEST IN PLACE GUYS?";
	}

	public Card getOrigin()
	{
		return card;
	}
}

