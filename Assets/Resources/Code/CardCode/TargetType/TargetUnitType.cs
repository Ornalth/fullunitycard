public class TargetUnitType
{
	
	public static int TARGET_NONE = 0;
	public static int TARGET_UNIT = 1;
	public static int TARGET_ALL = 2;

	public static int TARGET_ANY = TARGET_UNIT | TARGET_ALL;
	
}
