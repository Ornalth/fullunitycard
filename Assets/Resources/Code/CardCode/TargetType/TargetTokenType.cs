public class TargetTokenType
{
	public static int NONE = 0;
	public static int TOKEN = 1;
	public static int NOT_TOKEN = 1<<1;
	public static int ANY = TOKEN | NOT_TOKEN;
}
