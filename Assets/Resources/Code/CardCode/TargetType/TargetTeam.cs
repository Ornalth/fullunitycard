using UnityEngine;

public class TargetTeam
{
	public static int TEAM_NONE = 0;
	public static int TEAM_ALLY = 1; // ONLY OUR TEAM
	public static int TEAM_OPP = 1<<1; // ONLY OPPO
	public static int TEAM_BOTH = TEAM_ALLY | TEAM_OPP;

	public static int TEAM_SEND = 1<<2; // ORIGIN IS THIS
	public static int TEAM_RECEIVE = 1<<3; // RECIPIENT THIS
	public static int TEAM_CURRENT = 1<<4; // CURRENT TURN
	public static int TEAM_ANY = TEAM_ALLY | TEAM_OPP | TEAM_SEND | TEAM_RECEIVE | TEAM_CURRENT | TEAM_TARGET;

	public static int TEAM_TARGET = 1<<5; //THE TARGET SATISFIES TARGET TYPE

	public static int getTargetTeamFromCards(Card origin, Card target)
	{
		if (origin == null || target == null)
		{
			return TEAM_NONE;
		}
		if (origin.getTeam() == target.getTeam())
			return TEAM_ALLY;
		return TEAM_OPP;
	}

	public static int targetTeamFromString(string s)
	{
		if (s.Equals("TEAM_NONE"))
		{
			return TEAM_NONE;
		}
		else if (s.Equals("TEAM_ALLY"))
		{
			return TEAM_ALLY;
		}
		else if (s.Equals("TEAM_OPP"))
		{
			return TEAM_OPP;
		}
		else if (s.Equals("TEAM_RECEIVE"))
		{
			return TEAM_RECEIVE;
		}
		else if (s.Equals("TEAM_SEND"))
		{
			return TEAM_SEND;
		}
		else if (s.Equals("TEAM_BOTH"))
		{
			return TEAM_BOTH;
		}
		else if (s.Equals("TEAM_CURRENT"))
		{
			return TEAM_CURRENT;
		}
		else if (s.Equals("TEAM_TARGET"))
		{
			return TEAM_TARGET;
		}
		MyLogger.log("Not legal (targetteamfromstring return TEAM_BOTH");
		return TEAM_BOTH;
	}

	public static string targetTeamToString(int targetTeam)
	{
		if (targetTeam == TEAM_BOTH)
		{
			return "TEAM_BOTH";
		}
		else if (targetTeam == TEAM_ALLY)
		{
			return "TEAM_ALLY";
		}
		else if (targetTeam == TEAM_OPP)
		{
			return "TEAM_OPP";
		}
		else if (targetTeam == TEAM_CURRENT)
		{
			return "TEAM_CURRENT";
		}
		else if (targetTeam == TEAM_TARGET)
		{
			return "TEAM_TARGET";
		}
		//TODO
		return  "TEAM_BOTH";
	}
}
