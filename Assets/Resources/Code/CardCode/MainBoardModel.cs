using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine.Networking;

public class MainBoardModel: NetworkBehaviour {
	
	private GameConfiguration config;
	private static readonly float BASE_MAIN_TIME_MS = 600000;
	private static readonly float BASE_RESERVE_TIME_MS = 6000;
	private static readonly float CARRY_OVER_TIME_PERCENT = 0.2f;

	public static readonly int SPECIAL_TARGET_RESPONSE_KEY = 99;
	Player winner = null;
	LossDetails lossDetails = null;
	public int abilityTeam;
	
	//display
	public bool abilityMode = false;
	public Card abilityCard = null;
	public List<Card> abilityActiveCards;
	//Dark screen selector.actives.
	public List<ActiveAbility> abilityActives = null;
	public bool abilitySelectingMode = false;
	List<MainBoardVC> viewControllers = new List<MainBoardVC>();
	GameEventTypeSubscription abilitySub;

	public static MainBoardModel instance; //NOTE THIS MAY BE BAD.... 
	bool gameBegan = false;
	bool[] hasMulliganed;

	List<PlayerTimer> playerTimers;

	public MainBoardModel()
	{
		stack = new GameStack(this);
		abilitySub = new GameEventTypeSubscription(this);
		playerTimers = new List<PlayerTimer>();
		hasMulliganed = new bool[2];

	}
	public static MainBoardModel getInstance()
	{
		return instance;	
	}

	public void startGame()
	{
		startGameWithDecks(new int[] { 0, 1 });
	}

	public void startGameWithDecks(int[] playerDeckNumbers, bool isDraft = false, bool isAI = false)
	{
		GameConfiguration config = new GameConfiguration();
		config.numPlayers = 2;
		config.playerNames = new List<string>(new string[] { "Player 0", "Player 1" });
		config.playerDeckNumbers = new List<int>(playerDeckNumbers);
		config.timePerTurn = BASE_MAIN_TIME_MS;
		config.baseReserveTime = BASE_RESERVE_TIME_MS;
		config.gainPercentagePerReserve = CARRY_OVER_TIME_PERCENT;
		config.isDraft = isDraft;
		config.isAI = isAI;
		startGame(config);
	}


	public void startGameWithConfig(GameConfiguration configuration)
	{
		instance = this;
		startGame(config);
	}

	private void startGame(GameConfiguration configuration)
	{
		MyLogger.log("GAME STARTU");
		if (gameBegan)
		{
			MyLogger.log("??? Game already began");
			return;
		}
		gameBegan = true;
		this.config = configuration;
		tempStack = new List<StackDetails>();
		abilityActiveCards = new List<Card>();
		phase = new Phase ();
		land = CardLibrary.getPlains();
		land.setTeam(-1);
		players = new Player[Constants.NUM_PLAYERS];
		players [0] = new Player (this);//Human(this);
		players [1] = new Player (this);//Human(this);

		PlayerTimer timer = new PlayerTimer(configuration.timePerTurn, configuration.baseReserveTime);
		PlayerTimer timer2 = new PlayerTimer(configuration.timePerTurn, configuration.baseReserveTime);
		playerTimers.Add(timer);
		playerTimers.Add(timer2);
		players[0].init("Player 0", 0, getInitialDeck(0, configuration.playerDeckNumbers[0], configuration.isDraft));
		players[1].init("Player 1", 1, getInitialDeck(1, configuration.playerDeckNumbers[1], configuration.isDraft));
		
		//TODO FIXME
		if (configuration.isAI)
		{
			MyLogger.log("ISAIGAME");
			hasMulliganed[1] = true;
		}
		notifyGameStart();
	}

	public List<PlayerTimer> getPlayerTimers()
	{
		return playerTimers;
	}
	
	private List<Card> getInitialDeck(int playerNum, int deckNumber, bool isDraft)
	{	
		List<DeckCardData> deckData = isDraft ? DraftDeckParser.getInstance().getDeck(deckNumber).getCards() 
			: DeckParser.getInstance().getDeck(deckNumber).getCards(); 
		//;
		List<Card> deck = new List<Card>();
		foreach (DeckCardData data in deckData)
		{
			for (int i = 0; i < data.amount; i++)
			{
				Card c = CardLibrary.getCardFromID(data.cardID);
				deck.Add(c);
				c.setState(CardState.DECK);
				c.setTeam(playerNum);
			}
		}
		return deck;
	}

	public Card boardSelectedCard1 = null;
	Card handSelectedCard1 = null;
	Card boardSelectedCard2 = null;
	Card handSelectedCard2 = null;
	
	public void suscribeToModel(MainBoardVC vc)
	{
		viewControllers.Add(vc);
	}

	public void timerComplete(int team)
	{
		RpcTimerComplete(team);
	}

	[ClientRpc]
	public void RpcTimerComplete(int team)
	{
		MyLogger.log("GAMEOVER TIMEOUT");
		LossDetails detail = new LossDetails();
		detail.msg = "TIME OUT";
		setLoss(getPlayer(team), detail);
		notifyGameOver();
	}

	public void requestConcede(int team)
	{
		RpcTimerComplete(team);
	}

	[ClientRpc]
	public void RpcRequestConcede(int team)
	{
		MyLogger.log("Player " + team + " conceded.");
		LossDetails detail = new LossDetails();
		detail.msg = "CONCEDE";
		setLoss(getPlayer(team), detail);
		notifyGameOver();
	}


	public bool hasFinishedMulligan()
	{
		return hasMulliganed[0] && hasMulliganed[1];
	}

	public bool hasFinishedMulligan(int team)
	{
		return hasMulliganed[team];
	}

	public void requestMulligan(int team, string serializedIntList)
	{
		RpcRequestMulligan(team, serializedIntList);
	}

	[ClientRpc]
	public void RpcRequestMulligan(int team, string serializedIntList)
	{
		MyLogger.log("GOT MULLIGAN FROM TEAM " + team);
		if (hasMulliganed[team])
		{
			return;
		}
		List<int> cardIndices = SerializationHelper.deserializeIntList(serializedIntList);
		if (cardIndices.Count > Constants.MAX_MULLIGAN_CAP)
		{
			return;
		}
		getPlayer(team).mulligan(cardIndices);
		hasMulliganed[team] = true;
		if (hasFinishedMulligan())
		{
			notifyFinishedMulligan();
		}
	}

	public void requestMulliganHand(int team)
	{
		RpcRequestMulliganHand(team);
	}

	[ClientRpc]
	public void RpcRequestMulliganHand(int team)
	{
		MyLogger.log("GOT MULLIGAN FROM TEAM " + team);
		if (hasMulliganed[team])
		{
			return;
		}
		getPlayer(team).mulliganHand();
		hasMulliganed[team] = true;
		if (hasFinishedMulligan())
		{
			notifyFinishedMulligan();
		}
	}

	public void requestNextPhase(int team, string serializedFloatList)
	{
		RpcRequestPhase(team, serializedFloatList);
	}


	
	[ClientRpc]
	private void RpcRequestPhase(int team, string serializedFloatList)
	{
		List<float> floatTimes = SerializationHelper.deserializeFloatList(serializedFloatList);
		//FIXME put it back.
		//if (team == (turn ^ phase.getStartPriority()))
			nextPhase (floatTimes);
		//else
		//	MyLogger.log("Other person's phase");
		//MyLogger.log("NEXT PHASE?");
	}
	
	public void stackResolve(int team)
	{
		//MyLogger.log("Resolve clicked");
		
		if (stack.size() == 0)
		{
			MyLogger.log("NO STACK TO RESOLVE");
			return;
		}
		
		//TODO FIXME take out ||true in real build.
		if ((priority == team || true) && !request.inProgress())
		{
			MyLogger.log("Correct priority");
			if (resPassed)
			{
				MyLogger.log("SHOULD RESOLVE NOW");
			}
			RpcStackResolve();
			
		}
		else
		{
			MyLogger.log("CANT oppo's priority or request is in progress!");
		}
		
		
		//displayNothing (5);
	}
	
	[ClientRpc]
	private void RpcStackResolve()
	{
		
		if (resPassed)
		{
			updatePriority(priority ^ 1);
			continueResolveStack(null);
			lazyUpdate ();
		}
		else
		{
			resPassed = true;
			updatePriority(priority ^ 1);
		}
	}
	
	public void playerViewClicked (int playerNum, int byPlayerTeam)
	{
		if (byPlayerTeam == priority)
		{
			RpcPlayerViewClicked(playerNum);
		}
		else
		{
			MyLogger.log("WRONG PRIORIRTY");
		}
		
	}
	
	[ClientRpc]
	private void RpcPlayerViewClicked(int playerNum)
	{
		MyLogger.log("Board receive playerviewcvliked " + playerNum);
		/*if (request.inProgress())
		{
			requestAddCard(getPlayer(playerNum));
			return;
		}*/
		
		if (phase.getPhase () == Phase.ATTACKS_ALLY) 
		{
			if (boardSelectedCard1 != null && playerNum != turn)//boardSelectedCard1.getTeam() == turn)
			{
				declareAttacks( (CombatCard)boardSelectedCard1, (CombatCard)getPlayer(playerNum));
				boardSelectedCard1 = null;
				MyLogger.log("combat pair made");
			}
			else
			{
				boardSelectedCard1 = null;
				MyLogger.log("Faield attakced?");
			}
			lazyUpdateCombat();
		}
		else if (phase.getPhase () == Phase.BLOCKS_ENEMY) 
		{
			
			boardSelectedCard1 = null;
			MyLogger.log("Can't block player.");
			
			
			lazyUpdateCombat();
		}
	}
	
	
	//Ability
	public void selectedActiveAbility(ActiveAbility abil)
	{
		if (altMode)
		{

			int index = abilityActives.IndexOf (abil);
			RpcSelectedActiveAbility(index);
		}
		else
		{
			int index = abilityActives.IndexOf (abil);
			RpcSelectedActiveAbility(index);
		}
	}
	
	[ClientRpc]
	private void RpcSelectedActiveAbility(int index)
	{
		MyLogger.log("SELECTEDACTIVEABILITY - SELECT MODE FALSE");
		abilitySelectingMode = false;


		ActiveAbility abil = abilityActives [index];
		if (abilityMode)
		{
			if (abil == null)
			{
				//Cancel -> no ability selected
				MyLogger.log("CANCELLED ABILITY MODE");
				abilityMode = false;
				notifyAbilityModeChange();
			}
			else
			{
				MyLogger.log("Activated ABility");
				activateAbility(abilityCard.getTeam(), abilityCard, abil, new Dictionary<int, List<Card>>());
				abilityMode = false;
				notifyAbilityModeChange();
			}
			
		}
		else if (altMode)
		{
			if (abil == null)
			{
				//Cancel
				MyLogger.log("CANCELLED ABILITY MODE");
				MyLogger.log("ALTMODE FALSE IN SELECTEDACTIVEABILITY");
				altMode = false;
			}
			else
			{
				if (abil.canPayWithoutExtra(altPlayer, altCard))
				{
					altMode = false;
					MyLogger.log("Activated ABility");
					activateAltAbilityAbility(altCard.getTeam(), altCard, abil, new Dictionary<int, List<Card>>());
				}
				else
				{
					MyLogger.log("CANT PAY FOR THIS ALT!!");
					abilitySelectingMode = true;
				}
				
			}

		}
		else
		{
			Debug.LogError("ABILITY DOESNT EXIST");
		}


		lazyUpdateSelectActiveAbility();
		//Maybe need log update here.
	}
	
	public void selectActiveAbilities(int team)
	{
		if (priority == team)
		{
			RpcSelectActiveAbilities(team);
		}
		else
		{
			MyLogger.log("CANT select ability in oppo's priority");
		}
	}

	public void cancelSelectActiveAbilities(int team)
	{
		MyLogger.log("TODO cancelSelectActiveAbilities in model");
	}

	
	[ClientRpc]
	private void RpcSelectActiveAbilities(int team)
	{
		//TODO
		//USE NET VERSION
		//OK Sure.........?... maybe.. lets think about it.
		
		if (request.inProgress())
		{
			MyLogger.log("YOU CANNOT ACTIVATE ABILITY DURING REQUEST");
			return;
		}
		if (abilityMode)
		{
			MyLogger.log("TURN OFF ABILITY MODE ...??");
			abilityMode = false;
			notifyAbilityModeChange ();
		}
		else //if (getPlayer (team).hasBoardCards())
		{
			List<Card> boardCards = getPlayer (team).getBoardCards();
			List<Card> graveCards = getPlayer(team).getGrave();

			List<Card> activeAbilCards = new List<Card>();
			foreach (Card c in boardCards)
			{
				if (c.getActives().Count > 0)
				{
					//MyLogger.log(c.getName());
					activeAbilCards.Add(c);
				}
			}

			foreach (Card c in graveCards)
			{
				if (c.getActives().Count > 0)
				{
					//MyLogger.log(c.getName());
					activeAbilCards.Add(c);
				}
			}
			


			if (activeAbilCards.Count == 0)
			{
				MyLogger.log("NO CARDS WITH ABILITIES");
				return;
			}


			
			
			MyLogger.log("ABILITY MODE ON");
			abilityTeam = team;
			abilityMode = true;
			abilityActiveCards = activeAbilCards;
			notifyAbilityModeChange ();
		}	
	}

	public void graveCardClicked(Card c, int player)
	{
		List<Card> cards = getPlayer (player).getGrave ();
		int index = cards.IndexOf (c);
		if (priority == player)
		{
			RpcGraveCardClicked(player, index);
		}
		else
		{
			MyLogger.log("CANT CONTROL OPP BOARD ROW, NOT YOUR PRIO");
		}
	}
	
	[ClientRpc]
	private void RpcGraveCardClicked(int index, int player)//, int clickedPlayerTeam)//byte[] serCard)
	{
		List<Card> cards = getPlayer (player).getGrave ();
		Card c = cards [index];
		if (abilityMode)
		{
			List<ActiveAbility> actives = c.getActives();
			if (actives == null || actives.Count == 0)
			{
				MyLogger.log("THERE ARE NO ACTIVE ABILITIES ON TARGET CREATURE");
				abilityMode = false;
				notifyAbilityModeChange();

			}
			else if (actives.Count == 1)
			{
				MyLogger.log("ONLY ONE ACTIVE, SELECTING IT~!");
				//Try to activate
				abilityMode = false;
				activateAbility(player, c, actives[0], new Dictionary<int, List<Card>>());
				notifyAbilityModeChange();
				
			}
			else if (actives.Count > 1)
			{
				MyLogger.log("SELECT ABILITIES");
				//SELECTION OF ABILITIES TODO
				abilityActives = actives;
				abilityCard = c;
				abilitySelectingMode = true;
				//TELL
				lazyUpdateSelectActiveAbility();
				notifyAbilityModeChange();
			}
			
			return;
		}
		else
		{
			MyLogger.log("I dun get it.");
		}
	}

//Can only be up when doing request.
	public void requestViewCardClicked(Card c, int clickedPlayerTeam)
	{
		if (request.inProgress())
		{
			List<Card> cards = request.getTargets();
			int index = cards.IndexOf (c);
			RpcRequestViewCardClicked(index, clickedPlayerTeam);
		}
		else
		{
			MyLogger.log("I dun get it.");
		}
	}
	
	[ClientRpc]
	private void RpcRequestViewCardClicked(int index, int player)//, int clickedPlayerTeam)//byte[] serCard)
	{
		MyLogger.log("Board receive lbirayr click");
		
		if (request.inProgress())
		{
			NOTnet_requestAddCard(index, player);
			return;
		}
	}

	public void boardRowClicked(int player, Card c, int clickedPlayerTeam)
	{
		List<Card> cards = getPlayer (player).getBoardCards ();
		int index = cards.IndexOf (c);
		if (priority == clickedPlayerTeam)
		{
			RpcBoardRowClicked(player, index);
		}
		else
		{
			MyLogger.log("CANT CONTROL OPP BOARD ROW, NOT YOUR PRIO");
		}
	}
	
	[ClientRpc]
	private void RpcBoardRowClicked(int player, int index)//, int clickedPlayerTeam)//byte[] serCard)
	{
		List<Card> cards = getPlayer (player).getBoardCards ();
		Card c = cards [index];
		MyLogger.log("Board receive boardrowcardclicked");
		
		/*if (request.inProgress())
		{
			requestAddCard(c);
			return;
		}*/
		
		if (abilityMode)
		{
			List<ActiveAbility> actives = c.getActives();
			if (actives == null || actives.Count == 0)
			{
				MyLogger.log("THERE ARE NO ACTIVE ABILITIES ON TARGET CREATURE");
				abilityMode = false;
				notifyAbilityModeChange();

			}
			else if (actives.Count == 1)
			{
				MyLogger.log("ONLY ONE ACTIVE, SELECTING IT~!");
				//Try to activate
				abilityMode = false;
				activateAbility(player, c, actives[0], new Dictionary<int, List<Card>>());
				notifyAbilityModeChange();
				
			}
			else if (actives.Count > 1)
			{
				MyLogger.log("SELECT ABILITIES");
				//SELECTION OF ABILITIES TODO
				abilityActives = actives;
				abilityCard = c;
				abilitySelectingMode = true;
				//TELL
				lazyUpdateSelectActiveAbility();
				notifyAbilityModeChange();
			}
			
			return;
		}
		
		
//		MyLogger.log(phase.getPhase());
		//if (target
		//TODO targetting.
		if (phase.getPhase () == Phase.ATTACKS_ALLY) 
		{
			//Successful attack.
			if (boardSelectedCard1 != null && c.getTeam() != turn)//boardSelectedCard1.getTeam() == turn)
			{
				declareAttacks( (CombatCard)boardSelectedCard1, (CombatCard)c);
				boardSelectedCard1 = null;
//				MyLogger.log("combat pair made");
			}
			else if ((boardSelectedCard1 == null && c.getTeam() == turn) || (boardSelectedCard1 != null && c.getTeam() == turn))
			{
				//Selected the same card ------> deselect
				if (c == boardSelectedCard1)
				{
					boardSelectedCard1 = null;
				}
				else ////???????
				{
					if (((CombatCard)c).canDeclareAttack())
					{
						boardSelectedCard1 = c;
					
						// If the attacker is already attacking, remove that pair.
						for (int i = 0; i < combatArray.Count; i++)
							// CombatPair pair : combatArray)
						{
							if (combatArray[i].containsAttacker((CombatCard)c))
							{
								combatArray.RemoveAt(i);
								MyLogger.log("Swapped attacker");
								break;
							}
						}
					}
					else
					{
						MyLogger.log("DUDE CANT ATTACK!!!!");
					}
					
				}
				
			}
			else
			{
				boardSelectedCard1 = null;
				MyLogger.log("Faield attakced?");
			}
			lazyUpdateCombat();
		}
		else if (phase.getPhase () == Phase.BLOCKS_ENEMY) 
		{
			if (boardSelectedCard1 != null && c.getTeam() == turn)//boardSelectedCard1.getTeam() == turn)
			{
				declareBlockers( (CombatCard)boardSelectedCard1, (CombatCard)c);
				boardSelectedCard1 = null;
				MyLogger.log("combat pair made");
			}
			else if (boardSelectedCard1 == null && c.getTeam() != turn || (boardSelectedCard1 != null && c.getTeam() != turn))
			{
				if (c == boardSelectedCard1)
				{
					boardSelectedCard1 = null;
				}
				else
				{
					
					bool contain = false;
					for (int i = 0; i < combatArray.Count; i++)
					{
						if (combatArray[i].containsAttacked((CombatCard)c))
						{
							Debug.Log
								("Blocker is already being attacked -- CANNOT BLOCK!");
							boardSelectedCard1 = null;
							contain = true;
							break;
							
						}
						else if (combatArray[i].containsBlocker((CombatCard)c))
						{
							Debug.Log
								("Blocker is already blocking, moving blocker!!");
							combatArray[i].removeBlocker();
							boardSelectedCard1 = c;
							contain = true;
							break;
							
						}
					}
					if (!contain)
						boardSelectedCard1 = c;
					
				}
				lazyUpdateCombat();
				
			}
			else
			{
				boardSelectedCard1 = null;
				MyLogger.log("Faield def?");
			}
			
			lazyUpdateCombat();
		}
		else if (phase.getPhase () == Phase.SACRIFICE)
		{
			if (((CombatCard)c).canSalvage() && c.getTeam() == turn)
			{
				getPlayerFromCard(c).salvageCard(c);
				notifyAboutAll();
			}
		}
		
	}
	
	public void handCardClicked(Card c, int player)
	{
		List<Card> cards = getPlayer (player).getHand ();
		int index = cards.IndexOf (c);
		RpcHandCardClicked(player, index);
	}
	
	[ClientRpc]
	private void RpcHandCardClicked(int player, int index)//, int clickedPlayerTeam)
	{
		List<Card> cards = getPlayer (player).getHand ();
		Card c = cards [index];
		//MyLogger.log("Board recieve handcardclick");
		//TODO
		if (request.inProgress())
		{
			MyLogger.log("FK YOU CANT PLAY CARD IN REQUEST");
			//requestAddCard(c);
			return;
		}
		else if (player != priority)
		{
			MyLogger.log("CANT PLAY CARD IN NON PRIO");
			return;
		}
		playCard (player, c);
	}
	
	public void clickedEmptyBoard()
	{
		MyLogger.log("EMPTY BOARD");
		if (phase.getPhase () == Phase.ATTACKS_ALLY) 
		{
			boardSelectedCard1 = null;
			lazyUpdateCombat();
		}
		else if (phase.getPhase () == Phase.BLOCKS_ENEMY) 
		{
			boardSelectedCard1 = null;
			lazyUpdateCombat();
		}
	}
	
	public void clickedEmptyHand()
	{
		MyLogger.log("EMPTY HAND");
		
		if (phase.getPhase () == Phase.ATTACKS_ALLY) 
		{
			boardSelectedCard1 = null;
			lazyUpdateCombat();
		}
		else if (phase.getPhase () == Phase.BLOCKS_ENEMY) 
		{
			boardSelectedCard1 = null;
			lazyUpdateCombat();
		}
	}
	
	public void requestConfirmButton()
	{
		/*if (altMode)
		{
			RpcPayAlt();
		}
		else*/ 
		if (request.inProgress())
		{
			RpcFinishRequest();
			//finishRequest ();
		}
		else
		{
			Debug.LogError("CONFIRM WITH NO ALT AND REQUEST?");
		}
		
		
	}

	public void requestCancelButton()
	{
		if (altMode)
		{
			RpcPassAlt();
		}
		else if (request.inProgress())
		{
			RpcCancelRequest();
			//request.cancelRequest ();
		}
		else
		{
			Debug.LogError("CANCEL WITH NO ALT AND REQUEST?");
		}
		
		
	}
	
	public void requestAddCard(Card c, int fromPlayer)
	{
		int index = request.getTargetIndex(c);
		if (index == -1)
		{
			MyLogger.log("THE FK IS HAPPENING");
		}
		NOTnet_requestAddCard(index, fromPlayer);
	}

	[ClientRpc]
	private void RpcRequestAddCard(int index, int fromPlayer)
	{
		NOTnet_requestAddCard(index, fromPlayer);
	}

	private void NOTnet_requestAddCard(int index, int fromPlayer)
	{
		if(request.addTarget (index, fromPlayer))
		{
			MyLogger.log("Target added/removed");
			notifyRequestUpdate();
		}
		else
		{
			MyLogger.log("INVALID TARGET/REMOVEDTARGET");
			notifyRequestUpdate();
		}
	}
	
	/*
	 * UPDATING
	 */
	private void lazyUpdate()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.lazyUpdate();
		}
	}
	
	private void lazyUpdateSelectActiveAbility()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updateAbilitySelectionMode();
		}
	}
	
	private void lazyUpdateCombat()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.lazyUpdateCombat();
		}
	}
	
	private void notifyRequestUpdate()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updateRequestMode();
		}
	}
	private void notifyAbilityModeChange()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updateAbilityMode();
		}
	}

	private void notifyFinishedMulligan()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.hasFinishedMulliganUpdate();
		}
	}


	public void notifyPriorityUpdate()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updatePriority();
		}
	}

	public void notifyTimerUpdate()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updateTimers();
		}
	}

	private void notifyGameOver()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updateGameEnded();
		}
	}

	public void postMessage(string s)
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			MyLogger.log(s);
			vc.postMessage(s);
		}
	}
/*
	private void notifyDismissAbilityMode()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.dismissAbilityMode();
		}
	}
*/

/*
	private void notifyDismissRequestData()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.dismissRequestData();
		}
	}
	*/
	private void notifyAboutAll()
	{
		MyLogger.log("NOTIFY ALL");
		lazyUpdate ();
		lazyUpdateCombat ();
		//lazyUpdateSelectActiveAbility ();
		notifyCancelRequest ();
		notifyStack ();
		//notifyAboutAll ();
	}
	
	private void notifyStack()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.updateStack();
		}
	}

	private void notifyGameStart()
	{
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.gameStartReady();
		}
	}
	
	//TODO
	private void notifyCancelRequest()
	{
		if (request.inProgress())
		{
			MyLogger.log("REQUEST IN PROGRESS NOOB ?");
			return;
		}
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.cancelRequest();
		}
	}
	
	private void notifyRequestBegin()
	{
		
		if (!request.inProgress())
		{
			MyLogger.log("NO REQUEST, CANT NOTIFY REQ BEGIN!!! TODO CLEAN UP THIS JUNK");
		
			return;
		}
		foreach (MainBoardVC vc in viewControllers)
		{
			vc.requestBegin();
		}
	}
	
	
	private int turn = 0;
	//	private Phase phase = Phase.DRAW;
	private int priority = 0;
	private bool resPassed = false;
	private int realTurn = 0;
	public Request request = new Request();
	
	private bool stackMode = false;
	public List<CombatPair> combatArray = new List<CombatPair>();
	
	
	private bool gameOver = false;
	//private Player winner = null;
	
	private Player[] players;
	private GameStack stack;
	public Phase phase;
	
	public Land land;
	
	
	
	
	public Player getPlayer(int team)
	{
		if (team >= Constants.NUM_PLAYERS || team < 0)
			return null;
		return players[team];
	}

	
	void clearBoard()
	{
		if (isGameOver())
		{
			notifyGameOver();
		}
		else
		{
			for (int i = 0; i < players.Length; i++)
				getPlayer(i).clearBoard();
		}
	}
	
	void nextPhase(List<float> timerFloatList)
	{
		if (!hasFinishedMulligan())
		{
			MyLogger.log("Hasnt finished mulls.");
			return;
		}
		if (stack.size() != 0)
		{
			MyLogger.log("need to resolve stack before advancing phase " + stack.size());
			return;
		}
		
		if (altMode)
		{
			Debug.Log
				("need to finish resolving alts before advancing phase");
			return;
		}
		
		if (request.inProgress())
		{
			MyLogger.log("need to finish request before advance phase");
			return;
		}
		
		if (phase.getPhase() == Phase.ATTACKS_ALLY)
		{
			if(!verifyAttacks())
			{
				MyLogger.log("Attacks incorrect");
				return;
			}
			else
			{
				dizzyAttackers();
			}
		}
		
		//MAYBE NEED TO STOP REQUESTS HERE.
		
		phase.nextPhase();

		resPassed = false;
		
		switch (phase.getPhase())
		{
			//TODO
		case Phase.DRAW:
			updateTimers(timerFloatList);
			realTurn++;
			//printPhase();
			turn = (turn + 1) % Constants.NUM_PLAYERS;
		
			if (!getPlayer(turn).drawCard())
			{
				//gameOver(getPlayer(turn ^ 1));
				clearBoard();
			}
			
			beginTurn();
			break;
		case Phase.SACRIFICE:
			
			//TODO SAC PHASE
			break;
		case Phase.MAIN_ALLY:
			
			//TODO SAC PHASE
			break;
		case Phase.MAIN_ENEMY:
			
			//TODO SAC PHASE
			break;
		case Phase.ATTACKS_ALLY:
			
			MyLogger.log("Can declare atks now");
			break;
		case Phase.ATTACKS_ENEMY:
			//TODO skip attack.
			if (combatArray.Count == 0)
			{
				phase.skipCombatPhase();
			}
//			MyLogger.log("ENEMY CAST SPELLS...->");
			break;
		case Phase.BLOCKS_ENEMY:
			
			MyLogger.log("declare blocks");
			//MyLogger.log("op phase");
			//priority ^= 1;
			break;
		case Phase.BLOCKS_ALLY:
			
			//MyLogger.log("declare blocks");
			//MyLogger.log("op phase");
			//priority ^= 1;
			break;
			
		case Phase.MAIN_2ND_ALLY:
			
			MyLogger.log("Resolve atks");
			resolveAttacks();
			break;
		case Phase.END_ALLY:
			break;
		case Phase.END_ENEMY:
			break;
		case Phase.CLEANUP:
			eotCleanup();
			break;
		default:
			break;
			
		}
		
		updatePriority(turn ^ phase.getStartPriority());
		broadcastPhaseChange();

		
		//MyLogger.log("Current phase: " + phase.toString());
		//VIEW UPDATE
		
		lazyUpdate ();
		postMessage("Next Phase: " + phase.toString());
	}

	private void updateTimers(List<float> timerFloatList)
	{
		for (int i = 0; i < timerFloatList.Count; i++)
		{
			MyLogger.log(timerFloatList[i]);
		}
		PlayerTimer nextTurnTimer = playerTimers[turn ^ 1];
		float baseTimeLeft = timerFloatList[(turn ^ 1) * 2];
		float reserveTime = timerFloatList[(turn ^ 1) * 2 + 1];
		nextTurnTimer.setTime(config.timePerTurn);
		nextTurnTimer.setReserveTime(reserveTime + baseTimeLeft * config.gainPercentagePerReserve);
		notifyTimerUpdate();
	}
	
	private void updatePriority(int value)
	{
		priority = value;
		notifyPriorityUpdate();
	}

	private void broadcastPhaseChange()
	{
		//TODO FIXME IF ERROR
		abilitySub.respondToPhase(phase.getPhase(), turn);
		//checkLandReponseToPhaseChange(phase.getPhase(), turn);
		//getPlayer(turn).receivePhaseChange(phase.getPhase(), PhaseTeam.TEAM_ALLY);
		//getPlayer(turn ^ 1).receivePhaseChange(phase.getPhase(), PhaseTeam.TEAM_OPP);
	}
	
	private bool verifyAttacks()
	{
		
		List<Card> cs = getPlayer(turn).getCreatures();
		foreach (Card c in cs)
		{
			Creature card = (Creature)c;
			if (!card.isDizzy() && card.hasStatusEffect(Status.MUST_ATK) )
			{
				bool atk = false;
				foreach (CombatPair pair in combatArray)
				{
					if (pair.containsAttacker(card))
					{
						atk = true;
						break;
					}
				}
				if (!atk)
				{
					return false;
				}
			}
		}
		
		foreach (CombatPair pair in combatArray)
		{
			Card atker = pair.attacker;
			Card defender = pair.attacked;
			eventOccurred(GameEventType.DECLARE_ATTACK, atker, getPlayer(atker.getTeam()), Constants.IDK, defender);
		}
		return true;
		
	}
	
	public void eventOccurred(int ev, Card fromCard,
	                          Player fromPlayer)
	{

		eventOccurred(ev, fromCard, fromPlayer, 0, new List<Card>());
	}
	
	public void eventOccurred(int ev, Card fromCard,
	                          Player fromPlayer, int extras, Card recipient)
	{
		List<Card> cards = new List<Card>();
		cards.Add(recipient);
		eventOccurred(ev, fromCard, fromPlayer, extras, cards);
	}
	
	
	public void eventOccurred(int ev, Card origin,
	                          Player fromPlayer, int extras, List<Card> recipients)
	
	{
		ExecutedGameEvent eev = new ExecutedGameEvent();
		eev.gameEventType = ev;
		eev.origin = origin;
		eev.fromPlayer = fromPlayer;
		eev.recipients = recipients;
		eev.value = extras;

		eventOccurred(eev);
	}

	public int makeCardTakeDamage(Card source, Card target, int value, int[] overflow)
	{
		int dmgTaken = ((CombatCard)target).takeDamage(value, source, null);
		eventOccurred(GameEventType.DAMAGE, source,
								getPlayerFromCard(source), dmgTaken,
								target);
		return dmgTaken;
	}

	public void cardStateChanged(Card card)
	{
		abilitySub.cardStateChanged(card);
	}


	public void eventOccurred(ExecutedGameEvent theEvent)
	{
		//Sub before checking...
		if (theEvent.gameEventType == GameEventType.ENTER)
		{
			abilitySub.cardStateChanged(theEvent.origin);
			//abilitySub.sub(theEvent.origin);
		}
		
		switch (theEvent.gameEventType)
		{
			
		case GameEventType.COMBAT_DAMAGE:
			if (theEvent.origin.hasStatusEffect(Status.LIFELINK))
			{
				theEvent.fromPlayer.addHealth(theEvent.value, theEvent.origin);
			}
			break;
		case GameEventType.DAMAGE:
			break;
		case GameEventType.DEATH:
			break;
		case GameEventType.ATTACK:
			break;
		case GameEventType.RETAL:
			break;
		case GameEventType.ENTER:
		{
			if (theEvent.origin is Land)
			{
				if (getPlayerFromCard(land) != null)
				{
					eventOccurred(GameEventType.LEAVE, land,
					              getPlayerFromCard(land));
				}
				MyLogger.log("NEW LAND IS " + theEvent.origin.getName());
				land = (Land) theEvent.origin;
			}
			
			if (theEvent.origin.hasStatusEffect(Status.HASTE))
			{
				theEvent.origin.undizzy();
			}
			break;
		}
		case GameEventType.ENTER_GRAVE:
			break;
		case GameEventType.EXILE:
			break;
		case GameEventType.LEAVE:
			break;
		default:
			break;
			
		}

		//checkLandResponseToEvent(theEvent);

		//Dependency HERE!!! You must respond before the player responds, because the player will may move the card as as result.
		abilitySub.respondToEvent(theEvent);

				//TODO FIX ME IF BAD
		getPlayer(turn).respondToEvent(theEvent);
		getPlayer(turn ^ 1).respondToEvent(theEvent);
//Unsub after checking
		if (theEvent.gameEventType == GameEventType.LEAVE || 
			theEvent.gameEventType == GameEventType.ENTER_GRAVE ||
			theEvent.gameEventType == GameEventType.EXILE )
		{
			abilitySub.cardStateChanged(theEvent.origin);
		}
		clearBoard();
	}

	private void checkLandReponseToPhaseChange(int phase, int turn)
	{
		int phaseTeam;
		int team = land.getTeam();
		if (team == turn)
		{
			phaseTeam = PhaseTeam.TEAM_ALLY;
		}
		else
		{
			phaseTeam = PhaseTeam.TEAM_OPP;
		}

		foreach (PassiveAbility i in land.getPassives())
		{
			if (i.respondsToPhase(phase, phaseTeam))
			{
				addAbilitiesToStack(i, land, new List<Card>(), i.getAbilityResponseTargetGen());
			}
		}

		foreach (AltAbility i in land.getAlts())
		{
			if (i.respondsToPhase(phase, phaseTeam))
			{
				addAbilitiesToStack(i, land, new List<Card>(), i.getAbilityResponseTargetGen());
			}
		}
	}

	public void printDeck(int i)
	{
		getPlayer(i).printDeck();
	}
	
	void printHand(int i)
	{
		getPlayer(i).printHand();
	}
	
	private void playCard(int player, Card cardToPlay)
	{	
		playCard(player, cardToPlay, new List<List<Card>>(), new Dictionary<int, List<Card>>());
	}
	
	protected void playCard(int player, Card cardToPlay,
	                        List<List<Card>> extraCostTargets, Dictionary<int, List<Card>> targets)
	{
		
		if (request.inProgress())
		{
			MyLogger.log("need to finish request before advance phase");
			request.printMessage();
			return;
		}

		if (abilityMode)
		{
			MyLogger.log("CANT PLAY CARD IN ABILITY MODE");
			return;
		}
		/*
		if (phase.getPhase() == Phase.SACRIFICE)
		{
			MyLogger.log("YOU MAY NOT PLAY A CARD IN THE SACRIFICE PHASE");
			return;
		}*/

		if (cardToPlay == null)
		{
			MyLogger.log("MODEL:Play null card?");
			return;
		}
		bool success = false;
		

		
		// Can play spell if stack is not empty OR is own turn
		//TODO

		if (player == priority && 
			cardToPlay.getPlayablePhase().canPlayInThisPhase(phase.getPhase(), 
				TargetTeam.getTargetTeamFromCards(cardToPlay, getPlayer(turn)),
				stack.size()))
		{
			success = true;
		}
		else
		{
			success = false;
		}
		/*
		if (cardToPlay is Spell) // spell
		{
			
			if (stack.size() != 0)
			{
				// getPlayer(player].playCard(index);
				
				success = true;
			}
			else if (player == priority)
			{
				success = true;
			}
			
			
		}
		else
			// Creature/structure/LAND
		{
			if (stack.size() == 0 && player == turn && phase.allowCreature())
			{
				//getPlayer(player).playCard(index);
				success = true;
			}
			else
			{
				Debug.Log
					("stack not empty, or wrong turn or doesnt allow");
			}
		}
		*/
		if (success)
		{
			int result = getPlayer(player).playCard(cardToPlay,
			                                        extraCostTargets, targets);

			// will have played the card if success
			if (CardPlayResult.isFail(result))
			{
				// Swaps to target mode
				Debug.Log
					("Inside model, could play card (maybe requires targets?).");
				//TODO anonymous classes.
				
				
				if (CardPlayResult.failCost(result))
				{
					MyLogger.log("FAIL COST, CANT PAY");
				}
				else if (CardPlayResult.requiresExtraTargets(result))
				{
					MyLogger.log("Is targetting now for extras");

					List<TargetListObject> targetList = cardToPlay.getExtraCostTargetTypes();
					int targetListSize = extraCostTargets.Count;
					TargetListObject targetObj = targetList[targetListSize];
					TargetType extraCostTargetType = targetObj.targetType;
					request.setRequestListener(new ReqPlayCardListener(this,player,cardToPlay,extraCostTargets, targets));
					request.setReason(targetObj.desc);
					List<Card> allPossibleTargets = getTargetsForCard(cardToPlay,
			                                                  extraCostTargetType);
					request.requestTargets(getPlayer(player), cardToPlay,
					                   extraCostTargetType.numTargets,
					                   extraCostTargetType,
					                   extraCostTargetType.strict, true, allPossibleTargets);
					MyLogger.log("BEGIN EXTRA COST REQUEST");
					notifyRequestBegin();
					return;
				}
				else if (CardPlayResult.onlyRequiresTarget(result))
				{
					MyLogger.log("Is targetting now");
					beginTargettingForSpell(player, cardToPlay, extraCostTargets, targets);
					return;
				}
				else
				{
					MyLogger.log("UNKNOWN ETRROR?");
				}
				MyLogger.log(CardPlayResult.resultToString(result));
				
				//targetMode = false;
			}
			else
			{
				/*List<AltAbility> abilities = cardToPlay.getAlts();
				foreach (AltAbility ac in abilities)
				{
					foreach (ActiveAbility aV in ac.getAlts())
					MyLogger.log(aV.getDesc());
				}*/
				postMessage("Player " + player + " played " + cardToPlay.getName());
				clearBoard();
				//printBoard(player);
			}
			//VIEW UPDATE
			
			lazyUpdate ();
		}
		else
		{
			MyLogger.log("Not legal card for now");
		}
	}
	
	//TODO ANON CLASSES.
	public void beginTargettingForSpell(int player, Card c, List<List<Card>> extraCostTargets, Dictionary<int, List<Card>> targetDict)
	{
		PassiveAbility ability = c.getPassives()[0];

		TargetListObject targetObj = fillTargetsForAbility(c, ability, targetDict);
		if (targetObj != null)
		{
			TargetType requestTargetType = targetObj.targetType;
			List<Card> allPossibleTargets = getTargetsForCard(c,
			                                                  requestTargetType);
			request.setRequestListener(new ReqSpeListener(this,player, extraCostTargets, targetDict, getTargetDictionarySlot(targetDict)));
			request.requestTargets(getPlayer(player), c,
			                       requestTargetType.numTargets, requestTargetType,
			                       requestTargetType.strict, true, allPossibleTargets);
			request.setReason(targetObj.desc);
			MyLogger.log("REQUEST STATE " + request.inProgress());
			notifyRequestBegin();
		}
		else
		{
			MyLogger.log("ERROR BEGIN TARGETTING FO SPELLS BUT NO TARGET OBJ REQ'D!");
		}

		
	}
	
	public void beginTargettingForAbility(int player, Card c,
	                                      PassiveAbility ab, Dictionary<int, List<Card>> targetsSoFar, TargetListObject tlo)
	{

		TargetType abilityTType = tlo.targetType;

		MyLogger.log("TARGET TYPE OF BEGIN TARGETTIGN OR ABILITY " + abilityTType);
		int targetId = tlo.id;
		List<Card> allPossibleTargets = getTargetsForCard(c, abilityTType);
		
		request.setRequestListener(new ReqAbiListener(this, player, c, ab, targetsSoFar, targetId));                                         
		request.requestTargets(getPlayer(player), c,
		                       abilityTType.numTargets, abilityTType,
		                       abilityTType.strict, true, allPossibleTargets);
		request.setReason(tlo.desc);
		
		if (!request.inProgress())
		{
			/*
			foreach (MainBoardVC vc in viewControllers)
			{
				vc.stopAbilityMode();
			}
			*/
			notifyAbilityModeChange();
		}
		else
		{
			notifyRequestBegin();
		}
		
	}
	
	public void printBoard(int player)
	{
		getPlayer(player).printBoard();
	}
	
	void printPhase()
	{
		MyLogger.log("CURRENTPHASE IS : " + phase.getPhase());
		//MyLogger.log(phase.toString());
	}
	
	public int currentPlayer()
	{
		return turn;
	}

	public void toggleCounterStack(int prio)
	{
		MyLogger.log("Counter target stack, anything slower than" + prio);
		stack.toggleCounterStack(prio);
	}

	private void continueResolveStack(List<Card> lastActionCards)
	{
		
		stackMode = true;
		MyLogger.log("MODEL : Resolving Stack");
		while (true)
		{
			StackDetails det = stack.dequeue();
			if (det == null)
			{
				break;
			}
			currentExecutingStackDetail = det;

			MyLogger.log("Dequeueing " + det.ev.getDesc());
			if (det.result == null)
			{
				refreshTargetsForAbility(det.origin, (PassiveAbility)(det.ev), det.targetDict);
			}
			det.lastActionCards = lastActionCards;
			
			
			EventResult result = det.executeEvent(this);

			if (!result.done)
			{
				MyLogger.log("REEXECUTING");
				stack.reInsert(det);
				return;
			}
			currentExecutingStackDetail = null;
		}
		
		//stack.Clear();
		clearBoard();
		updatePriority(turn ^ phase.getStartPriority());
		stackMode = false;
		
		notifyStack ();
		lazyUpdateCombat();
		
		
		// printHand(1);
	}

	/*
	Hack to get this value to display in zoom card, maybe fix later? zzz. FIXME
	
	Now counter script is using it... gg 
	*/
	StackDetails currentExecutingStackDetail = null;
	public StackDetails getCurrentExecutingStackDetail()
	{
		return currentExecutingStackDetail;
	}

	public void executeInstantEvent(StackDetails det)
	{
		if (!det.executeEvent(this).done)
		{
			MyLogger.log("WHAT THE FK IS GOIN ON executeInstantEvent?");
		}
		lazyUpdate();
	}
	
	private void declareAttacks(CombatCard attacker, CombatCard atked)
	{
		if (attacker != null && attacker.isDizzy())
		{
			MyLogger.log("Atking creature is dizzy");
			return;
		}
		
		
		if (attacker.hasStatusEffect(Status.CANT_ATK))
		{
			MyLogger.log("Atking creature cant atk (status)");
			return;
		}
		
		
		
		if (attacker == null || atked == null)
		{
			MyLogger.log("atk/def is null? is going on.????");
		}
		else if (atked.hasStatusEffect(Status.HIDDEN))
		{
			MyLogger.log("Cannot attack hidden target");
		}
		
		else
		{
			
			// If the attacker is already attacking, remove that pair.
			for (int i = 0; i < combatArray.Count; i++)
				// CombatPair pair : combatArray)
			{
				if (combatArray[i].containsAttacker(attacker))
				{
					combatArray.RemoveAt(i);
					MyLogger.log("Swapped attacker");
					break;
				}
			}
			
			// If the attacked is already attacked, remove that pair.
			if (!(atked is Player))
			{
				for (int i = 0; i < combatArray.Count; i++)
					// CombatPair pair : combatArray)
				{
					if (combatArray[i].containsAttacked(atked))
					{
						combatArray.RemoveAt(i);
						MyLogger.log("Swapped defender");
						break;
					}
				}
			}
			
			CombatPair pair = new CombatPair();
			pair.setCombatPair(attacker, atked);
			combatArray.Add(pair);
			MyLogger.log("Success atk added");
			
		}
	}

	private void dizzyAttackers()
	{
		foreach (CombatPair pair in combatArray)
		{
			Card atker = pair.attacker;
			if (!atker.hasStatusEffect(Status.FOCUS))
			{
				atker.moreDizzy();
			}
		}
						
	}
	
	public void declareBlockers(CombatCard blocker, CombatCard attacker)
	{
		
		if (blocker.isDizzy())
		{
			MyLogger.log("Blocking creature is dizzy");
			return;
		}
		
		if (blocker.hasStatusEffect(Status.CANT_BLK))
		{
			MyLogger.log("BLOCKING CREATURE CANNOT BLOCK");
			return;
		}
		
		if (attacker.hasStatusEffect(Status.DUELIST))
		{
			MyLogger.log("CANNOT BLOCK CREATURE WITH DUELIST");
			return;
		}
		
		if (attacker.hasStatusEffect(Status.INVIS_ATK)
		    && !blocker.hasStatusEffect(Status.INVIS_ATK))
		{
			Debug.Log
				("blocker needs invis atk to block atker with invis atk");
			return;
		}
		
		if (attacker == null || blocker == null)
		{
			MyLogger.log("atk/def is null? zzz is going on.");
		}
		else
		{
			int attackerIndex = -1;
			int blockerIndex = -1;

			bool error = false;
			for (int i = 0; i < combatArray.Count; i++)
				// CombatPair pair : combatArray)
			{
				if (combatArray[i].containsBlocker(blocker))
				{
					Debug.Log
						("Blocker is already blocking, moving blocker!!");
					blockerIndex = i;
				}
				else if (combatArray[i].containsAttacked(blocker))
				{
					Debug.Log
						("Blocker is already being attacked -- CANNOT BLOCK!");
					
					error = true;
					//return;
				}
				if (combatArray[i].containsAttacker(attacker))
				{
					attackerIndex = i;
				}
			}
			if (attackerIndex == -1 )
			{
				Debug.Log
					("Invalid trying to block creature not attacking  at all");
				return;
			}


			if (blockerIndex != -1)
			{
				combatArray[blockerIndex].removeBlocker();
			}

			if (error)
			{
				return;
			}
			
			CombatPair pair = combatArray[attackerIndex];
			if (pair.attacked.getCardType() == CardType.PLAYER)//|| blocker.hasStatusEffect(Status.INTERCEPT))
			{
				if (blockerIndex != attackerIndex)
				{
					pair.setBlocker(blocker);
					MyLogger.log("Successful block set.");
				}
				else
				{
					MyLogger.log("Successful block remove");
				}
				
			}
			else
			{
				Debug.Log
					("Invalid trying to block creature not attacking player");
			}
		}
	}
	
	//TODO MAYBE CHANGE RESOLVE ATTAKCS TO NOT FACTOR IN DIZZY!!!!!
	void resolveAttacks()
	{
		foreach (CombatPair pair in combatArray)
		{
			CombatCard attacker = pair.attacker;
			CombatCard blocker = pair.blocker;
			CombatCard attacked = pair.attacked;
			if (!attacker.isDead() && (attacker is Player || attacker.getState() == CardState.BOARD))
			{
				//MyLogger.log("ATTACKER OK");
				// Got blocked >> overflow
				if (blocker != null)
				{
					if (!blocker.isDead()  && (blocker is Player || blocker.getState() == CardState.BOARD)) //BLOCKER EXECUTE BLOCK!
					{
						//MyLogger.log("BLOCKER OK");
						int[] overflow = new int[1];
						int dmg = attacker.attackCard(blocker, overflow);
						
						eventOccurred(GameEventType.ATTACK, attacker, getPlayer(attacker.getTeam()), Constants.IDK, blocker);
						
						//NO RETAL, N OATK BACK
						int dmg2 = 0;
						
						if (!attacker.hasStatusEffect(Status.NO_RETAL))
						{
							dmg2 = attacked.attackCard(blocker, null);
							eventOccurred(GameEventType.RETAL, blocker, getPlayer(blocker.getTeam()), Constants.IDK, attacker);
						}
						
						
						if (dmg > 0)
						{
							eventOccurred(GameEventType.COMBAT_DAMAGE, attacker,
							              getPlayer(attacker.getTeam()), dmg
							              - overflow[0], blocker);
						}
						
						if (dmg2 > 0)
						{
							eventOccurred(GameEventType.COMBAT_DAMAGE, blocker,
							              getPlayer(blocker.getTeam()), dmg2, attacker);
						}
						
						if (overflow[0] > 0
						    && attacked.getCardType() == CardType.PLAYER)
						{
							//MyLogger.log("OVERFLOW DAMAGE = 1/2");
							
							if (attacker.hasStatusEffect(Status.PIERCE))
							{
								attacked.takeDamage(overflow[0], attacker, null);
							}
							else if (blocker.isToken()) // Maybe more overflow?
							{
								attacked.takeDamage(overflow[0] * 3 / 4, attacker,
								                    null);
							}
							else
							{
								attacked.takeDamage(overflow[0] / 2, attacker, null);
							}
							
						}
					}
					else //BLOCKER IS GONE --- ONLY DO OVERFLOW
					{
						//MyLogger.log("DAMAGE fIRST TARGET");
						int[] overflow = new int[1];
						overflow[0] = attacker.getTotalAttack();
						
						
						if (overflow[0] > 0
						    && attacked.getCardType() == CardType.PLAYER)
						{
							//MyLogger.log("OVERFLOW DAMAGE = 1/2");
							
							if (attacker.hasStatusEffect(Status.PIERCE))
							{
								attacked.takeDamage(overflow[0], attacker, null);
							}
							else if (blocker.isToken()) // Maybe more overflow?
							{
								attacked.takeDamage(overflow[0] * 3 / 4, attacker,
								                    null);
							}
							else
							{
								attacked.takeDamage(overflow[0] / 2, attacker, null);
							}
							
						}
					}
				}
				else
					// NO BLOCKS IN THIS PAIR -> direct attack to
					// creature/player/structe etc.
				{
					//MyLogger.log("DAMAGE fIRST TARGET");
					//DAMAGE THE GUY
					if (!attacked.isDead() && (attacked is Player || attacked.getState() == CardState.BOARD))
					{
						int dmg = attacker.attackCard(attacked, null);
						
						eventOccurred(GameEventType.ATTACK, attacker,
						              getPlayer(attacker.getTeam()), Constants.IDK, attacked);
						int dmg2 = 0;
						
						if (!attacker.hasStatusEffect(Status.NO_RETAL))
						{
							dmg2 = attacked.attackCard(attacker, null);
							eventOccurred(GameEventType.RETAL, attacked,
							              getPlayer(attacked.getTeam()), Constants.IDK, attacker);
							
						}
						
						if (dmg > 0)
						{
							eventOccurred(GameEventType.COMBAT_DAMAGE, attacker,
							              getPlayer(attacker.getTeam()), dmg, attacked);
						}
						
						if (dmg2 > 0)
						{
							eventOccurred(GameEventType.COMBAT_DAMAGE, attacked,
							              getPlayer(attacked.getTeam()), dmg2, attacker);
						}
					}
					else //YOU ATTACK NOTHING.
					{
						//MyLogger.log("DAMAGE NOTHIGN?");
					}
				}
			}
		}
		//TODO THINK ABOUT REOSLVING STACK HERE ASAP.
		//continueResolveStack();
		combatArray.Clear();
		clearBoard();
		lazyUpdate ();
		lazyUpdateCombat ();
	}
	

	public void setLoss(Player p, LossDetails detailsObj)
	{
		if (!gameOver)
		{
			gameOver = true;
			winner = getOtherPlayer(p);
			lossDetails = detailsObj;
			MyLogger.log("GAME IS OVER");
		}
	}
/*
	[ClientRpc]
	public void RpcSetLoss(Player p, LossDetails detailsObj)
	{
		if (!gameOver)
		{
			gameOver = true;
			winner = getOtherPlayer(p);
			lossDetails = detailsObj;
			MyLogger.log("GAME IS OVER");
		}
	}
*/
	public bool isGameOver()
	{
		return gameOver;
	}
	
	public int getPriority()
	{
		return priority;
	}
	
	void eotCleanup()
	{
		for (int i = 0; i < players.Length; i++)
		{
			getPlayer(i).eotCleanup(getCurrentTurn());
		}
		clearBoard();
	}
	
	void beginTurn()
	{
		getPlayer(turn).generate(getRealTurn());
		getPlayer(turn).undizzyAll();
		getPlayer(turn).startTurnStatuses();
	}
	
	public int getRealTurn()
	{
		return realTurn / Constants.NUM_PLAYERS + 1;
	}
	
	//NULL ==== COMPLETE!
	//RETURNED TLO MEANS NEEDS REQUEST TLO, FILL IT IN VIA TARGET PLZ.
	public TargetListObject fillTargetsForAbility(Card origin, PassiveAbility ability, Dictionary<int, List<Card>> targetDict)
	{
		List<TargetListObject> targetList = ability.getTargetListObjects();

		foreach (TargetListObject targetObj in targetList)
		{
			if (!targetDict.ContainsKey(targetObj.id))
			{
				if (targetObj.targetGen == TargetGeneration.NEEDS_REQUEST)
				{	
					//MyLogger.log("THIS OBJ IS NEEDS REQUEST AND NOT IN DICT");
					return targetObj;
				}
				else if (targetObj.targetGen == TargetGeneration.MODEL_GEN)
				{
					List<Card> newTargets = getTargetsForCard(origin, targetObj.targetType);
					targetDict[targetObj.id] = newTargets;
					if (targetObj.removesSource())
					{
						newTargets.Remove(origin);
						//MyLogger.log("REMOVED ORIGIN");
					}
				}
				else
				{
					targetDict[targetObj.id] =  new List<Card>();
					//MyLogger.log("DAFUQ IS THIS EXTRA HAS NO CHANGE, THAT MAKES NO SENSE?");
				}
			}
		}
		return null;
	}

	public void refreshTargetsForAbility(Card origin, PassiveAbility ability, Dictionary<int, List<Card>> targetDict)
	{
		List<TargetListObject> targetList = ability.getTargetListObjects();

		foreach (TargetListObject targetObj in targetList)
		{
			if (!targetDict.ContainsKey(targetObj.id))
			{
				if (targetObj.targetGen == TargetGeneration.NEEDS_REQUEST)
				{	
				}
				else if (targetObj.targetGen == TargetGeneration.MODEL_GEN)
				{
					List<Card> newTargets = getTargetsForCard(origin, targetObj.targetType);
					targetDict[targetObj.id] = newTargets;
				}
				else
				{
				}
			}
		}
	}

	public void activateAltAbilityAbility(int player, Card origin, ActiveAbility ability, Dictionary<int, List<Card>> targetDict)
	{
		if (ability == null)
		{
			MyLogger.log("NO ABILITY EXISTS");
			return;
		}
		if (origin.isSurpressed())
		{
			
			MyLogger.log("SURPRESSED");
			return;
		}

		TargetListObject targetObj = fillTargetsForAbility(origin, ability, targetDict);
		if (targetObj != null)
		{
			beginTargettingForAbility(player, origin, ability, targetDict, targetObj);
			return;
		}
			//TODO FIX EXTRA ME PROBABLY SOMETHING WRONG BECAUSE WE DONT RUN THESE IF WE GO THROUGH REQUEST!!!!
			
		MyLogger.log("ACTIVATING ABILITY?");

		altCard = null;
		altAbility = null;
		altPlayer = null;
		MyLogger.log("ALTMODE FALSE IN ACTIVATEALTABILITY");
		altMode = false;
	
		activateAbilitySuccess(getPlayer(player), origin, ability,
		                                  targetDict);
		notifyAboutAll();
	}
	

	public void activateAbility(int player, Card origin, PassiveAbility abil, Dictionary<int, List<Card>> targetDict)
	{
		if (abil == null)
		{
			MyLogger.log("NO ABILITY EXISTS");
			return;
		}
		if (origin.isSurpressed())
		{
			
			MyLogger.log("SURPRESSED");
			return;
		}

		if (!(abil is ActiveAbility))
		{
			MyLogger.log("ACTIVATING NON ACTIVE ABILITY??... BUG HERE");
			notifyAboutAll();
			return;
		}



		ActiveAbility ability = (ActiveAbility) abil;
		TargetListObject targetObj = fillTargetsForAbility(origin, ability, targetDict);
		if (targetObj != null)
		{
			beginTargettingForAbility(player, origin, ability, targetDict, targetObj);
			//notifyRequestBegin();
			return;
		}
		else
		{
			activateAbilitySuccess(getPlayer(player), origin, ability, targetDict);
			//printBoard(0);
			notifyAboutAll ();
		}
	}

	private bool activateAbilitySuccess(Player player, Card origin, PassiveAbility ability, Dictionary<int, List<Card>> targetDict)
	{

		//We are activeability 
		if (ability is ActiveAbility && ((ActiveAbility)ability).canPay(player, origin, targetDict))
		{
			((ActiveAbility)ability).pay(player,origin,targetDict);
			addAbilitiesToStack(ability, origin, targetDict);
			return true;
		}
		else if (!(ability is ActiveAbility))
		{
			MyLogger.log("?????? THIS IS NOT AN ACTIVE ABILITY????? INSIDE ACTIVATEABILITYSUCCESS()");
			addAbilitiesToStack(ability, origin, targetDict);
			return true;
		}
		else
		{
			MyLogger.log("MAN HAPPENING CANT PAY ACTIVATE ABILITY!!!!");
			return false;
		}

	}
	
	
	public List<Card> getTargetsForCard(Card c, TargetType type)
	{
		if (type == null)
			return null;
		List<Card> targets = new List<Card>();
		
		//MyLogger.log("team name is " + c.getTeam());
		
		if (type.containsTeam(TargetTeam.TEAM_ALLY))
		{
			//MyLogger.log("CHECJ ALLY");
			targets.AddRange(getPlayer(c.getTeam()).getTargetsFor(c, type));
		}
		if (type.containsTeam(TargetTeam.TEAM_OPP))
		{
			//MyLogger.log("CHECJ OP");
			targets.AddRange(getPlayer(otherTeam(c.getTeam())).getTargetsFor(c,
			                                                                 type));
		}
		
		List<Card> remove = new List<Card>();
		foreach (Card card in targets)
		{
			/*if (!type.containsNames(c.getNameType()))
			{
				remove.Add(card);
			}*/
			if (!TargetType.satisfies(card, type, TargetTeam.getTargetTeamFromCards(c,card)))
			{
				remove.Add(card);
			}
		}
		targets = targets.Except(remove).ToList();;
		return targets;
		
	}
	
	public int otherTeam(int team)
	{
		return team ^ 1;
	}
	
	bool addingAbilities = false;
	int curAbility;
	List<StackDetails> tempStack = new List<StackDetails>();
	bool canResolveAlts = false;

	//....... OLD LEGACY
	public void addAbilitiesToStack(Ability i, Card origin, List<Card> targets, int targetGen)
	{

		MyLogger.log("OLD ADDING ABILITY OF : " + origin.getName() + " " + i.getDesc()  );
		StackDetails det = new StackDetails();
		det.ev = i;
		det.origin = origin;
		Dictionary<int, List<Card>> targetDict = new Dictionary<int, List<Card>>();
		targetDict[SPECIAL_TARGET_RESPONSE_KEY] = targets;
		det.targetDict = targetDict;
		
		printCardList(targets);
		

		bool shouldPass = false;
		if (TargetGeneration.isModelGeneratesTargets(targetGen))
		{
			MyLogger.log("model created and selected all targets for ability");
			List<Card> newTargets = getTargetsForCard(origin,
			                                          ((PassiveAbility)i).getEventTargetType()); //TODO FIXME
			//det.targets = newTargets;
			targetDict[SPECIAL_TARGET_RESPONSE_KEY] = newTargets;
			printCardList(newTargets);
			MyLogger.log("successfully added ability ot stack.");
			//stack.addAbilityToStack(det);
			/*if (det.extraDetailTargetType != null)
			{
				List<Card> extraDetailTargets = getTargetsForCard(
					det.origin, det.extraDetailTargetType);
				det.extraDetailTargets = extraDetailTargets;
			}*/
			shouldPass = true;
		}
		else if (TargetGeneration.isRequiresRequestForTargets(targetGen))
		{
			MyLogger.log("need to select targets for ability.");
			canResolveAlts = true;
			tempStack.Add(det);
			
			//resolveAlts();
		}
		else if (targetGen == TargetGeneration.NO_CHANGE)
		{
			MyLogger.log("successfully added ability ot stack.");
			//stack.addAbilityToStack(det);
			shouldPass = true;
		}

		if (det.ev is AltAbility)
		{

			MyLogger.log("ERROR GUYS::::::::need to select decision for ability.");
			canResolveAlts = true;
			tempStack.Add(det);
			shouldPass = false;
			//resolveAlts();
		}

		if (shouldPass)
		{
			addAbilitiesToStack(i, origin, targetDict);
		}
		else
		{
			//Reset Priority requirements.
			if (det.ev.requiresStack())
			{
				resPassed = false;
			}


			if (tempStack.Count > 0)
			{
				resolveAlts();
			}
			

			notifyStack();
		}
		
	}
	private bool verifyAbilityPreEffects(StackDetails det)
	{
		return ((PassiveAbility)det.ev).canPayStackCost(det.origin, det.targetDict, this);
	}

	private void payAbilityPreEffects(StackDetails det)
	{
		((PassiveAbility)det.ev).act(det.origin, det.targetDict, this);
	}

	public void addAbilitiesToStack(Ability i, Card origin, Dictionary<int, List<Card>> targetDict)
	{
		MyLogger.log("NEW ADDING ABILITY OF : " + origin.getName() + " " + i.getDesc()  );
		StackDetails det = new StackDetails();
		det.ev = i;
		det.origin = origin;
		det.targetDict = targetDict;

		//TODO FIX ALTS
		TargetListObject targetObj = fillTargetsForAbility(det.origin, (PassiveAbility)det.ev, targetDict);
		if (targetObj != null)
		{
			MyLogger.log("REQUESYT!");
			canResolveAlts = true;
			tempStack.Add(det);
		}
		else
		{
			if (verifyAbilityPreEffects(det))
			{
				payAbilityPreEffects(det);
				postMessage("Adding ability " + det.ev.getDesc() + " to stack.");
				stack.addAbilityToStack(det);
			}
			else
			{
				MyLogger.log("FAILED TO ADD ABILITY, PREEFFECTS FAILED.");
			}
		}
		//printCardList(targettingets);
		//		det.extraDetailTargetType = i.getExtraDetailTargetType();
		
/*
		if (TargetGeneration.isModelGeneratesTargets(targetGen))
		{
			MyLogger.log("model created and selected all targets for ability");
			List<Card> newTargets = getTargetsForCard(origin,
			                                          i.getTargetType());
			det.targets = newTargets;

			printCardList(newTargets);
			if (det.extraDetailTargetType != null)
			{
				List<Card> extraDetailTargets = getTargetsForCard(
					det.origin, det.extraDetailTargetType);
				det.extraDetailTargets = extraDetailTargets;
			}
		}

//		MyLogger.log("DET TARGET LENGTH " + det.targets.Count);
		if (TargetGeneration.isRequiresRequestForTargets(targetGen))
		{
			MyLogger.log("need to select targets for ability.");
			canResolveAlts = true;
			tempStack.Add(det);
			//resolveAlts();
		}
		else if (det.ev is AltAbility)
		{

			MyLogger.log("need to select decision for ability.");
			canResolveAlts = true;
			tempStack.Add(det);
			//resolveAlts();
		}
		else
		{

			MyLogger.log("successfully added ability ot stack.");

			stack.addAbilityToStack(det);
			//stack.addAllToStack(this,tempStack);
		}*/

		//Reset Priority requirements.
		if (det.ev.requiresStack())
		{
			resPassed = false;
		}

		if (tempStack.Count > 0)
		{
			resolveAlts();
		}
		

		notifyStack();
	}
	
	/*
	public void finishedAddingAbilities(Player p)
	{
		resolveAlts();
		notifyStack ();
	}*/
	
	private void resolveAlts()
	{	
		if (abilitySelectingMode || request.inProgress())
		{
			MyLogger.log("Bug fix me, resolve alts being called too often!");
			return;
		}

		while (curAbility < tempStack.Count && !abilitySelectingMode)
		{
			StackDetails det = tempStack[curAbility];
			curAbility++;
			MyLogger.log("TEMPSTACK SIZE" + tempStack.Count + " current is " + curAbility + det.ev.getDesc());
			if (det.ev is AltAbility)//if (det.ev.hasAltAbility())// && !det.origin.isDead())
			{
				//MyLogger.log("HELLO THIS IS ALT");
				altCard = det.origin;
				MyLogger.log("Det ORGIN" + det.origin);
				altAbility = (AltAbility)det.ev;
				altPlayer = getPlayerFromCard(det.origin);
				abilityTeam = altPlayer.getTeam();
				MyLogger.log("ALTMODE TRUE IN RESOLVE ALTS");
				altMode = true;
				abilityActives = altAbility.getAlts();
				abilityCard = altCard;
				abilitySelectingMode = true;
	
				lazyUpdateSelectActiveAbility();
				return;
			}
			else //Is a request.
			{

				Dictionary<int, List<Card>> targetDict = det.targetDict;
				TargetListObject targetObj = fillTargetsForAbility(det.origin, (PassiveAbility)det.ev, targetDict);
				if (targetObj != null)
				{
					MyLogger.log("REQUESYT!");
					beginTargettingForAbility(getPlayerFromCard(det.origin).getTeam(), det.origin, (PassiveAbility)det.ev, targetDict, targetObj);
					return;
				}
			}
		}

		curAbility = 0;
		abilityCard = null;
		lazyUpdateSelectActiveAbility();
		canResolveAlts = false;
		foreach (StackDetails det in tempStack)
		{
			if (verifyAbilityPreEffects(det))
			{
				payAbilityPreEffects(det);
				if (det.ev is AltAbility)
				{
					continue;
				}
				stack.addAbilityToStack(det);
			}
			else
			{
				MyLogger.log("FAILED TO ADD ABILITY, PREEFFECTS FAILED.");
			}
		}
		//stack.addAllToStack(tempStack);
		tempStack = new List<StackDetails>();
		notifyStack();
	}
	
	[ClientRpc]
	private void RpcPayAlt()
	{
		//TODO!!!!! ADD INDEX!!!!
		//FIXME FIX ME
		/*int index = 0;
		if (altAbility.canPay(altPlayer, altCard, index))
		{
			ActiveAbility altToPay = altAbility.getAbility(index);
			TargetListObject targetObj = m.fillTargetsForAbility(altPlayer, altCard, altToPay, targetDict);
			if (targetObj == null)
			{
				MyLogger.log("Alt paid for " + altCard);
				altAbility.payAlt(altPlayer, altCard, extras, index);
			}
			else
			{
				TargetType targType = targetObj.targetType;
				request.setRequestListener(new ReqAltListener(this, ));
				request.requestSac(altPlayer, altCard,
				                   targType.numTargets,
				                   targType, targType.strict,
				                   true);
				notifyRequestBegin();
				
			}
		}
	
		MyLogger.log("Cant pay cost or cant pay dizzy");
		altCard = null;
		altAbility = null;
		altPlayer = null;
		MyLogger.log("ALTMODE FALSE IN PAYALT");
		altMode = false;
		resolveAlts();
		
		notifyAboutAll ();	*/
	}
	[ClientRpc]
	private void RpcPassAlt()
	{
		altCard = null;
		altAbility = null;
		altPlayer = null;
		MyLogger.log("ALTMODE FALSE IN PASSALT");
		altMode = false;
		resolveAlts();
		
		notifyAboutAll ();
	}
	
	
	public Card altCard = null;
	public AltAbility altAbility = null;
	public Player altPlayer = null;
	private bool altMode = false;

	public Player getPlayerFromCard(Card c)
	{
		int team = c.getTeam();
		return getPlayer(team);
	}

	public void printGrave(int player)
	{
		getPlayer(player).printGrave();
	}
	
	public void searchDeck (Player targetPlayer, Card origin, int number, TargetType type, bool cancel,  int playerChoice)	{
		MyLogger.log("SEARCH DECK MODE");
		request.setRequestListener(new ReqModelActionListener(this, false));
		request.setReason("Search for " + number + " cards.");
		if (playerChoice == TargetTeam.TEAM_ALLY)
			request.searchDeck(targetPlayer, origin, number, type, type.strict,
			                       cancel,  targetPlayer);
		else if (playerChoice == TargetTeam.TEAM_OPP)
			request.searchDeck(targetPlayer, origin, number, type, type.strict,
			                       cancel,  getOtherPlayer(targetPlayer));
		else
			MyLogger.log("ERROR IN REQUEST SEARCH DECK");

		notifyRequestBegin();
	}

	public void requestDiscard(Player player, Card origin, int number,
	                           TargetType type, bool strict, bool cancel, int playerChoice)
	{
		MyLogger.log("DISCARD MODE");
		request.setRequestListener(new ReqModelActionListener(this, false));
		request.setReason("Discard " + number + " cards.");
		if (playerChoice == TargetTeam.TEAM_ALLY)
			request.requestDiscard(player, origin, number, type, type.strict,
			                       cancel, player);
		else if (playerChoice == TargetTeam.TEAM_OPP)
			request.requestDiscard(player, origin, number, type, type.strict,
			                       cancel, getOtherPlayer(player));
		else
			MyLogger.log("ERROR IN REQUEST DISCARD");

		notifyRequestBegin();
	}
	
	public Player getOtherPlayer(Player player)
	{
		if (player.getTeam() == 1)
		{
			return players[0];
		}
		else if (player.getTeam() == 0)
			return players[1];
		return null;
	}
	
	public void requestSacrifice(Player player, Card origin, int number,
	                             TargetType type, bool strict, bool cancel)
	{
				request.setRequestListener(new ReqModelActionListener(this, true));
				request.setReason("Sacrifice " + number + " cards.");
		request.requestSac(player, origin, number, type, type.strict, cancel);

		notifyRequestBegin();
		
	}
	
	// finreq
	[ClientRpc]
	private void RpcFinishRequest()
	{
		// resolve has to succeed
		if (!request.resolve())
		{
			MyLogger.log("REqu[est resolve failed.");
			
		}
		else
		{
			notifyRequestUpdate();
			notifyAboutAll();
		}
		
	}
	
	[ClientRpc]
	private void RpcCancelRequest()
	{
		if (request.cancelRequest ())
		{
			notifyCancelRequest();
		}
		else
		{
			MyLogger.log("YOU CANT CANCEL THIS REQUEST ");
		}
	}
	

	/**
	 * BEGIN VIEW interface.
	 * 
	 */
	public int getNumPlayers()
	{
		return Constants.NUM_PLAYERS;
	}
	
	public int getDeckSize(int player)
	{
		return getPlayer(player).getDeckSize();
	}
	
	public int getHandSize(int player)
	{
		return getPlayer(player).getHandSize();
	}
	
	public int getGraveSize(int player)
	{
		return getPlayer(player).getGraveSize();
	}
	
	public List<Card> getHand(int player)
	{
		return getPlayer(player).getHand();
	}
	
	public List<Card> getGrave(int player)
	{
		return getPlayer(player).getGrave();
	}
	
	public List<StackDetails> getStack()
	{
		return stack.getRealStack();
	}
	
	
	public List<Card> getSelectedRequestTargets()
	{
		return request.getSelectedTargets();
	}
	
	public List<Card> getPossibleRequestTargets()
	{
		return request.getTargets();
	}
	
	public List<Card> getPlayerCreatures(int player)
	{
		return getPlayer(player).getCreatures();
	}
	
	public List<Card> getPlayerStructures(int player)
	{
		return getPlayer(player).getStructures();
	}
	
	public Land getLand()
	{
		return land;
	}
	
	public int getPlayerHealth(int player)
	{
		return getPlayer(player).getHealth();
	}
	
	//TODO instead of all counter, count num?
	public void counterXStack(int numEventCountered)
	{
		/*
		for (int i = stack.Count - 2; i >= 0 && numEventCountered > 0; i--, numEventCountered--)
		{
			StackDetails det = stack[i];
			det.counter();
		}*/
	}
	
	public int getCurrentTurn()
	{
		return turn;
	}
	
	public Player getWinner()
	{
		return winner;
	}
	/*
	 * 
	 *FFS TODO REQ ANON CLASSES. 
	 * 
	 */
	private class ReqPlayCardListener : Request.RequestListener{
		
		MainBoardModel m;
		int player;
		Card cardToPlay;
		List<List<Card>> extraCostTargetsSoFar;
		Dictionary<int, List<Card>> targets;
		public ReqPlayCardListener(MainBoardModel wm, int p, Card c, List<List<Card>>extraCostTargets, Dictionary<int, List<Card>> targets )
		{
			m = wm;
			player = p;
			cardToPlay = c;
			this.targets = targets;
			extraCostTargetsSoFar = extraCostTargets;
		}
		//override
		public void requestDone(Request r, Card origin,
		                        List<Card> newExtraCostTargets)
		{
			extraCostTargetsSoFar.Add(newExtraCostTargets);
			m.playCard(player, cardToPlay, extraCostTargetsSoFar,
			           targets);
		}
		
		//override
		public void requestCancelled(Request r)
		{
			
		}
		
		//override
		public void requestNotEnoughTargets(Request r,
		                                    Card origin,
		                                    List<Card> extraCostTargets)
		{
			MyLogger.log("Not enough targets to sac or do whatever...??");
			
		}
		
	}
	
	private int getTargetDictionarySlot(Dictionary<int, List<Card>> targetList)
	{
		int dictSize = targetList.Count;
		if (targetList.ContainsKey(SPECIAL_TARGET_RESPONSE_KEY))
		{
			return dictSize-1;
		}
		return dictSize;
	}
	
	private class ReqSpeListener : Request.RequestListener{
		
		MainBoardModel m;
		int player;

		List<List<Card>> extraCostTargets;
		Dictionary<int, List<Card>> currentTargets;
		int targetSlot;

		public ReqSpeListener(MainBoardModel wm, int p, List<List<Card>> extratargets, Dictionary<int, List<Card>> currentTargets, int targetSlot)
		{
			m = wm;
			player = p;
			extraCostTargets = extratargets;
			this.currentTargets = currentTargets;
			this.targetSlot = targetSlot;
		}
		//override
		public void requestDone(Request r, Card origin,
		                        List<Card> targets)
		{
			currentTargets[targetSlot] = targets;
			MyLogger.log("FINISH SPELL REQUEST " + targetSlot);
			m.playCard(player, origin, extraCostTargets, currentTargets);
		}
		
		//override
		public void requestCancelled(Request r)
		{
			// TODO Auto-generated method stub
			
		}
		
		//override
		public void requestNotEnoughTargets(Request r, Card origin,
		                                    List<Card> targets)
		{


			m.postMessage("NOT ENOUGH TARGETS! CANCEL THIS ABILITY");
			//currentTargets[targetSlot] = targets;
			//m.playCard(player, origin, extraCostTargets, currentTargets);
		}
		
		
	}
	private class ReqAbiListener :Request.RequestListener
	{
		MainBoardModel m;
		int player;
		Card origin;
		PassiveAbility ability;
		Dictionary<int, List<Card>> targetsSoFar;
		int targetId;

		public ReqAbiListener(MainBoardModel wm, int p, Card or, PassiveAbility abi, Dictionary<int, List<Card>> targetsSoFar, int targetId)
		{
			m = wm;
			player = p;
			origin = or;
			ability = abi;
			this.targetsSoFar = targetsSoFar;
			this.targetId = targetId;
		}

		//override
		public void requestDone(Request r, Card origin,
		                        List<Card> targets)
		{
			targetsSoFar[targetId] = targets;
			TargetListObject targetObj = m.fillTargetsForAbility(origin, ability, targetsSoFar);
			if (targetObj != null)
			{
				m.beginTargettingForAbility(player, origin, ability, targetsSoFar, targetObj);
			}
			else
			{

				m.activateAbilitySuccess(m.getPlayer(player), origin, ability,
			                                  targetsSoFar);
			}


		}
		
		//override
		public void requestCancelled(Request r)
		{
			// TODO Auto-generated method stub
			
		}
		
		//override
		public void requestNotEnoughTargets(Request r, Card origin,
		                                    List<Card> targets)
		{
			MyLogger.log("FIX ME INSIDE REQABILSITENR");
			TargetType ttype = r.getTargetType();
			if (TargetType.satisfiesStrict(ttype.numTargets, targets.Count, ttype.strict))
			{
				targetsSoFar[targetId] = targets;
				TargetListObject targetObj = m.fillTargetsForAbility(origin, ability, targetsSoFar);
				if (targetObj != null)
				{
					m.beginTargettingForAbility(player, origin, ability, targetsSoFar, targetObj);
				}
				else
				{
					m.activateAbilitySuccess(m.getPlayer(player), origin, ability,
				                                  targetsSoFar);
				}
			}
			else
				MyLogger.log("CANT ACTIVATE ABILITY, NOT ENOUGH TARGETS & STRICT");
		}
		
	}
	/*
	private class ReqAbiForListener :Request.RequestListener
	{
		MainBoardModel m;
		int player;
		Card origin;
		PassiveAbility abil;
		
		public ReqAbiForListener(MainBoardModel wm, int p, Card or, PassiveAbility abi)
		{
			
			m = wm;
			player = p;
			origin = or;
			abil = abi;
		}
		//override
		public void requestDone(Request r, Card origin,
		                        List<Card> targets)
		{
			m.activateAbility(player, origin, abil,
			                  targets);
			
			
			
		}
		
		//override
		public void requestCancelled(Request r)
		{
			
		}
		
		//override
		public void requestNotEnoughTargets(Request r, Card origin,
		                                    List<Card> extraCostTargets)
		{
			MyLogger.log("Not enough targets to sac");
			
		}
	}*/
	
	//YEA FIX ME TODO 
	private class ReqAltListener :Request.RequestListener
	{
		MainBoardModel m;
		Dictionary<int, List<Card>> targetsSoFar;
		int targetId;

		public ReqAltListener(MainBoardModel wm, Dictionary<int, List<Card>> targetsSoFar, int targetId)
		{
			m = wm;
			this.targetsSoFar = targetsSoFar;
			this.targetId = targetId;
		}

		//override
		public void requestDone(Request r, Card origin,
		                        List<Card> targets)
		{
			//m.payAlt(targets);
		}
		
		//override
		public void requestCancelled(Request r)
		{
			
		}
		
		//override
		public void requestNotEnoughTargets(Request r,
		                                    Card origin, List<Card> extraCostTargets)
		{
			//MyLogger.log("Not enough targets to sac");
			
		}
	}
	
	private class ReqModelActionListener :Request.RequestListener
	{
		MainBoardModel m;
		bool recallStack;

		public ReqModelActionListener(MainBoardModel wm, bool recallStack)
		{
			m = wm;
			this.recallStack = recallStack;
		}
		//override
		public void requestDone(Request r, Card origin,
		                        List<Card> targets)
		{
//			MyLogger.log("REQUEST MODE " +  r.getMode());
			if (r.getMode() == Request.MODE_DISCARD)
			{
			//	MyLogger.log("DISCARD???? HELLO?????" + targets.Count);
				r.getPlayer().discard(targets);
			}
			else if (r.getMode() == Request.MODE_SAC)
			{
				r.getPlayer().sacrifice(targets);
			}
			else if (r.getMode() == Request.MODE_SEARCH_LIBRARY)
			{
				//OK DO TOPDECK
				//TODO THINK ABOUT SHUFFLE? WHY ALWAYS TRUE HERE.
				r.getPlayer().topDeck(targets, true);

			}
			if (m.stackMode)
			{
				MyLogger.log("CONTINUE RESOLVE STACK FROM REQUEST");
				m.continueResolveStack(targets);
			}
		}
		
		//override
		public void requestCancelled(Request r)
		{
			
		}
		
		//override
		public void requestNotEnoughTargets(Request r, Card origin,
		                                    List<Card> targets)
		{
			if (r.getMode() == Request.MODE_DISCARD)
			{
				r.getPlayer().discard(targets);
			}
			else if (r.getMode() == Request.MODE_SAC)
			{
				r.getPlayer().sacrifice(targets);
			}
			else if (r.getMode() == Request.MODE_SEARCH_LIBRARY)
			{
				//OK DO TOPDECK
				//TODO THINK ABOUT SHUFFLE? WHY ALWAYS TRUE HERE.
				r.getPlayer().topDeck(targets, true);

			}

			if (m.stackMode && recallStack)
			{
				// stackCounter++;
				// continueResolveStack();
				
				m.continueResolveStack(targets);
			}
		}
		
	}



///OTHER junk
	private void printCardList(List<Card> cards)
	{
		MyLogger.log("PRINT LIST");
		foreach (Card c in cards)
		{
			MyLogger.log(c.getName());
		}
		MyLogger.log("ENDLIST");
	}

	public void DEVAddCard(int playerNum, int cardNum)
	{
		Card c = CardLibrary.getCardFromID(cardNum);
		c.setTeam(playerNum);
		Player player = getPlayer(playerNum);
		player.specialSummon(c);
		MyLogger.log("DEV Special summon");
	}

	public PlayerTimer getPlayerTimer(int playerNum)
	{
		return playerTimers[playerNum];
	}
	
}