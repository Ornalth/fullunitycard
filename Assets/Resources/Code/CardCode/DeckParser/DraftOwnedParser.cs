﻿using System.Collections.Generic;
using LitJson;
using UnityEngine;
using System;
using System.IO;

public class DraftOwnedParser : BaseDeckParser {


	private static DraftOwnedParser INSTANCE = null;

	override protected string getFileLoc()
	{
		return Constants.DRAFT_CARDS_NAME;
	}

	private DraftOwnedParser(): base()
	{

	}

	public static DraftOwnedParser getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new DraftOwnedParser();
		}
		return INSTANCE;
	}

	public override JsonData convertDeckToJSON(DeckInfo deck)
	{
		List<int> genIds = CardLibrary.getBasicGeneratorCardIDs();
		Dictionary<int, int> dataDict = deck.getCardsAsDict();
		foreach (int id in genIds)
		{
			dataDict[id] = 40;
		}	
		DeckInfo modifiedDeck = new DeckInfo(deck.getName(), dataDict);
		return base.convertDeckToJSON(modifiedDeck);
	}
 }