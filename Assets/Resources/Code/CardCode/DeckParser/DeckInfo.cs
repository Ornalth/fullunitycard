using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class DeckInfo {

	public static readonly int MIN_NUM_CARDS = 40;
	public static readonly int MAX_COPIES = 4;

	private List<DeckCardData> cardData;
	private string name;

	public DeckInfo(string name)
	{
		this.name = name;
		cardData = new List<DeckCardData>();
	}

	public DeckInfo(string name, Dictionary<int,int> cardDataDict)
	{
		this.name = name;
		setCards(convertDictToCardDataList(cardDataDict));
	}

	public DeckInfo(string name, List<DeckCardData> cardDataList)
	{
		this.name = name;
		setCards(cardDataList);
	}

	public static List<DeckCardData> convertDictToCardDataList(Dictionary<int,int> deck)
	{
		List<DeckCardData> deckCards = new List<DeckCardData>();
		foreach (int key in deck.Keys)
		{
			DeckCardData data = new DeckCardData();
			data.cardID = key;
			data.amount = deck[key];
			deckCards.Add(data);
		}
		return deckCards;
	}

	public string getName()
	{
		return name;
	}

	public void setName(string s)
	{
		name = s;
	}

	public void setCards(List<DeckCardData> cards)
	{
		cardData = cards;
	}

	public List<DeckCardData> getCards()
	{
		return cardData;
	}

	public Dictionary<int,int> getCardsAsDict()
	{
		Dictionary<int, int> deck = new Dictionary<int, int>();
		foreach (DeckCardData data in cardData)
		{
			deck[data.cardID] = data.amount;
		}
		return deck;
	}

	public int getNumCards()
	{
		int numCards = 0;
		foreach (DeckCardData data in cardData)
		{
			numCards += data.amount;
		}
		return numCards;
	}

	//TODO ONLY DO GENERATORS?
	//CURRENTLY CONSIDERS ALL
	public int getMostCommonFaction()
	{
		int red, blue, green, colourless;
		red = blue = green = colourless = 0;
		foreach (DeckCardData data in cardData)
		{
			int id = data.cardID;
			Card card = CardLibrary.getCardFromID(id);
			int factions = card.getFactions();
			if (Faction.hasRed(factions))
			{
				red += data.amount;
			}
			if (Faction.hasBlue(factions))
			{
				blue += data.amount;
			}
			if (Faction.hasGreen(factions))
			{
				green += data.amount;
			}
			if (Faction.hasColourless(factions))
			{
				colourless += data.amount;
			}
		}

		return max(red,blue,green,colourless);
	}

	public Dictionary<int, int> getCardCostWithoutColourless()
	{
		Dictionary<int, int> dict = new Dictionary<int, int>();

		foreach (DeckCardData data in cardData)
		{
			int id = data.cardID;
			Card card = CardLibrary.getCardFromID(id);
			//int factions = card.getFactions();
			int[] rez = card.getCost();
			int totalRez = rez[1] + rez[2] + rez[3];
			if (dict.ContainsKey(totalRez))
			{
				dict[totalRez] += data.amount;
			}
			else
			{
				dict[totalRez] = data.amount;
			}
		}

		return dict;
	}

	public Dictionary<int, int> getCardCostWithColourless()
	{
		Dictionary<int, int> dict = new Dictionary<int, int>();

		foreach (DeckCardData data in cardData)
		{
			int id = data.cardID;
			Card card = CardLibrary.getCardFromID(id);
			//int factions = card.getFactions();
			int[] rez = card.getCost();
			int totalRez = rez[0] + rez[1] + rez[2] + rez[3];
			if (dict.ContainsKey(totalRez))
			{
				dict[totalRez] += data.amount;
			}
			else
			{
				dict[totalRez] = data.amount;
			}
		}

		return dict;
	}

	public Dictionary<int, int> getCardTypeMapping()
	{
		Dictionary<int, int> dict = new Dictionary<int, int>();

		foreach (DeckCardData data in cardData)
		{
			int id = data.cardID;
			Card card = CardLibrary.getCardFromID(id);
			int cardType = card.getCardType();
			if (dict.ContainsKey(cardType))
			{
				dict[cardType] += data.amount;
			}
			else
			{
				dict[cardType] = data.amount;
			}
		}

		return dict;
	}

	//Generator, Source, ...???
	public Dictionary<string, int> getNameTypeMapping()
	{
		Dictionary<string, int> dict = new Dictionary<string, int>();
		dict["GENERATOR"] = 0;
		dict["SOURCE"] = 0;
		dict["OTHER"] = 0;
		foreach (DeckCardData data in cardData)
		{
			int id = data.cardID;
			Card card = CardLibrary.getCardFromID(id);
			if (card.hasNameType("GENERATOR"))
			{
				dict["GENERATOR"]++;
			}
			else if (card.hasNameType("SOURCE"))
			{
				dict["SOURCE"]++;
			}
			else
			{
				dict["OTHER"]++;
			}
		}

		return dict;
	}


	private static int max(params int[] values)
    {
        return Enumerable.Max(values);
    }

//TODO IS VALID DECK
    public bool isValid()
    {
    	int totalNumCards = 0;
    	string msg = "";
    	bool valid = true;
    	foreach (DeckCardData data in cardData)
    	{
    		totalNumCards += data.amount;
    		if (data.amount > MAX_COPIES)
    		{
    			msg += "More than " + MAX_COPIES + " copies of " + CardLibrary.getCardFromID(data.cardID).getName() + "\n"; 
    			valid = false;
    		}
    	}
    	if (totalNumCards < MIN_NUM_CARDS)
    	{
    		msg += "Less than " + MIN_NUM_CARDS + "deck.";
   			valid = false;
    	}	
    	if (!valid)
    	{
    		MyLogger.log(msg);
    	}
    	return valid;
    }
}
