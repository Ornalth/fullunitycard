public struct DeckCardData
{
	public int cardID;
	public int amount;

	public override bool Equals(System.Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (obj is DeckCardData)
        {
            return (cardID == ((DeckCardData)obj).cardID) && (amount == ((DeckCardData)obj).amount);
        }
        return false;

    }

    public bool Equals(DeckCardData p)
    {
        if ((object)p == null)
        {
            return false;
        }

        return (cardID == p.cardID) && (amount == p.amount);
    }

    public override int GetHashCode()
    {
        return cardID % 5000 + amount * 1000;
    }
}