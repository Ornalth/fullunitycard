using System.Collections.Generic;
using LitJson;
using UnityEngine;
using System;
using System.IO;

public abstract class BaseDeckParser {

	private static readonly string JKEY_MAIN_DECKS = "decks";
	private static readonly string JKEY_DECK_NAME = "name";
	protected List<DeckInfo> decks;


	abstract protected string getFileLoc();

	protected BaseDeckParser()
	{
		refreshDecks();
	}

	public void refreshDecks()
	{
		decks = new List<DeckInfo>();
		parseDecks();
	}

	private void parseDecks()
	{
		JsonData mainObj = getFileJsonData(); 
		JsonData deckArray = (JsonData) mainObj[JKEY_MAIN_DECKS];

		for (int i = 0; i < deckArray.Count; i++)
		{
			DeckInfo deck = parseJSONDeck((JsonData) deckArray[i]);
			decks.Add(deck);
		}
//		MyLogger.log("DECKS fOUND" + deckArray.Count + " " + getFileLoc());
	}

	public DeckInfo getDeck(int playerNum)
	{
//		MyLogger.log("LOOKNIG fOR PLAYERNUM" + playerNum + " DECKS SIZE" + decks.Count);
		return decks[playerNum];
	}

	public List<DeckInfo> getAllDecks()
	{
		return decks;
	}

	public DeckInfo parseJSONDeck(JsonData deckNode)
	{
		DeckInfo info;
		string deckName = "";
		List<DeckCardData> deck = new List<DeckCardData>();
		foreach( String key in deckNode.Keys )
		{
			if (key.Equals(JKEY_DECK_NAME))
			{	
				deckName = (string)deckNode[key];
				continue;
			}
			DeckCardData data = new DeckCardData();
			bool success = ViewHelper.parseInt(key, out data.cardID);
			if (!success)
			{

			}
			data.amount =(int) deckNode[key];
			deck.Add(data);
		}
		info = new DeckInfo(deckName);
		info.setCards(deck);
		return info;
	}


	private JsonData getFileJsonData()
	{
		#if UNITY_WEBPLAYER
        	TextAsset textFile = (TextAsset)Resources.Load(getFileLoc(), typeof(TextAsset));
			string str = textFile.text;
			return JsonMapper.ToObject(str);
        #else
        	string str = File.ReadAllText(Constants.BASE_JSON_DIR + getFileLoc() + ".json");
        	return JsonMapper.ToObject(str);
		#endif
	}

	public bool writeDeck(int playerNum, DeckInfo deck, bool shouldValidateDeck = false)
	{
		if (shouldValidateDeck)
		{
			if (!deck.isValid())
			{
				MyLogger.log("THIS DECK IS NOT VALID, CANNOT SUBMTI!");
				MyLogger.log("IGNORE FOR NOW.");
				//return false;
			}
		}

		JsonData mainObj = getFileJsonData(); 
			
		//MyLogger.log("Writing player " + playerNum + " TO " + getFileLoc());
		JsonData deckArray = (JsonData) mainObj[JKEY_MAIN_DECKS];

		JsonData deckNode = convertDeckToJSON(deck);
		int numDecks =  deckArray.Count;

		//MyLogger.log("DECK ARRAY COUNT SO FAR" + deckArray.Count);
		for (int i = deckArray.Count; i < playerNum+1; i++)
		{
			//MyLogger.log("ADDED NEW BLANK TO ARRAY!" + i);
			deckArray.Add(getPlaceHolderJSONDeck());
			decks.Add(new DeckInfo("Placeholder", new List<DeckCardData>()));
		}
		deckArray[playerNum] = deckNode;

		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = true;
        writer.IndentValue = 4;
        JsonMapper.ToJson(mainObj, writer);
        #if UNITY_WEBPLAYER
        	throw new SystemException("DO NOT RW ASSETS WHILE ONLINE!");
        #else
        	File.WriteAllText(Constants.BASE_JSON_DIR + getFileLoc() + ".json", writer.ToString());
        #endif
//        MyLogger.log("WRITER.tostring " + writer.ToString());
        decks[playerNum] = deck;
		//File.WriteAllText(Constants.TEMP_CUSTOM_DECK_NAME, mainObj.ToString());
		return true;
	}

	public void clearAllDecks()
	{
		JsonData mainObj = getFileJsonData(); 
			
		JsonData deckArray = (JsonData) mainObj[JKEY_MAIN_DECKS];

		JsonData newDeckArray = new JsonData();
	
	//Hmmm..... TODO FIXME
		for (int i = 0; i < 2; i++)
		{
//			MyLogger.log("ADDED NEW BLANK TO ARRAY!" + i);
			newDeckArray.Add(getPlaceHolderJSONDeck());
		}
		mainObj[JKEY_MAIN_DECKS] = newDeckArray;

		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = true;
        writer.IndentValue = 4;
        JsonMapper.ToJson(mainObj, writer);
        #if UNITY_WEBPLAYER
        	throw new SystemException("DO NOT RW ASSETS WHILE ONLINE!");
        #else
        	File.WriteAllText(Constants.BASE_JSON_DIR + getFileLoc() + ".json", writer.ToString());
        #endif
//        MyLogger.log("WRITER.tostring " + writer.ToString());
        refreshDecks();
	}

	public JsonData getPlaceHolderJSONDeck()
	{
		JsonData data = new JsonData();	
		data[JKEY_DECK_NAME] = "PLACEHOLDER";
		return data;
	}

	public virtual JsonData convertDeckToJSON(DeckInfo deck)
	{
		JsonData newnode = new JsonData();
		List<DeckCardData> cards = deck.getCards();
		foreach (DeckCardData data in cards)
		{
			newnode[""+data.cardID] = data.amount;
		}
		newnode[JKEY_DECK_NAME] = deck.getName();
		return newnode;
	}
}
