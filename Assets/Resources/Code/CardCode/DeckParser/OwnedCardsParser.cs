﻿using System.Collections.Generic;
using LitJson;
using UnityEngine;
using System;
using System.IO;

public class OwnedCardsParser : BaseDeckParser {


	private static OwnedCardsParser INSTANCE = null;

	override protected string getFileLoc()
	{
		return Constants.OWNED_CARDS_NAME;
	}

	private OwnedCardsParser(): base()
	{

	}

	public static OwnedCardsParser getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new OwnedCardsParser();
		}
		return INSTANCE;
	}
}
