﻿public class DeckParser : BaseDeckParser {

	private static DeckParser INSTANCE = null;

	override protected string getFileLoc()
	{
		return Constants.DECK_NAME;
	}

	private DeckParser(): base()
	{
	}

	public static DeckParser getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new DeckParser();
		}
		return INSTANCE;
	}
}
