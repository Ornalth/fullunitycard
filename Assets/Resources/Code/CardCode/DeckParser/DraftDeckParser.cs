﻿using System.Collections.Generic;
using LitJson;
using UnityEngine;
using System;
using System.IO;

public class DraftDeckParser : BaseDeckParser {


	private static DraftDeckParser INSTANCE = null;

	override protected string getFileLoc()
	{
		return Constants.DRAFT_DECK_CARDS_NAME;
	}

	private DraftDeckParser(): base()
	{

	}

	public static DraftDeckParser getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new DraftDeckParser();
		}
		return INSTANCE;
	}
}
