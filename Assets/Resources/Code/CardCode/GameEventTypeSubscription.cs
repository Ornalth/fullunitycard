using System.Collections.Generic;
using UnityEngine;

public class GameEventTypeSubscription
{
	//Gameeventtype -> 
	private Dictionary<int, HashSet<Card>> gameEventTypeSubDict;
	private Dictionary<int, HashSet<Card>> phaseSubDict;
	private HashSet<Card> subbedCards;

	MainBoardModel model;

	public GameEventTypeSubscription(MainBoardModel model)
	{
		this.model = model;
		gameEventTypeSubDict = new Dictionary<int, HashSet<Card>>();
		foreach (int key in GameEventType.getAllEventTypes())
		{
			gameEventTypeSubDict[key] = new HashSet<Card>();
		}

		phaseSubDict = new Dictionary<int, HashSet<Card>>();
		foreach (int key in Phase.getAllPhases())
		{
			phaseSubDict[key] = new HashSet<Card>();
		}

		subbedCards = new HashSet<Card>();
	}

	public void cardStateChanged(Card c)
	{
		//int cardState = c.getState();
		if (c.getState() == CardState.BOARD)
		{
			MyLogger.log("SUBSCRIBED! " + c.getName());
			sub(c);
		}
		else
		{
			MyLogger.log("UNSUBBEd! " + c.getName());
			unsub(c);
		}
	}

	public void abilityAdded(Card c, PassiveAbility ability)
	{
		if (isSubbed(c))
		{
			if (ability is AltAbility || ability is PassiveAbility)
			{
				if (ability.getPhase() != Phase.ERROR)
				{
					HashSet<Card> cards = phaseSubDict[ability.getPhase()];
					cards.Add(c);
				}
				else if (ability.getEventResponseType() != GameEventType.ERROR)
				{
					HashSet<Card> cards = phaseSubDict[ability.getEventResponseType()];
					cards.Add(c);
				}
			}
		}
		else
		{
			sub(c);
		}
	}

	public void respondToPhase(int phase, int turn)
	{
		foreach (Card card in phaseSubDict[phase])
		{
			int phaseTeam;
			int team = card.getTeam();
			if (team == turn)
			{
				phaseTeam = PhaseTeam.TEAM_ALLY;
			}
			else
			{
				phaseTeam = PhaseTeam.TEAM_OPP;
			}
			foreach (PassiveAbility i in card.getPassives())
			{
				if (i.respondsToPhase(phase, phaseTeam))
				{
					model.addAbilitiesToStack(i, card, new List<Card>(), i.getAbilityResponseTargetGen());
				}
			}

			foreach (AltAbility i in card.getAlts())
			{
				if (i.respondsToPhase(phase, phaseTeam))
				{
					model.addAbilitiesToStack(i, card, new List<Card>(), i.getAbilityResponseTargetGen());
				}
			}
		}	
	}

	public void respondToEvent(ExecutedGameEvent eev)
	{
		MyLogger.log("RESPOND TO... " + GameEventType.eventTypeToString(eev.gameEventType));
		foreach (Card card in gameEventTypeSubDict[eev.gameEventType])
		{
			MyLogger.log("CHECKINg.. " + card.getName());
			processAbilitiesForCard(card, card.getPassives(), eev);
			List<PassiveAbility> alts = card.getAlts().ConvertAll(x => (PassiveAbility)x);
			processAbilitiesForCard(card, alts, eev);
		}
	}

	private void processAbilitiesForCard(Card card, List<PassiveAbility> abilities, ExecutedGameEvent eev)
	{
		foreach (PassiveAbility i in abilities)
		{
			if (i.respondsToEvent(eev.gameEventType, eev.origin, eev.recipients, card))
			{
				List<Card> targets = i.selectResponseEventCards(eev.origin, eev.recipients, card);
				model.addAbilitiesToStack(i, card, targets, i.getAbilityResponseTargetGen());
			}
		}
	}


	private bool isSubbed(Card c)
	{
		return subbedCards.Contains(c);
	}

	private void unsub(Card c)
	{
		if (!isSubbed(c))
		{
			return;
		}

		foreach (PassiveAbility ability in c.getPassives())
		{
			if (ability.getPhase() != Phase.ERROR)
			{
				HashSet<Card> cards = phaseSubDict[ability.getPhase()];
				cards.Remove(c);
			}
			else if (ability.getEventResponseType() != GameEventType.ERROR)
			{
				HashSet<Card> cards = gameEventTypeSubDict[ability.getEventResponseType()];
				cards.Remove(c);
			}
		}

		foreach (AltAbility ability in c.getAlts())
		{
			if (ability.getPhase() != Phase.ERROR)
			{
				HashSet<Card> cards = phaseSubDict[ability.getPhase()];
				cards.Remove(c);
			}
			else if (ability.getEventResponseType() != GameEventType.ERROR)
			{
				HashSet<Card> cards = gameEventTypeSubDict[ability.getEventResponseType()];
				cards.Remove(c);
			}
		}
		subbedCards.Remove(c);
	}

	private void sub(Card c)
	{
		if (isSubbed(c))
		{
			return;
		}
		else
		{
			foreach (PassiveAbility ability in c.getPassives())
			{
				if (ability.getPhase() != Phase.ERROR)
				{
					HashSet<Card> cards = phaseSubDict[ability.getPhase()];
					cards.Add(c);
				}
				else if (ability.getEventResponseType() != GameEventType.ERROR)
				{

					//MyLogger.log("Added ability to " + c.getName() + " " + GameEventType.eventTypeToString(ability.getEventResponseType()));
					HashSet<Card> cards = gameEventTypeSubDict[ability.getEventResponseType()];
					cards.Add(c);
				}
			}

			foreach (AltAbility ability in c.getAlts())
			{
				if (ability.getPhase() != Phase.ERROR)
				{
					HashSet<Card> cards = phaseSubDict[ability.getPhase()];
					cards.Add(c);
				}
				else if (ability.getEventResponseType() != GameEventType.ERROR)
				{
					HashSet<Card> cards = gameEventTypeSubDict[ability.getEventResponseType()];
					cards.Add(c);
				}
			}
			subbedCards.Add(c);
		}
	}

}
