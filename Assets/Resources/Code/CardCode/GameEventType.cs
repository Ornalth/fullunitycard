using System.Collections.Generic;

public class GameEventType
{
	//public enum type {ENTER, LEAVE, COMBAT_DAMAGE, EXILE, DEATH, ENTER_GRAVE,DAMAGE,ATTACK,RETAL,DECLARE_ATTACK};
	public const int ERROR = -1;
	public const int ENTER = 1;
	public const int LEAVE = 2;
	public const int COMBAT_DAMAGE = 3;
	public const int EXILE = 4;
	public const int DEATH = 5;
	public const int ENTER_GRAVE = 6;
	public const int DAMAGE = 7;
	public const int ATTACK = 8;
	public const int RETAL = 9;
	public const int DECLARE_ATTACK = 10;
	public const int CARD_DRAWN = 11;
	//public const int BOUNCE = 12;

	public const int COUNTER_REFILL = 50;
	public const int COUNTER_OVERFLOW = 51;
	public const int COUNTER_PAY = 52;
	public const int COUNTER_SET = 53;

	private static Dictionary<string, int> dict = new Dictionary<string, int> {
		{ "ENTER", ENTER },
		{ "LEAVE", LEAVE },
		{ "COMBAT_DAMAGE", COMBAT_DAMAGE },
		{ "EXILE", EXILE },
		{"DEATH", DEATH},
		{"ENTER_GRAVE",ENTER_GRAVE},
		{"DAMAGE",DAMAGE},
		{"ATTACK",ATTACK},
		{"RETAL",RETAL},
		{"DECLARE_ATTACK",DECLARE_ATTACK},
		{
			"COUNTER_REFILL", COUNTER_REFILL
		},
		{
			"COUNTER_OVERFLOW", COUNTER_OVERFLOW
		},
		{
			"COUNTER_PAY", COUNTER_PAY
		},
		{
			"COUNTER_SET", COUNTER_SET
		},
		{
			"CARD_DRAWN", CARD_DRAWN
		},
		//{
		//	"BOUNCE", BOUNCE
		//}

	};

	private static Dictionary<int, string> reverseDict = new Dictionary<int, string> {
		{ ENTER, "ENTER" },
		{ LEAVE, "LEAVE" },
		{ COMBAT_DAMAGE, "COMBAT_DAMAGE" },
		{ EXILE, "EXILE" },
		{DEATH, "DEATH"},
		{ENTER_GRAVE,"ENTER_GRAVE"},
		{DAMAGE,"DAMAGE"},
		{ATTACK,"ATTACK"},
		{RETAL,"RETAL"},
		{DECLARE_ATTACK,"DECLARE_ATTACK"},
		{
			COUNTER_REFILL, "COUNTER_REFILL"
		},
		{
			COUNTER_OVERFLOW, "COUNTER_OVERFLOW"
		},
		{
			COUNTER_PAY, "COUNTER_PAY"
		},
		{
			COUNTER_SET, "COUNTER_SET"
		},
		{
			CARD_DRAWN, "CARD_DRAWN"
		},
		//{
		//	BOUNCE, "BOUNCE"
		//}
	};


	public static int eventTypeFromString(string s)
	{
		return dict [s];
	}

	public static string eventTypeToString(int ty)
	{
		return reverseDict[ty];
	}

	public static List<int> getAllEventTypes()
	{
		return new List<int>(reverseDict.Keys);
	}
	//@Override
	public string toString(){
		return "GAME_EVENT_TYPE";
	}
}
