﻿using UnityEngine;
using System.Collections.Generic;

public class GameConfiguration {
	public int numPlayers = 2;
	public List<string> playerNames;
	public List<int> playerDeckNumbers;
	public float timePerTurn;
	public float baseReserveTime;
	public float gainPercentagePerReserve;
	public bool isDraft = false;

	public bool isAI = false;
}
