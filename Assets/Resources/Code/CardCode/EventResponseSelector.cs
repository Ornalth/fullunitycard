﻿using UnityEngine;
using System.Collections.Generic;

public class EventResponseSelector  {
	public static readonly int NONE = 0;
	public static readonly int ORIGIN = 1;
	public static readonly int RECIPIENT = 1 << 1;
	public static readonly int THIS = 1 << 2;

	public static readonly int ALL = ORIGIN | RECIPIENT | THIS;

	private static Dictionary<string, int> dict = new Dictionary<string, int> {
			{"NONE", NONE},
			{"THIS", THIS},
			{"RECIPIENT", RECIPIENT},
			{"ORIGIN", ORIGIN}
		
	};

	private static Dictionary<int, string> reverseDict = new Dictionary<int, string> {
			{NONE, "NONE"},
			{THIS, "THIS"},
			{RECIPIENT, "RECIPIENT"},
			{ORIGIN, "ORIGIN"}
	};


	public static int eventResponseSelectorFromString(string s)
	{
		return dict [s];
	}

	public static string eventResponseSelectorToString(int ty)
	{
		return reverseDict[ty];
	}
}
