
public class CounterCost
{

	public static int DEFAULT_COST = 0;
	public static int ALL_COST = -1; // REMOVE ALL COUNTERS!!!!

	public string name = Counter.DEFAULT_COUNTER_NAME;
	public int cost = DEFAULT_COST;

	public CounterCost()
	{

	}

	public CounterCost(CounterCost c)
	{
		this.name = c.name;
		this.cost = c.cost;
	}
}

