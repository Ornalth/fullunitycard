using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using UnityEngine.Networking;

public class DraftModel : NetworkBehaviour {

	private List<List<DeckCardData>> boosterPacks;
	private List<Dictionary<int, int>> playerDecks; // CardNumber -> Amount
	private List<bool> hasBoosterChangedList = new List<bool>();
	private List<int> currentPlayerDeckPickNumber = new List<int>();
	private int numRoundsLeft = 0;
	private int numPlayers;
	private CardSet cardSet;
	private bool hasinit = false;
	
	private List<PlayerDraftController> viewControllers = new List<PlayerDraftController>();

	void Start()
	{
		DontDestroyOnLoad(this.gameObject);
	}

	[Command]
	public void CmdInit(int numPlayers, int numRoundsLeft, CardSet cardSet)
	{
		List<List<DeckCardData>> boosters = generateBoosterPacks(numPlayers, numRoundsLeft, cardSet); 
		RpcInit(numPlayers, numRoundsLeft, cardSet, SerializationHelper.serializeBoosters(boosters));
	}

	[ClientRpc]
	private void RpcInit(int numPlayers, int numRoundsLeft, CardSet cardSet, string serializedBoostersString)
	{
		List<List<DeckCardData>> boosters = SerializationHelper.deserializeBoosters(serializedBoostersString);
		MyLogger.log("WE INITED!");
		hasinit = true;
		this.numPlayers = numPlayers;
		this.numRoundsLeft = numRoundsLeft;
		playerDecks = new List<Dictionary<int, int>>();
		for (int i = 0; i < numPlayers; i++)
		{
			playerDecks.Add(new Dictionary<int, int>());
		}

		this.cardSet = cardSet;
		boosterPacks = boosters;
		newRound(true);
	}

	private List<List<DeckCardData>> generateBoosterPacks(int numPlayers, int numRoundsLeft, CardSet cardSet)
	{
		List<List<DeckCardData>> packs = new List<List<DeckCardData>>();
		for (int numRounds = 0; numRounds < numRoundsLeft; numRounds++)
		{
			for (int i = 0; i < numPlayers; i++)
			{
				List<DeckCardData> boosterDeck = CardLibrary.getBoosterPackFromSet(cardSet);
				packs.Add(boosterDeck);
			}
		}
		return packs;
	}

	private void newRound(bool firstRound = false)
	{
		numRoundsLeft--;
		if (!firstRound)
		{
			//Remove first X drafted packs
			boosterPacks.RemoveRange(0, numPlayers);
			MyLogger.log("Num Packs left " + boosterPacks.Count);
		}

		hasBoosterChangedList = new List<bool>();
		currentPlayerDeckPickNumber = new List<int>();
		for (int i = 0; i < numPlayers; i++)
		{
			currentPlayerDeckPickNumber.Add(0);
			hasBoosterChangedList.Add(true);
		}
	}

	public bool hasBoosterChanged(int playerNum)
	{
		if (!hasinit)
		{
			return false;
		}
		return hasBoosterChangedList[playerNum];
	}

	public List<DeckCardData> getBoosterPack(int playerNum)
	{
		if (canShowPick(playerNum))
		{
			List<DeckCardData> data = getRealBoosterPack(playerNum);
			hasBoosterChangedList[playerNum] = false;
			return data;
		}
		else
		{
			return new List<DeckCardData>();
		}
	}

	private bool canShowPick(int playerNum)
	{
		int lastPlayerNum = (playerNum - 1 + numPlayers) % numPlayers;
		if (currentPlayerDeckPickNumber[lastPlayerNum] >= currentPlayerDeckPickNumber[playerNum])
			return true;
		return false; 
	}

	public Dictionary<int, int> getPlayerDraftDeck(int playerNum)
	{
		//Dictionary<int, int> deck = playerDecks[number];
		return playerDecks[playerNum];
	}

	private void updateDeck(int number, List<DeckCardData> deckLeft)
	{
		boosterPacks[number] = deckLeft;
	}

	private List<DeckCardData> getRealBoosterPack(int playerNum)
	{
		return boosterPacks[getBoosterPickNumber(playerNum)];
	}

	private int getBoosterPickNumber(int playerNum)
	{
		int pickNumber = currentPlayerDeckPickNumber[playerNum];
		pickNumber += playerNum;
		int boosterPickNumber = pickNumber % numPlayers;
		//MyLogger.log("GETTING BOOSTER PICK NUMBER PLAYER " + playerNum + " BOOSTER NUMBER " + boosterPickNumber + " " + pickNumber + " " + currentPlayerDeckPickNumber[playerNum]);
		return boosterPickNumber;
	}

	public void selectCard(int playerNum, int cardIndex)
	{
		RpcSelectCard(playerNum, cardIndex);
	} 

	public PlayerTimer getPlayerTimer(int team) {
		
		List<DeckCardData> draftPack = getRealBoosterPack(team);

		int numCardsLeft = draftPack.Count;
		//TODO
		PlayerTimer pt = new PlayerTimer(numCardsLeft * 50000.0f, 0);
		return pt;
	}

	public void timerComplete(int team)
	{
		if (isCurrentlyPicking(team))
		{
			pickRandomCard(team);
		}
		else
		{
			MyLogger.log("IGNORE timeout;");
		}
	}

	private void pickRandomCard(int team)
	{
		List<DeckCardData> draftPack = getRealBoosterPack(team);
		int index = UnityEngine.Random.Range(0, draftPack.Count);
		RpcSelectCard(team, index);
	}

	//TODO
	private bool isCurrentlyPicking(int team)
	{
		return false;
	}

	[ClientRpc]
	private void RpcSelectCard(int playerNum, int cardIndex)
	{
		List<DeckCardData> draftPack = getRealBoosterPack(playerNum);

		if (cardIndex >= draftPack.Count || cardIndex < 0)
		{
			MyLogger.log("WHAT THE FK");
			return;
		//	return false;
		}

		//Insert card into deck
		Dictionary<int, int> playerDeck = playerDecks[playerNum];
		DeckCardData data = draftPack[cardIndex];	
		if (playerDeck.ContainsKey(data.cardID))
		{
			playerDeck[data.cardID] += 1;  
		}
		else
		{
			playerDeck[data.cardID] = 1;
		}
		playerDecks[playerNum] = playerDeck;
		//Remove from booster
		draftPack.Remove(data);

		//Move to next pick that player
		currentPlayerDeckPickNumber[playerNum] = currentPlayerDeckPickNumber[playerNum] + 1;
		hasBoosterChangedList[playerNum] = true;
		//notifyTimerUpdate();
		//Check if we are complete

		if (isRoundCompleted())
		{
			if (numRoundsLeft > 0)
			{
				newRound();
				//notifyAll();
			}
			else
			{


				StartCoroutine(sSleep(playerNum));
				//TODO CALL ON SERVER!
				finishDraft();
				//finishDraft();
				//Notify netowrkmanagers to change player's stuff.
				//WE'RE DONE
			}
		}
		//return true;
	}

	private bool isRoundCompleted()
	{
		for (int i = 0; i < numPlayers; i++)
		{
			if (boosterPacks[i].Count > 0)
				return false;
		}
		return true;
	}

	bool draftFinished = false;
	[Command]
	private void CmdFinishDraft()
	{
		finishDraft();
	}

	private void finishDraft()
	{
		draftFinished = true;
		//if (draftFinished)
		//{
		//	firstFinish = false;
		saveAllDecks();// TODO ONLY SAVE OWN DECK	
		foreach (PlayerDraftController controller in viewControllers)
			controller.notifyFinished();


		//}
	}

	public bool isDraftFinished()
	{
		return draftFinished;
	}
    
    IEnumerator sSleep(int time) {
        yield return new WaitForSeconds(3*time);
    }

    private void saveAllDecks()
	{
		for (int i = 0; i < numPlayers; i++)
		{
			MyLogger.log(" NUM PLAYERS " + i + " " + playerDecks[i].Count);
			DeckInfo deck = new DeckInfo("DraftDeckPlayer " + i, playerDecks[i]);
			DraftOwnedParser.getInstance().writeDeck(i, deck);
			DraftDeckParser.getInstance().clearAllDecks();
		}
	}

	public void suscribeToModel(PlayerDraftController vc)
	{
		viewControllers.Add(vc);
	}

    //Notifies
    public void notifyTimerUpdate()
	{
		foreach (PlayerDraftController vc in viewControllers)
		{
			//vc.updateTimers();
		}
	}


	public void deckSaved(int team)
	{
		numReady++;
    	MyLogger.log("PLAYER " + " READIED.");
    	if (numReady == 2)
    	{
    		//Load app.
    		MyLogger.log("LETS PLAY");

    		GameObject mainBoardModelObject = Instantiate (Resources.Load("GameMainBoardModel")) as GameObject;
			ClientScene.RegisterPrefab(mainBoardModelObject);

	    	NetworkServer.Spawn(mainBoardModelObject);
	    	MainBoardVC[] vcs = new MainBoardVC[2];

	   

	    	GameObject serverObj = GameObject.FindWithTag("ServerData");
			ServerData serverDataScript = serverObj.GetComponent<ServerData>();
			List<NetworkConnection> conns = serverDataScript.getConns();
			List<short> pcIds = serverDataScript.getIds();

			for (int i = 0; i < conns.Count; i++)
			{
				NetworkConnection conn = conns[i];
				short playerControllerId = pcIds[i];
				
				GameObject player = serverDataScript.getSpawnPlayer();
		        NetworkServer.Spawn(player);
		        NetworkServer.ReplacePlayerForConnection(conn, player, playerControllerId);
		        player.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
		        vcs[i] = player.GetComponentInChildren<MainBoardVC> ();
				
			}
			vcs[0].initVC(0);
			//vcs[0].shouldSetDeck();
			vcs[1].initVC(1);

			
    		GameObject optsServer = Instantiate (Resources.Load("GameOptionsServer")) as GameObject;
			ClientScene.RegisterPrefab(optsServer);
	    	
	    	NetworkServer.Spawn(optsServer);
			GameOptionsServer optServerScript = optsServer.GetComponent<GameOptionsServer>();

	    	optServerScript.setDraft(true);
	    	optServerScript.setDeckId(0, 0);
	    	optServerScript.setDeckId(1, 1);
    	}	
    	else
    	{
    	}
		//RpcDeckSaved(team);
	}

	int numReady = 0;


}
