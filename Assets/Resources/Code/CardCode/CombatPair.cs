
public class CombatPair
{
	public bool isBlocking = false;
	public CombatCard attacker;
	public CombatCard attacked;
	public CombatCard blocker;
	
	public bool containsAttacker(CombatCard c)
	{
		if (c == attacker)
			return true;
		return false;
	}
	
	public bool containsBlocker(CombatCard c)
	{
		if (c == blocker)
			return true;
		return false;
	}
	
	public bool containsAttacked(CombatCard c)
	{
		if (c == attacked)
			return true;
		return false;
	}
	
	public void setBlocker(CombatCard block)
	{
		isBlocking = true;
		blocker = block;
	}
	
	public void setCombatPair(CombatCard atker, CombatCard atked)
	{
		attacker = atker;
		attacked = atked;
	}
	
	
	public void removeBlocker()
	{
		isBlocking = false;
		blocker = null;
	}
	
	public void removeCombatPair()
	{
		attacked = null;
		attacker = null;
		blocker = null;
	}
}
