
public class CardPlayResult
{
	public static int SUCCESS = 0;
	public static int NONE = 0;


	public static int FAILURE_EXTRA_COST = 1 << 1;
	public static int FAILURE_TARGET = 1<<2;
	public static int FAILURE_FULL = 1<<3;
	

	public static int FAILURE_COST = 1<<4;
	public static int FAILURE_UNKNOWN = 1<<6;
	public static int FAILURE_INVALID = 1<<7;
	public static int FAILURE_BAD_TARGET = 1 << 8;
	public static int FAILURE_UNIQUE_COLLISION = 1 << 9;

	public static int REQ_EXTRA = FAILURE_EXTRA_COST;
	public static int REQ_TARGET = FAILURE_TARGET ;
	public static int FAIL_OTHER = FAILURE_COST | FAILURE_FULL | FAILURE_UNKNOWN | FAILURE_INVALID | FAILURE_BAD_TARGET | FAILURE_UNIQUE_COLLISION;

	


	public static bool isFail(int result)
	{
		return result != SUCCESS;
	}
	
	public static bool onlyRequiresTarget(int result)
	{
		if ((REQ_TARGET & result) != 0)
		{
			if ((FAIL_OTHER & result) == 0)
				return true;
		}
		return false;
	}

	public static bool failCost(int result)
	{
		if ((result & FAILURE_COST) != 0)
		{
			return true;
		}
		return false;
	}

	public static string resultToString(int result)
	{
		string str = "ALL FAILURES::: ";
		if ((result & FAILURE_EXTRA_COST) != 0)
		{
			str += "FAILURE FAILURE_EXTRA_COST" + " ";
		}

		if ((result & FAILURE_TARGET) != 0)
		{
			str += "FAILURE TARGET" + " ";
		}

		if ((result & FAILURE_FULL) != 0)
		{
			str += "FAILURE FULL" + " ";
		}

		if ((result & FAILURE_COST) != 0)
		{
			str += "failure cost" + " ";
		}

		if ((result & FAILURE_INVALID) != 0)
		{
			str += "FAILURE INVALID" + " ";
		}

		if ((result & FAILURE_UNKNOWN) != 0)
		{
			str += "UNKNOWN" + " ";
		}

		if ((result & FAILURE_BAD_TARGET) != 0)
		{
			str += "failure bad target"  + " ";
		}

		if ((result & FAILURE_UNIQUE_COLLISION) != 0)
		{
			str += "failure unique duplicate name." + " ";
		}

		return str;
	}

	public static bool requiresExtraTargets(int result)
	{
		if ((REQ_EXTRA & result) != 0)
		{
			if ((FAIL_OTHER & result) == 0)
				return true;
		}
		return false;
	}
	
}
