using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Board
{
	public List<Card> creatures = new List<Card>();
	public List<Card> structures = new List<Card>();
	public Player player;

	public Board(Player p)
	{
		player = p;
	}

	public void playCard(Card c, Dictionary<int, List<Card>> targetDict)
	{

		if (c is Creature)
		{
			creatures.Add(c);
			c.setState(CardState.BOARD);
			c.setDizzy(Dizzy.DIZZY);
			postEvent(GameEventType.ENTER, c);
		}
		else if (c is Structure)
		{
			structures.Add(c);
			c.setState(CardState.BOARD);
			c.setDizzy(Dizzy.DIZZY);
			postEvent(GameEventType.ENTER, c);
		}
		else if (c is Spell)
		{
			c.setState(CardState.STACK);
			postEvent(GameEventType.ENTER, c);
			foreach (PassiveAbility i in c.getPassives())
			{
				player.addAbilitiesToStack(i,c, targetDict);
			}
		}
		else if (c is Land)
		{
			c.setState(CardState.BOARD);
			postEvent(GameEventType.ENTER, c);
		}

	}

	public void postEvent(int ev, Card c)
	{
		player.postEvent(ev, c);
	}


	//override
	public string toString()
	{
		string s = "Board ==";
		for (int i = 0; i < creatures.Count; i++)
		{
			s = s  + "----- " + creatures[i].toQuickString();
		}

		for (int i = 0; i < structures.Count; i++)
		{
			s = s  + "-----" + structures[i].toQuickString();
		}
		//MyLogger.log("NUM ON BOARD" + (creatures.Count + structures.Count));

		return s;
	}

	public string getDesc()
	{
		string s = "Board ==";
		for (int i = 0; i < creatures.Count; i++)
		{
			s = s  + "\n" + creatures[i].getDesc();
		}

		for (int i = 0; i < structures.Count; i++)
		{
			s = s  + "\n" + structures[i].getDesc();
		}

		return s;
	}


	public Card getCreature(int index)
	{
		if (index < 0 || index >= creatures.Count)
			return null;
		return creatures[index];
	}

	public Card getStructure(int index)
	{
		if (index < 0 || index >= structures.Count)
			return null;
		return structures[index];
	}

	//TODO do i want ot pass in the card itself here? its just status effects.
	public void startTurnStatuses()
	{

		List<Card> deadToDoomS = new List<Card>();
		List<Card> deadToDoomC = new List<Card>();


		foreach (Card c in creatures)
		{
			Creature card = (Creature)c;
			card.setTempAttack(0);
			card.setTempHealth(0);

			List<StatusEffect> regen = c.getStatusEffects(Status.REGEN);
			foreach (StatusEffect statusEff in regen)
			{
				card.regenHealth(statusEff.getCounter(), null);
			}

			List<StatusEffect> fleeting = c.getStatusEffects(Status.FLEETING);
			foreach (StatusEffect statusEff in fleeting)
			{
				if (statusEff.getDuration() <= 0)
				{
					((Creature) c).setDead(true);
					deadToDoomC.Add(c);
				}
			}

			List<StatusEffect> poison = c.getStatusEffects(Status.POISON);
			foreach (StatusEffect statusEff in poison)
			{
				((Creature) c).takeDamage(statusEff.getCounter(), card,null);
			}


			//card.refillCounters();
		}

		foreach (Card c in structures)
		{
			Structure card = (Structure)c;
			card.setTempAttack(0);
			card.setTempHealth(0);

			List<StatusEffect> regen = c.getStatusEffects(Status.REGEN);
			foreach (StatusEffect statusEff in regen)
			{
				card.regenHealth(statusEff.getCounter(), null);
			}

			List<StatusEffect> fleeting = c.getStatusEffects(Status.FLEETING);
			foreach (StatusEffect statusEff in fleeting)
			{
				if (statusEff.getDuration() <= 0)
				{
					((Structure) c).setDead(true);
					deadToDoomS.Add(c);
				}
			}

			List<StatusEffect> poison = c.getStatusEffects(Status.POISON);
			foreach (StatusEffect statusEff in poison)
			{
				((Structure) c).takeDamage(statusEff.getCounter(), card,null);
			}
			//card.refillCounters();
		}


		deleteDead(deadToDoomC, deadToDoomS);
	}

	public void eotCleanup(bool shouldDecay)
	{
		List<Card> deadToDoomS = new List<Card>();
		List<Card> deadToDoomC = new List<Card>();
		foreach (Card c in creatures)
		{
			Creature card = (Creature)c;
			card.setTempAttack(0);
			card.setTempHealth(0);


			if (shouldDecay)
			{
				List<StatusEffect> statuses = c.getStatusEffects(Status.BURN);
				foreach (StatusEffect statusEff in statuses)
				{
					card.takeDamage(statusEff.getCounter(), card,null);
				}

				List<int> Removed = card.decayStatusEffects();
				if (Removed.Contains(Status.DOOM))
				{
					((Creature) c).setDead(true);
					deadToDoomC.Add(c);
				}

			}
			
//			card.decayCounters();

		}
		foreach (Card c in structures)
		{
			Structure card = (Structure)c;
			card.setTempAttack(0);
			card.setTempHealth(0);
			if (shouldDecay)
			{
				List<StatusEffect> statuses = c.getStatusEffects(Status.BURN);
				foreach (StatusEffect statusEff in statuses)
				{
					card.takeDamage(statusEff.getCounter(), card,null);
				}

				List<int> Removed = card.decayStatusEffects();
				if (Removed.Contains(Status.DOOM))
				{
					((Structure) c).setDead(true);
					deadToDoomS.Add(c);
				}
			}
			
			
		//	card.decayCounters();

		}

		deleteDead(deadToDoomC, deadToDoomS);
	}

	List<Card> aboutToDie = new List<Card>();

	private void deleteDead(List<Card> crs, List<Card> strs)
	{
		structures = structures.Except (strs).ToList();
		creatures = creatures.Except(crs).ToList();
		aboutToDie.AddRange(strs);
		aboutToDie.AddRange(crs);
		foreach (Card c in crs)
		{
			MyLogger.log("A " + c.toQuickString() + " died.");
			postEvent(GameEventType.DEATH, c);
		}
		
		foreach (Card c in strs)
		{
			MyLogger.log("A " + c.toQuickString() + " died.");
			postEvent(GameEventType.DEATH, c);
		}

		aboutToDie.Clear();
	}

	public void clearBoard()
	{
		List<Card> deadStuff = new List<Card>();
		foreach (Card c in creatures)
		{
			if ( ((Creature)c).getTotalHealth() <= 0)
			{


				deadStuff.Add(c);
				((Creature)c).setDead(true);

			}
		
		}

		List<Card> deadStuff2 = new List<Card>();
		foreach (Card c in structures)
		{
			if ( ((Structure)c).getTotalHealth() <= 0)
			{

				MyLogger.log("A " + c.toQuickString() + " died.");
				
				((Structure)c).setDead(true);
				deadStuff2.Add(c);
			}
		}

		deleteDead(deadStuff, deadStuff2);
	}

	public void undizzyAll()
	{
		foreach (Card c in creatures)
		{
			c.undizzy();
		}

		foreach (Card c in structures)
		{
			c.undizzy();
		}

	}

	public void addCardToBoard(Card card)
	{
		if (card.getCardType() == CardType.CREATURE || card.getCardType() == CardType.STRUCTURE)
			playCard(card,null);
		else
		{
			MyLogger.log("add card to board requires struct or creature. in board . java");
		}
	}
	
	public void addCardToBoardSilently(Card card)
	{
		if (card is Creature)
		{
			card.setState(CardState.BOARD);
			creatures.Add(card);
		}
		else if (card is Structure)
		{
			card.setState(CardState.BOARD);
			structures.Add(card);
		}
		else if (card is Land)
		{
			card.setState(CardState.BOARD);
			postEvent(GameEventType.ENTER, card);
		}
	}

	public void replaceCard(Card curCard, Card newCard)
	{
		int idx = -1;
		idx = creatures.IndexOf(curCard);
		if (idx >= 0)
		{
			creatures[idx] = newCard;
			curCard.setState(CardState.NONE);
		}
		else
		{
			idx = structures.IndexOf(newCard);
			if (idx >= 0)
			{
				structures[idx] = newCard;
				curCard.setState(CardState.NONE);
			}
			else
			{
				//ERRORRRR
				MyLogger.log("REPLACING NONEXISTANT CARD ?????");
			}
		}



	}

//TODO I FORGOT WHAT SILENT WAS FOR . SILENCE
	public void kill(Card c, bool silent)
	{
		if (creatures.Contains(c))
		{
			List<Card> cards = new List<Card>();
			cards.Add(c);
			deleteDead(cards, new List<Card>());
			/*if (!silent)
			{
				MyLogger.log("A " + c.toQuickString() + " died via kill call.");
				postEvent(GameEventType.DEATH, c);
			}
			creatures.Remove(c);
		*/
		}
		else if (structures.Contains(c))
		{
			List<Card> cards = new List<Card>();
			cards.Add(c);
			deleteDead(new List<Card>(), cards);
			/*
			if (!silent)
			{
				MyLogger.log("A " + c.toQuickString() + " died via kill call.");
				postEvent(GameEventType.DEATH, c);
			}
			structures.Remove(c);
			*/
		}

		//deadStuff.Add(c);

	}

	public ActiveAbility getAbility(int cardType, int slot, int abilityNumber)
	{
		if (cardType == CardType.STRUCTURE)
		{
			Structure c = (Structure)structures[slot];
			if (c.getActives().Count > abilityNumber)
			{
				return c.getActives()[abilityNumber];
			}
		}
		else if (cardType == CardType.CREATURE)
		{
			Creature c = (Creature)creatures[slot];
			if (c.getActives().Count > abilityNumber)
			{
				return c.getActives()[abilityNumber];
			}
		}
		else
		{
			MyLogger.log("ERROR IN ABILITY REQUIRES TARTGGET IN SIDE BOARd.java");
		}
		return null;
	}

	
	public List<Card> getTargetsFor(Card c, TargetType type)
	{
		List<Card> targets = new List<Card>();
		if (type.containsCardType(CardType.STRUCTURE))
		{
			targets.AddRange(structures);
		}

		if (type.containsCardType(CardType.CREATURE))
		{
			targets.AddRange(creatures);
		}
		return targets;
	}

	public Card getCard(int cardType, int slot)
	{
		if (cardType == CardType.STRUCTURE)
		{
			return structures[slot];

		}
		else if (cardType == CardType.CREATURE)
		{
			return creatures[slot];

		}
		else
		{
			MyLogger.log("ERROR IN ABILITY REQUIRES TARTGGET IN SIDE BOARd.java");
		}
		return null;
	}

	public void respondToPhaseChange(int phase, int phaseTeam)
	{
		searchBoardForAbility(phase,phaseTeam);
	}

	public void respondToEvent(ExecutedGameEvent eev)
	{
		searchBoardForAbility(eev);
	}

	private bool searchBoardForAbility(int phase, int phaseTeam)
	{
		bool hasOne = false;
		foreach (Card c in creatures)
		{
			foreach (PassiveAbility i in c.getPassives())
			{
				if (i.respondsToPhase(phase, phaseTeam))
				{
					//Need to request targets!
					if (!c.isSurpressed())
						player.addAbilitiesToStack(i,c, new List<Card>(), i.getAbilityResponseTargetGen());
					hasOne = true;
				}
			}

			foreach (AltAbility i in c.getAlts())
			{
				if (i.respondsToPhase(phase, phaseTeam))
				{
					//Need to request targets!
					if (!c.isSurpressed())
						player.addAbilitiesToStack(i,c, new List<Card>(), i.getAbilityResponseTargetGen());
					hasOne = true;
				}
			}
		}

		foreach (Card c in structures)
		{
			foreach (PassiveAbility i in c.getPassives())
			{
				if (i.respondsToPhase(phase, phaseTeam))
				{
					//Need to request targets!
					if (!c.isSurpressed())
						player.addAbilitiesToStack(i,c, new List<Card>(), i.getAbilityResponseTargetGen());
					hasOne = true;
				}
			}

			foreach (AltAbility i in c.getAlts())
			{
				if (i.respondsToPhase(phase, phaseTeam))
				{
					//Need to request targets!
					if (!c.isSurpressed())
						player.addAbilitiesToStack(i,c, new List<Card>(), i.getAbilityResponseTargetGen());
					hasOne = true;
				}
			}

		}
		return hasOne;
	}

	private bool searchBoardForAbility(ExecutedGameEvent eev)
	{
		bool hasOne = false;
		foreach (Card c in creatures)
		{
			hasOne |= processAbilitiesForCardFromGameEvent(c, c.getPassives(), eev);
			List<PassiveAbility> alts = c.getAlts().ConvertAll(x => (PassiveAbility)x);
			hasOne |= processAbilitiesForCardFromGameEvent(c, alts, eev);
		}

		foreach (Card c in structures)
		{
			hasOne |= processAbilitiesForCardFromGameEvent(c, c.getPassives(), eev);
			List<PassiveAbility> alts = c.getAlts().ConvertAll(x => (PassiveAbility)x);
			hasOne |= processAbilitiesForCardFromGameEvent(c, alts, eev);
		}

		foreach (Card c in aboutToDie)
		{
			hasOne |= processAbilitiesForCardFromGameEvent(c, c.getPassives(), eev);
			List<PassiveAbility> alts = c.getAlts().ConvertAll(x => (PassiveAbility)x);
			hasOne |= processAbilitiesForCardFromGameEvent(c, alts, eev);
		}
		return hasOne;
	}

	private bool processAbilitiesForCardFromGameEvent(Card c, List<PassiveAbility> abilities, ExecutedGameEvent eev)
	{
		bool hasOne = false;
		foreach (PassiveAbility i in abilities)
		{
			if (i.respondsToEvent(eev.gameEventType, eev.origin, eev.recipients, c))
			{
				List<Card> targets = i.selectResponseEventCards(eev.origin, eev.recipients, c);
				player.addAbilitiesToStack(i, c, targets, i.getAbilityResponseTargetGen());
				hasOne = true;
			}
		}
		return hasOne;
	}

	public void sacrifice(List<Card> targets)
	{
		foreach (Card target in targets)
		{
			sacrifice(target);
		}
	}


	public void sacrifice(Card target)
	{
		kill(target, false);
	}

	public bool bounce(Card target) {
		if (target is Creature)
		{
			if(creatures.Remove(target))
			{
				postEvent(GameEventType.LEAVE, target);
				return true;
			}
		}
		else if (target is Structure)
		{
			if(structures.Remove(target))
			{
				postEvent(GameEventType.LEAVE, target);
				return true;
			}
		}
		MyLogger.log("Fail bounce card type?");
		return false;
	}

	public bool exile(Card target) {
		if (target is Creature)
		{
			if(creatures.Remove(target))
			{
				postEvent(GameEventType.LEAVE, target);
				return true;
			}
		}
		else if (target is Structure)
		{
			if(structures.Remove(target))
			{
				postEvent(GameEventType.LEAVE, target);
				return true;
			}
		}
		MyLogger.log("Fail exile card type?");
		return false;
	}

	public List<Card> getAllCards()
	{
		return allCards();
	}
	private List<Card> allCards()
	{
		List<Card> all = new List<Card>();
		all.AddRange (creatures);
		all.AddRange (structures);

		return all;
	}

	public List<Card> getCreatures()
	{
		return creatures;
	}

	public List<Card> getStructures()
	{
		return structures;
	}

	public bool hasCards()
	{
		if (creatures.Count > 0 || structures.Count > 0)
				return true;
		return false;
	}
}
