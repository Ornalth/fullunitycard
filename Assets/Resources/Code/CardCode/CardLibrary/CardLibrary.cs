using System.Collections.Generic;
using System.Collections;
//using LitJson;
//using System.SystemException;
using UnityEngine;
using System;
using System.IO;
using LitJson;
using System.Linq;

//TODO  consider redoing self.


public class CardLibrary
{
	private static readonly string VERSION = "1.0.0";
	private static readonly string JKEY_VERSION = "version";



	private static readonly string JKEY_MAINCARDS = "cards";

	private static readonly string JKEY_ID = "id";
	private static readonly string JKEY_CARDTYPE = "cardtype";
	private static readonly string JKEY_NAME = "name";
	private static readonly string JKEY_FACTIONS = "factions";
	private static readonly string JKEY_COST = "cost";
	private static readonly string JKEY_DESC = "desc";
	private static readonly string JKEY_TYPE = "type";
	private static readonly string JKEY_RARITY = "rarity";
	private static readonly string JKEY_CARD_SET = "set";
	private static readonly string JKEY_ARTIST = "artist";
	private static readonly string JKEY_REFS = "refs";

	private static readonly string JKEY_ATTACK = "attack";
	private static readonly string JKEY_HEALTH = "health";
	private static readonly string JKEY_SALVAGE = "salvage";

	private static readonly string JKEY_STATUS = "status";
	private static readonly string JKEY_STATUSDURATION = "duration";
	private static readonly string JKEY_STATUSCOUNTER = "sCounter";
	private static readonly string JKEY_STATUSES = "statuses";
	private static readonly string JKEY_TARGETTYPE = "targetType";
	private static readonly string JKEY_EXTRACOST = "extracost";

	private static readonly string JKEY_TARGET_LIST = "targetList";
	private static readonly string JKEY_TARGET_LIST_ID = "targListID";
	private static readonly string JKEY_PROPERTIES = "props";

	private static readonly string JKEY_EXTRACOST_PRETARGETTYPES = "preTargetTypes";
	private static readonly string JKEY_EXTRACOST_EFFECTS = "preEffects";

	private static readonly string JKEY_DRAW = "draw";


	private static readonly string JKEY_ABILITY = "abilities";
	private static readonly string JKEY_ABILITY_SPEED = "speed";
	private static readonly string JKEY_ABILITY_UNCOUNTERABLE = "noCounter";
	private static readonly string JKEY_ABILITY_DISPLAY_NAME = "displayName";
	private static readonly string JKEY_ABILITY_REMOVE_SOURCE = "removeSource";
	private static readonly string JKEY_ABILITY_TARGET_GEN = "targetGen";



	private static readonly string JKEY_ABILITY_MODEL_REFS = "effRefs";
	private static readonly string JKEY_ABILITY_PHASE = "phase";
	private static readonly string JKEY_ABILITY_PHASE_EVENT = "event";
	private static readonly string JKEY_ABILITY_PHASE_TEAM = "eventTeam";

	private static readonly string JKEY_ABILITY_GAME_EVENT = "response";
	private static readonly string JKEY_ABILITY_GAME_EVENT_EVENT = "event";
	private static readonly string JKEY_ABILITY_GAME_EVENT_TEAM = "eventTeam";
	private static readonly string JKEY_ABILITY_GAME_EVENT_SELECTOR = "eventSelector";

	private static readonly string JKEY_ACTIVE = "active";
	private static readonly string JKEY_GRAVE_ABIL = "grave";
	private static readonly string JKEY_PASSIVE = "passive";
	private static readonly string JKEY_ALTERNATE = "alternate";
	private static readonly string JKEY_ABILITY_ALTERNATE_OPTION = "alt";

	private static readonly string JKEY_COST_DIZZY = "costdizzy";
	private static readonly string JKEY_REMOVE_STATUS = "remove";

	private static readonly string JKEY_ADD_CARD_TO_ZONE = "addCard";
	private static readonly string JKEY_ZONE = "zone";
	private static readonly string JKEY_ZONE_TARGET_ZONE = "targetZone";
	private static readonly string JKEY_ZONE_TEAM = "team";
	private static readonly string JKEY_PLAYER_LIFE = "playerLife";
	private static readonly string JKEY_OPP_PLAYER_LIFE = "oppPlayerLife";

	private static readonly string JKEY_TARGET_CARD_STATE = "cardState";
	private static readonly string JKEY_TARGET_IDS = "IDS";
	private static readonly string JKEY_TARGET_TEAM = "team";
	private static readonly string JKEY_TARGET_NUMBER = "number";
	private static readonly string JKEY_TARGET_STRICT = "strict";
	private static readonly string JKEY_TARGET_EXTRAS = "extras";
	private static readonly string JKEY_TARGET_STATUSES = "targetStatuses";
	private static readonly string JKEY_TARGET_EXCLUDE_TARGET = "excludeTargetType";
	private static readonly string JKEY_TARGET_DIZZY_STATES = "dizzyStates";

	private static readonly string JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS = "prevSuccess";
	private static readonly string JKEY_ABILITY_EFFECT_TARGET_LIST_NUMBER = "targetListRefNumber";
	private static readonly string JSON_ABILITY_EFFECT_CUSTOM = "custom";
	private static readonly string JKEY_CUSTOM_EFFECT = "customEffects";
	private static readonly string JKEY_CUSTOM_EFFECT_NAME = "uniqueName";
	private static readonly string JKEY_CUSTOM_EFFECT_CLASS = "className";

	private static readonly string JKEY_COUNTER = "counter";
	private static readonly string JKEY_COUNTER_NAME = "name";
	private static readonly string JKEY_COUNTER_CURRENT_VAL = "cur";
	private static readonly string JKEY_COUNTER_MAX_VAL = "max";

	public static readonly string JKEY_UNIQUE_TYPES = "uniqueType";

	private static readonly string JKEY_STATFLAG_STATUSES = "flagStatuses";
	private static readonly string JKEY_STATFLAG_TYPES = "flagNameTypes";


	private static Dictionary<string,string> abilityEffectDict = new Dictionary<string,string>
	{
			{"AddAttackAbility", "attack"},
			{"AddHealthAbility", "health"},
			{"BounceAbility", "bounce"},
			{"CounterTargetAbility", "counterTarget"},
			{"DamageAbility", "damage"},
			{"DestroyAbility", "destroy"},
			{"DirectLifeAbility", JKEY_PLAYER_LIFE},
			{"DiscardAbility", "discard"},
			{"DizzyAbility", "dizzy"},
			{"DrawAbility", JKEY_DRAW},
			{"ExileAbility", "exile"},
			{"MillAbility", "mill"},
			{"NameTypeAbility", "nameType"},
			{"PurgeAbility", "purge"},
			{"RefundAbility", "refund"},
			{"StatusAbility", JKEY_STATUSES},
			{"SummonAbility", "summon"},
			{"ZoneChangeAbility", "zone"},
			{"AddAbility", "extraAbil"},
			{"AddCounterAbility", "addCounter"},
			{"AddCardToZoneAbility", JKEY_ADD_CARD_TO_ZONE}
			//{}
		
	};
	//private static HashMap<int, Card> LIBRARY = new HashMap<int, Card>();

	//Map CardSet -> Rarity -> List<Cards>
	//private static Dictionary<CardSet, <Dictionary<int, List<Card>>> CARD_SET_LIBRARY;
	private static Dictionary<CardSet, Dictionary<Rarity, List<Card>>> CARD_SET_LIBRARY;
	private static Dictionary<int, Card> LIBRARY;
	private static List<int> BASIC_GENERATOR_CARD_IDS;
	private static Card[] LIBRARY_LIST;
	private static bool savedIsResource;
	private static string savedPath;

	public static void initLibrary()
	{
		if (LIBRARY != null && LIBRARY.Keys.Count > 0)
		{
			return;
		}
		UnityEngine.Random.seed = Constants.SEED;
		readJSON(Constants.LIBRARY_NAME, true);
		initLibraryList();
		initCardSetDictionary();
	}

	private static void initLibraryList()
	{
		LIBRARY_LIST = new Card[LIBRARY.Count];
		LIBRARY.Values.CopyTo(LIBRARY_LIST, 0);
	}
	public static Card getCardFromID(int ID)
	{
		Card c;
		if (LIBRARY.TryGetValue(ID, out c))
		{
			return copyCard(c);
		}

		Debug.LogError("ERROR CARD WITH ID " + ID + " DOES NOT EXIST - CARDLIBRARY");
		return null;
	}
	public static Land getPlains()
	{
		Card l;
		if (LIBRARY.TryGetValue(9999, out l))
		{
			return (Land) l;
		}
		return null;//(Land)(copyCard ((Card)LIBRARY [9999]));

		//return null;
	}
	public static Card getCardFromPosition(int position)
	{
		if (position < 0 || position >= LIBRARY.Count)
		{
			MyLogger.log("POsition too big" + position);
			return null;
		}

		// Random generator = new Random();

		// Object randomValue = values[generator.nextInt(values.length)];
		return copyCard((Card) LIBRARY_LIST[position]);
	}
	
	public static int getLibrarySize()
	{
		return LIBRARY.Count;
	}

	//Dont mess with this...
	public static Dictionary<int, Card> getLibrary()
	{
		return LIBRARY;
	}

	public static Card getRandomCard()
	{
		int id = UnityEngine.Random.Range (0, LIBRARY.Count);
		return getCardFromPosition (id);
	}
	public static int getRandomCardID()
	{
		int id = UnityEngine.Random.Range (0, LIBRARY.Count);
		return getCardIDFromPosition (id);
	}
	public static int getCardIDFromPosition(int position)
	{
		if (position < 0 || position >= LIBRARY.Count)
		{
			MyLogger.log("POsition too big" + position);
			return -1;
		}
		// Random generator = new Random();
		
		// Object randomValue = values[generator.nextInt(values.length)];
		return LIBRARY_LIST [position].getID ();
	}

	public static List<int> getAllCardIds()
	{
		List<int> ids = new List<int>();
		foreach (Card c in LIBRARY_LIST)
		{
			ids.Add(c.getID());
		}
		return ids;
	}

	private static Card copyCard(Card c)
	{
		Card newCard = null;
		if (c is Creature)
		{
			newCard = new Creature((Creature) c);
		}
		else if (c is Structure)
		{
			newCard = new Structure((Structure) c);
		}
		else if (c is Spell)
		{
			newCard = new Spell((Spell) c);
		}
		else if (c is Land)
		{
			newCard = new Land((Land) c);
		}
		else
		{
			MyLogger.log("In card library, couldnt copy card? dont know waht type");
		}

		return newCard;
	}

	public static Card getCardFromID(int summonID, bool istoken, Player p, int state)
	{
		Card c = CardLibrary.getCardFromID(summonID);
		c.setToken(istoken);
		c.setState(state);
		c.setTeam(p.getTeam());
		return c;
	}

	private static bool readJSON(string fileName, bool isResource)
	{
		savedPath = fileName;
		savedIsResource = isResource;
		LIBRARY_LIST = null;
		LIBRARY = new Dictionary<int, Card>();
		BASIC_GENERATOR_CARD_IDS = new List<int>();

		try
		{

//			MyLogger.log("HELLO BEGIN READ JsonData");
		//	TextAsset textFile = Resources.Load("LibraryReal.json") as TextAsset; 



			JsonData mainObj = getCurrentFileJsonData(); 
			if (VERSION.Equals(mainObj[JKEY_VERSION]))
			{
				MyLogger.log("PARSING VERSION " + VERSION + " CARDLIBRARY ");
			}
			readCustomAbilityEffects(mainObj);
//			MyLogger.log("HELLO BEGIN PARSE CARD");
			JsonData cardArray = (JsonData) mainObj[JKEY_MAINCARDS];

			for (int i = 0; i < cardArray.Count; i++)
			{
				JsonData cardObj = (JsonData) cardArray[i];
				string newcardtypestring = (string) cardObj[JKEY_CARDTYPE];
				int newcardtype =  CardType.cardTypeFromString(newcardtypestring);
				
				Card card;
				if (newcardtype == CardType.CREATURE)
				{
					card = new Creature();
				}
				else if (newcardtype == CardType.STRUCTURE)
				{
					card = new Structure();
				}
				else if (newcardtype == CardType.SPELL)
				{
					card = new Spell();
				}
				else if (newcardtype == CardType.LAND)
				{
					card = new Land();
				}
				else
				{
					MyLogger.log("ERROR IN PARSER JSON DIDNT FIND CARD TYPE" + " NUMBER WAS " + newcardtype);
					continue;
				}
				int id =(int) cardObj[JKEY_ID];

				card.setID(id);
				
				if (Constants.SHOULD_OUTPUT_PARSE_ID)
				MyLogger.log("PARSING ID" + id + " " + cardObj[JKEY_NAME]);

				card.setName((string)cardObj[JKEY_NAME]);
				//MyLogger.log(cardObj["name"]);
				if (JsonDataContainsKey(cardObj, JKEY_RARITY))
					card.setRarity((int) cardObj[JKEY_RARITY]);
				if (JsonDataContainsKey(cardObj, JKEY_CARD_SET))
					card.setCardSet(CardSetConverter.cardSetFromString((string)cardObj[JKEY_CARD_SET]));
				if (JsonDataContainsKey(cardObj, JKEY_ARTIST))
					card.setArtistName((string)cardObj[JKEY_ARTIST]);
				JsonData factionsArray = ((JsonData) cardObj[JKEY_FACTIONS]);
				int factions = 0;
				for (int j = 0; j < factionsArray.Count; j++)
				{
					string factionStr = (string) factionsArray[j];
					factions |= (Faction.stringToFaction(factionStr));
				}
				card.setFactions(factions);

				JsonData costArray = cardObj[JKEY_COST];
				int[] cost = new int[Constants.NUM_REZ];
				for (int j = 0; j < Constants.NUM_REZ; j++)
				{
					cost[j] =(int) costArray[j];
				}
				card.setCost(cost);

				if (card is CombatCard)
				{

					if (JsonDataContainsKey(cardObj, JKEY_SALVAGE))
					{
						JsonData salvageArray = cardObj[JKEY_SALVAGE];
						int[] salvage = new int[Constants.NUM_REZ];
						for (int k = 0; k < Constants.NUM_REZ; k++)
						{
							salvage[k] =(int) salvageArray[k];
						}
						//MyLogger.log(salvage[0]);
						((CombatCard)card).setSalvageValue(salvage);
					}
					else
					{
						MyLogger.log("HOW DOESBOARD CARD NOT HAVE SALVAGE VALUE????" + id);
						throw new SystemException(); //TODO mark salvages.
					}

					if (JsonDataContainsKey(cardObj, JKEY_UNIQUE_TYPES))
					{
						JsonData uNameTypesArray = (JsonData) cardObj[JKEY_UNIQUE_TYPES];
						
						List<string> typeAr = new List<string>();
						for (int j = 0; j < uNameTypesArray.Count; j++)
						{
							typeAr.Add((string) uNameTypesArray[j]);
						}
						((CombatCard)card).setUniqueNameType(typeAr);
					}

				}

				
				if (JsonDataContainsKey(cardObj, JKEY_ATTACK))
				{
					if (card is Creature)
					{
						((Creature) card).setAttack((int) cardObj[JKEY_ATTACK]);
					}
					else if (card is Structure)
					{
						((Structure) card).setAttack((int) cardObj[JKEY_ATTACK]);
					}
				}
				if (JsonDataContainsKey(cardObj, JKEY_HEALTH))
				{
					if (card is Creature)
					{
						((Creature) card).setHealth((int)cardObj[JKEY_HEALTH]);
					}
					else if (card is Structure)
					{
						((Structure) card).setHealth((int)cardObj[JKEY_HEALTH]);
					}
				}

				if (JsonDataContainsKey(cardObj, JKEY_STATUSES))
				{
					JsonData statusArray = ((JsonData) cardObj
					                         [JKEY_STATUSES]);
					for (int j = 0; j < statusArray.Count; j++)
					{
						JsonData statusObj = (JsonData) statusArray[j];
						int  s = Status.statusFromString((string) statusObj
						                                 [JKEY_STATUS]);
						int duration = -1;
						int counter = -1;
						if (JsonDataContainsKey(statusObj, JKEY_STATUSDURATION))
						{
							duration = (int)statusObj[JKEY_STATUSDURATION];
						}
						
						if (JsonDataContainsKey(statusObj, JKEY_STATUSCOUNTER))
						{
							counter =  (int)statusObj[JKEY_STATUSCOUNTER];
						}
						
						StatusEffect eff = new StatusEffect(s, duration,
						                                    counter);
						card.addStatusEffect(eff);
						
					}
					card.setFactions(factions);
				}
				
				if (JsonDataContainsKey(cardObj, JKEY_COUNTER))
				{
					JsonData counterArray = (JsonData) cardObj[JKEY_COUNTER];
					List<Counter> counters = new List<Counter>();
					for (int k = 0; k < counterArray.Count; k++)
					{
						JsonData counterNode = (JsonData)counterArray[k];
					
						Counter counter = new Counter();
						if (JsonDataContainsKey(counterNode, JKEY_COUNTER_NAME))
						{
							counter.setName((string)counterNode[JKEY_COUNTER_NAME]);
						}
						if (JsonDataContainsKey(counterNode, JKEY_COUNTER_MAX_VAL))
						{
							counter.setMaxCount((int) counterNode[JKEY_COUNTER_MAX_VAL]);
						}
						if (JsonDataContainsKey(counterNode, JKEY_COUNTER_CURRENT_VAL))
						{
							counter.setCount((int) counterNode[JKEY_COUNTER_CURRENT_VAL]);
						}
						counters.Add(counter);
					}
					card.setCounters(counters);
				}


				if (JsonDataContainsKey(cardObj, JKEY_DESC))
				{
					card.setDesc((string)cardObj[JKEY_DESC]);
				}
				
				if (JsonDataContainsKey(cardObj, JKEY_TYPE))
				{
					JsonData nameTypesArray = (JsonData) cardObj[JKEY_TYPE];
					
					List<string> typeAr = new List<string>();
					for (int j = 0; j < nameTypesArray.Count; j++)
					{
						typeAr.Add((string) nameTypesArray[j]);
					}

					if (typeAr.Contains(Constants.KEYWORD_BASIC_GENERATOR))
					{
						BASIC_GENERATOR_CARD_IDS.Add(card.getID());
//						MyLogger.log("THIS CARD HAS BASIC" + card.getID());
					}

					card.setNameType(typeAr);
				}

				if (JsonDataContainsKey(cardObj, JKEY_EXTRACOST_PRETARGETTYPES))
				{
					//MyLogger.log("PRE !!!TARGETTING");
					JsonData jTargetListArray = (JsonData) cardObj[JKEY_EXTRACOST_PRETARGETTYPES];
					List<TargetListObject> targetListObjs = new List<TargetListObject>();
					for (int j = 0; j < jTargetListArray.Count; j++)
					{
						JsonData targetListNode = (JsonData) jTargetListArray[j];
						targetListObjs.Add(parseTargetListObject(targetListNode));
					}
					card.setExtraCostTargetTypes(targetListObjs);

				}

				if (JsonDataContainsKey(cardObj, JKEY_EXTRACOST_EFFECTS))
				{
					//MyLogger.log("PRE !!!EFFECTS!!!");
					List<PreAbilityObject> preAbilityObjs = new List<PreAbilityObject>();
					
					JsonData jPreAbilityArray = (JsonData) cardObj[JKEY_EXTRACOST_EFFECTS];
					for (int j = 0; j < jPreAbilityArray.Count; j++)
					{

						JsonData preAbilityNode = (JsonData) jPreAbilityArray[j];
						PreAbilityObject preObj = parsePreAbilityObject(preAbilityNode);
						preAbilityObjs.Add(preObj);
					}
					card.setExtraCostEffects(preAbilityObjs);
				}
				
				/*
				if (JsonDataContainsKey(cardObj, JKEY_EXTRACOST))
				{
					JsonData extracostObj = (JsonData) cardObj
						[JKEY_EXTRACOST];
					int extracosttype = ExtraCost
						.stringToExtraCost((string) extracostObj
						                   ["costtype"]);
					card.setExtraCost(extracosttype);
					
					JsonData extratargetObj = (JsonData) extracostObj
						[JKEY_TARGETTYPE];
					TargetType targType = parseTargetType(extratargetObj);
					
					if (extracosttype == ExtraCost.COST_SAC)
					{
						targType.setCardState(CardState.BOARD);
						targType.setTargetTeam(TargetTeam.TEAM_ALLY);
					}
					else if (extracosttype == ExtraCost.COST_DISCARD)
					{
						targType.setCardState(CardState.HAND);
						targType.setTargetTeam(TargetTeam.TEAM_ALLY);
					}
					else if (extracosttype == ExtraCost.COST_MILL)
					{
						targType.setCardState(CardState.DECK);
						targType.setTargetTeam(TargetTeam.TEAM_ALLY);
					}
					else if (extracosttype == ExtraCost.COST_SAC_SELF)
					{
						// targType.setCardState(CardState.BOARD);
						targType.setTargetTeam(TargetTeam.TEAM_NONE); // YOU DO
						// NOT
						// NEED
						// A
						// TARGETTYPE
						// IF
						// SELF
					}
					
					card.setExtraCostTargetType(targType);
				}
*/


				if (JsonDataContainsKey(cardObj, JKEY_ABILITY))
				{
//					MyLogger.log("PARSE ABIL");
					JsonData abilArray = (JsonData) cardObj[JKEY_ABILITY];
					for (int j = 0; j < abilArray.Count; j++)
					{
						JsonData abilObj = (JsonData) abilArray[j];

						Ability abil = parseAbility(null, abilObj);

						card.addAbilityDuringCreation(abil);
						/*
						if (abil is AltAbility)
						{
							card.addAlt(abil);
						}
						else if (abil is ActiveAbility)
						{
							card.addActive(abil);
						}
						else if (abil is PassiveAbility)
						{
							card.addPassive(abil);
						}
						*/
					}
				}
				
				// !!! here assert change
				//assert (LIBRARY[card.getID()));
				if (LIBRARY.ContainsKey(card.getID()))
				{
					MyLogger.log("???? ERROR CARD ID " + card.getID ());
					throw new SystemException();
				}

				LIBRARY[card.getID()] = card;
				//LIBRARY.put(card.getID(), card);
			}
			
		}
		catch (SystemException err)// (System.Exception ex)//(System.Exception ex)
		{
			MyLogger.log("ERROR IN CARDLIBRARY PARSE");
			MyLogger.log(err.Message);
			MyLogger.log(err.StackTrace);
			return false;
		}

		return true;
		
	}

	private static TargetListObject parseTargetListObject(JsonData targetListNode)
	{
		TargetListObject targetListObj = new TargetListObject();
		JsonData targetTypeNode = (JsonData) targetListNode[JKEY_TARGETTYPE];
		targetListObj.targetType = parseTargetType(targetTypeNode);
		if (JsonDataContainsKey(targetListNode, JKEY_DESC))
		{
			targetListObj.desc = (string) targetListNode[JKEY_DESC];
		}
		if (JsonDataContainsKey(targetListNode, JKEY_ABILITY_TARGET_GEN))
		{
			int targGen = TargetGeneration.targetGenerationFromString((string)targetListNode[JKEY_ABILITY_TARGET_GEN]);
			targetListObj.targetGen = targGen;
		}

		if (JsonDataContainsKey(targetListNode, JKEY_PROPERTIES))
		{
			JsonData jPropertiesArray = (JsonData) targetListNode[JKEY_PROPERTIES];
			int prop = TargetListObject.PROPERTIES_NONE;
			for (int k = 0; k < jPropertiesArray.Count; k++)
			{
				prop |= TargetListObject.propertiesFromString((string)jPropertiesArray[k]);
			}
			targetListObj.properties = prop;
		}
		targetListObj.id =(int) targetListNode[JKEY_TARGET_LIST_ID];
		return targetListObj;
	}
	private static bool readCustomAbilityEffects(JsonData mainObj)
	{
		CustomAbilityEffectManager mgr = CustomAbilityEffectManager.getInstance();
		Dictionary<string,Type> dict = new Dictionary<string, Type>();
		try
		{
			JsonData jAbilityArray = (JsonData) mainObj[JKEY_CUSTOM_EFFECT];

			for (int i = 0; i < jAbilityArray.Count; i++)
			{
				JsonData abilityObj = (JsonData) jAbilityArray[i];
				string abilName = (string)abilityObj[JKEY_CUSTOM_EFFECT_NAME];
				string className = (string)abilityObj[JKEY_CUSTOM_EFFECT_CLASS];
				Type typeName = Type.GetType(className);
				dict[abilName] = typeName;
			}
		}
		catch (SystemException err)// (System.Exception ex)//(System.Exception ex)
		{
			MyLogger.log("ERROR IN CARDLIBRARY PARSE CUSTOM ABILITIES ");
			MyLogger.log(err.Message);
			MyLogger.log(err.StackTrace);
			return false;
		}
		
		mgr.setAbilDictionary(dict);
		return true;
	}
	private static PreAbilityObject parsePreAbilityObject(JsonData preAbilityNode)
	{
		PreAbilityObject preObj = new PreAbilityObject();

		JsonData effectsNode = (JsonData) preAbilityNode["theEffects"];
		List<AbilityEffect> abilEffs = new List<AbilityEffect>();
		for (int k = 0; k < effectsNode.Count; k++)
		{
			AbilityEffect abilEff = parseAbilityEffect(effectsNode[k]);
			if (!(abilEff is IPreAbilityEffect))
			{
				//MyLogger.log("ERROR! This ability effect does not have IPreAbilityEffect");
			}
			abilEffs.Add(abilEff);
		}
		
		preObj.effects = abilEffs;
	
		if (JsonDataContainsKey(preAbilityNode, JKEY_DESC))
		{
			preObj.desc = (string) preAbilityNode[JKEY_DESC];
		}
		if (JsonDataContainsKey(preAbilityNode, "targetListRefNumber"))
		{
			preObj.targetListNum = (int)preAbilityNode["targetListRefNumber"];
		}
		return preObj;
	}
//Return the new ability created.
	//origin instead abil,
	//Create new ability based on type inside.
	private static Ability parseAbility(Ability origin, JsonData abilObj)
	{
		Ability abil;


		if (JsonDataContainsKey(abilObj, JKEY_ACTIVE))
		{
			abil = new ActiveAbility();
		}
		else if (JsonDataContainsKey(abilObj, JKEY_ALTERNATE))
		{
			abil = new AltAbility();
		}
		else if (JsonDataContainsKey(abilObj, JKEY_GRAVE_ABIL)) 
		{
			abil = new GraveAbility();
		}
		else
		{
			abil = new PassiveAbility();
		}			
			
		PassiveAbility hackAbility = (PassiveAbility) abil;	

		abil.setDesc((string) abilObj[JKEY_DESC]);
		abil.setAbilitySpeed(AbilitySpeed.speedFromString((string)abilObj[JKEY_ABILITY_SPEED]));
//MyLogger.log("MODEL DATA REFS");
		if (JsonDataContainsKey(abilObj, JKEY_ABILITY_MODEL_REFS))
		{
			JsonData effRefArray = ((JsonData) abilObj[JKEY_ABILITY_MODEL_REFS]);
			List<AbilityDataHelper> dataHelpers = new List<AbilityDataHelper>();
			for (int j = 0; j < effRefArray.Count; j++)
			{
				dataHelpers.Add(parseAbilityDataHelper(effRefArray[j]));
			}
			hackAbility.setModelData(dataHelpers);
		}


		if (JsonDataContainsKey(abilObj, JKEY_ABILITY_REMOVE_SOURCE))
		{
			abil.shouldIgnoreSource((bool) abilObj[JKEY_ABILITY_REMOVE_SOURCE]);
		}

		if (JsonDataContainsKey(abilObj, JKEY_ABILITY_DISPLAY_NAME))
		{
			abil.setDisplayName((string)abilObj[JKEY_ABILITY_DISPLAY_NAME]);
		}
		
		if (JsonDataContainsKey(abilObj, JKEY_COST))
		{
			JsonData costArray = ((JsonData) abilObj[JKEY_COST]);
			int[] cost = new int[Constants.NUM_REZ];
			for (int j = 0; j < Constants.NUM_REZ; j++)
			{
				cost[j] =(int) costArray[j];
			}
			if (abil is ActiveAbility)
			{
				((ActiveAbility) abil).setCost(cost);
			}
			else
			{
				MyLogger.log("ERROR? or did you parse a cost in alt?");
				//abil.setAltCost(cost);
			}
		}
		if (JsonDataContainsKey(abilObj, JKEY_COST_DIZZY))
		{
			int dizzycost = Dizzy.dizzyFactorFromString((string) abilObj
			                                            [JKEY_COST_DIZZY]);
			if (abil is ActiveAbility)
			{
				
				((ActiveAbility) abil).setDizzyCost(dizzycost);
				
			}
			else
			{
				//abil.setAltDizzyCost(dizzycost);
				MyLogger.log("ERROR? or did you parse a cost in alt?");
			}
		}
		if (JsonDataContainsKey(abilObj, JKEY_EXTRACOST_EFFECTS))
		{
//			MyLogger.log("PRE !!!EFFECTS!!! IN ABILITY");
			List<PreAbilityObject> preAbilityObjs = new List<PreAbilityObject>();
			
			JsonData jPreAbilityArray = (JsonData) abilObj[JKEY_EXTRACOST_EFFECTS];
			for (int j = 0; j < jPreAbilityArray.Count; j++)
			{
				PreAbilityObject preObj = new PreAbilityObject();
				JsonData preAbilityNode = (JsonData) jPreAbilityArray[j];
				JsonData effectsNode = (JsonData) preAbilityNode["theEffects"];
				List<AbilityEffect> abilEffs = new List<AbilityEffect>();
				for (int k = 0; k < effectsNode.Count; k++)
				{
					AbilityEffect abilEff = parseAbilityEffect(effectsNode[k]);
					if (!(abilEff is IPreAbilityEffect))
					{
						MyLogger.log("ERROR! This ability effect does not have IPreAbilityEffect");
					}
					abilEffs.Add(abilEff);
				}
				
				preObj.effects = abilEffs;
			
				if (JsonDataContainsKey(preAbilityNode, JKEY_DESC))
				{
					preObj.desc = (string) preAbilityNode[JKEY_DESC];
				}
				if (JsonDataContainsKey(preAbilityNode, "targetListRefNumber"))
				{
					preObj.targetListNum = (int)preAbilityNode["targetListRefNumber"];
				}
				preAbilityObjs.Add(preObj);
			}
			abil.setExtraCostEffects(preAbilityObjs);
		}
		/*
		if (JsonDataContainsKey(abilObj, JKEY_EXTRACOST))
		{
			JsonData abilExtraCostObj = (JsonData) abilObj[JKEY_EXTRACOST];
			int extracosttype = ExtraCost
				.stringToExtraCost((string) abilExtraCostObj
				                   ["costtype"]);
			
			JsonData extratargetObj = (JsonData) abilExtraCostObj
				[JKEY_TARGETTYPE];
			TargetType targType = parseTargetType(extratargetObj);
			
			if (extracosttype == ExtraCost.COST_SAC)
			{
				targType.setCardState(CardState.BOARD);
				targType.setTargetTeam(TargetTeam.TEAM_ALLY);
			}
			else if (extracosttype == ExtraCost.COST_DISCARD)
			{
				targType.setCardState(CardState.HAND);
				targType.setTargetTeam(TargetTeam.TEAM_ALLY);
			}
			else if (extracosttype == ExtraCost.COST_MILL)
			{
				targType.setCardState(CardState.DECK);
				targType.setTargetTeam(TargetTeam.TEAM_ALLY);
			}
			else if (extracosttype == ExtraCost.COST_SAC_SELF)
			{
				targType.setCardState(CardState.BOARD);
				targType.setTargetTeam(TargetTeam.TEAM_ALLY);
			}
			
			if (abil is ActiveAbility)
			{
				((ActiveAbility) abil).setExtraCost(extracosttype);
				((ActiveAbility) abil).setExtraCostTargetType(targType);
			}
			else
			{
			//	abil.setAltExtraCost(extracosttype);
			//	abil.setAltExtraCostTargetType(targType);
				MyLogger.log("ERROR? or did you parse a cost in alt?");
			}
			
		}*/

		if (JsonDataContainsKey(abilObj, JKEY_ABILITY_PHASE))
		{
			JsonData phaseObj = (JsonData) abilObj[JKEY_ABILITY_PHASE];
			int phase = Phase.phaseFromString((string) phaseObj[JKEY_ABILITY_PHASE_EVENT]);
			((PassiveAbility)abil).setPhase(phase);
			if (JsonDataContainsKey(phaseObj, JKEY_ABILITY_PHASE_TEAM))
			{
				int phaseTeam = PhaseTeam.phaseTeamFromString((string) phaseObj
				                                              [JKEY_ABILITY_PHASE_TEAM]);
				((PassiveAbility)abil).setPhaseTeam(phaseTeam);
			}

			if (JsonDataContainsKey(phaseObj, JKEY_TARGETTYPE))
			{
				JsonData targetObj = (JsonData) phaseObj[JKEY_TARGETTYPE];
				TargetType targType = parseTargetType(targetObj);
				
				((PassiveAbility)abil).setEventTargetType(targType);
			}

			
		}
		else if (JsonDataContainsKey(abilObj, JKEY_ABILITY_GAME_EVENT))
		{
			
			JsonData responseObj = (JsonData) abilObj[JKEY_ABILITY_GAME_EVENT];
			int response;
			response = GameEventType.eventTypeFromString((string) responseObj
			                                             [JKEY_ABILITY_GAME_EVENT_EVENT]);
			((PassiveAbility)abil).setEventResponseType(response);
			
			if (JsonDataContainsKey(responseObj, JKEY_ABILITY_GAME_EVENT_TEAM))
			{
				((PassiveAbility)abil).setEventTargetTeam(TargetTeam
				                        .targetTeamFromString((string) responseObj
				                      [JKEY_ABILITY_GAME_EVENT_TEAM]));
			}

			if (JsonDataContainsKey(responseObj, JKEY_TARGETTYPE))
			{
				JsonData targetObj = (JsonData) responseObj[JKEY_TARGETTYPE];
				TargetType targType = parseTargetType(targetObj);
				
				((PassiveAbility)abil).setEventTargetType(targType);
			}

			if (JsonDataContainsKey(responseObj, JKEY_ABILITY_GAME_EVENT_SELECTOR))
			{
				JsonData eventSelectorNames = (JsonData) responseObj[JKEY_ABILITY_GAME_EVENT_SELECTOR];
				
				int selector = EventResponseSelector.NONE;
				for (int j = 0; j < eventSelectorNames.Count; j++)
				{
					selector |= EventResponseSelector.eventResponseSelectorFromString((string)eventSelectorNames[j]);
				}
				((PassiveAbility)abil).setEventResponseSelector(selector);
			}
		}
		/*
		if (JsonDataContainsKey(abilObj, JKEY_TARGETTYPE))
		{
			JsonData targetObj = (JsonData) abilObj[JKEY_TARGETTYPE];
			TargetType targType = parseTargetType(targetObj);
			
			abil.setTargetType(targType);
		}
		*/
		if (JsonDataContainsKey(abilObj, JKEY_ABILITY_ALTERNATE_OPTION))
		{
			if (abil is AltAbility)
			{
				JsonData altAbilObj = (JsonData) abilObj[JKEY_ABILITY_ALTERNATE_OPTION];

				for (int ii = 0; ii < altAbilObj.Count; ii++)
				{
					JsonData altNode = altAbilObj[ii];
					//ActiveAbility alt = new ActiveAbility();
					Ability altAbil = parseAbility(null, altNode);
					//
					//TODO CHECK IF ACTIVE OR ALT OR ELSE FAIL!
				
					((AltAbility)abil).addAltAbility((ActiveAbility)altAbil);
				}
			}
			else
			{
				MyLogger.log("??? IS THIS NOT AN ALT");
			}
		}


		if (JsonDataContainsKey(abilObj, "postEffects"))
		{
			List<PreAbilityObject> preAbilityObjs = new List<PreAbilityObject>();
			
			JsonData jPreAbilityArray = (JsonData) abilObj["postEffects"];
			for (int j = 0; j < jPreAbilityArray.Count; j++)
			{

				JsonData preAbilityNode = (JsonData) jPreAbilityArray[j];
				PreAbilityObject preObj = parsePreAbilityObject(preAbilityNode);
				preAbilityObjs.Add(preObj);
			}

			hackAbility.setAbilityEffects(preAbilityObjs);
		}

		if (JsonDataContainsKey(abilObj, JKEY_TARGET_LIST))
		{
			JsonData jTargetListArray = (JsonData) abilObj["targetList"];
			List<TargetListObject> targetListObjs = new List<TargetListObject>();
			for (int j = 0; j < jTargetListArray.Count; j++)
			{
				JsonData targetListNode = (JsonData) jTargetListArray[j];
				targetListObjs.Add(parseTargetListObject(targetListNode));
			}
			hackAbility.setTargetTypeList(targetListObjs);

		}


//MyLogger.log("BEGIN ALT ABILITY");
		
//MyLogger.log("END ALT ABILITY");
		//HMM TODO//
		if (JsonDataContainsKey(abilObj, "counterCost"))
		{
			if (abil is ActiveAbility)
			{
				List<CounterCost> counterCostArray = new List<CounterCost>();
				JsonData jCounterCostArray = (JsonData) abilObj["counterCost"];
				for (int k = 0;  k < jCounterCostArray.Count; k++)
				{
					JsonData node = (JsonData) jCounterCostArray[k];
					CounterCost cCost = new CounterCost();

					if (JsonDataContainsKey(node, "name"))
					{
						cCost.name = (string)node["name"];
					} 
					if (JsonDataContainsKey(node, "cost"))
					{
						cCost.cost = (int)node["cost"];
					}
					counterCostArray.Add(cCost);
				}
				((ActiveAbility) abil).setCounterCost( counterCostArray);
			}
			else
			{
				MyLogger.log("Non-Active ability cannot have counter cost (USE DECAY)");
			}
		}
		
		//TODO HMMM MAYBE SHOULD BE IN EFFECT!@!$!@$!@#@#@!#!@!!
		/*if (JsonDataContainsKey(abilObj, "eventCounter"))
		{
			abil.setNumEventCountered( abilObj["eventCounter"].AsInt);
		}*/
		
		if (JsonDataContainsKey(abilObj, JKEY_ABILITY_UNCOUNTERABLE))
		{
			abil.setCounterable(false);
		}
		
		//MyLogger.log(abil.getDesc());
		return abil;

	}

	private static AbilityEffect parseAbilityEffect(JsonData effectObj)
	{
		if (JsonDataContainsKey(effectObj, JSON_ABILITY_EFFECT_CUSTOM))
		{
			CustomAbilityEffectManager mgr = CustomAbilityEffectManager.getInstance();
			CustomAbilityEffect abilEff = (CustomAbilityEffect)Activator.CreateInstance(mgr.getAbilityEffect((string)effectObj[JSON_ABILITY_EFFECT_CUSTOM][JKEY_CUSTOM_EFFECT_NAME]));
			abilEff.handleJSON(effectObj[JSON_ABILITY_EFFECT_CUSTOM]);
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		//JsonData effectObj = (JsonData) abilObj["theEffects"];
		if (JsonDataContainsKey(effectObj, "self"))
		{
			MyLogger.log("FOUND SELF OUTSIDE!");
			throw new SystemException();

			
		}



		if (JsonDataContainsKey(effectObj, "addCounter"))
		{
			AddCounterAbility abilEff = new AddCounterAbility();

			abilEff.modelDataRefs = parseModelDataRefs(effectObj["addCounter"]);
			abilEff.setSelf(JsonDataContainsKey(effectObj["addCounter"], "self"));
			if (JsonDataContainsKey(effectObj["addCounter"], "directSet"))
			{
				abilEff.setDirect(true);
			}

			if (JsonDataContainsKey(effectObj["addCounter"], "full"))
			{
				abilEff.setIsFull(true);
			}

			if (JsonDataContainsKey(effectObj["addCounter"], "max"))
			{
				abilEff.setMaxAmount((int)effectObj["addCounter"]["max"]);
			}

			if (JsonDataContainsKey(effectObj["addCounter"], JKEY_COUNTER_NAME))
			{
				abilEff.setCounterName((string)effectObj["addCounter"][JKEY_COUNTER_NAME]);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;

		}
		
		if (JsonDataContainsKey(effectObj, "destroy"))
		{
			DestroyAbility abilEff = new DestroyAbility();
		
		
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["destroy"], JKEY_TARGETTYPE) ? effectObj["destroy"][JKEY_TARGETTYPE] : null);
			abilEff.setSilent((bool)effectObj["destroy"]["silent"]);
			abilEff.setSelf(JsonDataContainsKey(effectObj["destroy"], "self"));
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "dizzyFactor"))
		{
			DizzyAbility abilEff = new DizzyAbility();
		
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj, JKEY_TARGETTYPE) ? effectObj[JKEY_TARGETTYPE] : null);
		

			abilEff.setDizzyFactor(Dizzy.dizzyFactorFromString((string) effectObj["dizzyFactor"]["factor"]));
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "bounce"))
		{
			BounceAbility abilEff = new BounceAbility();

	
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["bounce"], JKEY_TARGETTYPE) ? effectObj["bounce"][JKEY_TARGETTYPE] : null);
			//abilEff.setSelf(JsonDataContainsKey(effectObj, "self"));
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		if (JsonDataContainsKey(effectObj, "counterTarget"))
		{
			CounterTargetAbility abilEff = new CounterTargetAbility();
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "purge"))
		{
			PurgeAbility abilEff = new PurgeAbility();
		
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["purge"], JKEY_TARGETTYPE) ? effectObj["purge"][JKEY_TARGETTYPE] : null);
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}

			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "exile"))
		{
			ExileAbility abilEff = new ExileAbility();
		
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["exile"], JKEY_TARGETTYPE) ? effectObj["exile"][JKEY_TARGETTYPE] : null);
			abilEff.setSelf(JsonDataContainsKey(effectObj["exile"], "self"));
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}

			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "refund"))
		{
			RefundAbility abilEff = new RefundAbility();
			abilEff.modelDataRefs = parseModelDataRefs(effectObj["refund"]);
			abilEff.setSelf(JsonDataContainsKey(effectObj["refund"], "self"));
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["refund"], JKEY_TARGETTYPE) ? effectObj["refund"][JKEY_TARGETTYPE] : null);
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "attack"))
		{
			AddAttackAbility abilEff = new AddAttackAbility();

			abilEff.modelDataRefs = parseModelDataRefs(effectObj["attack"]);
			abilEff.setSelf(JsonDataContainsKey(effectObj["attack"], "self"));
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["attack"], JKEY_TARGETTYPE) ? effectObj["attack"][JKEY_TARGETTYPE] : null);
			if (JsonDataContainsKey(effectObj["attack"], "directSet"))
			{
				abilEff.setDirect(true);
			}
			if (JsonDataContainsKey(effectObj["attack"], "temp"))
			{
					abilEff.setTemp(true);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "health"))
		{
			AddHealthAbility abilEff = new AddHealthAbility();

			abilEff.modelDataRefs = parseModelDataRefs(effectObj["health"]);
			abilEff.setSelf(JsonDataContainsKey(effectObj["health"], "self"));
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["health"], JKEY_TARGETTYPE) ? effectObj["health"][JKEY_TARGETTYPE] : null);
			if (JsonDataContainsKey(effectObj["health"], "directSet"))
			{
				abilEff.setDirect(true);
			}

			if (JsonDataContainsKey(effectObj["health"], "temp"))
			{
					abilEff.setTemp(true);
			}

			if (JsonDataContainsKey(effectObj["health"], "regen"))
			{
				abilEff.setIsRegen(true);
			}
				
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		
		if (JsonDataContainsKey(effectObj, "damage"))
		{
			DamageAbility abilEff = new DamageAbility();

			abilEff.modelDataRefs = parseModelDataRefs(effectObj["damage"]);
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["damage"], JKEY_TARGETTYPE) ? effectObj["damage"][JKEY_TARGETTYPE] : null);
			abilEff.setSelf(JsonDataContainsKey(effectObj["damage"], "self"));
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		

		
		if (JsonDataContainsKey(effectObj, JKEY_DRAW))
		{
			DrawAbility abilEff = new DrawAbility();
			abilEff.setSelf(JsonDataContainsKey(effectObj[JKEY_DRAW], "self"));
			abilEff.modelDataRefs = parseModelDataRefs(effectObj[JKEY_DRAW]);
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj[JKEY_DRAW], JKEY_TARGETTYPE) ? effectObj[JKEY_DRAW][JKEY_TARGETTYPE] : null);
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		if (JsonDataContainsKey(effectObj, "mill"))
		{
			MillAbility abilEff = new MillAbility();
			abilEff.modelDataRefs = parseModelDataRefs(effectObj["mill"]);
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj["mill"], JKEY_TARGETTYPE) ? effectObj["mill"][JKEY_TARGETTYPE] : null);
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
				return abilEff;
		}


		if (JsonDataContainsKey(effectObj, "discard"))
		{
			JsonData discardObj = (JsonData) effectObj["discard"];
			DiscardAbility abilEff = new DiscardAbility();

			abilEff.modelDataRefs = parseModelDataRefs(discardObj);
		
			TargetType testtype = parseTargetType(JsonDataContainsKey(discardObj, JKEY_TARGETTYPE) ? discardObj[JKEY_TARGETTYPE] : null);
//			MyLogger.log("testtype" + testtype.cardStates);
			abilEff.discardTargType =  testtype;
			abilEff.setSelf(JsonDataContainsKey(effectObj["discard"], "self"));
			if (JsonDataContainsKey(discardObj, "strict"))
			{
				abilEff.setDiscardStrict((bool) discardObj["strict"]);
			}

			if (JsonDataContainsKey(discardObj, "random"))
			{
				abilEff.setDiscardRandom((bool) discardObj["random"]);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		if (JsonDataContainsKey(effectObj, "searchDeck"))
		{
			JsonData searchObj = (JsonData) effectObj["searchDeck"];
			SearchDeckAbility abilEff = new SearchDeckAbility();

			abilEff.modelDataRefs = parseModelDataRefs(searchObj);
			abilEff.setSelf(JsonDataContainsKey(effectObj["searchDeck"], "self"));
			TargetType testtype = parseTargetType(JsonDataContainsKey(searchObj, JKEY_TARGETTYPE) ? searchObj[JKEY_TARGETTYPE] : null);
//			MyLogger.log("testtype" + testtype.cardStates);
			abilEff.searchTargType =  testtype;

			if (JsonDataContainsKey(searchObj, "random"))
			{
				abilEff.setDeckRandom((bool) searchObj["random"]);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		//TODO TARGETTYPE?
		if (JsonDataContainsKey(effectObj, "summon"))
		{
			SummonAbility abilEff = new SummonAbility();

			JsonData summonObj = (JsonData) effectObj["summon"];


			abilEff.modelDataRefs = parseModelDataRefs(summonObj);

			if (JsonDataContainsKey(summonObj, "ID"))
			{
				JsonData summonIDArray = (JsonData) summonObj["ID"];
				List<int> ids = new List<int>();
				for (int ii = 0; ii < summonIDArray.Count; ii++)
				{
					ids.Add((int) summonIDArray[ii]);
				}
				abilEff.setSummonIDs(ids);
			}
			
			if (JsonDataContainsKey(summonObj, "attack"))
			{
				abilEff.setSummonAttack();
			}
			
			if (JsonDataContainsKey(summonObj, "health"))
			{
				abilEff.setSummonHealth();
			}
			
			if (JsonDataContainsKey(summonObj, "number"))
			{
				abilEff.setSummonCount( (int)summonObj["number"]);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}
		
		//TODO TARGETTYPE?
		if (JsonDataContainsKey(effectObj, "extraAbil"))
		{
			AddAbility abilEff = new AddAbility();

			JsonData addAbilArray = (JsonData) effectObj["extraAbil"]["newAbils"];
			for (int j = 0; j < addAbilArray.Count; j++)
			{
				JsonData addAbilObj = (JsonData) addAbilArray[j];
				Ability newAbil = parseAbility(null, addAbilObj);
				//card.addAbilityDuringCreation(newAbil);
				abilEff.addNewAbility(newAbil);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
			
		}

		//TODO TARGETTYPE?
		if (JsonDataContainsKey(effectObj, JKEY_STATUSES))
		{
			StatusAbility abilEff = new StatusAbility();
			JsonData statusArray = (JsonData) effectObj[JKEY_STATUSES];
			for (int j = 0; j < statusArray.Count; j++)
			{
				JsonData statusObj = (JsonData) statusArray[j];
				bool remove = false;
				if (JsonDataContainsKey(statusObj, JKEY_REMOVE_STATUS))
				{
					remove = true;
				}
				
				int s = Status.statusFromString((string) statusObj
				                                 [JKEY_STATUS]);
				int duration = -1;
				int counter = -1;
				if (JsonDataContainsKey(statusObj, JKEY_STATUSDURATION))
				{
					duration = (int) statusObj[JKEY_STATUSDURATION];
				}
				
				if (JsonDataContainsKey(statusObj, JKEY_STATUSCOUNTER))
				{
					counter = (int) statusObj[JKEY_STATUSCOUNTER];
				}
				
				StatusEffect eff = new StatusEffect(s, duration, counter);
				if (remove)
				{
					abilEff.addRemoveStatus(eff);
				}
				else
				{
					abilEff.addAddStatus(eff);
				}
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		if (JsonDataContainsKey(effectObj, "nameType"))
		{
			NameTypeAbility abilEff = new NameTypeAbility();
			JsonData nameTypeArray = (JsonData) effectObj["nameType"];
			for (int j = 0; j < nameTypeArray.Count; j++)
			{
				JsonData nameTypeObj = (JsonData) nameTypeArray[j];
				bool remove = false;
				if (JsonDataContainsKey(nameTypeObj, "remove"))
				{
					remove = true;
				}
				
				string s = (string)nameTypeObj["type"];
				
				if (remove)
				{
					abilEff.addRemoveNameType(s);
				}
				else
				{
					abilEff.addAddNameType(s);
				}
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}


		if (JsonDataContainsKey(effectObj, JKEY_ADD_CARD_TO_ZONE))
		{
			AddCardToZoneAbility abilEff = new AddCardToZoneAbility();

			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj[JKEY_ADD_CARD_TO_ZONE], JKEY_TARGETTYPE) ? effectObj[JKEY_ADD_CARD_TO_ZONE][JKEY_TARGETTYPE] : null);
			abilEff.setTargetZone(CardState.cardStateFromString((string)effectObj[JKEY_ADD_CARD_TO_ZONE][JKEY_ZONE_TARGET_ZONE]));


			

			JsonData addCardObj = (JsonData) effectObj[JKEY_ADD_CARD_TO_ZONE];
			//abilEff.setTargetZone(CardState.cardStateFromString((string)zoneObj[JKEY_ZONE_TARGET_ZONE]));
			
			abilEff.modelDataRefs = parseModelDataRefs(addCardObj);
			if (JsonDataContainsKey(addCardObj, JKEY_ZONE_TEAM))
			{
				string teamStr = ((string) addCardObj[JKEY_ZONE_TEAM]);
				int targetTeam = TargetTeam.targetTeamFromString(teamStr);
				// int states = CardState.NONE;
				abilEff.setZoneTargetTeam(targetTeam);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
				return abilEff;
		}

		if (JsonDataContainsKey(effectObj, JKEY_ZONE))
		{
			ZoneChangeAbility abilEff = new ZoneChangeAbility();

				abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj[JKEY_ZONE], JKEY_TARGETTYPE) ? effectObj[JKEY_ZONE][JKEY_TARGETTYPE] : null);
				abilEff.setTargetZone(CardState.cardStateFromString((string)effectObj[JKEY_ZONE][JKEY_ZONE_TARGET_ZONE]));


			

			JsonData zoneObj = (JsonData) effectObj[JKEY_ZONE];
			//abilEff.setTargetZone(CardState.cardStateFromString((string)zoneObj[JKEY_ZONE_TARGET_ZONE]));
			if (JsonDataContainsKey(zoneObj, JKEY_ZONE_TEAM))
			{
				string teamStr = ((string) zoneObj[JKEY_ZONE_TEAM]);
				int targetTeam = TargetTeam.targetTeamFromString(teamStr);
				// int states = CardState.NONE;
				abilEff.setZoneTargetTeam(targetTeam);
			}
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
				return abilEff;
		}

		
		if (JsonDataContainsKey(effectObj, JKEY_PLAYER_LIFE))
		{
			DirectLifeAbility abilEff = new DirectLifeAbility();
			abilEff.modelDataRefs = parseModelDataRefs(effectObj[JKEY_PLAYER_LIFE]);
			abilEff.targType = parseTargetType(JsonDataContainsKey(effectObj[JKEY_PLAYER_LIFE], JKEY_TARGETTYPE) ? effectObj[JKEY_PLAYER_LIFE][JKEY_TARGETTYPE] : null);
			abilEff.setSelf(JsonDataContainsKey(effectObj[JKEY_PLAYER_LIFE], "self"));
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		if (JsonDataContainsKey(effectObj, JKEY_OPP_PLAYER_LIFE))
		{
			DirectLifeAbility abilEff = new DirectLifeAbility();
			abilEff.modelDataRefs = parseModelDataRefs(effectObj[JKEY_OPP_PLAYER_LIFE]);
			abilEff.setIsOpp(true);
			if (JsonDataContainsKey(effectObj, JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS))
			{
				abilEff.setRequirePreviousSuccess((bool) effectObj[JKEY_ABILITY_EFFECT_REQ_PREVIOUS_SUCCESS]);
			}
			return abilEff;
		}

		MyLogger.log("YOU DUN FKED uP");
		return null;
	}

	private static List<int> parseModelDataRefs(JsonData node)
	{
	//MyLogger.log("PARSE MODLE DATA REF");
		List<int> refs = new List<int>();
		if (JsonDataContainsKey(node, JKEY_REFS))
		{
			JsonData refArray = (JsonData) node[JKEY_REFS];
			for (int i = 0; i < refArray.Count; i++)
			{
				refs.Add((int) refArray[i]);
			}
		}

		return refs;
	}
	
	private static AbilityDataHelper parseAbilityDataHelper(JsonData dataObj)
	{
		//MyLogger.log("PARSE MODLE ABILITY DATA HELPER");
		AbilityDataHelper helper = new AbilityDataHelper();
		
		if (JsonDataContainsKey(dataObj, JKEY_TARGETTYPE))
		{
			helper.targetType = parseTargetType(JsonDataContainsKey(dataObj, JKEY_TARGETTYPE) ? dataObj[JKEY_TARGETTYPE] : null);
		}

		if (JsonDataContainsKey(dataObj, "source"))
		{
			helper.source = (int)dataObj["source"];
		}

		if (JsonDataContainsKey(dataObj, "type"))
		{
			helper.typeCounter = (int)dataObj["type"];
		}

		if (JsonDataContainsKey(dataObj, "modifier"))
		{
			helper.modifier = (int)dataObj["modifier"];
		}

		if (JsonDataContainsKey(dataObj, "prevFilter"))
		{
			helper.prevFilter = (int)dataObj["prevFilter"];
		}

		if (JsonDataContainsKey(dataObj, "multiplier"))
		{
			helper.multiplier = (double)dataObj["multiplier"];
		}

		if (JsonDataContainsKey(dataObj, "constant"))
		{
			helper.setConstant((int)dataObj["constant"]);
		}

		if (JsonDataContainsKey(dataObj, "addConstant"))
		{
			helper.setAddConst((int)dataObj["addConstant"]);
		}


		if (JsonDataContainsKey(dataObj, "statFlag") )
		{
			JsonData flagArray = (JsonData) dataObj["statFlag"];
		
			
			for (int j = 0; j < flagArray.Count; j++)
			{
				JsonData flagNode = (JsonData) flagArray[j];
				StatFlags sFlag = new StatFlags();
				int flags = StatFlags.NONE;
				if (JsonDataContainsKey(flagNode, "flag"))
				{
					JsonData strArray = (JsonData)flagNode["flag"];
					for (int k = 0; k < strArray.Count; k++)
					{
						flags |= StatFlags
						.flagFromString((string) strArray[k]);
					}
				}
				sFlag.flags = flags;
				if (JsonDataContainsKey(flagNode, "multiplier"))
				{
					sFlag.multiplier = (double)flagNode["multiplier"];
				}

				if (JsonDataContainsKey(flagNode, JKEY_STATFLAG_STATUSES))
				{
					JsonData statusArray = ((JsonData) flagNode
					                         [JKEY_STATFLAG_STATUSES]);
					List<int> statuses = new List<int>();
					for (int k = 0; k < statusArray.Count; k++)
					{
						String status = (string) statusArray[k];
						int s = Status.statusFromString(status);
						statuses.Add(s);
					}
					sFlag.statuses = statuses;
				}

				if (JsonDataContainsKey(flagNode, JKEY_STATFLAG_TYPES))
				{
					JsonData nameTypesArray = (JsonData) flagNode[JKEY_STATFLAG_TYPES];
					
					List<string> typeAr = new List<string>();
					for (int k = 0; k < nameTypesArray.Count; k++)
					{
						typeAr.Add((string) nameTypesArray[k]);
					}
					sFlag.nameTypes = typeAr;
				}
				
				helper.addStatFlag(sFlag);
			}
		}
		return helper;
	}
	private static TargetType parseTargetType(JsonData targetObj)
	{

		if (targetObj == null)
		{
			return null;
		}	
		
		TargetType targType = new TargetType();
		if (JsonDataContainsKey(targetObj, JKEY_CARDTYPE))
		{
			int cardtypes = CardType.NONE;
			JsonData cardtypearray = (JsonData) targetObj[JKEY_CARDTYPE];
			for (int j = 0; j < cardtypearray.Count; j++)
			{

				string newcardtypestring = (string)cardtypearray[j];
				cardtypes |= CardType.cardTypeFromString(newcardtypestring);
			}
			targType.cardTypes = cardtypes;
		}
		
		if (JsonDataContainsKey(targetObj, JKEY_TARGET_NUMBER))
		{
			targType.numTargets = ((int) targetObj[JKEY_TARGET_NUMBER]);
			//targType.targetUnitType = TargetUnitType.TARGET_UNIT;
			if (JsonDataContainsKey(targetObj, JKEY_TARGET_STRICT))
			{
				targType.strict = StrictType.typeFromString((string) targetObj
				                                            [JKEY_TARGET_STRICT]);
			}
		}

		if (JsonDataContainsKey(targetObj, JKEY_FACTIONS))
		{
			JsonData factionsArray = ((JsonData) targetObj[JKEY_FACTIONS]);
			int factions = 0;
			for (int j = 0; j < factionsArray.Count; j++)
			{
				string factionStr = (string) factionsArray[j];
				factions |= (Faction.stringToFaction(factionStr));
			}
			targType.targetFaction = factions;
		}
		
		if (JsonDataContainsKey(targetObj, JKEY_TARGET_CARD_STATE))
		{
			JsonData stateArray = ((JsonData) targetObj[JKEY_TARGET_CARD_STATE]);
			int states = CardState.NONE;
			for (int j = 0; j < stateArray.Count; j++)
			{
				string stateStr = (string) stateArray[j];
				states |= (CardState.cardStateFromString(stateStr));
			}
			//MyLogger.log("PARSED STATE " + states);
			targType.cardStates = states;
		}
		
		if (JsonDataContainsKey(targetObj, JKEY_TARGET_TEAM))
		{
			string teamStr = ((string) targetObj[JKEY_TARGET_TEAM]);
			int targetTeam = TargetTeam.targetTeamFromString(teamStr);
			// int states = CardState.NONE;
			targType.setTargetTeam(targetTeam);
		}
		
		if (JsonDataContainsKey(targetObj, JKEY_TYPE))
		{
			JsonData nameTypesArray = (JsonData) targetObj[JKEY_TYPE];
			List<string> typeAr = new List<string>();
			for (int j = 0; j < nameTypesArray.Count; j++)
			{
				typeAr.Add((string) nameTypesArray[j]);
			}
			targType.nameTypes = typeAr;
		}
		
		if (JsonDataContainsKey(targetObj, JKEY_TARGET_IDS))
		{
			JsonData idsArray = (JsonData) targetObj[JKEY_TARGET_IDS];
			List<int> idsAr = new List<int>();
			for (int j = 0; j < idsArray.Count; j++)
			{
				idsAr.Add((int) idsArray[j]);
			}
			targType.targetSpecificIDs = idsAr;
		}

		if (JsonDataContainsKey(targetObj, JKEY_TARGET_EXTRAS))
		{
			JsonData extrasArray = ((JsonData) targetObj[JKEY_TARGET_EXTRAS]);
			int extras = TargetExtras.NONE;
			for (int j = 0; j < extrasArray.Count; j++)
			{
				string extrasStr = (string) extrasArray[j];
				extras |= (TargetExtras.extrasFromString(extrasStr));
			}
			targType.extras = extras;
		}

		if (JsonDataContainsKey(targetObj, JKEY_TARGET_STATUSES))
		{
			JsonData statusArray = ((JsonData) targetObj
			                         [JKEY_TARGET_STATUSES]);
			List<int> statuses = new List<int>();
			for (int k = 0; k < statusArray.Count; k++)
			{
				String status = (string) statusArray[k];
				int s = Status.statusFromString(status);
				statuses.Add(s);
			}
			targType.statuses = statuses;
		}

		if (JsonDataContainsKey(targetObj, JKEY_TARGET_EXCLUDE_TARGET))
		{
			TargetType type = parseTargetType(targetObj[JKEY_TARGET_EXCLUDE_TARGET]);
			//MyLogger.log("EXCLUDE TARGET TYPE");
			targType.excludeTargetType = type;
		}

		if (JsonDataContainsKey(targetObj, JKEY_TARGET_DIZZY_STATES))
		{
			JsonData dizzyStateArray = ((JsonData) targetObj[JKEY_TARGET_DIZZY_STATES]);
			int dizzy = 0;
			for (int k = 0; k < dizzyStateArray.Count; k++)
			{
				String dState = (string) dizzyStateArray[k];
				dizzy |= Dizzy.dizzyFromString(dState);
			}
			targType.dizzyStates = dizzy;
		}


		
		return targType;
	}

	private static void initCardSetDictionary()
	{
		MyLogger.log("INIT CARD SET");
		CARD_SET_LIBRARY = new Dictionary<CardSet, Dictionary<Rarity, List<Card>>>();
		foreach (CardSet cSet in Enum.GetValues(typeof(CardSet)))
		{
			Dictionary<Rarity, List<Card>> cardsFromSet = new Dictionary<Rarity, List<Card>>();

			foreach (Rarity rare in Enum.GetValues(typeof(Rarity)))
			{
				cardsFromSet[rare] = new List<Card>();
			}
			CARD_SET_LIBRARY[cSet] = cardsFromSet;
		}

		foreach (Card card in LIBRARY_LIST)
		{
			Dictionary<Rarity, List<Card>> cardsFromSet = CARD_SET_LIBRARY[card.getCardSet()];
			
			List<Card> cards = cardsFromSet[RarityConverter.convertRarity(card.getRarity())];
			cards.Add(card);
			cardsFromSet[RarityConverter.convertRarity(card.getRarity())] = cards;
			CARD_SET_LIBRARY[card.getCardSet()] = cardsFromSet;
		}
	}

	public static List<DeckCardData> getBoosterPackFromSet(CardSet cardSet)
	{
		List<DeckCardData> boosterCards = new List<DeckCardData>();
		Dictionary<Rarity, List<Card>> cardsFromSet = CARD_SET_LIBRARY[cardSet];
		
		int boosterStructureIndex = UnityEngine.Random.Range (0, Constants.GET_BOOSTER_CARD_MAPPING().Count);
		Dictionary<Rarity, int> boosterMapping = Constants.GET_BOOSTER_CARD_MAPPING()[boosterStructureIndex];
		List<Card> selectedCards = new List<Card>();
		foreach (Rarity rarity in boosterMapping.Keys)
		{
			System.Random random = new System.Random();
			List<Card> cards = cardsFromSet[rarity];
			selectedCards.AddRange((cards.OrderBy(x => UnityEngine.Random.Range (0, Int32.MaxValue)).Take(boosterMapping[rarity]).ToList()));
		}
		foreach (Card card in selectedCards)
		{
			DeckCardData cardData = new DeckCardData();
			cardData.cardID = card.getID();
			cardData.amount = 1;
			boosterCards.Add(cardData);
		}

		return boosterCards;
	}
	//PRAGMA MARK - CUSTOM LIBRARY
	
	public static bool initCustomLibrary(string path)
	{
		return readJSON(path, false);
	}

	public static List<int> getBasicGeneratorCardIDs()
	{
		return BASIC_GENERATOR_CARD_IDS;
	}


	public static JsonData convertCardToJSON(Card c)
	{
		JsonData newnode = new JsonData();
	
		//Overwrite.
		/*
		if (LIBRARY.ContainsKey(c.getID()))
		{
			//find node
			//delete node
			
			//Modify. it
		}
		*/

		newnode[JKEY_ID] = c.getID();
		newnode[JKEY_CARDTYPE] = CardType.cardTypeToString(c.getCardType());
		
		newnode[JKEY_ARTIST] = c.getArtistName();
		newnode[JKEY_RARITY] = c.getRarity();
		newnode[JKEY_CARD_SET] = CardSetConverter.cardSetToString(c.getCardSet());

		newnode[JKEY_NAME] = c.getName();



		List<string> factions = Faction.factionToStringArray(c.getFactions());
		JsonData factionArray = new JsonData();
		foreach (string str in factions)
		{
			factionArray[-1] = str;
		}
		newnode[JKEY_FACTIONS] = factionArray;


		int[] costs = c.getCost();
		JsonData costArray = new JsonData();
		foreach (int cost in costs)
		{
			//if (costs[i] != 0)
			costArray[-1] = cost;
		}
		newnode[JKEY_COST] = costArray;
		newnode[JKEY_DESC] = c.getDesc();


		List<string> nameTypes = c.getNameType();
		JsonData nameTypeArray = new JsonData();
		if (nameTypes != null && nameTypes.Count > 0)
		{
			foreach (string nameT in nameTypes)
			{
				nameTypeArray[-1] = nameT;
			}
			newnode[JKEY_TYPE] = nameTypeArray;
		}
		
		
		if(c is CombatCard)
		{
			CombatCard cc = (CombatCard)c;
			newnode[JKEY_ATTACK] = cc.getAttack();
			newnode[JKEY_HEALTH] = cc.getHealth();

			int[] salvages = cc.getSalvageValue();
			JsonData salvageArray = new JsonData();
			foreach (int salvage in salvages)
			{
				//if (costs[i] != 0)
				salvageArray[-1] = salvage;
			}
			newnode[JKEY_SALVAGE] = salvageArray;
			
			
			List<StatusEffect> statuses = cc.getStatuses();
			JsonData statusArray = new JsonData();
			if (statuses != null && statuses.Count > 0)
			{
				foreach (StatusEffect eff in statuses)
				{
					JsonData node = new JsonData();
					node[JKEY_STATUS] = Status.statusToString(eff.getStatus());
					node[JKEY_STATUSDURATION] = eff.getDuration();
					node[JKEY_STATUSCOUNTER] = eff.getCounter();
					statusArray[-1] = node;
				}
				newnode[JKEY_STATUSES] = statusArray;
			}

			List<string> uNameTypes = cc.getUniqueNameType();
			JsonData jUNameTypeArray = new JsonData();
			if (uNameTypes != null && uNameTypes.Count > 0)
			{
				foreach (string nameT in uNameTypes)
				{
					jUNameTypeArray[-1] = nameT;
				}
				newnode[JKEY_UNIQUE_TYPES] = jUNameTypeArray;
			}
			
		}
		

		//TODO COUNTERS
		//TODO EXTRACOST
		
		List<Counter> counters = c.getCounters();
		if (counters != null && counters.Count > 0)
		{
			JsonData jCounterArray = new JsonData();
			foreach (Counter counter in counters)
			{
				JsonData counterNode = new JsonData();
				counterNode[JKEY_COUNTER_CURRENT_VAL] = counter.getCount();
				counterNode[JKEY_COUNTER_MAX_VAL] = counter.getMaxCount();
				counterNode[JKEY_COUNTER_NAME] = counter.getName();
				jCounterArray[-1] = counterNode;
			}
			newnode[JKEY_COUNTER] = jCounterArray;
		}

		List<Ability> abilities = c.getAllAbilities();
		if (abilities.Count > 0)
		{

			JsonData abilityArray = new JsonData();
			foreach (Ability ability in abilities)
			{
				JsonData abilNode = convertAbilityToJSON(ability);
				abilityArray[-1] = abilNode;
			}

			//JsonData mainAbilityNode = new JsonData();
			//mainAbilityNode[JKEY_ABILITY] = abilityArray;
			newnode[JKEY_ABILITY] = abilityArray;
		}
		

		return newnode;
	}

	private static JsonData convertAbilityToJSON(Ability baseAbility)
	{
		JsonData abilityNode = new JsonData();

		if (baseAbility is ActiveAbility)
		{
			abilityNode[JKEY_ACTIVE] = true;
		}
		else if (baseAbility is AltAbility)
		{
			abilityNode[JKEY_ALTERNATE] = true;
		}
		else if (baseAbility is PassiveAbility)
		{

		}
		else
		{
			//JUST CRASH NOW?
			MyLogger.log("CardiLibrary creatying nulll ability.");
		}
		//EASY SHORTCUT PLZ.
		PassiveAbility abil = (PassiveAbility) baseAbility;
		abilityNode[JKEY_DESC] = abil.getDesc();
		abilityNode[JKEY_ABILITY_DISPLAY_NAME] = abil.getDisplayName();
		List<AbilityDataHelper> dataHelpers = abil.getModelData();
		if (dataHelpers != null && dataHelpers.Count > 0)
		{
			JsonData dataHelperArray = new JsonData();
			foreach (AbilityDataHelper helper in dataHelpers)
			{
				dataHelperArray[-1] = convertAbilityDataHelperToJSON(helper);
			}
			abilityNode[JKEY_ABILITY_MODEL_REFS] = dataHelperArray;
		}

		if (abil is ActiveAbility)
		{
			ActiveAbility activeAbil = (ActiveAbility) abil;

			int[] costArray = activeAbil.getCost();
			if (costArray != null && hasValueInArray(costArray))
			{
				JsonData jCostArray = new JsonData();
				for (int i = 0; i < Constants.NUM_REZ; i++)
				{
					jCostArray[-1] = costArray[i];
				}
				abilityNode[JKEY_COST] = jCostArray;
			}

			if (activeAbil.getDizzyCost() != ActiveAbility.DEFAULT_DIZZY_COST)
			{
				string dizzyCost = Dizzy.dizzyFactorToString(activeAbil.getDizzyCost());
				abilityNode[JKEY_COST_DIZZY] = dizzyCost;
			}

			if (activeAbil.getCounterCost().Count > 0)
			{
				List<CounterCost> counterCost = activeAbil.getCounterCost();
				JsonData jCounterCostArray = new JsonData();
				foreach (CounterCost cost in counterCost)
				{
					JsonData costNode = new JsonData();
					costNode["name"] = cost.name;
					costNode["cost"] = cost.cost;
					jCounterCostArray[-1] = costNode;
				}

				abilityNode["counterCost"] = jCounterCostArray;
			}
		}

		if (abil is PassiveAbility)
		{
			PassiveAbility passiveAbil = (PassiveAbility) abil;
			if (passiveAbil.getPhase() != Phase.ERROR)
			{
				JsonData phaseNode = new JsonData();
				string phase = Phase.phaseToString(passiveAbil.getPhase());
				phaseNode[JKEY_ABILITY_PHASE_EVENT] = phase;
				if (passiveAbil.getPhaseTeam() != PassiveAbility.DEFAULT_PHASE_TARGET_TEAM)
				{
					phaseNode[JKEY_ABILITY_PHASE_TEAM] = PhaseTeam.phaseTeamToString(passiveAbil.getPhaseTeam());
				}
				abilityNode[JKEY_ABILITY_PHASE] = phaseNode;
			}

			if (passiveAbil.getEventResponseType() != GameEventType.ERROR)
			{
				JsonData gameEventNode = new JsonData();
				string eventType = GameEventType.eventTypeToString(passiveAbil.getEventResponseType());
				gameEventNode[JKEY_ABILITY_GAME_EVENT_EVENT] = eventType;
				if (passiveAbil.getEventTargetTeam() != PassiveAbility.DEFAULT_EVENT_TARGET_TEAM)
				{
					gameEventNode[JKEY_ABILITY_GAME_EVENT_TEAM] = GameEventType.eventTypeToString(passiveAbil.getEventTargetTeam());
				}
				abilityNode[JKEY_ABILITY_GAME_EVENT] = gameEventNode;
			}
/*
			if (passiveAbil.getTargetType() != null && !passiveAbil.getTargetType().isDefault())
			{
				abilityNode[JKEY_TARGETTYPE] = convertTargetTypeToJSON(passiveAbil.getTargetType()); 
			}
*/
			if (abil.isThisCounterable())
			{
				abilityNode[JKEY_ABILITY_UNCOUNTERABLE] = true;
			}
		}

		if (abil is AltAbility)
		{
			AltAbility altAbil = (AltAbility) abil;
			JsonData jAbilityArray = new JsonData();
			foreach (ActiveAbility aAbil in altAbil.getAlts())
			{
				JsonData abilNode = convertAbilityToJSON(aAbil);
				jAbilityArray[-1] = abilNode;
			}
			abilityNode[JKEY_ABILITY_ALTERNATE_OPTION] = jAbilityArray;
		}

/*		List<AbilityEffect> abilEffs = abil.getAbilityEffects();
		JsonData jAbilityEffectArray = new JsonData();
		foreach(AbilityEffect eff in abilEffs)
		{
			JsonData effNode = convertAbilityEffectToJSON(eff);
			jAbilityEffectArray[-1] = effNode;
		}

*/
		return abilityNode;
	}

	private static JsonData convertAbilityEffectToJSON(AbilityEffect eff)
	{
		JsonData effNode = new JsonData();
		if (eff.targType != null && eff.targType.isDefault())
		{
			effNode[JKEY_TARGETTYPE] = convertTargetTypeToJSON(eff.targType);
		}

		if (eff.modelDataRefs != null && eff.modelDataRefs.Count > 0)
		{
			JsonData refArray = new JsonData();
			foreach (int dataref in eff.modelDataRefs)
			{
				refArray[-1] = dataref;
			}
			effNode[JKEY_REFS] = refArray;
		}

		if (eff is CustomAbilityEffect)
		{
			CustomAbilityEffect abilEff = (CustomAbilityEffect) eff;
			effNode[JKEY_CUSTOM_EFFECT_NAME] =  abilEff.getUniqueName();
			effNode[JKEY_CUSTOM_EFFECT_CLASS] = abilEff.GetType().Name;
			abilEff.handleJSON(effNode);
			JsonData rEffNode = new JsonData();
			rEffNode[JSON_ABILITY_EFFECT_CUSTOM] = effNode; 
			return rEffNode;
			//return effNode;
		}


		if (eff is DestroyAbility)
		{
			if (((DestroyAbility)eff).isThisSilent())
			{
				effNode["silent"] = true;
			}
		}

		if (eff is DizzyAbility)
		{
			effNode["factor"] = Dizzy.dizzyFactorToString(((DizzyAbility)eff).getDizzyFactor());
		}

		if (eff is IDirectSetAbility)
		{
			if (((IDirectSetAbility)eff).isDirect())
			{
				effNode["directSet"] = true;
			}
		}

		if (eff is ITempAddAbility)
		{
			if (((ITempAddAbility)eff).isTemp())
			{
				effNode["temp"] = true;
			}
		}

		if (eff is IFullEffect)
		{
			if (((ITempAddAbility)eff).isTemp())
			{
				effNode["full"] = true;
			}
		}

		if (eff is SummonAbility)
		{
			SummonAbility sumEff = (SummonAbility)eff;
			List<int> ids = sumEff.getSummonIDs();
			JsonData jIDs = new JsonData();
			foreach (int id in ids)
			{
				jIDs[-1] = id;
			}
			effNode["ID"] = jIDs;
		
			if (sumEff.getSummonCount() != SummonAbility.DEFAULT_SUMMON_COUNT)
			{
				effNode["number"] = sumEff.getSummonCount();
			}

			if (sumEff.isSummonAttack())
			{
				effNode["attack"] = sumEff.getSummonAttack();
			}

			if (sumEff.isSummonHealth())
			{
				effNode["health"] = sumEff.getSummonHealth();
			}
		}
		
		if (eff is AddAbility)
		{
			JsonData jAddAbilityArray = new JsonData();
			foreach (Ability abil in ((AddAbility)eff).getAllNewAbilities())
			{
				JsonData newAbilNode = convertAbilityToJSON(abil);
				jAddAbilityArray[-1] = newAbilNode;
			}
			effNode["extraAbil"] = jAddAbilityArray;
		}


		if (eff is StatusAbility)
		{
			MyLogger.log("TODO STATUS CONVERT PROBABLY BROKEN");
			JsonData jStatusArray = new JsonData();
			foreach (StatusEffect sEff in ((StatusAbility)eff).getAddStatus())
			{
				JsonData statusNode = new JsonData();
				statusNode[JKEY_STATUS] = Status.statusToString(sEff.getStatus());
				
				//MAYBE TO DO
				if (sEff.getDuration() != StatusEffect.DEFAULT_DURATION)
				{
					statusNode[JKEY_STATUSDURATION] = sEff.getDuration();
				}

				if (sEff.getCounter() != StatusEffect.DEFAULT_COUNTER)
				{
					statusNode[JKEY_STATUSCOUNTER] = sEff.getCounter();
				}
				jStatusArray[-1] = statusNode;
			}

			foreach (StatusEffect sEff in ((StatusAbility)eff).getRemoveStatus())
			{
				JsonData statusNode = new JsonData();
				statusNode[JKEY_STATUS] = Status.statusToString(sEff.getStatus());
				
				//MAYBE TO DO
				if (sEff.getDuration() != StatusEffect.DEFAULT_DURATION)
				{
					statusNode[JKEY_STATUSDURATION] = sEff.getDuration();
				}

				if (sEff.getCounter() != StatusEffect.DEFAULT_COUNTER)
				{
					statusNode[JKEY_STATUSCOUNTER] = sEff.getCounter();
				}
				statusNode[JKEY_REMOVE_STATUS] = true;
				jStatusArray[-1] = statusNode;
			}
			effNode[JKEY_STATUSES] = jStatusArray;
		}

		if (eff is ZoneChangeAbility)
		{
			//JsonData zoneNode = new JsonData();
			ZoneChangeAbility zEff = (ZoneChangeAbility)eff;
			effNode[JKEY_ZONE_TARGET_ZONE] = CardState.cardStateToString(zEff.getTargetZone());
			if (zEff.getZoneTargetTeam() != ZoneChangeAbility.DEFAULT_TARGET_TEAM)
			{
				effNode[JKEY_ZONE_TEAM] = TargetTeam.targetTeamToString(zEff.getZoneTargetTeam());
			}
		}

		if (eff is DiscardAbility)
		{
			JsonData discardNode = new JsonData();
			DiscardAbility dEff = (DiscardAbility)eff;
			if (dEff.getStrict() != DiscardAbility.DEFAULT_STRICT)
			{
				effNode["strict"] = true;
			}

			if (dEff.getRandom() != DiscardAbility.DEFAULT_RANDOM)
			{
				effNode["random"] = true;
			}
			//TODO!!!!!!!!!!!!!! ALOT MORE TO CONVERT
		}

		if (eff is AddCounterAbility)
		{
			effNode["max"] = ((AddCounterAbility)eff).getMaxAmount();
		}


//THIS MUST BE THE LAST, EFF NODE IS REPLACED!~!!!
		if (eff is ISelfEffect)
		{
			if (((ISelfEffect)eff).getIsSelf())
			{
				JsonData newNode = new JsonData();
				JsonData tempNode = effNode;
				effNode = newNode;
				effNode["self"] = tempNode;
			}
		}

		JsonData realEffNode = new JsonData();
		realEffNode[getAbilityEffectNodeName(eff)] = effNode; 
		return realEffNode;
	}


	private static string getAbilityEffectNodeName(AbilityEffect eff)
	{
		//TODO custom ability stuff.
		string className = eff.GetType().Name;
		return abilityEffectDict[className];
	}
	private static JsonData convertAbilityDataHelperToJSON(AbilityDataHelper helper)
	{
		JsonData node = new JsonData();

		if (helper.source != AbilityDataHelper.DEFAULT_SOURCE)
		{
			node["source"] = helper.source;
		}

		if (helper.typeCounter != AbilityDataHelper.DEFAULT_TYPE_COUNTER)
		{
			node["type"] = helper.typeCounter;
		}

		if (helper.modifier != AbilityDataHelper.DEFAULT_MODIFIER)
		{
			node["modifier"] = helper.modifier;
		}

		if (helper.prevFilter != AbilityDataHelper.DEFAULT_PREV_FILTER)
		{
			node["prevFilter"] = helper.prevFilter;
		}

		if (helper.multiplier != AbilityDataHelper.DEFAULT_MULTIPLIER)
		{
			node["multiplier"] = (float)(helper.multiplier);
		}

		if (helper.constantVal != AbilityDataHelper.DEFAULT_CONSTANT)
		{
			node["constant"] = helper.constantVal;
		}

		if (helper.addConst != AbilityDataHelper.DEFAULT_ADD_CONSTANT)
		{
			node["addConstant"] = helper.addConst;
		}

		List<StatFlags> flags = helper.getStatFlags();
		if (flags != null && flags.Count > 0)
		{
			JsonData flagArray = new JsonData();
			foreach (StatFlags flag in flags)
			{
				JsonData flagNode = new JsonData();
				if (flag.flags != StatFlags.NONE)
				{
					List<string> flagStrArray = flag.flagsToStringArray();
					JsonData jFlagStrArray = new JsonData ();
					foreach (string str in flagStrArray)
					{
						jFlagStrArray[-1] = str;
					}
					flagNode["flag"] = jFlagStrArray;
				}
				
				if (flag.multiplier != StatFlags.DEFAULT_MULTIPLIER)
				{
					flagNode["multiplier"] = (float)(flag.multiplier); 
				}
				flagArray[-1] = flagNode;
			}
		}

		if (helper.targetType != null && helper.targetType.isDefault())
		{
			node[JKEY_TARGETTYPE] = convertTargetTypeToJSON(helper.targetType);
		}
		return node;
	}


//TODO
	public static JsonData convertTargetTypeToJSON(TargetType targType)
	{
		JsonData node = new JsonData();

		if (targType.cardTypes != TargetType.DEFAULT_CARD_TYPE)
		{
			JsonData cardTypeArray = new JsonData();
			List<string> cardTypes = CardType.cardTypeToArray(targType.cardTypes);
			foreach (string cardType in cardTypes)
			{
				cardTypeArray[-1] = cardType;
			}
			node[JKEY_CARDTYPE] = cardTypeArray;
		}

		if (targType.numTargets != TargetType.DEFAULT_NUM_TARGETS)
		{
			node["number"] = targType.numTargets;

			if (targType.strict != TargetType.DEFAULT_STRICT)
			{
				node["strict"] = StrictType.strictToString(targType.strict);
			}
		}

		if (targType.targetFaction != TargetType.DEFAULT_FACTIONS)
		{
			List<string> factions = Faction.factionToStringArray(targType.targetFaction);
			JsonData factionArray = new JsonData();
			foreach (string str in factions)
			{
				factionArray[-1] = str;
			}
			node[JKEY_FACTIONS] = factionArray;
		}

		if (targType.cardStates != TargetType.DEFAULT_CARD_STATES)
		{
			JsonData cardStateArray = new JsonData();
			List<string> cardStates = CardState.cardStateToArray(targType.cardStates);
			foreach (string state in cardStates)
			{
				cardStateArray[-1] = state;
			}
			node[JKEY_TARGET_CARD_STATE] = cardStateArray;
		}

		if (targType.extras != TargetType.DEFAULT_EXTRAS)
		{
			JsonData jExtrasArray = new JsonData();
			List<string> extrasArray = TargetExtras.extrasToArray(targType.extras);
			foreach (string extra in extrasArray)
			{
				jExtrasArray[-1] = extra;
			}
			node[JKEY_TARGET_EXTRAS] = jExtrasArray;
		}

		if (targType.targetToken != TargetType.DEFAULT_TOKEN)
		{
			//TODO
		}

		if (targType.targetTeam != TargetType.DEFAULT_TARGET_TEAM)
		{
			node[JKEY_TARGET_TEAM] = TargetTeam.targetTeamToString(targType.targetTeam);
		}

		if (targType.nameTypes.Count > 0)
		{
			List<string> nameTypes = targType.nameTypes;
			JsonData nameTypeArray = new JsonData();
			if (nameTypes != null && nameTypes.Count > 0)
			{
				foreach (string nameT in nameTypes)
				{
					nameTypeArray[-1] = nameT;
				}
				node[JKEY_TYPE] = nameTypeArray;
			}
		}

		if (targType.targetSpecificIDs.Count > 0)
		{
			List<int> ids = targType.targetSpecificIDs;
			JsonData idArray = new JsonData();
			foreach (int id in idArray)
			{
				//if (costs[i] != 0)
				idArray[-1] = id;
			}
			node[JKEY_TARGET_IDS] = idArray;
		}



		return node;
	}


	private static JsonData getCurrentFileJsonData()
	{
		//MyLogger.log("HELLO FUKE EXISTS" + savedPath);
		string str = null;

		if (savedIsResource)
		{
			TextAsset textFile = (TextAsset)Resources.Load(savedPath, typeof(TextAsset));
			str = textFile.text;
		}
		else
		{

				//MyLogger.log("HELLO FUKE EXISTS" + savedPath);
			if (File.Exists(savedPath))
			{
				// Create a file to write to. 
				//string readText = File.ReadAllText(savedPath);
				//str = readText;

				TextAsset textFile = (TextAsset)Resources.Load(savedPath, typeof(TextAsset));
				str = textFile.text;
				if (str != null)
				{
					MyLogger.log("NOT NULL STR");
				}
			}
		}

		return JsonMapper.ToObject(str);
	}

	public static void saveCards(Card c)
	{
		List<Card> cards = new List<Card>();
		cards.Add(c);
		saveCards(cards);
	}

	public static void saveCards(List<Card> cards)
	{
		List<JsonData> nodeArray = new List<JsonData>();
		foreach (Card card in cards)
		{
			

			if (validateCard(card))
			{
				if (LIBRARY.ContainsKey(card.getID()))
				{
					LIBRARY[card.getID()] = card;
					MyLogger.log("OVERWROTE" + card.getID());
				}
				else
				{
					LIBRARY[card.getID()] = card;
				}
			}
			else
			{

				MyLogger.log("NOT VALID CARD" + card.getID());
				
			}
		}

	}

	public static bool commitCardChanges()
	{
		List<JsonData> nodeArray = new List<JsonData>();
		foreach (int key in LIBRARY.Keys)
		{
			Card card = LIBRARY[key];
			if (validateCard(card))
			{
				JsonData node = convertCardToJSON(card);
				nodeArray.Add(node);
			}
			else
			{

				MyLogger.log("NOT VALID CARD" + card.getID());
				//success = false;
			}
		}

		MyLogger.log("SAVING " + nodeArray.Count + " CARDS TO DB");
		
		return saveToFile(nodeArray);

	}

	private static bool saveToFile(List<JsonData> currentCardNodes)//JsonData node, string filePath)
	{
		JsonData mainObj = getCurrentFileJsonData();
		
		MyLogger.log("MAINOBJ " + mainObj.ToString());
	
		JsonData cardArray = new JsonData();
		foreach (JsonData node in currentCardNodes)
		{
			cardArray[-1] = node;
		}
		mainObj[JKEY_MAINCARDS] = cardArray;
		MyLogger.log("MAINOBJ " + mainObj.ToString());

		MyLogger.log("YOU SHOULDNT BE WRITING AS WEB");

		//TODO FIXME
		#if UNITY_WEBPLAYER
        	throw new SystemException();
        #else
        	File.WriteAllText(Constants.TEMP_CUSTOM_LIB_NAME, mainObj.ToString());
        #endif
		return true;//TODO?
	}


	private static bool validateCard(Card c)
	{
		if (c.getID() <= 0)
		{
			return false;
		}
		//TODO...
		//else if (c.getCardType)
		//else if (c.getName)

		return true;
	}

	private static bool hasValueInArray(int[] ar)
	{
		foreach (int i in ar)
		{
			if (i != 0)
			{
				return true;
			}
		}
		return false;
	}

	/*


    Use JsonData for {} elements

    Use JsonData for [] elements

    Use JSONData(value) for nodes containing data.

	*/

	static public  bool JsonDataContainsKey(JsonData data,string key)
    {
        bool result = false;
        if(data == null)
            return result;
        if(!data.IsObject)
        {
            return result;
        }
        IDictionary tdictionary = data as IDictionary;
        if(tdictionary == null)
            return result;
        if(tdictionary.Contains(key))
        {
            result = true;
        }
        return result;
    }
}