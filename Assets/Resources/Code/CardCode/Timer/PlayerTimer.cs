﻿using UnityEngine;
using System;

//All values in millis.
public class PlayerTimer {

	float mainTime;
	float reserveTime;
	
	public PlayerTimer(float time1, float time2)
	{
		mainTime = time1;
		reserveTime = time2;
	}

	public void setReserveTime(float time)
	{
		reserveTime = time;
	}
	public void addReserveTime(float time)
	{
		reserveTime += time;
	}

	public void setTime(float time)
	{
		this.mainTime = time;
	}

	public void addTime(float time)
	{
		this.mainTime += time;
	}

	public string getTimeString()
	{
		TimeSpan ts = TimeSpan.FromMilliseconds(mainTime);
		return string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
	}

	public string getReserveTimeString()
	{
		TimeSpan ts = TimeSpan.FromMilliseconds(reserveTime);
		return string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
	}

	public float getTime()
	{
		return mainTime;
	}

	public float getReserveTime()
	{
		return reserveTime;
	}
}
