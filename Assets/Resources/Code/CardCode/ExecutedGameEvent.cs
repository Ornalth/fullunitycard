using System.Collections.Generic;

public class ExecutedGameEvent
{
	public int gameEventType;
	public Card origin;
	public Player fromPlayer;
	public List<Card> recipients = new List<Card>();
	public int value = 0;
}
