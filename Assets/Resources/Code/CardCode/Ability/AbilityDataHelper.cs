/*
protected List<AbilityDataHelper> abilityHelperList; // fcn pters that return ints -- getValue()
	protected AbilityResult AbilityResult;
		int abilitySpeed;
	int abilityType;

	*/
using UnityEngine;
using System.Collections.Generic;
public class AbilityDataHelper
{
	//
	public const int SOURCE_MODEL = 1 << 1;
	public const int SOURCE_PREV_RESULT = 1 << 2;
	public const int SOURCE_TARGETS =  1 << 3;
	public const int SOURCE_SELF =  1 << 4;

	//MUTEX
	public const int TYPE_COUNT_NONE = 0;
	public const int TYPE_COUNT_STAT =  1 << 1;
	public const int TYPE_COUNT_COUNT =  1 << 2;

//PREV FILTER
	public const int PREV_RESULT_KILL = 1 << 1;
	public const int PREV_RESULT_DAMAGE =  1 << 2;
	public const int PREV_RESULT_HEAL =  1 << 3;


	//STATFLAGS FILTER

	public const int MODIFIER_REMOVE_SELF =  1 << 1;
	public const int MODIFIER_TARGET_TYPE =  1 << 2;
	public const int MODIFIER_ORIGINAL_COPIES = 1 << 3;

	public static int DEFAULT_SOURCE = 0;
	public static int DEFAULT_TYPE_COUNTER = 0;
	public static int DEFAULT_MODIFIER = 0;
	public static int DEFAULT_PREV_FILTER = 0;
	public static int DEFAULT_CONSTANT = 0;
	public static int DEFAULT_ADD_CONSTANT = 0;
	public static double DEFAULT_MULTIPLIER = 1.0;
//OUTSIDE SETTERS
	public TargetType targetType;
	public int source;
	public int typeCounter;
	public List<StatFlags> flags;
	public int modifier;
	public int prevFilter;

	public int addConst = 0;

	//For Avg.
	public int numHit = 0;
	public int value = 0;
	public bool hasSet = false;

	public double multiplier = 1.0;
	private bool isConstant = false;
	public int constantVal = 0;

	public AbilityDataHelper()
	{
		typeCounter = DEFAULT_TYPE_COUNTER;
		flags = new List<StatFlags>();
		modifier = DEFAULT_MODIFIER;
		source = DEFAULT_SOURCE;
		prevFilter = DEFAULT_PREV_FILTER;

	}

	protected AbilityDataHelper(AbilityDataHelper helper)
	{
		if (helper.targetType != null)
			targetType = new TargetType(helper.targetType);


		source = helper.source;
		typeCounter = helper.typeCounter;
		flags = new List<StatFlags>();
		foreach (StatFlags flag in helper.flags)
		{
			flags.Add(new StatFlags(flag));
		}
		//statFlag = helper.statFlag;
		modifier = helper.modifier;
		prevFilter = helper.prevFilter;
		numHit = helper.numHit;
		value = helper.value;
		hasSet = helper.hasSet;
		multiplier = helper.multiplier;
		addConst = helper.addConst;

		constantVal = helper.constantVal;
		isConstant = helper.isConstant;

	}

	public void addStatFlag(StatFlags flag)
	{
		flags.Add(flag);
	}

	public List<StatFlags> getStatFlags()
	{
		return flags;
	}

	public void setMultiplier(double mult)
	{
		multiplier = mult;
	}
	
	public double getMultiplier()
	{
		return multiplier;
	}
	
	public void setConstant(int constant)
	{
		constantVal = constant;
		value = constant;
		hasSet = true;
		isConstant = true;
	}

	
	public void setAddConst(int c)
	{
		addConst = c;
	}

	public void reset()
	{
		if (!isConstant)
		{
			hasSet = false;
			value = 0;
		}
	}

	public int getValue(Card source, List<Card> targets, MainBoardModel model, Ability owner, AbilityResult prevResult)
	{
		if (hasSet)
		{
			return value;
		}
		double val = 0;
		hasSet = true;
		switch(typeCounter)
		{
			case TYPE_COUNT_NONE:
			{
				MyLogger.log("???");
				return 0;
//				break;
			}
			case TYPE_COUNT_STAT:
			{
				val = doTypeStat(source, targets, model, owner, prevResult) * multiplier + addConst;
				break;
			}
			case TYPE_COUNT_COUNT:
			{
				val = doTypeCount(source, targets, model, owner, prevResult) * multiplier + addConst;
				break;
			}
			default:
				break;
		}
		value = (int)val;
		return value;
	}	


	int doTypeCount(Card source, List<Card> targets, MainBoardModel model, Ability owner, AbilityResult prevResult)
	{
		int val = 0;
		if (isModel())
		{
			val += model.getTargetsForCard(source , targetType).Count;
		}


//TODO
//Uhh i dunno about ALL THESE BELOW LOL. deifnitely didnt think about this when writing zzz.

		if (isPrevResult())
		{

			MyLogger.log("is prev result");
			if (isPrevResultKill())
			{
				val += prevResult.kill;
			}
			if (isPrevResultDamage())
			{
				val += prevResult.damage;
			}
			if (isPrevResultHeal())
			{
				val += (-prevResult.damage);
			}
		}

		if (isTargets())
		{

			MyLogger.log("is targets");
			val += getCardsThatSatisfy(targets,targetType).Count;
		}

		if (isSelf())
		{
			MyLogger.log("IS SELF");
			val += 1;//getCardsThatSatisfy(source,targetType).Count;
		}

		if (isRemoveSelf())
		{
			MyLogger.log("REMOVE SELF");
			val -= 1;//getCardsThatSatisfy(source,targetType).Count;
		}
		MyLogger.log("fINAL "  + val);
		return val;
	}

	int doTypeStat(Card source, List<Card> targets, MainBoardModel model, Ability owner, AbilityResult prevResult)
	{
		//TODO
		double statChange = 0;

		List<Card> actualTargets = null;
/*
		if (isModel())
		{
			List<Card> cards = model.getTargetsForCard(source , targetType);

			
			foreach (Card card in cards)
			{
				foreach (StatFlags flag in flags)
				{
					statChange += flag.parseStatChange(card); 
				}
			}
		}
*/
/*

		if (HelperUtils.testBits(this.source, SOURCE_TARGETS))
		{
			MyLogger.log("SOURCE TARGETS" + flags);
			foreach (Card card in targets)
			{
				foreach (StatFlags flag in flags)
				{
					statChange += flag.parseStatChange(card); 
				}
			}
		}
*/
/*
		if (HelperUtils.testBits(this.source, SOURCE_SELF))
		{
			MyLogger.log("SOURCE SELF" + flags);
			foreach (StatFlags flag in flags)
			{
				statChange += flag.parseStatChange(source); 
			}
		}
*/

		if (isModel())
		{
			actualTargets = model.getTargetsForCard(source , targetType);
		}

		if (HelperUtils.testBits(this.source, SOURCE_TARGETS))
		{
			actualTargets = targets;
		}

		if (HelperUtils.testBits(this.source, SOURCE_SELF))
		{
			actualTargets = new List<Card>();
			actualTargets.Add(source);
		}


		if (modifier == MODIFIER_ORIGINAL_COPIES)
		{
			List<Card> originalCards = new List<Card>();
			foreach (Card c in actualTargets)
			{
				originalCards.Add(CardLibrary.getCardFromID(c.getID()));
			}
			actualTargets = originalCards;
		}

		foreach (Card card in actualTargets)
		{
			foreach (StatFlags flag in flags)
			{
				statChange += flag.parseStatChange(card); 
			}
		}
		MyLogger.log("STATCHANGE GOTTEN" + statChange);
		return (int)(statChange + 0.0001);
	}

	//SourceType
	bool isModel()
	{
		return (source & SOURCE_MODEL) != 0;
	}

	bool isPrevResult()
	{
		return (source & SOURCE_PREV_RESULT) != 0;
	}

	bool isTargets()
	{
		return (source & SOURCE_TARGETS) != 0;
	}

	bool isSelf()
	{
		return (source & SOURCE_SELF) != 0;
	}

//Filters
	bool isPrevResultKill()
	{
		return (prevFilter & PREV_RESULT_KILL) != 0;
	}

	bool isPrevResultDamage()
	{
		return (prevFilter & PREV_RESULT_DAMAGE) != 0;
	}

	bool isPrevResultHeal()
	{
		return (prevFilter & PREV_RESULT_HEAL) != 0;
	}


	//Modifiers
	bool isRemoveSelf()
	{
		return (modifier & MODIFIER_REMOVE_SELF) != 0;
	}

	public AbilityDataHelper deepCopy()
	{
		AbilityDataHelper dataHelper = new AbilityDataHelper(this);
		return dataHelper;
	}

	private List<Card> getCardsThatSatisfy(List<Card> targets, TargetType targetType)
	{

		List<Card> ans = new List<Card>();
		//int targetTeam = TargetTeam.TEAM_ALLY;
		foreach (Card card in targets)
		{
			if (TargetType.satisfies(card, targetType, targetType.targetTeam))
			{
				ans.Add(card);
			}
		}
		return ans;
	}

	
}


/*
//add to statflags?....
public static int parseStatChange(int flags, Card source)
	{
		int statChange = 0;

		if (ally)
		{
			if (source is Creature)
			{
				if ((StatFlags.ALLY_ATK & statFlags) != 0)
				{
					statChange += ((Creature) source).getAttack();
				}

				if ((StatFlags.ALLY_HEALTH & statFlags) != 0)
				{
					statChange += ((Creature) source).getHealth();
				}

				if ((StatFlags.ALLY_TEMPATK & statFlags) != 0)
				{
					statChange += ((Creature) source).getTempAttack();
				}

				if ((StatFlags.ALLY_TEMPHEALTH & statFlags) != 0)
				{
					statChange += ((Creature) source).getTempHealth();
				}
			}
			else if (source is Structure)
			{
				if ((StatFlags.ALLY_ATK & statFlags) != 0)
				{
					statChange += ((Structure) source).getAttack();
				}

				if ((StatFlags.ALLY_HEALTH & statFlags) != 0)
				{
					statChange += ((Structure) source).getHealth();
				}

				if ((StatFlags.ALLY_TEMPATK & statFlags) != 0)
				{
					statChange += ((Structure) source).getTempAttack();
				}

				if ((StatFlags.ALLY_TEMPHEALTH & statFlags) != 0)
				{
					statChange += ((Structure) source).getTempHealth();
				}
			}
			else if (source is Player)
			{
				if ((StatFlags.ALLY_HEALTH & statFlags) != 0)
				{
					statChange += ((Player) source).getHealth();
				}

				if ((StatFlags.ALLY_TEMPHEALTH & statFlags) != 0)
				{
					statChange += ((Player) source).getTempHealth();
				}
			}
		}
		else
		{

			if (source is Creature)
			{
				if ((StatFlags.TARGET_ATK & statFlags) != 0)
				{
					statChange += ((Creature) source).getAttack();
				}

				if ((StatFlags.TARGET_HEALTH & statFlags) != 0)
				{
					statChange += ((Creature) source).getHealth();
				}

				if ((StatFlags.TARGET_TEMPATK & statFlags) != 0)
				{
					statChange += ((Creature) source).getTempAttack();
				}

				if ((StatFlags.TARGET_TEMPHEALTH & statFlags) != 0)
				{
					statChange += ((Creature) source).getTempHealth();
				}
			}
			else if (source is Structure)
			{
				if ((StatFlags.TARGET_ATK & statFlags) != 0)
				{
					statChange += ((Structure) source).getAttack();
				}

				if ((StatFlags.TARGET_HEALTH & statFlags) != 0)
				{
					statChange += ((Structure) source).getHealth();
				}

				if ((StatFlags.TARGET_TEMPATK & statFlags) != 0)
				{
					statChange += ((Structure) source).getTempAttack();
				}

				if ((StatFlags.TARGET_TEMPHEALTH & statFlags) != 0)
				{
					statChange += ((Structure) source).getTempHealth();
				}
			}
			else if (source is Player)
			{
				if ((StatFlags.TARGET_HEALTH & statFlags) != 0)
				{
					statChange += ((Player) source).getHealth();
				}

				if ((StatFlags.TARGET_TEMPHEALTH & statFlags) != 0)
				{
					statChange += ((Player) source).getTempHealth();
				}
			}

		}

		return statChange;
	}

*/