﻿using UnityEngine;
using System.Collections;

public class AbilitySpeed
{

	public static readonly int S = 0; 
	public static readonly int A = 1;
	public static readonly int B = 2; 
	public static readonly int C = 3; 
	public static readonly int D = 4; 
	public static readonly int E = 5;
	
	public static int speedFromString(string name){
		if (name.Equals("S"))
			return AbilitySpeed.S;
		else if (name.Equals("A"))
			return AbilitySpeed.A;
		else if (name.Equals("B"))
			return AbilitySpeed.B;
		else if (name.Equals("C"))
			return AbilitySpeed.C;
		else if (name.Equals("D"))
			return AbilitySpeed.D;
		else if (name.Equals("E"))
			return AbilitySpeed.E;
		
		return AbilitySpeed.S;
	}
	
	public static string speedToString(int speedType)
	{
		if (speedType == S)
			return "S";
		else if (speedType == A)
			return "A";
		else if (speedType == B)
			return "B";
		else if (speedType == C)
			return "C";
		else if (speedType == D)
			return "D";
		else if (speedType == E)
			return "E";

		return null;
	}
}
