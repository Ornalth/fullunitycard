using System.Collections.Generic;

public class AbilityResult
{
	public bool success = false;
	public int kill = 0;
	public int damage = 0;
	public int healthChanged = 0;
	public int atkChanged = 0;
	public int mill = 0;
	public int draw = 0;
	public int discard = 0;
	public int sac = 0;
	public int library = 0;
	public int addedAbil = 0;
	public int addAttack = 0;
	public int addHealth = 0;
	public int bounce = 0;
	public int exiled = 0;
	public int purge = 0;

	public int directLife = 0;
	
	public List<Card> targets = new List<Card>();

	public int red = 0;
	public int blue = 0;
	public int green = 0;
	public int gray = 0;

	public int summon = 0;
	public int zone = 0;

	public int countersAdded = 0;
	public List<string> customEffectSuccess = new List<string>();
	public void addCustomAbilitySuccess (string s)
	{
		customEffectSuccess.Add(s);
	}

	public int searchDeck = 0;
}

