using UnityEngine;
using System.Collections.Generic;


//TODO MORE FLAGS EX. REFUND.
public class StatFlags 
{
	public double multiplier;
	public int flags;
	public List<int> statuses = new List<int>();
	public List<string> nameTypes = new List<string>();

	public static double DEFAULT_MULTIPLIER = 1.0;
	public static int NONE = 0;



	public static int ATK = 1 << 1;
	public static int HEALTH = 1 << 2;
	public static int TEMP_ATK = 1 << 3 ;
	public static int TEMP_HEALTH = 1 << 4;

	public static int COST_GRAY = 1 << 8;
	public static int COST_RED = 1 << 9;
	public static int COST_GREEN = 1 << 10;
	public static int COST_BLUE = 1 << 11;

	public static int SAC_GRAY = 1 << 12;
	public static int SAC_RED = 1 << 13;
	public static int SAC_GREEN = 1 << 14;
	public static int SAC_BLUE = 1 << 15;

//PlayerStatFlags
	public static int GRAY = 1 << 16;
	public static int RED = 1 << 17;
	public static int GREEN = 1 << 18;
	public static int BLUE = 1 << 19;
	public static int ALL_MANA = GRAY | RED | GREEN | BLUE;


	public static int NAME_TYPES = 1 << 30;
	public static int STATUSES = 1 << 31;
	public static int TOTAL_ATK = ATK | TEMP_ATK;
	public static int TOTAL_HEALTH = HEALTH | TEMP_HEALTH;
	public static int ALL_STAT = ATK | HEALTH | TEMP_ATK | TEMP_HEALTH;
	public static int ALL_SAC = SAC_GRAY | SAC_RED | SAC_GREEN | SAC_BLUE;
	public static int ALL_COST = COST_GRAY | COST_RED | COST_GREEN | COST_BLUE;
	

	private static Dictionary <int, string> dict = new Dictionary<int, string> (){
	        { NONE, "NONE"},

	        { ATK, "ATK"},
	        { HEALTH, "HEALTH"},
	        { TEMP_ATK, "TEMP_ATK"},
	        { TEMP_HEALTH, "TEMP_HEALTH"},

	        { COST_GRAY, "COST_GRAY"},
	        { COST_RED, "COST_RED"},
	        { COST_GREEN, "COST_GREEN"},
	        { COST_BLUE, "COST_BLUE"},

	        { SAC_GRAY, "SAC_GRAY"},
	        { SAC_RED, "SAC_RED"},
	        { SAC_GREEN, "SAC_GREEN"},
	        { SAC_BLUE, "SAC_BLUE"},

			{ RED, "RED"},
			{ GREEN, "GREEN"},
			{ BLUE, "BLUE"},
			{ GRAY, "GRAY"},

	        { TOTAL_ATK, "TOTAL_ATK"},
	        { TOTAL_HEALTH, "TOTAL_HEALTH"},
	        { ALL_STAT, "ALL_STAT"},
	        { ALL_SAC, "ALL_SAC"},
	        { ALL_COST, "ALL_COST"},
	        { ALL_MANA, "ALL_MANA"},

	        {STATUSES, "STATUSES"},
	        {NAME_TYPES, "NAME_TYPES"}


	    };

	    	private static Dictionary <int, string> simpleDict = new Dictionary<int, string> (){
	        { NONE, "NONE"},

	        { ATK, "ATK"},
	        { HEALTH, "HEALTH"},
	        { TEMP_ATK, "TEMP_ATK"},
	        { TEMP_HEALTH, "TEMP_HEALTH"},

	        { COST_GRAY, "COST_GRAY"},
	        { COST_RED, "COST_RED"},
	        { COST_GREEN, "COST_GREEN"},
	        { COST_BLUE, "COST_BLUE"},

	        { SAC_GRAY, "SAC_GRAY"},
	        { SAC_RED, "SAC_RED"},
	        { SAC_GREEN, "SAC_GREEN"},
	        { SAC_BLUE, "SAC_BLUE"},

			{ RED, "RED"},
			{ GREEN, "GREEN"},
			{ BLUE, "BLUE"},
			{ GRAY, "GRAY"},

	        {STATUSES, "STATUSES"},
	        {NAME_TYPES, "NAME_TYPES"}
	   
	    };

	    private static Dictionary <string, int> reverseDict = new Dictionary<string, int> (){
	        { "NONE", NONE},
	        
	        { "ATK", ATK},
	        { "HEALTH", HEALTH},
	        { "TEMP_ATK", TEMP_ATK},
	        { "TEMP_HEALTH", TEMP_HEALTH},

	        { "COST_GRAY", COST_GRAY},
	        { "COST_RED", COST_RED},
	        { "COST_GREEN", COST_GREEN},
	        { "COST_BLUE", COST_BLUE},

	        { "SAC_GRAY", SAC_GRAY},
	        { "SAC_RED", SAC_RED},
	        { "SAC_GREEN", SAC_GREEN},
	        { "SAC_BLUE", SAC_BLUE},

			{ "RED", RED},
			{ "GREEN", GREEN},
			{ "BLUE", BLUE},
			{ "GRAY", GRAY},

	        { "TOTAL_ATK", TOTAL_ATK},
	        { "TOTAL_HEALTH", TOTAL_HEALTH},
	        { "ALL_STAT", ALL_STAT},
	        { "ALL_SAC", ALL_SAC},
	        { "ALL_COST", ALL_COST},

	        {"STATUSES", STATUSES},
	        {"NAME_TYPES", NAME_TYPES}
	    };


	public StatFlags()
	{

		multiplier = 1.0;
	}

	public StatFlags(StatFlags s)
	{
		multiplier = s.multiplier;
		flags = s.flags;
		statuses = new List<int>();
		foreach (int i in s.statuses)
		{
			statuses.Add(i);
		}
		this.nameTypes = new List<string>(s.nameTypes);
	}


	//TODO?. maybe not evne use this in the end fk.
	public static int flagFromString(string str)
	{
		int val = NONE;
		if (reverseDict.TryGetValue(str, out val))
		{

		}
		return val;
	}

	public List<string> flagsToStringArray()
	{
		int beginFlags = flags;
		List<string> strs = new List<string>();
		foreach (int key in simpleDict.Keys)
		{
			if ((key & beginFlags) != 0)
			{
				strs.Add(simpleDict[key]);
			}
		}
		return strs;
	}

	public double parseStatChange(Card target)
	{
		int statFlags = flags;
		int statChange = 0;

		if (target is CombatCard)
		{
			if ((StatFlags.ATK & statFlags) != 0)
			{
				statChange += ((CombatCard) target).getAttack();
			}

			if ((StatFlags.HEALTH & statFlags) != 0)
			{
				statChange += ((CombatCard) target).getHealth();
			}

			if ((StatFlags.TEMP_ATK & statFlags) != 0)
			{
				statChange += ((CombatCard) target).getTempAttack();
			}

			if ((StatFlags.TEMP_HEALTH & statFlags) != 0)
			{
				statChange += ((CombatCard) target).getTempHealth();
			}


			int[] sacCost = ((CombatCard) target).getSalvageValue();

			if ((StatFlags.SAC_GRAY & statFlags) != 0)
			{
				statChange += sacCost[0];
			}

			if ((StatFlags.SAC_RED & statFlags) != 0)
			{
				statChange += sacCost[1];
			}

			if ((StatFlags.SAC_GREEN & statFlags) != 0)
			{
				statChange += sacCost[2];
			}

			if ((StatFlags.SAC_BLUE & statFlags) != 0)
			{
				statChange += sacCost[3];
			}
		}

		int[] cost = target.getCost();

		if ((StatFlags.COST_GRAY & statFlags) != 0)
		{
			statChange += cost[0];
		}

		if ((StatFlags.COST_RED & statFlags) != 0)
		{
			statChange += cost[1];
		}

		if ((StatFlags.COST_GREEN & statFlags) != 0)
		{
			statChange += cost[2];
		}

		if ((StatFlags.COST_BLUE & statFlags) != 0)
		{
			statChange += cost[3];
		}

		if (target is Player)
		{
			int []rez = ((Player)target).getResources();
			if (HelperUtils.testBits(statFlags, StatFlags.GRAY))
			{
				statChange += rez[0];
			} 


			if (HelperUtils.testBits(statFlags, StatFlags.RED))
			{
				statChange += rez[1];
			} 


			if (HelperUtils.testBits(statFlags, StatFlags.GREEN))
			{
				statChange += rez[2];
			} 


			if (HelperUtils.testBits(statFlags, StatFlags.BLUE))
			{
				statChange += rez[3];
			} 
		}

		if (HelperUtils.testBits(statFlags, StatFlags.STATUSES))
		{
			foreach (int status in statuses)
			{
				if (target.hasStatusEffect(status))
				{
					statChange++;
					break;
				}
			}
		}


		if (HelperUtils.testBits(statFlags, StatFlags.NAME_TYPES))
		{
			List<string> names = target.getNameType();
			for (int i = 0; i < nameTypes.Count; i++)
			{
				if (names.Contains(nameTypes[i]))
				{
					statChange++;
					break;
				}
			}
		}
		return statChange * multiplier;
	}

}