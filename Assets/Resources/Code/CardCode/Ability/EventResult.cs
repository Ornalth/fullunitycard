﻿
using System.Collections.Generic;

public class EventResult 
{
	public Card source;
	public List<Card> targets;
	public MainBoardModel model;
	public Ability abilSource;
	public AbilityResult result;

	//Saved sources
	public int currentEff = 0;
	public int currentAbilEff = 0;
	public bool done;
	public List<Card> lastActionCards;


}