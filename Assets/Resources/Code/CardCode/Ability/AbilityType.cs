﻿using UnityEngine;
using System.Collections;

public class AbilityType
{

	public const int NONE = 0;
	public const int PASSIVE = 1;
	public const int ACTIVE = 1<<1; // 2
	public const int ALTERNATE = 1 << 3; // 8
	public const int CUSTOM = 1 << 4; // 8
	public const int ANY = PASSIVE | ACTIVE | ALTERNATE | CUSTOM;
	
	public static int abilityTypeFromString(string name){
		if (name.Equals("PASSIVE"))
			return AbilityType.PASSIVE;
		else if (name.Equals("ACTIVE"))
			return AbilityType.ACTIVE;
		else if (name.Equals("ALT"))
			return AbilityType.ALTERNATE;
		else if (name.Equals("CUSTOM"))
			return AbilityType.CUSTOM;
		return AbilityType.NONE;
	}
	
	public static string abilityTypeToString(int abilityType)
	{
		if (abilityType == PASSIVE)
			return "PASSIVE";
		else if (abilityType == ACTIVE)
			return "ACTIVE";
		else if (abilityType == ALTERNATE)
			return "ALTERNATE";
		else if (abilityType == CUSTOM)
			return "CUSTOM";
		return null;
	}

}
