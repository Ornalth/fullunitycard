using System.Collections.Generic;

public class DestroyAbility : AbilityEffect, ISelfEffect, IPreAbilityEffect
{
	bool isSilent;
    bool isSource = false;

	public DestroyAbility():base()
	{
		isSilent = false;
	}

	public DestroyAbility(DestroyAbility abil):base(abil)
	{
		this.isSilent = abil.isSilent;
		this.isSource = abil.isSource;
	}

	public void setSilent(bool sil)
	{
		isSilent = sil;
	}

	public bool isThisSilent()
	{
		return isSilent;
	}

	    public void setSelf(bool b)
    {
        isSource = b;
    }
        public bool getIsSelf()
    {
        return isSource;
    }
	
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		if (isSource)
		{
			Player player = model.getPlayerFromCard(source);
			player.kill(source, isSilent);
			workedTargets.Add(source);
			prevResult.kill += 1;
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					Player player = model.getPlayerFromCard(target);
					player.kill(target, isSilent);
					workedTargets.Add(target);
					prevResult.kill += 1;
				}
			}
		}
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}
	



	override public AbilityEffect deepCopy()
	{
		DestroyAbility abil = new DestroyAbility(this);
		return abil;
	}

	override public string getDesc()
	{
		return "destroy abil";
	}

	public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
	{
		if (isSource)
		{
			if (!source.isDead())
			{
				return true;
			}
		}
		else
		{
			foreach (Card target in targets)
			{
				CombatCard card = (CombatCard)target;
				if (card.isDead())
				{
					return false;
				}	
			}
		}
		return true;
	}
	
	public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
	{
		if (isSource)
		{
			Player player = model.getPlayerFromCard(source);
			player.kill(source, isSilent);
		}
		else
		{
			foreach (Card target in targets)
			{
				Player player = model.getPlayerFromCard(target);
				player.kill(target, isSilent);
			}
		}
	}
}