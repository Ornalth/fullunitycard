using System.Collections.Generic;

public class DamageAbility : AbilityEffect, ISelfEffect
{
	bool isSource = false;
	public DamageAbility():base()
	{

	}
	private int getDmgEff()
	{
		return values[0];
	}

	protected DamageAbility(DamageAbility a):base(a)
	{
		isSource = a.isSource;
	}

    public void setSelf(bool b)
    {
        isSource = b;
    }
    
    public bool getIsSelf()
    {
        return isSource;
    }

	override public AbilityEffect deepCopy()
	{
		DamageAbility d = new DamageAbility(this);
		return d;
	}
	
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		if (isSource)
		{
			int value = getDmgEff();
			int dmgTaken = model.makeCardTakeDamage(source, source, value, null);
			prevResult.damage += dmgTaken;
			workedTargets.Add(source);
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					int value = getDmgEff();
					int dmgTaken = model.makeCardTakeDamage(source, target, value, null);
					prevResult.damage += dmgTaken;
					workedTargets.Add(target);
				}
			}
		}

		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public string getDesc()
	{
		return "dmg abil";
	}
}