using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityEffect
{

	public List<int> modelDataRefs;
	public List<int> values;
	public TargetType targType;
	public bool requiresPreviousSuccess = false;

	public AbilityEffect()
	{
		modelDataRefs = new List<int>();
		values = new List<int>();
	}

	public AbilityEffect(AbilityEffect a)
	{
		modelDataRefs = new List<int>();
		foreach (int i in a.modelDataRefs)
		{
			modelDataRefs.Add(i);
		}

		values = new List<int>();
		foreach (int i in a.values)
		{
			values.Add(i);
		}

		if (a.targType != null)
			targType = new TargetType (a.targType);
		requiresPreviousSuccess = a.requiresPreviousSuccess;
	}

	protected abstract bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards);
	public void setRequirePreviousSuccess(bool r)
	{
		requiresPreviousSuccess = r;
	}
	public bool activate(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, TargetType sourceTargetType, AbilityResult prevResult, List<Card> lastActionCards)
	{
		if (requiresPreviousSuccess && !prevResult.success)
		{
			return false;
		}
		bool wasNull = false;
		if (targType == null)
		{
			wasNull = true;
			targType = sourceTargetType;
			//targType = abilSource.getTargetType();
		}

		//Get DATA REFs. done inside ability.
		//values = new List<int>();
		//foreach (int dataRef in modelDataRefs)
		//{
		//	values.Add(abilSource.modelData[dataRef].getValue(source,targets,model,abilSource,prevResult));	
		//}


		bool success = theEvent(source,targets,model,abilSource,prevResult, lastActionCards);

		if (wasNull)
			targType = null;
		return success;
	}	

	public abstract string getDesc();
	public abstract AbilityEffect deepCopy();
}

public interface IDirectSetAbility
{
	void setDirect(bool b);
	bool isDirect();
}

public interface ITempAddAbility
{
	void setTemp(bool b);
	bool isTemp();
}

public interface ISelfEffect
{
	void setSelf(bool b);
	bool getIsSelf();
}

public interface IFullEffect
{
	bool isFull();
	void setIsFull(bool b);
}

public interface IPreAbilityEffect
{
	bool canSatisfy(Card source, List<Card> targets, MainBoardModel model);
	void preEffectAct(Card source, List<Card> targets, MainBoardModel model);
}