using System.Collections.Generic;
using UnityEngine;

public class AddHealthAbility : AbilityEffect, IDirectSetAbility, ITempAddAbility, ISelfEffect
{

    bool bTemp = false;
    bool isSource = false;
    bool directSet = false;
    bool isRegen = false;

    public AddHealthAbility():base()
    {

    }
    private int getValue()
    {
        return values[0];
    }

    protected AddHealthAbility(AddHealthAbility a):base(a)
    {
        isSource = a.isSource;
        this.bTemp = a.bTemp;
        this.directSet = a.directSet;
        this.isRegen = a.isRegen;
    }

    override public AbilityEffect deepCopy()
    {
        AddHealthAbility d = new AddHealthAbility(this);
        return d;
    }

     public bool isDirect()
    {
        return directSet;
    }

    public bool isTemp()
    {
        return bTemp;
    }

    public bool getIsSelf()
    {
        return isSource;
    }

    public void setDirect(bool b)
    {
        directSet = b;
    }

    public void setTemp(bool b)
    {
        bTemp = b;
    }

    public void setSelf(bool b)
    {
        isSource = b;
    }

    public void setIsRegen(bool b)
    {
        isRegen = b;
    }
    

    override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
    {
        List<Card> workedTargets = new List<Card>();
//        int avalue = getValue();
        if (isSource)
        {
            int value = getValue();

            if (bTemp)
            {
                ((CombatCard)source).addTempHealth(value, source);
            }
            else if (directSet)
            {
                ((CombatCard)source).setHealth(value);
            }
            else if (isRegen)
            {
                ((CombatCard)source).regenHealth(value, source);
            }
            else
            {
                ((CombatCard)source).addHealth(value, source);
            }
            prevResult.addHealth += value;
            workedTargets.Add(source);
        }
        else
        {
            foreach (Card target in targets)
            {
                if (TargetType.satisfies(target, targType,
                        TargetTeam.getTargetTeamFromCards(source, target)))
                {
                    int value = getValue();
                    if (bTemp)
                    {
                        ((CombatCard)target).addTempHealth(value, source);
                    }
                    else if (directSet)
                    {
                        ((CombatCard)target).setHealth(value);
                    }
                    else if (isRegen)
                    {
                        ((CombatCard)target).regenHealth(value, source);
                    }
                    else
                    {
                        ((CombatCard)target).addHealth(value, source);
                    }
                    prevResult.addHealth += value;
                    workedTargets.Add(target);
                }
            }
        }
        bool success = workedTargets.Count >= targets.Count;
        prevResult.success = success;
        prevResult.targets = workedTargets;
        return success;
    }

    override public string getDesc()
    {
        return "ADDGHEALTH ABIL";
    }
}