using System.Collections.Generic;

public class AddAttackAbility : AbilityEffect, IDirectSetAbility, ITempAddAbility, ISelfEffect
{

	bool bTemp = false;
	bool isSource = false;
	bool directSet = false;

	public AddAttackAbility():base()
	{

	}

	protected AddAttackAbility(AddAttackAbility a):base(a)
	{
		this.bTemp = a.bTemp;
		isSource = a.isSource;
		this.directSet = a.directSet;
	}

	private int getValue()
	{
		return values[0];
	}	

	public bool isDirect()
	{
		return directSet;
	}

	public bool isTemp()
	{
		return bTemp;
	}

	public bool getIsSelf()
	{
		return isSource;
	}

	public void setDirect(bool b)
	{
		directSet = b;
	}

	public void setTemp(bool b)
	{
		bTemp = b;
	}

	public void setSelf(bool b)
	{
		isSource = b;
	}
	
	override public AbilityEffect deepCopy()
	{
		AddAttackAbility d = new AddAttackAbility(this);
		return d;
	}
	
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		if (isSource)
		{
			int value = getValue();
			if (bTemp)
			{
				((CombatCard)source).addTempAttack(value, source);
			}
			else if (directSet)
			{
				((CombatCard)source).setAttack(value);
			}
			else
			{
				((CombatCard)source).addAttack(value, source);
			}
			prevResult.addAttack += value;
			workedTargets.Add(source);
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					int value = getValue();
					if (bTemp)
					{
						((CombatCard)target).addTempAttack(value, source);
					}
					else if (directSet)
					{
						((CombatCard)target).setAttack(value);
					}
					else
					{
						((CombatCard)target).addAttack(value, source);
					}
					prevResult.addAttack += value;
					workedTargets.Add(target);
				}
			}
		}
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public string getDesc()
	{
		return "ADDATK ABIL";
	}
	
}