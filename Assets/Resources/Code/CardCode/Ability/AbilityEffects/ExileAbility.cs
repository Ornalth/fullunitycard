using System.Collections.Generic;

public class ExileAbility : AbilityEffect, ISelfEffect, IPreAbilityEffect
{
	bool isSource = false;

	public ExileAbility():base()
	{
	}

	public ExileAbility(ExileAbility abil):base(abil)
	{
		this.isSource = abil.isSource;
	}

	public void setSelf(bool b)
    {
        isSource = b;
    }
        public bool getIsSelf()
    {
        return isSource;
    }

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		if (isSource)
		{
			Player player = model.getPlayerFromCard(source);
			player.exile(source);
			workedTargets.Add(source);
			prevResult.exiled += 1;
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					Player player = model.getPlayerFromCard(target);
					player.exile(target);
					workedTargets.Add(target);
					prevResult.exiled += 1;
				}
			}
		}

		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}


	override public AbilityEffect deepCopy()
	{
		ExileAbility abil = new ExileAbility(this);
		return abil;
	}
	
	override public string getDesc()
	{
		return "EXILEABIL";
	}

	public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
	{/*
		if (isSource)
		{
			if (!source.isDead())
			{
				return true;
			}
		}
		else
		{
			foreach (Card target in targets)
			{
				CombatCard card = (CombatCard)target;
				if (card.isDead())
				{
					return false;
				}	
			}
		}*/
		return true;
	}
	
	public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
	{
		if (isSource)
		{
			Player player = model.getPlayerFromCard(source);
			player.exile(source);
		}
		else
		{
			foreach (Card target in targets)
			{
				Player player = model.getPlayerFromCard(target);
				player.exile(target);
			}
		}
	}

}