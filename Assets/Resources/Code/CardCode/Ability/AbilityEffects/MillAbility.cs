using System.Collections.Generic;

public class MillAbility : AbilityEffect, IPreAbilityEffect
{
	public MillAbility():base()
	{
	}

	public MillAbility(MillAbility abil):base(abil)
	{
	}

	private int getMillValue()
	{
		return values[0];
	}
	
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{

		List<Card> workedTargets = new List<Card>();
		
		foreach (Card target in targets)
		{
			if (target.getCardType() == CardType.PLAYER)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					int mill = getMillValue();
					prevResult.mill += mill;
					workedTargets.Add(target);
					((Player) target).mill(mill);
				}
			}
			else
			{
				//System.out
				//		.println("In passive ability event: non player cannot draw cards");
			}
		}
				bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		MillAbility abil = new MillAbility(this);
		return abil;
	}
	override public string getDesc()
	{
		return "mill abil";
	}

	//All cards should be in hand and ALLY
    public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
    {
        Player player = model.getPlayerFromCard(source);
        if (player.getDeckSize() >= getMillValue())
        {
        	return true;
        }
        return false;
    }
    
    //SELF DISCARD ALL CARDS.
    public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
    {
        Player player = model.getPlayerFromCard(source);
        player.mill(getMillValue());
    }
}