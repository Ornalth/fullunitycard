using System.Collections.Generic;

public class NameTypeAbility : AbilityEffect {


	List<string> addNameTypes;// = new List<string>();
	List<string> removeNameTypes;// = new List<string>();

	public NameTypeAbility():base()
	{
		addNameTypes = new List<string>();
		removeNameTypes = new List<string>();
	}

	public NameTypeAbility(NameTypeAbility abil):base(abil)
	{
		addNameTypes = new List<string>();
		foreach (string eff in abil.addNameTypes)
		{
			addNameTypes.Add(eff);
		}
		removeNameTypes = new List<string>();
		foreach (string eff in abil.removeNameTypes)
		{
			removeNameTypes.Add(eff);
		}
	}


	public void addAddNameType(string e)
	{
		addNameTypes.Add(e);
	}

	public void addRemoveNameType(string e)
	{
		removeNameTypes.Add(e);
	}

	public List<string> getAddNameTypes()
	{
		return addNameTypes;
	}

	public List<string> getRemoveNameTypes()
	{
		return removeNameTypes;
	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				foreach (string eff in addNameTypes)
				{
					target.addNameType(eff);
				}

				foreach (string eff in removeNameTypes)
				{
					target.removeNameType(eff);
				}
				workedTargets.Add(target);
				//TODO PREV RESUTL.
			}
		}

				bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		NameTypeAbility abil = new NameTypeAbility(this);
		return abil;
	}

	override public string getDesc()
	{
		return "name type adding abil";
	}

}