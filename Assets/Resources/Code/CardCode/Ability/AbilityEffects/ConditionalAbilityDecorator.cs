using System.Collections.Generic;

//TODO
public class ConditionalAbilityDecorator : AbilityEffect 
{
	AbilityEffect wrappedEffect;

	public ConditionalAbilityDecorator():base()
	{
	}

	public ConditionalAbilityDecorator(ConditionalAbilityDecorator abil):base(abil)
	{
		wrappedEffect = abil.getAbilityEffect().deepCopy();
	}

	public AbilityEffect getAbilityEffect()
	{
		return wrappedEffect;
	}
	
	public void setAbilityEffect(AbilityEffect eff)
	{
		wrappedEffect = eff;	
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		//TODO FIXME
		/*
		bool cond = true;
		if (cond)
		{
			return wrappedEffect.activate(source, targets, model, abilSource, prevResult, lastActionCards);//wrappedEffect.theEvent(source, targets, model, abilSource, prevResult, lastActionCards);
		}
		else
		{
			prevResult.success = false;
			return false;
		}*/

		return false;
	}

	override public AbilityEffect deepCopy()
	{
		ConditionalAbilityDecorator abil = new ConditionalAbilityDecorator(this);
		return abil;
	}
	override public string getDesc()
	{
		return "CONDITIONAL + " + wrappedEffect.getDesc();
	}
}