using System.Collections.Generic;

public class ZoneChangeAbility : AbilityEffect
{
	public static int DEFAULT_TARGET_TEAM = TargetTeam.TEAM_ALLY;

	
	protected int toZone = CardState.NONE;
	protected int zoneTargetTeam = DEFAULT_TARGET_TEAM;

	public ZoneChangeAbility():base()
	{
	}

	public ZoneChangeAbility(ZoneChangeAbility abil):base(abil)
	{
		toZone = abil.toZone;
		zoneTargetTeam = abil.zoneTargetTeam;
	}
	
		public void setTargetZone(int toZone)
	{
		this.toZone = toZone;
	}

	public void setZoneTargetTeam(int targTeam)
	{
		this.zoneTargetTeam = targTeam;
	}

	public int getTargetZone()
	{
		return toZone;
	}

	public int getZoneTargetTeam()
	{
		return zoneTargetTeam;
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		Player recip = model.getPlayerFromCard(source);
		if (zoneTargetTeam != TargetTeam.TEAM_ALLY)
		{
			recip = model.getOtherPlayer(recip);
		}
		
		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				workedTargets.Add(target);
				prevResult.zone++;
				Player player = model.getPlayerFromCard(target);
				player.removeCard(target);
				recip.addCard(target, toZone);
			}
		}

	
						bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		ZoneChangeAbility abil = new ZoneChangeAbility(this);
		return abil;
	}

	override public  string getDesc()
	{
		return "Zonechange EFF";
	}
}
