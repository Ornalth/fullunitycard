using System.Collections.Generic;

public class BounceAbility : AbilityEffect
{
	public BounceAbility():base()
	{

	}

	public BounceAbility(BounceAbility abil):base(abil)
	{

	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				Player player = model.getPlayerFromCard(target);
				player.bounce(target);
				prevResult.bounce += 1;
				
				workedTargets.Add(target);
			}
		}


        bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

		override public string getDesc()
	{
		return "bounce abil abil";
	}

	override public AbilityEffect deepCopy()
	{
		BounceAbility abil = new BounceAbility(this);
		return abil;
	}
}