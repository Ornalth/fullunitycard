using System.Collections.Generic;

public class PurgeAbility : AbilityEffect 
{
	public PurgeAbility():base()
	{
	}

	public PurgeAbility(PurgeAbility abil):base(abil)
	{
	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{

		List<Card> workedTargets = new List<Card>();
		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				target.purge();
				prevResult.purge += 1;
				workedTargets.Add(target);
			}
		}

		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		PurgeAbility abil = new PurgeAbility(this);
		return abil;
	}
	override public string getDesc()
	{
		return "purge abil";
	}
}