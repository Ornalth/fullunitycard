using System.Collections.Generic;
using UnityEngine;


public class DirectLifeAbility : AbilityEffect , ISelfEffect, IPreAbilityEffect
{
	bool isSource = false;
	bool isOpp = false;
	public DirectLifeAbility():base()
	{
	}

	public DirectLifeAbility(DirectLifeAbility abil):base(abil)
	{
		this.isSource = abil.isSource;
		this.isOpp = abil.isOpp;
	}

    public void setSelf(bool b)
    {
        isSource = b;
    }
    public bool getIsSelf()
    {
        return isSource;
    }

    public void setIsOpp(bool b)
    {
        isOpp = b;
    }
    public bool getIsOpp()
    {
        return isOpp;
    }

	private int getValue()
	{
		return values[0];
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{

		List<Card> workedTargets = new List<Card>();
		
		int value = getValue();

		if (isSource)
		{
			Player playerSource = model.getPlayerFromCard(source);
			playerSource.addHealth(value, source);
			prevResult.directLife += value;
			workedTargets.Add(playerSource);
		}
		else if (isOpp)
		{
			Player playerSource = model.getPlayerFromCard(source);
			Player opp = model.getOtherPlayer(playerSource);
			opp.addHealth(value, source);
			prevResult.directLife += value;
			workedTargets.Add(opp);
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					Player targetSource = model.getPlayerFromCard(target);
					targetSource.addHealth(value, source);
					prevResult.directLife += value;
					workedTargets.Add(target);
				}
			}
		}

		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		DirectLifeAbility abil = new DirectLifeAbility(this);
		return abil;
	}


	override public string getDesc()
	{
		return "Direct life abil";
	}

	//All cards should be in hand and ALLY
    public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
    {
        int value = getValue();
        if (isSource)
		{
			Player playerSource = model.getPlayerFromCard(source);
			return playerSource.getHealth() - value > 0;
		}
		else if (isOpp)
		{
			Player playerSource = model.getPlayerFromCard(source);
			Player opp = model.getOtherPlayer(playerSource);
			return opp.getHealth() - value > 0;

		}
		return false;
    }
    
    //SELF DISCARD ALL CARDS.
    public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
    {
    	int value = getValue();
        if (isSource)
		{
			Player playerSource = model.getPlayerFromCard(source);
			playerSource.addHealth(value, source);
		}
		else if (isOpp)
		{
			Player playerSource = model.getPlayerFromCard(source);
			Player opp = model.getOtherPlayer(playerSource);
			opp.addHealth(value, source);
		}
    }
}