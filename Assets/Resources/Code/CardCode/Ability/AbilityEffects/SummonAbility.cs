using System.Collections.Generic;
using UnityEngine;

public class SummonAbility : AbilityEffect {
	bool shouldSetHealth = false;
	bool shouldSetAttack = false;
	List<int> summonIDs;
	
	public static int DEFAULT_SUMMON_COUNT = 1;
	int summonCount = DEFAULT_SUMMON_COUNT;
	
	public SummonAbility():base()
	{
		summonIDs = new List<int>();
		//summonCount = 1;
	}

	public SummonAbility(SummonAbility abil):base(abil)
	{
		shouldSetHealth = abil.shouldSetHealth;
		shouldSetAttack = abil.shouldSetAttack;
		summonIDs = new List<int>();
		foreach (int i in abil.summonIDs)
		{
			summonIDs.Add(i);
		}
		summonCount = abil.summonCount;
	}

	public void setSummonHealth()
	{
		shouldSetHealth = true;
	}

	public void setSummonAttack()
	{
		shouldSetAttack = true;
	}
	
	public void setSummonIDs(List<int> ids)
	{
		summonIDs = ids;
	}

	public List<int> getSummonIDs()
	{
		return summonIDs;
	}

	public void setSummonCount(int count)
	{
		summonCount = count;
	}

	public int getSummonCount()
	{
		return summonCount;

	}

	public bool isSummonAttack()
	{
		return shouldSetAttack;
	}

	public bool isSummonHealth(){
		return shouldSetHealth;
	}

	public int getSummonHealth()
	{
		return values[1];
	}

	public int getSummonAttack()
	{
		return values[0];
	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		Player playerSource = model.getPlayerFromCard(source);
//		Player oppSource = model.getOtherPlayer(playerSource);

		int summonIndex =  UnityEngine.Random.Range(0,summonIDs.Count);
	
		if (targets.Count == 0)
		{
		
			targets.Add(playerSource);
		}
//		MyLogger.log("SUMONING" + targets.Count + " " + summonCount);
		foreach (Card p in targets)
		{
			Player player = model.getPlayerFromCard(p);
			workedTargets.Add(player);
			for (int iii = 0; iii < summonCount; iii++)
			{
				Card c = CardLibrary.getCardFromID(summonIDs[summonIndex]);
				c.setToken(true);
				c.setState( CardState.BOARD);
				c.setTeam(player.getTeam());

				CombatCard cc = (CombatCard)c;
				if (shouldSetAttack)
					cc.setAttack(getSummonAttack());
				if (shouldSetHealth)
					cc.setHealth(getSummonHealth());
					

				//if (numTargets >= targets.size())
				//{
					player.specialSummon(c);
			//		prevResult.summon += 1;
			//	}
				//else
				//{
					/*if (targets.get(numTargets).getTeam() == source
							.getTeam())
					{
						playerSource.specialSummon(c);
						prevResult.summon += 1;
					}
					else
					{
						oppSource.specialSummon(c);
						prevResult.summon += 1;
					}*/
					//player.specialSummon(c);
					prevResult.summon += 1;
				//}

			}
		}
				bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		SummonAbility abil = new SummonAbility(this);
		return abil;
	}

	override public  string getDesc()
	{
		return "SUMMON EFF";
	}
}