using System.Collections.Generic;
using UnityEngine;

public class RefundAbility : AbilityEffect, ISelfEffect, IPreAbilityEffect {

	bool isSource = false;

	public RefundAbility():base()
	{
	}

	public RefundAbility(RefundAbility abil):base(abil)
	{
		this.isSource = abil.isSource;
	}

	public void setSelf(bool b)
    {
        isSource = b;
    }
        public bool getIsSelf()
    {
        return isSource;
    }

	private int getRefundGray()
	{
		return values[0];
	}

	private int getRefundRed()
	{
		return values[1];
	}
	private int getRefundGreen()
	{
		return values[2];
	}
	private int getRefundBlue()
	{
		return values[3];
	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
//		abilityResult = new AbilityResult();

		int[] reclaimGain = new int[Constants.NUM_REZ];
				
		reclaimGain[0] = getRefundGray();
		reclaimGain[1] = getRefundRed();
		reclaimGain[2] = getRefundGreen();
		reclaimGain[3] = getRefundBlue();
		MyLogger.log("RECLAIM " + reclaimGain[0] + " " + reclaimGain[1] + " " + reclaimGain[2] + " " + reclaimGain[3]); 
		if (isSource)
		{
			MyLogger.log("SS+SCO");
			model.getPlayerFromCard(source).addRez(reclaimGain);
			workedTargets.Add(source);
			prevResult.gray += reclaimGain[0] ;
			prevResult.red += reclaimGain[1] ;
			prevResult.green += reclaimGain[2] ;
			prevResult.blue += reclaimGain[3] ;
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					model.getPlayerFromCard(target).addRez(reclaimGain);
					workedTargets.Add(target);
					prevResult.gray += reclaimGain[0] ;
					prevResult.red += reclaimGain[1] ;
					prevResult.green += reclaimGain[2] ;
					prevResult.blue += reclaimGain[3] ;
				}
		
			}
		}
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		RefundAbility abil = new RefundAbility(this);
		return abil;
	}

	override public string getDesc()
	{
		return "refund";
	}

	//All cards should be in hand and ALLY
    public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
    {
        Player player = model.getPlayerFromCard(source);
        int[] reclaimGain = new int[Constants.NUM_REZ];
				
		reclaimGain[0] = getRefundGray();
		reclaimGain[1] = getRefundRed();
		reclaimGain[2] = getRefundGreen();
		reclaimGain[3] = getRefundBlue();

		return player.canPayResourceCost(reclaimGain);
    }
    
    //SELF DISCARD ALL CARDS.
    public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
    {
        Player player = model.getPlayerFromCard(source);
        int[] reclaimGain = new int[Constants.NUM_REZ];
				
		reclaimGain[0] = getRefundGray();
		reclaimGain[1] = getRefundRed();
		reclaimGain[2] = getRefundGreen();
		reclaimGain[3] = getRefundBlue();

		player.payCost(reclaimGain);
    }
}