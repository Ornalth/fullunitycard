using System.Collections.Generic;
using UnityEngine;


public class SearchDeckAbility : CoroutineAbility, ISelfEffect
{
    public static readonly bool DEFAULT_RANDOM = false;

    public TargetType searchTargType = new TargetType(); // can only deck specific
    // cards
    public bool deckRandom = DEFAULT_RANDOM;
    //private int topXCards = 999;
    public bool isSelf = false;
    //public bool playerChoice = false;//TRUE = TRUE PICK, FALSE = SELF PICK;
    
    public int playerChoiceTargetTeam = TargetTeam.TEAM_ALLY;

    private bool hasStoredState = false;
    private Card ssource;
    private List<Card> stargets;
    private MainBoardModel smodel;
    private Ability sabilSource;
    private AbilityResult sprevResult;
    private int sindex;
    private bool reqModel = false;


    public SearchDeckAbility():base()
    {

    }
    private int getTopXCards()
    {
        return values[0];
    }

    protected SearchDeckAbility(SearchDeckAbility a):base(a)
    {
        searchTargType = a.searchTargType;
        deckRandom = a.deckRandom;
        playerChoiceTargetTeam = a.playerChoiceTargetTeam;
        isSelf = a.isSelf;
    //    topXCards = a.topXCards;
    }
/*
    public void setTopXCards(int x)
    {
    	topXCards = x;
    }

    public int getTopXCards()
    {
    	return topXCards;
    }
*/

    public void setDeckRandom(bool random)
    {
        deckRandom = random;
    }

    public bool getRandom()
    {
        return deckRandom;
    }


    public void setSelf(bool b)
    {
        isSelf = b;
    }

    public bool getIsSelf()
    {
        return isSelf;
    }

    override public AbilityEffect deepCopy()
    {
        SearchDeckAbility d = new SearchDeckAbility(this);
        return d;
    }

    override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
    {
        if (lastActionCards == null)
        {
             MyLogger.log("NO LAST ACTION CARDS");
        }
        else
        {
            MyLogger.log("LAST ACTION CARDS " + lastActionCards.Count);
        }
       
        //reqModel = false;
       // restoreState();
        reqModel = false;
        int index = 0;
        if(hasStoredState)
        {
            MyLogger.log("RESTORE SEARCH SAvE SAVE STATE");
            index = sindex;
            hasStoredState = false;
            source = ssource;
            targets = stargets;
            model = smodel;
            abilSource = sabilSource;
            prevResult = sprevResult;

        }
        else

        {
            MyLogger.log("NOTHIGN TO RESTGORE");
        }

        List<Card> workedTargets = new List<Card>();

        if (isSelf )
        {
            if (index < 1)
            {
                MyLogger.log("SELF");
                int value = getTopXCards();
                Player playerSource = model.getPlayerFromCard(source);
                model.searchDeck(playerSource, source,
                                        value, searchTargType,
                                        deckRandom, playerChoiceTargetTeam);
                prevResult.searchDeck += value;
                workedTargets.Add(source);
                index++;
                saveState(source,  targets, model, abilSource, prevResult,index);
                //TODO i dunno maybe we dont need to here.
                return false;
            }
            else
            {
                 if (lastActionCards == null)
                {
                     MyLogger.log("NO LAST ACTION CARDS");
                }
                else
                {
                    MyLogger.log("LAST ACTION CARDS " + lastActionCards.Count);
                    Player playerSource = model.getPlayerFromCard(source);
                    playerSource.topDeck(lastActionCards, true);
                }
                return true;
            }
        }
        else
        {
             MyLogger.log("TAGETS");
            int value = getTopXCards();
            for (int i = index; i < targets.Count; i++)
            {
                Card target = targets[i];
                if (target.getCardType() == CardType.PLAYER)
                {
                    MyLogger.log("what IS THIS CARD " + target + " " + target.getState());
                    if (TargetType.satisfies(target, targType, TargetTeam
                            .getTargetTeamFromCards(source, target)))
                    {

                        model.searchDeck(((Player)target), source,
                                value, searchTargType,
                                deckRandom, playerChoiceTargetTeam);
                        //modelStatus++; // make sure we move to next target!
                        //reqModel = true; //TODO?
                        prevResult.searchDeck += value;
                        saveState(source,  targets, model, abilSource, prevResult, i);
                        return false;
                    }
                }
                else
                {
                    Debug.Log
                            ("In passive ability event: non player cannot deck cards");
                }
            }
        }
        bool success = workedTargets.Count >= targets.Count;
        prevResult.success = success;
        prevResult.targets = workedTargets;
        reqModel = false;
        resetState();
        return success;
    }

    override public bool requiresModel()
    {
        return reqModel;
    }

    override protected  void saveState(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, int index)
    {
        MyLogger.log("SEARCH SAVE STATE");
        reqModel = true;
        hasStoredState = true;
        ssource = source;
        stargets = targets;
        smodel = model;
        sabilSource = abilSource;
        sprevResult = prevResult;
        sindex = index + 1;

    }
    protected void resetState()
    {
        reqModel = false;
        hasStoredState = false;
        ssource = null;
        stargets = null;
        smodel = null;
        sabilSource = null;
        sprevResult = null;
    }
    override public string getDesc()
    {
        return "SEARCH DECK ABIL";
    }
}