using System.Collections.Generic;

public class AddAbility : AbilityEffect
{
	public List<PassiveAbility> extraPassives;
	public List<ActiveAbility> extraActives;
	public List<AltAbility> extraAlts;
	public List<GraveAbility> extraGraves;

	public AddAbility()
	{
		extraAlts = new List<AltAbility>();
		extraActives = new List<ActiveAbility>();
		extraPassives = new List<PassiveAbility>();
		extraGraves = new List<GraveAbility>();
	}

	public AddAbility(AddAbility abil):base(abil)
	{
		this.extraPassives = new List<PassiveAbility>();
		for (int i = 0; i < abil.extraPassives.Count; i++)
		{
			extraPassives.Add(((PassiveAbility)abil.extraPassives[i].deepCopy()));
		}
		
		this.extraActives = new List<ActiveAbility>();
		for (int i = 0; i < abil.extraActives.Count; i++)
		{
			extraActives.Add(((ActiveAbility)abil.extraActives[i].deepCopy()));
		}

		this.extraAlts = new List<AltAbility>();
		for (int i = 0; i < abil.extraAlts.Count; i++)
		{
			extraAlts.Add(((AltAbility)abil.extraAlts[i].deepCopy()));
		}

		this.extraGraves = new List<GraveAbility>();
		for (int i = 0; i < abil.extraAlts.Count; i++)
		{
			extraGraves.Add(((GraveAbility)abil.extraGraves[i].deepCopy()));
		}
	}

	public void addActive(ActiveAbility act)
	{
		extraActives.Add(act);
	}

	public void addPassive(PassiveAbility pas)
	{
		extraPassives.Add(pas);
	}

	public void addAlt(AltAbility alt)
	{
		extraAlts.Add(alt);
	}

	public void addGrave(GraveAbility grave)
	{
		extraGraves.Add(grave);
	}

	public void addNewAbility(Ability abil)
	{
		if (abil is AltAbility)
		{
			addAlt((AltAbility)abil);
		}
		else if (abil is GraveAbility)
		{
			addGrave((GraveAbility)abil);
		}
		else if (abil is ActiveAbility)
		{
			addActive((ActiveAbility)abil);
		}
		else if (abil is PassiveAbility)
		{
			addPassive((PassiveAbility)abil);
		}
	}

	public List<Ability> getAllNewAbilities()
	{

		List<Ability> abils = new List<Ability>();
		abils.AddRange(extraPassives.ConvertAll(x => (Ability)x));
		abils.AddRange(extraActives.ConvertAll(x => (Ability)x));
		abils.AddRange(extraAlts.ConvertAll(x => (Ability)x));
		abils.AddRange(extraGraves.ConvertAll(x => (Ability)x));
		return abils;
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				foreach (PassiveAbility abil in extraPassives)
					target.addPassive(abil.deepCopy() as PassiveAbility);
				foreach (ActiveAbility abil in extraActives)
					target.addActive(abil.deepCopy() as ActiveAbility);
				foreach (AltAbility abil in extraAlts)
					target.addAlt(abil.deepCopy() as AltAbility);
				foreach (GraveAbility abil in extraGraves)
					target.addGraveAbility(abil.deepCopy() as GraveAbility);
				prevResult.addedAbil += 1;
				workedTargets.Add(target);
			}
		}

		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public string getDesc()
	{
		return "ADD ABILITY ABIL";
	}
	
	override public AbilityEffect deepCopy()
	{
		AddAbility abil = new AddAbility(this);
		return abil;
	}
}
