﻿using System.Collections.Generic;
using LitJson;
using UnityEngine;

public class ReplaceCustomAbility : CustomAbilityEffect
{
	AbilityDataHelper dataHelper;
	public ReplaceCustomAbility():base()
	{
		init();
	}

	protected ReplaceCustomAbility(ReplaceCustomAbility a):base(a)
	{
		init();
	}

	private void init()
	{
		dataHelper = new AbilityDataHelper();
	}

	override public AbilityEffect deepCopy()
	{
		ReplaceCustomAbility d = new ReplaceCustomAbility(this);
		return d;
	}
	

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();

		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				Player player = model.getPlayerFromCard(target);
				Card c = CardLibrary.getCardFromID(target.getID());
				c.setDizzy(Dizzy.DIZZY);
				c.setState( CardState.BOARD);
				c.setTeam(player.getTeam());
				player.replaceCard(target, c);

				workedTargets.Add(target);
			}
		}
		
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		prevResult.addCustomAbilitySuccess(getUniqueName());
		return success;
	}

	override public string getDesc()
	{
		return "Replace target card on board with original copy.";
	}
	
	override public string getUniqueName()
	{
		return "customReplace";
	}
	
	override public void handleJSON(JsonData effectObj)
	{
	}

	override public void convertToJSON(JsonData originalObj)
	{
	}

}
