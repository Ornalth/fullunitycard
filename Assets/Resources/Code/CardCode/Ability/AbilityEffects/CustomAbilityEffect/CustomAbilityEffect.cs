﻿using UnityEngine;
using System.Collections;
using LitJson;

public abstract class CustomAbilityEffect : AbilityEffect
{
	public abstract string getUniqueName();//must be unique, must be built at code runtime,.
	public abstract void handleJSON(JsonData effectObj);
	public abstract void convertToJSON(JsonData originalObj);

	public CustomAbilityEffect():base()
	{

	}

	protected CustomAbilityEffect(CustomAbilityEffect c):base(c)
	{
		
	}
}