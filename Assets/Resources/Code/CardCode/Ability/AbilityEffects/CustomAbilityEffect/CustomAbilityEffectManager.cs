﻿using UnityEngine;
using System.Collections.Generic;
using LitJson;
using System;

public class CustomAbilityEffectManager 
{

	private Dictionary<string, Type> abilDictionary;
	private static CustomAbilityEffectManager instance;

	private CustomAbilityEffectManager()
	{
		abilDictionary = new Dictionary<string, Type>();
	}
	public static CustomAbilityEffectManager getInstance ()
	{
		if (instance == null)
		{
			instance = new CustomAbilityEffectManager();
		}
		return instance;
	}

	public void setAbilDictionary(Dictionary<string, Type> dict)
	{
		abilDictionary = dict;
	}

	public Type getAbilityEffect(string s)
	{
		Type type = null;
		if (abilDictionary.TryGetValue(s, out type))
		{

		}
		return type;
	}
}


