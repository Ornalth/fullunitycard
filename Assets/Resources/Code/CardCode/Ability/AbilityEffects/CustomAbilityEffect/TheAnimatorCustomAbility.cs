using System.Collections.Generic;
using LitJson;
using UnityEngine;

public class TheAnimatorCustomAbility : CustomAbilityEffect
{
	AbilityDataHelper dataHelper;
	public TheAnimatorCustomAbility():base()
	{
		init();
	}

	protected TheAnimatorCustomAbility(TheAnimatorCustomAbility a):base(a)
	{
		init();
	}

	private void init()
	{
		dataHelper = new AbilityDataHelper();
		dataHelper.source = AbilityDataHelper.SOURCE_TARGETS;
		dataHelper.typeCounter = AbilityDataHelper.TYPE_COUNT_STAT;
		List<StatFlags> flags = new List<StatFlags>();
		StatFlags flag = new StatFlags();
		flag.flags = StatFlags.ALL_MANA;
		flags.Add(flag);
		dataHelper.flags = flags;
	}

	override public AbilityEffect deepCopy()
	{
		TheAnimatorCustomAbility d = new TheAnimatorCustomAbility(this);
		return d;
	}
	
	private int getTotalRes(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult)
	{
		dataHelper.reset();
		return dataHelper.getValue(source, targets, model, abilSource, prevResult);
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();

		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				Player player = model.getPlayerFromCard(source);
				List<Card> tempNewTarget = new List<Card>();
				tempNewTarget.Add(player);
				int totalRes = getTotalRes (source,tempNewTarget, model, abilSource, prevResult);
				MyLogger.log("TOTAL REZ " + totalRes);
				totalRes += 100; //Add 25 to all 4
				int rez = totalRes/Constants.NUM_REZ; // Divide resources evenly
				int []newResources = new int[Constants.NUM_REZ];
				for (int i = 0; i < Constants.NUM_REZ; i++)
					newResources[i] = rez;
				player.setResources(newResources);
				workedTargets.Add(target);
			}
		}
		
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		prevResult.addCustomAbilitySuccess(getUniqueName());
		return success;
	}

	override public string getDesc()
	{
		return "Add all mana from player, split evenly.";
	}
	
	override public string getUniqueName()
	{
		return "customSplitMana";
	}
	
	override public void handleJSON(JsonData effectObj)
	{
	}

	override public void convertToJSON(JsonData originalObj)
	{
	}

}
