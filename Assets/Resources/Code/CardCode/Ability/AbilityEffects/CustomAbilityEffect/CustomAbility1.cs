using System.Collections.Generic;
using LitJson;
using UnityEngine;

public class CustomAbility1 : CustomAbilityEffect
{
	private int randomStoredValue = 0;
	
	public CustomAbility1():base()
	{

	}

	protected CustomAbility1(CustomAbility1 a):base(a)
	{
		randomStoredValue = a.randomStoredValue;
	}

	override public AbilityEffect deepCopy()
	{
		CustomAbility1 d = new CustomAbility1(this);
		return d;
	}
	
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();

		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				MyLogger.log("I AM CUSTOM : MY VALUE : " + randomStoredValue);
			}
		}
		
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public string getDesc()
	{
		return "CUSTOM TEST ABIL 1";
	}
	
	override public string getUniqueName()
	{
		return "customAbil1";
	}
	
	override public void handleJSON(JsonData effectObj)
	{
		randomStoredValue = (int)effectObj["test"]; 
	}

	override public void convertToJSON(JsonData originalObj)
	{
		originalObj["test"] = randomStoredValue;
	}

}

