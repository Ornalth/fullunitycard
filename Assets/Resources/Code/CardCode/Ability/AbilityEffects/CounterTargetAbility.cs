using System.Collections.Generic;

public class CounterTargetAbility : AbilityEffect
{
	int amount;

	public CounterTargetAbility():base()
	{

	}

	public CounterTargetAbility(CounterTargetAbility abil):base(abil)
	{

	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		/*
		TODO ONLY DOES THE OLD DISSIPATE VERSION
		*/
		model.toggleCounterStack(model.getCurrentExecutingStackDetail().getPriority(model.getCurrentTurn()));
		return true;
	}

		override public string getDesc()
	{
		return "counter target abil";
	}

	override public AbilityEffect deepCopy()
	{
		CounterTargetAbility abil = new CounterTargetAbility(this);
		return abil;
	}
}