using UnityEngine;
using System.Collections.Generic;

public class AddCounterAbility : AbilityEffect, IDirectSetAbility, ISelfEffect, IFullEffect
{
	public static readonly bool DEFAULT_FULL = false;
	bool isToFull = DEFAULT_FULL; //TODO
	bool isSource = false;
	bool directSet = false;
	string counterName = Counter.DEFAULT_COUNTER_NAME;
	int maxAmount = Counter.INFINITE;

	public AddCounterAbility():base()
	{
	}

	protected AddCounterAbility(AddCounterAbility a):base(a)
	{
		isSource = a.isSource;
		this.directSet = a.directSet;
		this.counterName = a.counterName;
		isToFull = a.isToFull;
		maxAmount = a.maxAmount;
	}

	private int getValue()
	{
		return values[0];
	}	

	public bool isDirect()
	{
		return directSet;
	}


	public bool getIsSelf()
	{
		return isSource;
	}

	public void setSelf(bool b)
	{
		isSource = b;
	}
	

	public void setDirect(bool b)
	{
		directSet = b;
	}

	public void setCounterName(string name)
	{
		counterName = name;
	}

	public string getCounterName()
	{
		return counterName;
	}

	public bool isFull()
	{
		return isToFull;
	}

	public void setIsFull(bool b)
	{
		isToFull = b;
	}

	public void setMaxAmount(int i)
	{
		maxAmount = i;
	}

	public int getMaxAmount()
	{
		return maxAmount;
	}

	override public AbilityEffect deepCopy()
	{
		AddCounterAbility d = new AddCounterAbility(this);
		return d;
	}
	
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		int value = getValue();
	
		if (isSource)
		{

			if (directSet)
			{
				source.directSetCounter(counterName,value, source, maxAmount);
				
			}
			else if (isToFull)
			{
				source.refillAllCounter(counterName, source, maxAmount);
			}
			else
			{
				source.refillCounter(counterName, value, source, maxAmount);

		
			}

				prevResult.countersAdded += value; //TODO FOR FULL... zzzz
				workedTargets.Add(source);

		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					if (directSet)
					{
						target.directSetCounter(counterName,value, source, maxAmount);
					}
					else if (isToFull)
					{
						target.refillAllCounter(counterName, source , maxAmount);
					}
					else
					{
						target.refillCounter(counterName, value, source, maxAmount);
					}
					prevResult.countersAdded += value;
					workedTargets.Add(target);
				}
			}
		}
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public string getDesc()
	{
		return "ADD COUNTER ABIL";
	}
	
}