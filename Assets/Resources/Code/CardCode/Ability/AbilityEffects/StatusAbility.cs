using System.Collections.Generic;

public class StatusAbility : AbilityEffect {


	List<StatusEffect> addStatuses;// = new List<StatusEffect>();
	List<StatusEffect> removeStatuses;// = new List<StatusEffect>();

	public StatusAbility():base()
	{
		addStatuses = new List<StatusEffect>();
		removeStatuses = new List<StatusEffect>();
	}

	public StatusAbility(StatusAbility abil):base(abil)
	{
		addStatuses = new List<StatusEffect>();
		foreach (StatusEffect eff in abil.addStatuses)
		{
			addStatuses.Add(new StatusEffect(eff));
		}
		removeStatuses = new List<StatusEffect>();
		foreach (StatusEffect eff in abil.removeStatuses)
		{
			removeStatuses.Add(new StatusEffect(eff));
		}
	}


	public void addAddStatus(StatusEffect e)
	{
		addStatuses.Add(e);
	}

	public void addRemoveStatus(StatusEffect e)
	{
		removeStatuses.Add(e);
	}

	public List<StatusEffect> getAddStatus()
	{
		return addStatuses;
	}

	public List<StatusEffect> getRemoveStatus()
	{
		return removeStatuses;
	}
	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		foreach (Card target in targets)
		{
			if (TargetType.satisfies(target, targType,
					TargetTeam.getTargetTeamFromCards(source, target)))
			{
				foreach (StatusEffect eff in addStatuses)
				{
					StatusEffect statuseff = new StatusEffect(eff);
					target.addStatusEffect(statuseff);
				}

				foreach (StatusEffect eff in removeStatuses)
				{
					StatusEffect statuseff = new StatusEffect(eff);
					target.removeStatusEffect(statuseff);
				}
				workedTargets.Add(target);
				//TODO PREV RESUTL.
			}
		}

				bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		StatusAbility abil = new StatusAbility(this);
		return abil;
	}

	override public string getDesc()
	{
		return "status effecgt adding abil";
	}

}