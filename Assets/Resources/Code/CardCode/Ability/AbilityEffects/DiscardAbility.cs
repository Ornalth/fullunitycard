using System.Collections.Generic;
using UnityEngine;


public class DiscardAbility : CoroutineAbility, ISelfEffect, IPreAbilityEffect
{
    public static readonly bool DEFAULT_STRICT = true;
    public static readonly bool DEFAULT_RANDOM = false;

    public TargetType discardTargType = null; // can only discard specific
    // cards
    public bool discardStrict = DEFAULT_STRICT;
    public bool discardRandom = DEFAULT_RANDOM;
    //public bool playerChoice = false;//TRUE = TRUE PICK, FALSE = SELF PICK;
    private bool hasStoredState = false;
    public int playerChoiceTargetTeam = TargetTeam.TEAM_ALLY;

    private Card ssource;
    private List<Card> stargets;
    private MainBoardModel smodel;
    private Ability sabilSource;
    private AbilityResult sprevResult;
    private int sindex;
    private bool reqModel = false;
    public bool isSource = false;

    public DiscardAbility():base()
    {

    }
    private int getValue()
    {
        return values[0];
    }

    protected DiscardAbility(DiscardAbility a):base(a)
    {
        discardTargType = a.discardTargType;
        discardStrict = a.discardStrict;
        discardRandom = a.discardRandom;
        playerChoiceTargetTeam = a.playerChoiceTargetTeam;
        isSource = a.isSource;
    }

    public void setSelf(bool b)
    {
        isSource = b;
    }

    public bool getIsSelf()
    {
        return isSource;
    }

    public void setDiscardStrict(bool strict)
    {
        this.discardStrict = strict;
    }

    public void setDiscardRandom(bool random)
    {
        discardRandom = random;
    }

    public bool getStrict()
    {
        return discardStrict;
    }

    public bool getRandom()
    {
        return discardRandom;
    }

    override public AbilityEffect deepCopy()
    {
        DiscardAbility d = new DiscardAbility(this);
        return d;
    }

    override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
    {
        //reqModel = false;
       // restoreState();
        reqModel = false;
        int index = 0;
        if(hasStoredState)
        {
            MyLogger.log("RESTORE DISCARD SAVE STATE");
            index = sindex;
            hasStoredState = false;
            source = ssource;
            targets = stargets;
            model = smodel;
            abilSource = sabilSource;
            prevResult = sprevResult;

        }
        else

        {
            MyLogger.log("NOTHIGN TO RESTGORE");
        }

        List<Card> workedTargets = new List<Card>();

        if (isSource)
        {
            int value = getValue();
            Player playerSource = model.getPlayerFromCard(source);
            if (discardRandom)
            {
                playerSource.discardRandomly(source, value, discardTargType);
                prevResult.discard += value;
                workedTargets.Add(source);
            }
            else
            {
                ((Player) playerSource).requestDiscard(source,
                                    value, discardTargType, discardStrict,
                                     playerChoiceTargetTeam);
                prevResult.discard += value;
                workedTargets.Add(source);
                saveState(source,  targets, model, abilSource, prevResult,0);
                //TODO i dunno maybe we dont need to here.
                return false;
            }
            
        }
        else
        {
            int value = getValue();
            for (int i = index; i < targets.Count; i++)
            {
                Card target = targets[i];
                if (target.getCardType() == CardType.PLAYER)
                {
                    MyLogger.log("WHAT IS THIS CARD " + target + " " + target.getState());
                    if (TargetType.satisfies(target, targType, TargetTeam
                            .getTargetTeamFromCards(source, target)))
                    {
                        MyLogger.log("FAIL?");
                        if (discardRandom)
                        {
                            ((Player) target).discardRandomly(source, value, discardTargType);
                            prevResult.discard += value;
                            workedTargets.Add(source);
                        }
                        else
                        {

                            ((Player) target).requestDiscard(source,
                                    value, discardTargType, discardStrict,
                                     playerChoiceTargetTeam);
                            //modelStatus++; // make sure we move to next target!
                            //reqModel = true; //TODO?
                            prevResult.discard += value;
                            workedTargets.Add(source);
                            saveState(source,  targets, model, abilSource, prevResult, i);
                            return false;
                        }
                    }
                    else

                    {
                        MyLogger.log("NO GOOD");
                    }
                }
                else
                {
                    Debug.Log
                            ("In passive ability event: non player cannot discard cards");
                }
            }
        }
        bool success = workedTargets.Count >= targets.Count;
        prevResult.success = success;
        prevResult.targets = workedTargets;
        reqModel = false;
        resetState();
        return success;
    }



    override public bool requiresModel()
    {
        return reqModel;
    }

    override protected  void saveState(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, int index)
    {
        MyLogger.log("REWQUEST DISCARD SAVE STATE");
        reqModel = true;
        hasStoredState = true;
        ssource = source;
        stargets = targets;
        smodel = model;
        sabilSource = abilSource;
        sprevResult = prevResult;
        sindex = index + 1;

    }
    protected void resetState()
    {
        reqModel = false;
        hasStoredState = false;
        ssource = null;
        stargets = null;
        smodel = null;
        sabilSource = null;
        sprevResult = null;
    }
    override public string getDesc()
    {
        return "DISCARD ABIL";
    }

//All cards should be in hand and ALLY
    public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
    {
        Player player = model.getPlayerFromCard(source);
        if (discardRandom)
        {
            if (getValue() >= player.getHand().Count)
            {
                return true;
            }
            return false;
        }
        foreach (Card card in targets)
        {
            if (card.getTeam() != source.getTeam() || card.getState() != CardState.HAND)
                return false;
        }
        return true;
    }
    
    //SELF DISCARD ALL CARDS.
    public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
    {
        Player player = model.getPlayerFromCard(source);
        if (discardRandom)
        {
            player.discardRandomly(source, getValue(), discardTargType);
        }
        else
        {
            player.discard(targets);
        }
    }
}