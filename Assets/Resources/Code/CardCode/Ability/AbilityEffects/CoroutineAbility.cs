using System.Collections.Generic;


public abstract class CoroutineAbility : AbilityEffect
{
	public CoroutineAbility():base()
	{

	}

	public CoroutineAbility(CoroutineAbility abil):base(abil)
	{

	}
    public abstract bool requiresModel();
    protected abstract void saveState(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, int index);
}
