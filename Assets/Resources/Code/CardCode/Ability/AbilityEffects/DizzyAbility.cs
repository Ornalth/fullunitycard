using System.Collections.Generic;
using UnityEngine;

public class DizzyAbility : AbilityEffect , ISelfEffect, IPreAbilityEffect
{
	int dizzyFactor;
	bool isSource = false;

	public DizzyAbility():base()//int factor)
	{
		dizzyFactor = Dizzy.DizzyFactor.F_NONE;
	//	dizzyFactor = factor;
	}
	public DizzyAbility(DizzyAbility diz):base(diz)
	{
		dizzyFactor = diz.dizzyFactor;
		isSource = diz.isSource;
	}

	public void setDizzyFactor(int  factor)
	{
		dizzyFactor = factor;
	}

    public void setSelf(bool b)
    {
        isSource = b;
    }
        public bool getIsSelf()
    {
        return isSource;
    }

	public int getDizzyFactor()
	{
		return dizzyFactor;
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{

		List<Card> workedTargets = new List<Card>();

		if (isSource)
		{
			Dizzy dizzy = source.getDizzy();
			dizzy.modifyDizzy(dizzyFactor);
			source.setDizzy(dizzy.getDizzy());
			workedTargets.Add(source);
		}
		else
		{
			foreach (Card target in targets)
			{
				if (TargetType.satisfies(target, targType,
						TargetTeam.getTargetTeamFromCards(source, target)))
				{
					Dizzy dizzy = target.getDizzy();
					dizzy.modifyDizzy(dizzyFactor);
					target.setDizzy(dizzy.getDizzy());
					workedTargets.Add(target);
				}
			}
		}

		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public string getDesc()
	{
		return "dizzy abil";
	}

	override public AbilityEffect deepCopy()
	{
		DizzyAbility abil = new DizzyAbility(this);
		return abil;
	}

	//All cards should be in hand and ALLY
    public bool canSatisfy(Card source, List<Card> targets, MainBoardModel model)
    {
    	if (isSource)
    	{
    		return !source.isDizzy();
    	}
    	foreach (Card target in targets)
		{
			MyLogger.log("CHECK");
			if (target.isDizzy())
				return false;
		}
		return true;
    }
    
    //SELF DISCARD ALL CARDS.
    public void preEffectAct(Card source, List<Card> targets, MainBoardModel model)
    {
    	if (isSource)
    	{
    		Dizzy dizzy = source.getDizzy();
			dizzy.modifyDizzy(dizzyFactor);
			source.setDizzy(dizzy.getDizzy());
    	}
    	else
    	{
    		foreach (Card target in targets)
			{
				Dizzy dizzy = target.getDizzy();
				dizzy.modifyDizzy(dizzyFactor);
				target.setDizzy(dizzy.getDizzy());
			}
    	}
    	
    }
}