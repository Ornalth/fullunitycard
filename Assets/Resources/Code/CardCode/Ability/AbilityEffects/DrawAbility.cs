using System.Collections.Generic;
using UnityEngine;

public class DrawAbility : AbilityEffect , ISelfEffect
{
	bool isSource = false;
	public DrawAbility():base()
	{
	}

	public DrawAbility(DrawAbility abil):base(abil)
	{
		isSource = abil.isSource;
	}

    public void setSelf(bool b)
    {
        isSource = b;
    }

    public bool getIsSelf()
    {
        return isSource;
    }

	private int getDrawNumber()
	{
		return values[0];
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		MyLogger.log(targets.Count);
		List<Card> workedTargets = new List<Card>();
		if (isSource)
		{
			Player playerSource = model.getPlayerFromCard(source);
			int count = getDrawNumber();
			playerSource.drawXCards(count);
			prevResult.draw += count;
			workedTargets.Add(playerSource);
			MyLogger.log("WHAT THE FK?");
		}
		else
		{
			foreach (Card target in targets)
			{
				if (target.getCardType() == CardType.PLAYER)
				{
					if (TargetType.satisfies(target, targType,
							TargetTeam.getTargetTeamFromCards(source, target)))
					{
						int count = getDrawNumber();
						((Player) target).drawXCards(count);
						prevResult.draw += count;
						workedTargets.Add(target);
					}
					else 
					{
					MyLogger.log("WHAT THE FK FAILED PresrLAYER?");

					}
				}
				else
				{
					MyLogger.log("WHAT THE FK FAILED PLAYER?" + target.getName());
					//System.out
					//		.println("In passive ability event: non player cannot draw cards");
				}
			}
		}
		
		bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		DrawAbility abil = new DrawAbility(this);
		return abil;
	}


	override public  string getDesc()
	{
		return "DRAW EFF";
	}
}