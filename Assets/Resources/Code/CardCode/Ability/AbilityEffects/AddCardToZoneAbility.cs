using System.Collections.Generic;

public class AddCardToZoneAbility : AbilityEffect
{
	public static int DEFAULT_TARGET_TEAM = TargetTeam.TEAM_ALLY;

	
	protected int toZone = CardState.NONE;
	protected int zoneTargetTeam = DEFAULT_TARGET_TEAM;

	public AddCardToZoneAbility():base()
	{
	}

	public AddCardToZoneAbility(AddCardToZoneAbility abil):base(abil)
	{
		toZone = abil.toZone;
		zoneTargetTeam = abil.zoneTargetTeam;
	}
	
		public void setTargetZone(int toZone)
	{
		this.toZone = toZone;
	}

	public void setZoneTargetTeam(int targTeam)
	{
		this.zoneTargetTeam = targTeam;
	}

	public int getTargetZone()
	{
		return toZone;
	}

	public int getZoneTargetTeam()
	{
		return zoneTargetTeam;
	}

	private int getCardID()
	{
		return values[0];
	}

	override protected bool theEvent(Card source, List<Card> targets, MainBoardModel model, Ability abilSource, AbilityResult prevResult, List<Card> lastActionCards)
	{
		List<Card> workedTargets = new List<Card>();
		Player recip = model.getPlayerFromCard(source);
		if (zoneTargetTeam != TargetTeam.TEAM_ALLY)
		{
			recip = model.getOtherPlayer(recip);
		}
		
		Card card = CardLibrary.getCardFromID(getCardID());
		recip.addCard(card, toZone);
		

		bool success = true;
						//bool success = workedTargets.Count >= targets.Count;
		prevResult.success = success;
		prevResult.targets = workedTargets;
		return success;
	}

	override public AbilityEffect deepCopy()
	{
		AddCardToZoneAbility abil = new AddCardToZoneAbility(this);
		return abil;
	}

	override public  string getDesc()
	{
		return "addcardtozone EFF";
	}
}
