using System.Collections.Generic;
using System;
using UnityEngine;

public class ActiveAbility : PassiveAbility
{

	public static readonly int DEFAULT_DIZZY_COST = Dizzy.DizzyFactor.F_NONE;
	public static readonly int DEFAULT_COUNTER_COST = 0;

	protected int[] cost = new int[Constants.NUM_REZ];
	protected int dizzyCost = DEFAULT_DIZZY_COST;

	List<CounterCost> counterCost = new List<CounterCost>();

	public ActiveAbility():base()
	{

	}

	public ActiveAbility(ActiveAbility p) : base(p)
	{
		Array.Copy(p.cost, this.cost,
		           p.cost.Length);

		this.dizzyCost = p.dizzyCost;
		this.counterCost = p.counterCost;
		
		counterCost = new List<CounterCost>();
		foreach (CounterCost cCost in p.counterCost)
		{
			counterCost.Add(new CounterCost(cCost));
		}
	}

	//Setters
	virtual	public void setCost(int[] cost2)
	{
		cost = cost2;

	}

	virtual	public void setDizzyCost(int dizzy)
	{
		dizzyCost = dizzy;

	}


	virtual public void setCounterCost(List<CounterCost> cost)
	{
		counterCost = cost;
	}

	virtual public List<CounterCost> getCounterCost()
	{
		return counterCost;
	}


	

	public int[] getCost()
	{
		return cost;
	}

	virtual public int getDizzyCost()
	{
		return dizzyCost;
	}

	override public int getAbilityType()
	{
		return AbilityType.ACTIVE;
	}

	override public string getDesc()
	{
		return "-> " + desc;
	}

//ACTING ON DATA

	virtual public bool canPay(Player p, Card origin, Dictionary<int, List<Card>> targetDict)
	{
		//Resource Payment
		if (p.canPayResourceCost(this.cost) && canPayDizzy(p,origin) && validateTargets(origin, targetDict))

		{
			if (origin.canPayCounter(counterCost, origin))
			{
//				MyLogger.log("CAN PAY COUNTER!!!");
				return true;
			}
			MyLogger.log("CANT PAY COUNTER");

		}
		MyLogger.log("CANT PAY SOMETHING");
		return false;
	}

	virtual protected bool canPayDizzy(Player p, Card origin)
	{
		if(this.dizzyCost == Dizzy.DizzyFactor.F_DIZZY || this.dizzyCost == Dizzy.DizzyFactor.F_DIZZY_LOCK)
			return !origin.isDizzy();
		return true;
	}

	virtual public bool canPayWithoutExtra(Player p, Card origin)
	{
		if (p.canPayResourceCost(this.cost) && canPayDizzy(p,origin))

		{
			if (origin.canPayCounter(counterCost, origin))
			{
				return true;
			}

		}
		return false;
	}

	virtual public bool canPayExtra(Card origin, Dictionary<int, List<Card>> cards)
	{
		/*
		if (hasExtraCost())
		{
			int extraCost = getExtraCostType();
			TargetType extraType = getExtraCostTargetType();
			int numExtraCost = extraType.numTargets;

			if (extraCost == ExtraCost.COST_SAC)
			{
				if (cards == null)
				{
					return false;
				}
				// Anything that doesnt satisfy needs to be removed.
				//
				foreach (Card target in cards)
				{
					if (!target.isDead()
							&& TargetType.satisfies(target,extraType, TargetTeam
									.getTargetTeamFromCards(origin, target)))
					{
						numExtraCost--;
					}
					else
					{
						//bad target
						return false;
					}
				}

				// too few/many targets
				if (numExtraCost != 0)
					return false;
			}
			else if (extraCost == ExtraCost.COST_DISCARD)
			{
				if (cards == null)
				{
					return false;
				}
				// Anything that doesnt satisfy needs to be removed.
				//
				foreach (Card target in cards)
				{
					if (TargetType.satisfies(target,extraType, TargetTeam
							.getTargetTeamFromCards(origin, target)))
					{
						numExtraCost--;
					}
					else
					{
						return false;
					}
				}

				// not enough targets
				if (numExtraCost != 0)
					return false;
			}
			//TODO mill (actually mill alwys true, sac self always true)
		}
*/
		return true;

	}

	virtual public bool pay(Player p, Card origin, Dictionary<int, List<Card>> targetDict)
	{
		if (canPay(p, origin,targetDict) )
		{
			p.payCost(this.cost);
			payDizzy(origin);
			payCounter(origin);
			payExtraCost(p, origin, targetDict);
			return true;
		}
		return false;
		//return false;
	}

	virtual protected void payDizzy(Card origin)
	{
		origin.getDizzy ().modifyDizzy (dizzyCost);

	}
	virtual protected void payExtraCost(Player player, Card origin,  Dictionary<int, List<Card>> targetDict) {
		//TODO FIXME EXTRACOST CHANGE
		/*if (origin.hasExtraCost())
		{
			int cost = origin.getExtraCost();

			if (cost == ExtraCost.COST_SAC)
			{
				player.sacrifice(targets);
			}
			else if (cost == ExtraCost.COST_DISCARD)
			{
				player.discard(targets);
			}
			else if (cost == ExtraCost.COST_MILL)
			{
				player.mill(origin.getExtraCostTargetType().numTargets);
			}
			else if (cost == ExtraCost.COST_SAC_SELF)
			{
				player.sacrifice(origin);
			}
		}*/
	}

	override public EventResult theEvent(Card source, Dictionary <int, List<Card>> targetDict, MainBoardModel model, Ability abilSource, AbilityResult result, EventResult currentResult)
	{
		return base.theEvent(source, targetDict, model, abilSource, result, currentResult);
	}

	virtual public void payCounter(Card origin)
	{
		MyLogger.log("PAY COUNTER!!!");
		origin.payCounter(counterCost, origin);
	}

//SPECIAL
	override public Ability deepCopy()
	{
		ActiveAbility p = new ActiveAbility(this);
		return p;
	}

	

}
