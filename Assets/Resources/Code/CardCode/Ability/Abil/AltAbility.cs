using System.Collections.Generic;
using UnityEngine;
//TODO...?...
public class AltAbility : ActiveAbility
{
	int paidAltIndex;
	List<ActiveAbility> altAbilities;
	public AltAbility()
	{
		altAbilities = new List<ActiveAbility>();
		paidAltIndex = -1;
	}

	public AltAbility(AltAbility alt):base(alt)
	{
		altAbilities = new List<ActiveAbility>();
		foreach (ActiveAbility abil in alt.altAbilities)
		{
			altAbilities.Add(((ActiveAbility)abil.deepCopy()));
		}
		paidAltIndex = -1;
	}


	//SETTERS 
	public void addAltAbility(ActiveAbility alt)
	{
		altAbilities.Add(alt);
	}

	//GETTERS
	public List<ActiveAbility> getAlts()
	{
		return altAbilities;
	}

		override public int getAbilityType()
	{
		return AbilityType.ALTERNATE;
	}

	public ActiveAbility getAbility(int i )
	{
		return altAbilities[i];
	}
	//DO STUFF

	override public EventResult theEvent(Card source, Dictionary <int, List<Card>> targetDict, MainBoardModel model, Ability abilSource, AbilityResult result, EventResult currentResult)
	{
		if (paidAltIndex == -1)
		{
			return base.theEvent(source, targetDict, model, abilSource, result, currentResult);
		}
		else
		{
			int temp = paidAltIndex;
			paidAltIndex = -1;
			return altAbilities[temp].theEvent(source, targetDict, model, abilSource, result, currentResult);
		}
		/*if (hasPaid())
		{
			return base.theEvent(source, targets, extraDetailTargets, model);
		}
		else
		{
			MyLogger.log("In ActiveAbility, did not pay for ability!~");
		}
		return false;*/
	}



	public bool pay(Player p, Card origin, Dictionary<int, List<Card>> targetDict, int index)
	{
		if (paidAltIndex != -1)
		{
			MyLogger.log("????? ERROR - ALREADY PAID ALT.");
		}

		if(altAbilities[index].pay(p, origin, targetDict))
		{
			paidAltIndex = index;
			return true;
		}
		return false;
	}

	public bool canPay(Player p, Card origin, int index)
	{
		return altAbilities[index].canPayWithoutExtra(p, origin);
	}

	public bool canPayExtra(Player p, Card origin, Dictionary<int, List<Card>> targetDict, int index)
	{
		return altAbilities[index].canPayExtra(origin, targetDict);
	}
//Special
	override public Ability deepCopy()
	{
		AltAbility abil = new AltAbility(this);
		return abil;
	}

	public bool payAlt(Player p, Card origin, Dictionary<int, List<Card>> targetDict, int index)
	{
		return altAbilities[index].pay(p, origin, targetDict);
		// return false;
	}

	/*public bool canPayAlt(Player p, Card origin, List<Card>extras, int index)
	{
		ActiveAbility abil = altAbilities[index];
		return abil.canPay(p,origin, extras);
	}*/
}	
