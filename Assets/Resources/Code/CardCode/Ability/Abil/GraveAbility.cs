﻿using UnityEngine;
using System.Collections;

public class GraveAbility : ActiveAbility {
	public GraveAbility():base()
	{

	}

	public GraveAbility(GraveAbility p) : base(p)
	{
	}

	override public Ability deepCopy()
	{
		GraveAbility p = new GraveAbility(this);
		return p;
	}
}
