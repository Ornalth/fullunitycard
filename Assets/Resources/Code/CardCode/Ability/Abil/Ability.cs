using System.Collections.Generic;
using UnityEngine;

public abstract class Ability 
{
	public abstract int getAbilityType();
	protected bool isCounterable = true;

	protected string desc;
	
	protected int speed;
	protected List<TargetListObject> targetListTypes;
	protected List<PreAbilityObject> extraCostEffects;
	protected List<PreAbilityObject> effects;
	//protected TargetType targType;//targType = new TargetType();
	protected string displayName;
	protected bool ignoreSource;
	//protected int targetGen;

	public Ability()
	{
		//effects = new List<AbilityEffect>();
		//modelData = new List<AbilityDataHelper>();
		isCounterable = true;
		desc = "no desc so far";
		displayName = "EMPTY DISPLAY NAME";
		speed = AbilitySpeed.A;
	//	targType = new TargetType();
		ignoreSource = false;
		//targetGen = TargetGeneration.NO_CHANGE;

		extraCostEffects = new List<PreAbilityObject>();
		targetListTypes = new List<TargetListObject>();
		effects = new List<PreAbilityObject>();
	}

	public Ability(Ability abil)
	{
		isCounterable = abil.isCounterable;
		desc = abil.desc;
		speed = abil.speed;
	//	targType = new TargetType(abil.targType);
		displayName = abil.displayName;
		ignoreSource = abil.ignoreSource;
		//targetGen = abil.targetGen;

		extraCostEffects = new List<PreAbilityObject>();
		foreach (PreAbilityObject eff in abil.extraCostEffects)
		{
			extraCostEffects.Add(new PreAbilityObject(eff));
		}

		this.targetListTypes = new List<TargetListObject>();
		foreach (TargetListObject tt in abil.targetListTypes)
		{
			targetListTypes.Add(new TargetListObject(tt));
		}


		effects = new List<PreAbilityObject>();
		foreach (PreAbilityObject eff in abil.effects)
		{
			effects.Add(new PreAbilityObject(eff));
		}
	}

	public List<PreAbilityObject> getExtraCostEffects()
	{
		return extraCostEffects;
	}

	public void setExtraCostEffects(List<PreAbilityObject> effs)
	{
		extraCostEffects = effs;
	}
	public List<TargetListObject> getTargetListObjects()
	{
		return targetListTypes;
	}

	public void setTargetTypeList(List<TargetListObject> tts)
	{
		targetListTypes = tts;
	}

	public List<PreAbilityObject> getAbilityEffects()
	{
		return effects;
	}

	public void setAbilityEffects(List<PreAbilityObject> effs)
	{
		effects = effs;
	}

	public string getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(string s)
	{
		displayName = s;
	}


	public void shouldIgnoreSource(bool s)
	{
		ignoreSource = s;
	}

	public void setAbilitySpeed(int sp)
	{
		speed = sp;
	}
	public int getAbilitySpeed()
	{
		return speed;
	}

	public void setDesc(string desc)
	{
		this.desc = desc;
	}

	/*public void setTargetType(TargetType t)
	{
		targType = t;
	}


	public TargetType getTargetType()
	{
		return targType;
	}
	*/
	public void setCounterable(bool counter)
	{
		this.isCounterable = counter;
	}

	public bool isThisCounterable()
	{
		return isCounterable;
	}
	
	public bool requiresStack()
	{
		return speed != AbilitySpeed.S;
	}

	public int getAbilityResponseTargetGen()
	{
		foreach (TargetListObject targetListObj in targetListTypes)
		{
			if (targetListObj.id == MainBoardModel.SPECIAL_TARGET_RESPONSE_KEY)
			{
				MyLogger.log("TARGETGEN is " + targetListObj.targetGen );
				return targetListObj.targetGen;
			}
		}
		return TargetGeneration.NO_CHANGE;
		//return targetGen;
	}
	
	public abstract Ability deepCopy();
	public abstract void counterThis();
	public abstract void uncounterThis();
	public abstract EventResult theEvent(Card source, Dictionary<int, List<Card>> targetDict, MainBoardModel model, Ability abilSource, AbilityResult prevResult, EventResult currentResult);
	public abstract string getDesc();
}
