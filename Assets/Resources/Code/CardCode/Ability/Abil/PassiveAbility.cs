using UnityEngine;
using System.Collections.Generic;

//TODO ALL THE PASSIVE SAVING STUFF THAT NEEDS TO BE DONE EVERYWHERE LOLOL.
//ERROR HERE TODO NOTE

public class PassiveAbility : Ability 
{

	public static readonly int DEFAULT_PHASE_TARGET_TEAM = PhaseTeam.TEAM_BOTH;
	public static readonly int DEFAULT_EVENT_TARGET_TEAM = TargetTeam.TEAM_BOTH;
	/**
	 * Responds to!
	 * 
	 */
	protected int phase = Phase.ERROR;
	protected int phaseTeam = PhaseTeam.TEAM_BOTH;
	//protected GameEventType eventType = null;
	protected int eventType = GameEventType.ERROR;
	protected int eventTargetTeam = TargetTeam.TEAM_BOTH;
	protected TargetType eventTargetType;
	protected bool hasSetEventTargetType;
	protected int eventResponseSelector = EventResponseSelector.ORIGIN;

	//protected AltAbility altAbility; // Alt extends passive?
	
	protected List<AbilityDataHelper> modelData; 
	protected bool countered = false;



	public PassiveAbility()
	{
		eventTargetType = new TargetType();
		hasSetEventTargetType = false;
		modelData = new List<AbilityDataHelper>();
	}

	public PassiveAbility(PassiveAbility p): base(p)
	{
		//TODO
		if (p != null)
		{
			
			phase = p.phase;
			phaseTeam = p.phaseTeam;
			eventType = p.eventType;
			eventTargetTeam = p.eventTargetTeam;
			eventTargetType = new TargetType(p.eventTargetType);
			hasSetEventTargetType = p.hasSetEventTargetType;


			eventResponseSelector = p.eventResponseSelector;	

			modelData = new List<AbilityDataHelper>();
			foreach (AbilityDataHelper helper in p.modelData)
			{
				modelData.Add(helper.deepCopy());
			}

		}
	}

	override public EventResult theEvent(Card source, Dictionary <int, List<Card>> targetDict, MainBoardModel model, Ability abilSource, AbilityResult result, EventResult currentResult)
	{
		//FIXME
		//UHHH MAYBE I SOHULD DO SMETOHNG ELSE FOR THIS
		
		//TODO FIX ME EXTRACOST
		/*if (ignoreSource)
		{
			targets.Remove(source);
			MyLogger.log("REMOVED SOURCE");
		}
*/
	
		EventResult eventResult = currentResult;
		if (eventResult == null)
		{
			eventResult = new EventResult();
			eventResult.source = source;
			//eventResult.targets = targets;
			eventResult.model = model;
			eventResult.abilSource = abilSource;
			result = new AbilityResult();
			eventResult.result = result;
		}
//		MyLogger.log("ACTIVATING EVENT!!!!");
	
		
		if (countered && isCounterable)
		{
			eventResult.done = true;
			result.success = false;
			countered = false;
			return eventResult;
		}


		Dictionary<int, List<AbilityDataHelper>> helpersDict = new Dictionary<int,  List<AbilityDataHelper>>();


		int currentIndex = eventResult.currentEff;
//		MyLogger.log("THIS EVENT HAS " + effects.Count + " EFFECTS!!!");
		for (; currentIndex < effects.Count; currentIndex++)
		{
//			MyLogger.log("Activating PreabilityObj " + currentIndex);
			PreAbilityObject abilObj = effects[currentIndex];
			int targetListIndex = abilObj.targetListNum;
			TargetType targType = findTargetTypeForIndex(targetListIndex);
			List<Card> targets = targetDict[targetListIndex];
			eventResult.targets = targets;

			List<AbilityDataHelper> curHelpers;
			if (helpersDict.ContainsKey(targetListIndex))
			{
				curHelpers = helpersDict[targetListIndex];
			}
			else
			{
				foreach (AbilityDataHelper helper in modelData)
				{
					helper.reset();
					helper.getValue(source,targets,model,abilSource,result);
				}
				curHelpers = modelData;
				helpersDict[targetListIndex] = curHelpers;
			}

			List<AbilityEffect> effs = abilObj.effects;

			for (int currentAbilEffIndex = eventResult.currentAbilEff; currentAbilEffIndex < effs.Count; currentAbilEffIndex++)
			{
				AbilityEffect eff = effs[currentAbilEffIndex];
				//Get DATA REFs.
				List<int> newValues = new List<int>();
				foreach (int dataRef in eff.modelDataRefs)
				{
//					MyLogger.log("VALUE ADDED " + modelData[dataRef]);
					newValues.Add(modelData[dataRef].getValue(eventResult.source,eventResult.targets,eventResult.model,eventResult.abilSource,result));
				}
				eff.values = newValues;
				MyLogger.log("ACTIVATING Effect Number " + currentAbilEffIndex + " : " + eff.getDesc());

				bool success = eff.activate(eventResult.source,eventResult.targets,eventResult.model,this, targType, eventResult.result, eventResult.lastActionCards);
				if (eff is CoroutineAbility && ((CoroutineAbility)eff).requiresModel())
				{
					eventResult.done = false;
					eventResult.currentEff = currentIndex;
					eventResult.currentAbilEff = currentAbilEffIndex;
					return eventResult;
				}
			}
			eventResult.currentAbilEff = 0;
		}
/*
		for(Ability chains in chainAbilities)
		{
			chainAbilities.theEvent(source,targets,model,this,result);
		}
*/
		eventResult.done = true;
		return eventResult;
	}

	private TargetType findTargetTypeForIndex(int index)
	{
		foreach (TargetListObject tlo in targetListTypes)
		{
			if (tlo.id == index)
			{
				return tlo.targetType;
			}
		}
		MyLogger.log("NO TARGETTYPE FOR INDEX" + index);
		return null;
	}
/*
	public EventResult continueEvent(EventResult eventResult)
	{

		
		int index = eventResult.currentEff;

		for (int i = index; i < eventResult.targets.Count; i++)
		{
			AbilityEffect eff = effects[i];
			//if (i == index)//in effects
			//{
				//()
			//}
			bool success = eff.activate(eventResult.source,eventResult.targets,eventResult.model,this,eventResult.result);
			if (eff is CoroutineAbility)
			{
				eventResult.done = false;
				eventResult.currentEff = i;
				return eventResult;
			}
		}


		return eventResult;
	}*/

	override public void counterThis()
	{
		MyLogger.log("COUNTERED THIS");
		countered = true;
	}

	override public void uncounterThis()
	{
		MyLogger.log("UN---COUNTERED THIS");
		countered = false;
	}


		public void setPhaseTeam(int phaseTeam)
	{
		this.phaseTeam = phaseTeam;
	}


	public void setEventTargetTeam(int eventTargetTeam)
	{
		this.eventTargetTeam = eventTargetTeam;
	}

		public void setEventResponseType(int eventType)
	{
		this.eventType = eventType;
	}

	public void setEventTargetType(TargetType type)
	{
		this.eventTargetType = type;
		hasSetEventTargetType = true;
	}

	public TargetType getEventTargetType()
	{
		return eventTargetType;
	}

	public void setEventResponseSelector(int selector)
	{
		this.eventResponseSelector = selector;
	}

	public int getPhase()
	{
		return phase;
	}

	public void addModelDataHelper(AbilityDataHelper helper)
	{
		modelData.Add(helper);
	}
/*
	public void addAbilityEffect(AbilityEffect eff)
	{
		//MyLogger.log("ADD EFF");
		effects.Add(eff);
	}
*/
	public void setModelData(List<AbilityDataHelper> helpers)
	{
		modelData = helpers;
	}

//PUBLIC GETTERS


	public List<AbilityDataHelper> getModelData()
	{
		return modelData;
	}

	override public int getAbilityType()
	{
		return AbilityType.PASSIVE;
	}

	public int getPhaseTeam()
	{
		return phaseTeam;
	}

		public int getEventTargetTeam()
	{
		return eventTargetTeam;
	}

		public int getEventResponseType()
	{
		return eventType;
	}

		public void setPhase(int phase)
	{
		this.phase = phase;
	}

//PUBLIC EXTRA dATA GETTERS	


	override public string getDesc()
	{
		return desc;
	}

	public bool respondsToNone()
	{
		if (this.phase == Phase.ERROR && this.eventType == Phase.ERROR)
		{
			return true;
		}
		return false;
	}

	public bool respondsToEvent(int type, Card origin,
			List<Card> recipients, Card thisCard)
	{
		if (type == eventType)
		{

				MyLogger.log("TYPE is " + GameEventType.eventTypeToString(type));
			Card recipient = null;
			if (recipients.Count > 0)
				recipient = recipients[0];
			if (recipient == null)
			{
				//MyLogger.log("RECIP null " + origin.getName());
			}

			int targTeam = TargetTeam.getTargetTeamFromCards(origin,
					recipient);
			if (targTeam == TargetTeam.TEAM_NONE)
			{
				targTeam = TargetTeam.getTargetTeamFromCards(origin,
					thisCard);
			}



//NO REASON TO CHECK ORIGIN AGAIN IF WE ARE SEND/RECEIVE, WE ARE SURE IT IS GOOD.
			
//			MyLogger.log("ORIGIN NAME IS " + origin.getName());
//			MyLogger.log("EVENT TEAM " + (eventTargetTeam == TargetTeam.TEAM_SEND).ToString());
//			MyLogger.log( " " + "CONTAINS " + origin.containsAbility(this).ToString());
			//IGNORE TARGETTYPE IF TEAM_SEND, JUST NEED TO KNOW IF SELF!
			if (eventTargetTeam == TargetTeam.TEAM_SEND
					&& origin.containsAbility(this)) // YOU ARE CAUSING THE EFFECT
			{
				if (hasSetEventTargetType && recipient != null)
				{
					return TargetType.satisfies(recipient, eventTargetType,
						 TargetTeam.getTargetTeamFromCards(recipient, thisCard));
				}
				return true;
			}
			else if (eventTargetTeam == TargetTeam.TEAM_RECEIVE
					&& recipient != null && recipient.containsAbility(this)) // YOU REACT TO EVENT
			{
				if (hasSetEventTargetType)
				{
					return TargetType.satisfies(recipient, eventTargetType,
						TargetTeam.getTargetTeamFromCards(recipient, thisCard));
				}
				return true;
			}

			//err hack.
			if (hasSetEventTargetType)
			{
				if (!TargetType.satisfies(origin, eventTargetType,
							TargetTeam.TEAM_BOTH))
				{
			//		MyLogger.log(eventTargetType.toString());
				//	MyLogger.log("Card Origin " + origin.getName() + " TIS CAHR" + thisCard.getName());
					return false;
				}
			}
			
				//	&& eventTargetType.satisfies(origin)
//			MyLogger.log("TARGET TEAM IS " + targTeam + " EVENTARGETTEAM IS " + eventTargetTeam);
			// correct team!
			if ((targTeam & eventTargetTeam) != 0)
			{

				return true;
			}
			else if (eventTargetTeam == TargetTeam.TEAM_TARGET) // matches target type?
			{
				return true;
			}

		}
		//MyLogger.log(eventTargetType.toString());
	//	MyLogger.log("Card Origin " + origin.getName() + " TIS CAHR" + thisCard.getName());
		return false;
	}

	public List<Card> selectResponseEventCards(Card origin, List<Card> recipients, Card thisCard)
	{
		List<Card> whatTheHeck = new List<Card>();
		if (HelperUtils.testBits(eventResponseSelector, EventResponseSelector.ORIGIN))
		{
//			MyLogger.log("ORIGIN");
			whatTheHeck.Add(origin);
		}

		if (HelperUtils.testBits(eventResponseSelector, EventResponseSelector.RECIPIENT))
		{
//			MyLogger.log("aL RCECIPS");
			whatTheHeck.AddRange(recipients);
		}
		if (HelperUtils.testBits(eventResponseSelector, EventResponseSelector.THIS))
		{
//			MyLogger.log("THIS CARD");
			whatTheHeck.Add(thisCard);
		}
		return whatTheHeck;
	}

	public bool respondsToPhase(int p, int phaseTeam)
	{
		if (p == this.phase)
		{
			if ( (this.phaseTeam & phaseTeam) != 0)
			{
				
				return true;
			}
			MyLogger.log("FAIL PHASE TEAM got " + phaseTeam + " expected " + this.phaseTeam);
		}
		return false;
	}

//SPECIAL
	override public Ability deepCopy()
	{
		PassiveAbility p = new PassiveAbility(this);
		return p;
	}

	virtual public bool validateTargets(Card origin, Dictionary<int, List<Card>> targetDict)
	{
		List<TargetListObject> targetListTypes = this.targetListTypes;
		foreach (TargetListObject tlo in targetListTypes)
		{
			if (!tlo.isSatisfied(origin, targetDict))
			{
				return false;
			}
		}
		return true;
	}

	public bool canPayStackCost(Card origin, Dictionary<int, List<Card>> targetDict, MainBoardModel model)
	{
		foreach (PreAbilityObject abilObj in extraCostEffects)
		{
			List<AbilityEffect> effs = abilObj.effects;

			if (!targetDict.ContainsKey(abilObj.targetListNum))
			{
				return false;
			}

			List<Card> targets = targetDict[abilObj.targetListNum];

			foreach (AbilityEffect eff in effs)
			{
				if (!(eff is IPreAbilityEffect))
				{
					MyLogger.log("ERROR! THIS ABILITY IS NOT A PREABILITY EFFECT INSIDE EXTRACOSTEFFECTS");
					return false;
				}

				IPreAbilityEffect preEffect = (IPreAbilityEffect)eff;
				List<int> newValues = new List<int>();
				foreach (int dataRef in eff.modelDataRefs)
				{
					//DO NOT USE ABILITY RESULT HERE!!!
					newValues.Add(modelData[dataRef].getValue(origin, targets, model, this, new AbilityResult()));	
				}
				eff.values = newValues;
				if (!preEffect.canSatisfy(origin,targets,model))
				{
					return false;
				}
			}
		}
		return true;
	}

	public bool act(Card origin, Dictionary<int, List<Card>> targetDict, MainBoardModel model)
	{
		foreach (PreAbilityObject abilObj in extraCostEffects)
		{
			List<AbilityEffect> effs = abilObj.effects;
			List<Card> targets = targetDict[abilObj.targetListNum];

			foreach (AbilityEffect eff in effs)
			{
				IPreAbilityEffect preEffect = (IPreAbilityEffect)eff;



				List<int> newValues = new List<int>();
				foreach (int dataRef in eff.modelDataRefs)
				{
					//DO NOT USE ABILITY RESULT HERE!!!
					newValues.Add(modelData[dataRef].getValue(origin, targets, model, this, new AbilityResult()));	
				}
				eff.values = newValues;
				MyLogger.log("ACTIVATING " + eff.getDesc());
				preEffect.preEffectAct(origin, targets, model);
				/*if (eff is CoroutineAbility && ((CoroutineAbility)eff).requiresModel())
				{
					MyLogger.log("ERROR! YOU SHOULD NOT REQ MODEL AFTER THIS!!!!!!");
					return false;
				}*/

			}
		}


		return true;
	}
}