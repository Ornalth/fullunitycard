

public class CardModifier
{	

	/**
	 * FACTORS
	 * 
	 */
	// Factors multiply by the stat flag
	//protected int targetStatFactor = 0; // nope fml. need to create a distinct one for each
	protected int targetAtkFactor = 0;
	protected int targetHealthFactor = 0;
	protected int targetTempHealthFactor = 0;
	protected int targetTempAtkFactor = 0;
	
	protected bool isDamage = false;
	protected int targetDamageFactor = 0;
	protected int statFlags = StatFlags.NONE; // All flag bits set will add (ALLY WILL AFFECT BOTH)
											// ENEMY ONLY AFFECT ENEMY
											// MAYBE ANOTHER FLAG FOR BOTH?
	protected bool enemyTargetSelfFlag = false; // Flag for enemy to affect self (also note must be only 1 enemy)
	//(ADD ALL STAT FLAGS)
	// hack for now ... how to do?.. maybe enum flags?...
	// /If flag true -> activate this? (
	// CURRENTLY ONLY ALLOW ALLY -- OR ONLY ENEMY
	
	/**
	 * All the Different effects in abilities
	 * 
	 */
	
	protected bool isMill = false;
	protected int millNum = 0;
	protected int millFactor = 0;
	protected bool isDestroy = false;

	protected bool isRefund = false;
	protected int[] refundFactor = new int[Constants.NUM_REZ];
	protected int[] refundConst = new int[Constants.NUM_REZ];
	protected bool isBounce = false;
	protected bool isExile = false;
	protected bool isPurge = false;
	
	protected bool isDizzy = false;
	protected int dizzyFactor; 
	
	protected bool isChangeStat = false;
	protected int atk = 0;
	protected int health = 0;
	protected int tempAtk = 0;
	protected int tempHealth = 0;
	protected int damage = 0;

	/**
	 * Summon
	 */
	protected bool isSummon = false;
	protected int[] summonID = null;
	protected bool isSummonSpecial = false;
	protected int summonAttack = -1;
	protected int summonHealth = -1;

	protected bool isDraw = false;
	protected int drawNum = 0;

	protected bool isDiscard = false;
	protected int discardNum = 0;
	protected TargetType discardTargType = null; // can only discard specific cards
	protected bool discardStrict = true;
	protected bool discardRandom = false;
	protected int playerChoice = TargetTeam.TEAM_ALLY;
	
	protected bool isSacrifice = false;
	protected int sacNum = 0;
	protected TargetType sacTargType = null; // can onyl sac specific cards.
	protected bool sacStrict = true;
	protected bool sacRandom = false;
	
	protected bool isLibrary = false;
	protected int libNum = 0;
	protected TargetType libTargType = null; // can onyl sac specific cards.
	protected bool libStrict = true;
	protected bool libRandom = false;
	protected int libCardState = CardState.NONE;
	protected bool libShuffle = true;
	protected int libTopX = 9999;

}
