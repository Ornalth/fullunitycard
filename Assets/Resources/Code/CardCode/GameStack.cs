
using System.Collections.Generic;
using UnityEngine;

//Its really a queue.
public class GameStack
{
	List<StackDetails> stack;
	bool isCountering;
	int counteringPrio;
	MainBoardModel model;

	public GameStack(MainBoardModel modelRef)
	{
		isCountering = false;
		stack = new List<StackDetails>();
		model = modelRef;
	}
	
	public void reInsert(StackDetails det)
	{
		stack.Insert(0, det);
	}

	private void insertIntoStack(int turn, StackDetails newDet)
	{
		bool success = false;
		int priority = newDet.getPriority(turn);
		int i;
		for (i = 0; i < stack.Count; i++)
		{
			StackDetails det = stack[i];
			int oldPr = det.getPriority(turn);
			
			if (oldPr > priority)
			{
				stack.Insert(i, newDet);
				success = true;
				break;
				//OR RETURN;
			}

		}

		if (!success)
		{
			stack.Add(newDet);
		}
	}

	public void addAbilityToStack(StackDetails det)
	{
		if (!det.ev.requiresStack())
		{
			
			MyLogger.log("EXECUTED INSTANTLY.");
			//instants.Add(det);
			model.executeInstantEvent(det);
		}
		else
		{
			insertIntoStack(model.getCurrentTurn(), det);
		}

	}

	public void addAllToStack(List<StackDetails> dets)
	{
		foreach (StackDetails det in dets)
		{
			addAbilityToStack(det);
		}

	}

	public StackDetails dequeue()
	{
		if (stack.Count == 0)
		{
			isCountering = false;
			return null;
		}
		StackDetails det = stack[0];
		stack.RemoveAt(0);
		if (isCountering && det.getPriority(model.getCurrentTurn()) >= counteringPrio )
		{
			det.counterThisAbility();
		}
		return det;
	}

	public int size()
	{
		return stack.Count;
	}

	public List<StackDetails>getRealStack()
	{
		return stack;
	}

	public void toggleCounterStack(int prio)
	{
		isCountering = !isCountering;
		counteringPrio = prio;
	}
}