using System.Collections.Generic;

public class Constants
{
	public static bool SHOULD_LOG = true;
	public static bool SHOULD_ANIMATE = false;

	public static int MAX_BOARD = 5;
	public static int INITIAL_HAND_SIZE = 6;
	public static int TEST_DECK_SIZE = 20;
	public static int PLAYER_STARTING_HEALTH = 100;
	public static int NUM_REZ = 4;
	public static int NUM_PLAYERS = 2;
	public static int SEED  = 0;
	public static int INITIAL_REZ_AMT = 1000;
	public static bool SHOULD_OUTPUT_PARSE_ID = false;
	public static int DRAFT_NUM_ROUNDS = 2;
	public static int MAX_MULLIGAN_CAP = 2;
	// 0 has angel for testing global
	 // 12

	 //0 == GAME MODE 
	 //1 == MODIFY JSON MODE
	public static int LIBRARY_MODE = 0;
	public static int ERROR_DMG = 0;
	public static int ERROR_GENERAL = 213890;

	//public static string LIBRARY_NAME = "Code/JSONS/LibraryTest_DISCARD";
	//public static string LIBRARY_NAME = "Code/JSONS/Library_Creating";
	//public static string DECK_NAME = "Code/JSONS/Deck_Creating";


	//public static string LIBRARY_NAME = "Code/JSONS/MODIFYJSON";
	//public static string DECK_NAME = "Code/JSONS/Deck_MODIFYING";

	public static string BASE_JSON_DIR = "C:/Users/Ornalth/Desktop/UnityProj/CardUnity/Assets/Resources/";

	public static string OWNED_CARDS_NAME = "Code/JSONS/Real/Owned_Real";
	public static string LIBRARY_NAME = "Code/JSONS/Real/LibraryReal";
	public static string DECK_NAME = "Code/JSONS/Real/Deck_Real";

	public static string DRAFT_DECK_CARDS_NAME = "Code/JSONS/Real/Draft_Deck_Real";
	public static string DRAFT_CARDS_NAME = "Code/JSONS/Real/Draft_Real";


	//public static string LIBRARY_NAME = "Code/JSONS/Real/BALANCE_TEST";
	//SAC NEEDS SEED 4;


	//public static string LIBRARY_NAME = "Library.txt";
	public static int IDK = 634523;
	public static int BASE_COLOUR_GEN_RATE = 5;

	//public static int TEAM_BOTH = TEAM_ALLY | TEAM_OPP;

	//Environment.CurrentDirectory + "/Assets/Resources/Code/ZZZZZEXTRAS/" + "LibraryReal5.json"

	public static string TEMP_CUSTOM_LIB_NAME = "C:/Users/Ornalth/Desktop/UnityProj/CardUnity/Assets/Resources/Code/JSONS/LibraryCustomCreationTest.json";
	public static string TEMP_CUSTOM_DECK_NAME = "C:/Users/Ornalth/Desktop/UnityProj/CardUnity/Assets/Resources/Code/JSONS/Real/Deck_Real.json";
	public static string TEMP_CUSTOM_OWNED_NAME = "C:/Users/Ornalth/Desktop/UnityProj/CardUnity/Assets/Resources/Code/JSONS/Real/Owned_Real.json";
	public static string MODIFY_JSON = "C:/Users/Ornalth/Desktop/UnityProj/CardUnity/Assets/Resources/Code/JSONS/MODIFYJSON.json";
	public static string BALANCE_TEST = "C:/Users/Ornalth/Desktop/UnityProj/CardUnity/Assets/Resources/Code/JSONS/BALANCE_TEST.json";
	
	public static string KEYWORD_BASIC_GENERATOR = "BASIC";
	
	public static readonly string IMAGEGEN_CURRENT_IMAGE_DIRECTORY = "C:\\Users\\Ornalth\\Desktop\\UnityProj\\CardUnity\\Assets\\Resources\\ProcessedCardImages";
	//public static readonly string IMAGEGEN_LOGFILE = @"C:\Users\Ornalth\Desktop\UnityProj\CardUnity\Assets\Resources\Code\JSONS" + "\\LOG.txt"; 
	public static readonly string IMAGEGEN_LOGFILE = IMAGEGEN_CURRENT_IMAGE_DIRECTORY + "\\LOG.txt"; 
	
	private static List<Dictionary<Rarity, int>> BOOSTER_CARD_ARRAY_MAPPING = null;


	public static List<Dictionary<Rarity, int>> GET_BOOSTER_CARD_MAPPING()
	{
		if (BOOSTER_CARD_ARRAY_MAPPING == null)
		{
			BOOSTER_CARD_ARRAY_MAPPING = new List<Dictionary<Rarity,int>>();
			Dictionary<Rarity,int> mapping1 = new Dictionary<Rarity, int>{
			
					{Rarity.COMMON, 10},
					{Rarity.UNCOMMON, 3},
					{Rarity.RARE, 1},
					{Rarity.VERY_RARE, 1}
			};
			Dictionary<Rarity,int> mapping2 = new Dictionary<Rarity, int>{
			
					{Rarity.COMMON, 9},
					{Rarity.UNCOMMON, 4},
					{Rarity.RARE, 2},
					{Rarity.VERY_RARE, 0}
			};
			/*Dictionary<Rarity,int> mapping2 = new Dictionary<Rarity, int>{
			
					{Rarity.COMMON, 2},
					{Rarity.UNCOMMON, 0},
					{Rarity.RARE, 0},
					{Rarity.VERY_RARE, 0}
			};*/
			Dictionary<Rarity,int> mapping3 = new Dictionary<Rarity, int>{
			
					{Rarity.COMMON, 8},
					{Rarity.UNCOMMON, 5},
					{Rarity.RARE, 1},
					{Rarity.VERY_RARE, 1}
			};
			BOOSTER_CARD_ARRAY_MAPPING.Add(mapping1);
			BOOSTER_CARD_ARRAY_MAPPING.Add(mapping2);
			BOOSTER_CARD_ARRAY_MAPPING.Add(mapping3);
		}

		return BOOSTER_CARD_ARRAY_MAPPING;

	}

}
