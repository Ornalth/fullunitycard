using UnityEngine;

public class StatusEffect
{
	public static int INFINITE = 999999;
	
	private int status;
	
	public static int DEFAULT_DURATION = INFINITE;
	public static int DEFAULT_COUNTER = 0;

	private int counter = DEFAULT_COUNTER;

	private int duration = DEFAULT_DURATION;
	private bool counterDecay = false;
	private bool destroyOnCounterDecay = false;
	


	public StatusEffect()
	{
		status = 0;
		duration = INFINITE;
		counter = 0;
	}

	public StatusEffect(int s)
	{
		status = s;
		duration = INFINITE;
		counter = 0;
	}
	
	public StatusEffect(int s, int duration, int counter)
	{
		status = s;
		
		setDuration(duration);
		setCounter(counter);
	}

	public StatusEffect(StatusEffect statusEffect)
	{
		this.status = statusEffect.getStatus();
		setDuration(statusEffect.getDuration());
		setCounter(statusEffect.getCounter());
		counterDecay = statusEffect.counterDecay;
		destroyOnCounterDecay = statusEffect.destroyOnCounterDecay;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public int getDuration()
	{
		return duration;
	}

	public void setDuration(int duration)
	{
		if (duration <= 0)
			duration = INFINITE;
		else
			this.duration = duration;
	}

	public int getCounter()
	{
		return counter;
	}

	public void setCounter(int counter)
	{
		
		this.counter = counter;
	}

	public bool decay()
	{
		if (!lastForever())
		{
			duration--;
			MyLogger.log("DURATION OF THIS " + Status.statusToString(status) + " = " + duration);
			if (duration <= 0)
			{
				return true;
			}
		}
		
		if (counterDecay)
		{
			counter--;
			if (counter <= 0 && destroyOnCounterDecay)
			{
				return true;
			}
		}
		return false;
	}

	public bool isSameEffect(int newEffect)
	{
		return newEffect == this.getStatus();
	}

	// TODO
	public void join(StatusEffect newEffect)
	{
		setCounter(this.counter + newEffect.counter);
		setDuration(this.duration + newEffect.duration);

	}

	//override
	public string toString()
	{
		string s = Status.statusToString(status);
		s = s + " dur:";
		if (lastForever())
		{
			s = s + "NA";

		} else
		{
			s = s + duration;
		}
		s = s + " cnt:" + counter;
		return s;
	}

	public bool lastForever()
	{
		return duration >= INFINITE;
	}
	
	public void setCounterDecay(bool b)
	{
		counterDecay = b;
	}
	
	public void setEffectDestroyOnCounterDecay(bool b)
	{
		destroyOnCounterDecay = b;
	}
	
	public string getDesc()
	{
		string s = Status.statusToString(status);
		s = "\n\t" + s;
		if (!lastForever())
		{
			s = s + " dur:" + duration;
		}
		if (counter> 0 || counterDecay||destroyOnCounterDecay)
			s = s + " cnt:" + counter;
		return s;
	}

	public bool removeCounter()
	{
		counter--;
		if (counter <= 0)
			return true;
		return false;
	}


}
