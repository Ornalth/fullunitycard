using System.Collections.Generic;
using System.Linq;

public class Status
{
	public const int NONE = 0;
	public const int FOCUS = 1;
	public const int LIFELINK = 2;
	public const int HASTE = 3;
	public const int HIDDEN = 4;
	public const int PIERCE = 5;
	public const int CANT_ATK = 6;
	public const int CANT_BLK = 7;
	public const int INVIS_ATK = 8;
	public const int SURPRESS = 9;
	public const int INTERCEPT = 10;
	public const int DUELIST = 11;
	public const int NO_RETAL = 12;
	public const int REGEN = 13;
	public const int RAZE = 14;
	public const int MUST_ATK = 15;
	public const int SHROUD = 16;
	public const int DOOM = 17;
	public const int PROTECTION = 18;
	public const int BURN = 19;
	public const int CURSED = 20;
	public const int POISON = 21;


	public const int FLEETING = 30;
	public const int SECOND_LIFE = 31;



	public const int DAMAGE_MULTIPLY_RED = 41;
	public const int DAMAGE_MULTIPLY_BLUE = 42;
	public const int DAMAGE_MULTIPLY_GREEN = 43;
	public const int DAMAGE_MULTIPLY_COLOURLESS = 44;


	int status = NONE;
	public static bool isJoinableStatus(int status)
	{
		if (status == BURN ||
			status == POISON ||
			status == REGEN
			)
			return true;
		return false;
	}
	private static Dictionary<int, string> dict = new Dictionary<int, string> {
		{ NONE, "NONE"},
		{ FOCUS, "FOCUS" },
		{ LIFELINK, "LIFELINK" },
		{ HASTE, "HASTE" },
		{ HIDDEN, "HIDDEN"},
		{ PIERCE, "PIERCE" },
		{ CANT_ATK, "CANT_ATK"},
		{ CANT_BLK, "CANT_BLK"},
		{ INVIS_ATK, "INVIS_ATK"},
		{ SURPRESS, "SURPRESS"},
		{ INTERCEPT, "INTERCEPT"},
		{ DUELIST, "DUELIST" },
		{ NO_RETAL, "NO_RETAL"},
		{ REGEN, "REGEN"},
		{ RAZE, "RAZE"},
		{ MUST_ATK, "MUST_ATK"},
		{ SHROUD, "SHROUD"},
		{ DOOM, "DOOM"},
		{ PROTECTION, "PROTECTION"},
		{ BURN, "BURN"},
		{ CURSED, "CURSED"},
		{ POISON, "POISON"},

		{ DAMAGE_MULTIPLY_RED, "DAMAGE_MULTIPLY_RED"},
		{ DAMAGE_MULTIPLY_BLUE, "DAMAGE_MULTIPLY_BLUE"},
		{ DAMAGE_MULTIPLY_GREEN, "DAMAGE_MULTIPLY_GREEN"},
		{ DAMAGE_MULTIPLY_COLOURLESS, "DAMAGE_MULTIPLY_COLOURLESS"},
		{ FLEETING, "FLEETING"},
		{ SECOND_LIFE, "SECOND_LIFE"}
	};

	public static Dictionary<string, int> reverseDict = new Dictionary<string, int >()
	{
		{ "NONE", NONE},
		{ "FOCUS", FOCUS },
		{ "LIFELINK", LIFELINK },
		{ "HASTE", HASTE },
		{ "HIDDEN", HIDDEN},
		{ "PIERCE", PIERCE },
		{ "CANT_ATK", CANT_ATK},
		{ "CANT_BLK", CANT_BLK},
		{ "INVIS_ATK", INVIS_ATK},
		{ "SURPRESS", SURPRESS},
		{ "INTERCEPT", INTERCEPT},
		{ "DUELIST", DUELIST },
		{ "NO_RETAL", NO_RETAL},
		{ "REGEN", REGEN},
		{ "RAZE", RAZE},
		{ "MUST_ATK", MUST_ATK},
		{ "SHROUD", SHROUD},
		{ "DOOM", DOOM},
		{ "PROTECTION", PROTECTION},
		{ "BURN", BURN},
		{ "CURSED", CURSED},
		{ "POISON", POISON},
		
		{ "DAMAGE_MULTIPLY_RED", DAMAGE_MULTIPLY_RED},
		{ "DAMAGE_MULTIPLY_BLUE", DAMAGE_MULTIPLY_BLUE},
		{ "DAMAGE_MULTIPLY_GREEN", DAMAGE_MULTIPLY_GREEN},
		{ "DAMAGE_MULTIPLY_COLOURLESS", DAMAGE_MULTIPLY_COLOURLESS},
		{ "FLEETING", FLEETING},
		{ "SECOND_LIFE", SECOND_LIFE}
	};

	/*
	FOCUS(FOCUS), // no dizzy on attack
	LIFELINK(LIFELINK), // gain hp on dmg
	HASTE(HASTE), //no sickness
	HIDDEN(HIDDEN), // Cannot be attacked
	PIERCE(PIERCE), //100% overflow
	BURN(BURN), //take dmg per turn
	CANT_ATK(CANT_ATK), //
	CANT_BLK(CANT_BLK),
	INVIS_ATK(INVIS_ATK), SURPRESS(SURPRESS),
	INTERCEPT(INTERCEPT),
	DUELIST(DUELIST),
	NO_RETAL(NO_RETAL), // first strike 100% no retal.
	REGEN(REGEN),
	RAZE(RAZE), MUST_ATK(MUST_ATK), SHROUD(SHROUD),
	DOOM(DOOM),
	PROTECTION(PROTECTION);




*/


	public static int statusFromString(string s)
	{
		return reverseDict [s];
	}

	public static string statusToString(int s)
	{
		return dict [s];
	}

	public static List<string> getAllStatusStrings()
	{
		return reverseDict.Keys.ToList();
	}
}
