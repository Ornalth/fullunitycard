﻿using UnityEngine;
using System.Collections;

public class CardFontHelper {

	private static readonly string BLUE_MANA = "|";
	private static readonly string RED_MANA = "`";
	private static readonly string GREEN_MANA = "]";
	private static readonly string GRAY_MANA = "[";
	private static readonly string DIZZY = "}";
	private static readonly string DIZZY_LOCK = "{";
	private static readonly string ACTIVE = "@";
	private static readonly string LIFE = ";";
	private static readonly string GRAVE = "^";
	private static readonly string DECK = "\\";
	private static readonly string HAND = "_";
	
	public static string getBlueMana()
	{
		return BLUE_MANA; 
	}

	public static string getRedMana()
	{
		return RED_MANA; 
	}

	public static string getGreenMana()
	{
		return GREEN_MANA; 
	}

	public static string getGrayMana()
	{
		return GRAY_MANA; 
	}

	public static string getLife()
	{
		return LIFE;
	}

	public static string getHand()
	{
		return HAND;
	}

	public static string getGrave()
	{
		return GRAVE;
	}
	public static string getDeck()
	{
		return DECK;
	}
	public static string getActive()
	{
		return ACTIVE;
	}

	public static string getDizzy()
	{
		return DIZZY;
	}
	public static string getDizzyLock()
	{
		return DIZZY_LOCK;
	}


	public static string getCostStr(int[] cost)
	{
		string s = "";
		if (cost[0] > 0)
			s = s + cost[0] + getGrayMana();
		if (cost[1] > 0)
			s = s + cost[1] + "\\1" + getRedMana() + "\\0";
		if (cost[2] > 0)
			s = s + cost[2] + "\\2" + getGreenMana() + "\\0";
		if (cost[3] > 0)
			s = s + cost[3] + "\\3" + getBlueMana() + "\\0";
		if (s.Equals (""))
			return "Zero";
		return s;
	}
}
