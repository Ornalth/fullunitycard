using UnityEngine;
using System.Collections;

public class MyLogger
{

	public static void log(string message)
	{
		if (Constants.SHOULD_LOG)
			Debug.Log(message);
	}

	public static void log(int message)
	{
		log("" + message);
	}

	public static void log(float message)
	{
		log("" + message);
	}
}
