﻿using UnityEngine;
using System.Collections;

public class MoveMeshLayer : MonoBehaviour {

	void Start ()
	{
		this.GetComponent<Renderer>().sortingLayerID = this.transform.parent.GetComponent<Renderer>().sortingLayerID;
		this.GetComponent<Renderer>().sortingOrder = this.transform.parent.GetComponent<Renderer>().sortingOrder + 5;
	}

	public void setSortingLayerID(int id)
	{
		this.GetComponent<Renderer>().sortingLayerID = id;
		this.GetComponent<Renderer>().sortingOrder = this.transform.parent.GetComponent<Renderer>().sortingOrder + 5;
	}
}
