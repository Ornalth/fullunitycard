using UnityEngine;
using System.Collections.Generic;
using System;

public class ImageLibrary {

	static Dictionary <int, Sprite> cardImageLibrary = new Dictionary<int, Sprite>();
	// Use this for initialization
	static string baseCardString = "image";
	static string baseStr = "ProcessedCardImages/";
	static readonly string SET_ICON_LOC = "SetIcons/";
	static readonly string SPEED_ICON_LOC = "AbilitySpeedImages/";
	static readonly string REQUEST_ICON_LOC = "PlayerInfoImages/";
	static readonly string STATUS_ICON_LOC = "StatusIcon/";
	static readonly string CARD_BG_LOC = "CardBase/Square/";
	static readonly string CARD_BG_HIGHLIGHT_LOC = "CardBase/Rounded/";
	static readonly string CARD_COUNTER_TOKEN_LOC = "CounterTokenImages/";

	static Dictionary <string, Sprite> baseLibrary = null;
	static Dictionary <string, Sprite> baseLibraryOutlined = null;
	static Dictionary <string, Sprite> setIconDictionary = null;
	static Dictionary <string, Sprite> abilitySpeedDictionary = null;
	static Dictionary <string, Sprite> counterTokenDictionary = null;
	
	static Dictionary <int, Sprite> cardStateDictionary = null;
	static Dictionary <int, Sprite> statusIconLibrary = null;
	static Dictionary <int,Sprite> dizzyImageDict = null;

	static string[] baseCardTypes = new string[] {"hidden", "RGB", "RB", "RG", "GB", "R", "G", "B", "C"};
	static string[] baseCardTypesOutlined = new string[] {"hidden", "RGB_Outline", "RB_Outline", "RG_Outline", "GB_Outline", "R_Outline", "G_Outline", "B_Outline", "C_Outline"};
	// Use this for initialization
	static Sprite highlightImage;

	public static Sprite getDizzyImage(int dizzy)
	{
		if (dizzyImageDict == null)
		{
			getDizzyImageDict();
		}
		return dizzyImageDict[dizzy];
	}

	private static void getDizzyImageDict()
	{
		dizzyImageDict = new Dictionary<int, Sprite>();
		string baseStr = "Dizzy/";
		//string[] strs = new string[] {"DIZZY", "DIZZY_LOCK", "UNDIZZY"};

		Sprite spr = Resources.Load <Sprite> (baseStr + "UNDIZZY");
		dizzyImageDict[Dizzy.UNDIZZY] = spr;

		spr = Resources.Load <Sprite> (baseStr + "DIZZY");
		dizzyImageDict[Dizzy.DIZZY] = spr;

		spr = Resources.Load <Sprite> (baseStr + "DIZZY_LOCK");
		dizzyImageDict[Dizzy.DIZZY_LOCK] = spr;
	}



	public static Sprite getImageForCard(Card c)
	{
		return getImageForCard(c.getID());
	}

	public static Sprite getImageForCard(int cardID)
	{
		Sprite ans;
		if (cardImageLibrary.TryGetValue(cardID, out ans))
		{
			return ans;
		}
		else
		{
			Sprite spr = Resources.Load <Sprite> (baseStr + cardID);
			if (spr != null)
			{
				cardImageLibrary[cardID] = spr;
				return spr;
			}
			return getDefaultSprite();
		}
	}

	public static Sprite getCardStateImage(int cardState)
	{
		if (cardStateDictionary == null)
		{
			getCardStateIconLibrary();
		}

		Sprite ans;
		if (!cardStateDictionary.TryGetValue(cardState, out ans))
		{
			MyLogger.log("NO SPRITE NAMED" + cardState);
		}
		return ans;
	}

	private static void getCardStateIconLibrary()
	{
		if (cardStateDictionary == null)
		{
			cardStateDictionary = new Dictionary<int, Sprite>();
			
			List<string> cardStates = CardState.getPossibleCardStateList();
			foreach (string cardState in cardStates)
			{
				Sprite spr = Resources.Load <Sprite> (REQUEST_ICON_LOC + cardState);
				cardStateDictionary[CardState.cardStateFromString(cardState)] = spr;
			}
		}
	}

	public static Sprite getStatusIcon(StatusEffect eff)
	{

		int status = eff.getStatus();
		if (statusIconLibrary == null)
		{
			getStatusIconLibrary();
		}

		Sprite ans;
		if (!statusIconLibrary.TryGetValue(status, out ans))
		{
			MyLogger.log("NO SPRITE NAMED" + status);
		}
		return ans;
	}

	private static void getStatusIconLibrary()
	{
		if (statusIconLibrary == null)
		{
			statusIconLibrary = new Dictionary<int, Sprite>();
			
			Dictionary <string, int> tempDic = Status.reverseDict;
			foreach (string iconName in tempDic.Keys)
			{
				Sprite spr = Resources.Load <Sprite> (STATUS_ICON_LOC + iconName);
				statusIconLibrary[tempDic[iconName]] = spr;
			}
		}
	}

	public static Sprite getHighlightImage()
	{
		if (highlightImage == null)
		{
			highlightImage = Resources.Load <Sprite> (CARD_BG_HIGHLIGHT_LOC + "baseHighlight");
		}
		return highlightImage;
	}

	public static Sprite getCounterTokenImage(Counter counter)
	{
		if (counterTokenDictionary == null)
		{
			getCounterTokenDictionary();
		}

		String counterName = counter.getName();
		Sprite ans;
		if (!counterTokenDictionary.TryGetValue(counterName, out ans))
		{
			MyLogger.log("NO SPRITE NAMED" + counterName);
			ans = counterTokenDictionary["GENERIC"];
		}
		return ans;
	}

	public static Sprite getAbilitySpeedImage(int speed)
	{
		if (abilitySpeedDictionary == null)
		{
			getAbilitySpeedDictionary();
		}

		String speedStr = AbilitySpeed.speedToString(speed); 
		Sprite ans;
		if (!abilitySpeedDictionary.TryGetValue(speedStr, out ans))
		{
			MyLogger.log("NO SPRITE NAMED" + speedStr);
		}
		return ans;
	}

	public static Sprite getCardSetImage(CardSet cSet, Rarity rarity)
	{
		if (setIconDictionary == null)
		{
			getSetIconDictionary();
		}

		string cSetName = CardSetConverter.cardSetToString(cSet);
		string rSetName = RarityConverter.rarityToString(rarity);
		
		Sprite ans;
		if (!setIconDictionary.TryGetValue(cSetName + rSetName, out ans))
		{
			MyLogger.log("NO SPRITE NAMED" + cSetName + rSetName);
		}
		return ans;
	}

	private static void getAbilitySpeedDictionary()
	{
		abilitySpeedDictionary = new Dictionary <string, Sprite>();
		String[] speeds = new String[]{"A","B","C","D","E","S"};
		foreach(String s in speeds)
		{

			Sprite spr = Resources.Load <Sprite> (SPEED_ICON_LOC + "speed" + s + "icon");
			if (spr != null)
			{

				abilitySpeedDictionary[s] = spr;
			}
			else
			{
				MyLogger.log("OMFG NO SPRITE NAMED " + "speed" + s + "icon");
			}
			
		}
	}

	private static void getSetIconDictionary()
	{
		setIconDictionary = new Dictionary <string, Sprite>();
		foreach(CardSet cSet in Enum.GetValues(typeof(CardSet)))
		{
			foreach(Rarity rarity in Enum.GetValues(typeof(Rarity)))
			{
				string cSetName = CardSetConverter.cardSetToString(cSet);
				string rSetName = RarityConverter.rarityToString(rarity);
				Sprite spr = Resources.Load <Sprite> (SET_ICON_LOC + cSetName + rSetName);
				if (spr != null)
				{

					setIconDictionary[cSetName + rSetName] = spr;
				}
				else
				{
					MyLogger.log("OMFG NO SPRITE NAMED " + cSetName + rSetName);
				}
			}
		}
	}

	private static void getCounterTokenDictionary()
	{
		counterTokenDictionary = new Dictionary <string, Sprite>();
		String[] counters = new String[]{"GENERIC", "SLIME", "TROG", "CURSE", "SIF"};
		foreach(String s in counters)
		{

			Sprite spr = Resources.Load <Sprite> (CARD_COUNTER_TOKEN_LOC + s + "Counter");
			if (spr != null)
			{

				counterTokenDictionary[s] = spr;
			}
			else
			{
				MyLogger.log("NO" + s + "COUNTER");
			}
			
		}
	}

	public static Sprite getDefaultSprite()
	{
		Sprite ans;
		if (!cardImageLibrary.TryGetValue(0, out ans))
		{
			ans = Resources.Load <Sprite> (baseStr + "default");
			cardImageLibrary.Add (0, ans);
		}
		return ans;
	}

	public static void reset()
	{
		cardImageLibrary = new Dictionary<int, Sprite>();
	}

	public static void setBaseOutlined(SpriteRenderer renderer, int faction)
	{
		if (baseLibrary == null)
		{
			getCardBaseLibrary();
		}

		string str = "";
		//		bool red = false;
		//		bool blue = false;
		//		bool green = false;
		//TODO
		if ((Faction.FACTION_RED & faction)!= 0)
		{
			//			red = true;
			str = str + "R";
		}
		
		if ((Faction.FACTION_GREEN & faction) != 0)
		{
			//			green = true;
			str = str + "G";
		}
		
		if ((Faction.FACTION_BLUE & faction) != 0)
		{
			//			blue = true;
			str = str + "B";
		}
		
		if (!str.Equals(""))
		{
			str = str + "_Outline";
			Sprite spr;
			baseLibraryOutlined.TryGetValue(str, out spr);
			
			if (spr == null)
			{
				Debug.LogError("THIS SPRITE HAS NO VALUE " + str);
				return;
			}
			renderer.sprite = spr;
		}
		else
		{
			//str = str + "_Outline";
			renderer.sprite = baseLibraryOutlined["C_Outline"];
		}
	}

	public static void setBase(SpriteRenderer renderer, int faction)
	{
		if (baseLibrary == null)
		{
			getCardBaseLibrary();
		}

		string str = "";
		//		bool red = false;
		//		bool blue = false;
		//		bool green = false;
		//TODO
		if ((Faction.FACTION_RED & faction)!= 0)
		{
			//			red = true;
			str = str + "R";
		}
		
		if ((Faction.FACTION_GREEN & faction) != 0)
		{
			//			green = true;
			str = str + "G";
		}
		
		if ((Faction.FACTION_BLUE & faction) != 0)
		{
			//			blue = true;
			str = str + "B";
		}
		
		if (!str.Equals(""))
		{
			Sprite spr;
			baseLibrary.TryGetValue(str, out spr);
			
			if (spr == null)
			{
				Debug.LogError("THIS SPRITE HAS NO VALUE " + str);
				return;
			}
			renderer.sprite = spr;
		}
		else
		{
			//str = str + "_Outline";
			renderer.sprite = baseLibrary["C"];
		}
	}


	public static void setBaseHidden(SpriteRenderer renderer)
	{
		if (baseLibrary == null)
		{
			getCardBaseLibrary();
		}
		
		renderer.sprite = baseLibrary["hidden"];
	}

	private static void getCardBaseLibrary()
	{
		
		baseLibrary = new Dictionary<string, Sprite>();
		baseLibraryOutlined = new Dictionary<string, Sprite>();
		string baseStr = CARD_BG_LOC;
		
		foreach (string baseCardType in baseCardTypes)
		{
			//MyLogger.log("DOWNLOADING " + baseStr + baseCardType);
			Sprite spr = Resources.Load <Sprite> (baseStr + baseCardType);
			baseLibrary[baseCardType] = spr;
		}

		foreach(string baseCardType in baseCardTypesOutlined)
		{
			//MyLogger.log("DOWNLOADING " + baseStr + baseCardType);
			Sprite spr = Resources.Load <Sprite> (baseStr + baseCardType);
			baseLibraryOutlined[baseCardType] = spr;
		}
	
	}
}
