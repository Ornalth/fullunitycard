﻿public class HelperUtils
{
	public static bool testBits(int source, int test)
	{
		return (source & test) == test;
	}
}