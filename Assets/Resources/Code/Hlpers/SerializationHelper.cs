﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using UnityEngine.Networking;

public class SerializationHelper {

	public static char delimiter = '|';
	public static string serializeBoosters(List<List<DeckCardData>> boosters)
	{

		StringBuilder builder = new StringBuilder();
		int numBoosters = boosters.Count;
		int sizeEach = boosters[0].Count;
		builder.Append(numBoosters).Append(delimiter).Append(sizeEach).Append(delimiter);
		for (int i = 0; i < numBoosters; i++)
		{
			for (int j = 0; j < sizeEach; j++)
			{
				DeckCardData data = boosters[i][j];
				builder.Append(data.cardID).Append(delimiter).Append(data.amount).Append(delimiter);
			}
		}
		builder.Length--;
		return builder.ToString();
	}

	public static List<List<DeckCardData>> deserializeBoosters(string serializedBoostersString)
	{
		string[] tokens = serializedBoostersString.Split(delimiter);
		List<List<DeckCardData>> boosters = new List<List<DeckCardData>>();
		int numBoosters = Int32.Parse(tokens[0]);
		int sizeEach = Int32.Parse(tokens[1]);
		int currentToken = 2;
		for (int i = 0; i < numBoosters; i++)
		{
			List<DeckCardData> currentBooster = new List<DeckCardData>();
			for (int j = 0; j < sizeEach; j++)
			{
				DeckCardData data = new DeckCardData();
				data.cardID = Int32.Parse(tokens[currentToken++]);
				data.amount = Int32.Parse(tokens[currentToken++]);
				currentBooster.Add(data);
			}
			boosters.Add(currentBooster);
		}
		return boosters;
	}

	public static string serializeFloatList(List<float> floats)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < floats.Count; i++)
		{
			builder.Append(floats[i]).Append(delimiter);
		}
		if (builder.Length > 0)
		{
			builder.Length--;
		}
		string str = builder.ToString();
		return str;
	}

	public static List<float> deserializeFloatList(string str)
	{
		List<float> floats = new List<float>();
		if (str.Length == 0)
		{
			return floats;
		}
		string[] tokens = str.Split(delimiter);
		for (int i = 0; i < tokens.Length; i++)
		{
			floats.Add(float.Parse(tokens[i]));
		}
		return floats;
	}

	public static string serializeIntList(List<int> ints)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < ints.Count; i++)
		{
			builder.Append(ints[i]).Append(delimiter);
		}
		if (builder.Length > 0)
		{
			builder.Length--;
		}
		string str = builder.ToString();
		return str;
	}

	public static List<int> deserializeIntList(string str)
	{
		List<int> ints = new List<int>();
		if (str.Length == 0)
		{
			return ints;
		}
		string[] tokens = str.Split(delimiter);
		for (int i = 0; i < tokens.Length; i++)
		{
			ints.Add(int.Parse(tokens[i]));
		}
		return ints;
	}
/*
	public static string serializeFloatList(List<float> floats)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < floats.Count; i++)
		{
			builder.Append(floats[i]).Append(delimiter);
		}
		builder.Length--;
		string str = builder.ToString();
		return str;
	}

	public static List<float> deserializeFloatList(string str)
	{
		string[] tokens = str.Split(delimiter);
		List<float> floats = new List<float>();
		for (int i = 0; i < tokens.Length; i++)
		{
			floats.Add(float.Parse(tokens[i]));
		}
		return floats;
	}
*/
}

