using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System;

public class ImageGenHelper{

	public static void checkImages()
	{

		string[] fileNames = Directory.GetFiles( Constants.IMAGEGEN_CURRENT_IMAGE_DIRECTORY );
	
		List<string> finishedFileNames = fileNames.Where( x => x.EndsWith( ".png" ) ).ToList();
//MyLogger.log("HMM " + finishedFileNames[0] + " "  + " " + finishedFileNames[1] + " " + finishedFileNames.Count);
		List<string> unseenNames = new List<string>();
		Dictionary<string, int> cardNamesDict = new Dictionary<string, int>();
		Dictionary<int, Card> LIBRARY = CardLibrary.getLibrary();
		Regex rgx = new Regex("[^a-zA-Z0-9]");
		foreach( int id in LIBRARY.Keys ) {
			//Card c = LIBRARY[id];
			//cardNamesDict.Add( rgx.Replace(c.getName().ToLower(), ""), c.getID() );
			cardNamesDict.Add(id.ToString(), id);
		}

		File.Create( Constants.IMAGEGEN_LOGFILE).Close();


		System.IO.StreamWriter file = 
            new System.IO.StreamWriter( Constants.IMAGEGEN_LOGFILE);
		foreach( string fName in finishedFileNames ) {
			//MyLogger.log( Path.GetFileNameWithoutExtension( fName ).ToLower() );
			string newKey = Path.GetFileNameWithoutExtension( fName ).ToLower();
			//file.WriteLine("FOUND " + newKey);
			
			if( cardNamesDict.ContainsKey( newKey) ) {
				int id = cardNamesDict[newKey];
				cardNamesDict.Remove( newKey );
				//System.IO.File.Copy( fName,  Constants.IMAGEGEN_TARGET_IMAGE_DIRECTORY + "\\" + id + ".png", true );
			} else {
				unseenNames.Add( newKey );
			}
		}

		file.WriteLine("-----------------------BEGIN MISSING CARDS-------------- " + cardNamesDict.Count);
		foreach( string fName in cardNamesDict.Keys) {
			int cardId = 0;
			Int32.TryParse(fName, out cardId);
			Card card = LIBRARY[cardId];
			file.WriteLine( "Missing... " + " cardid " + cardNamesDict[fName] + " " + card.getName()  );
		}
		file.WriteLine("-----------------------BEGIN UNSEEN NAMES--------------");
		foreach( string fName in unseenNames) {
			file.WriteLine( "FOUND " + fName + " but unused." );
		}
		file.Flush();
		MyLogger.log("COMPLETE");

	}
}
