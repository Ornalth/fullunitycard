using UnityEngine;
using System.Collections.Generic;

public class MulliganViewScript : MonoBehaviour, ISpriteButtonListener, ICardContainerListener {

	MainBoardVC mainBoardVCScript;


	ZoomCardScript zoomCardScript;
	TypogenicText messageText;

	List<Card> cards;

	HandViewScript handViewScript;
	// Use this for initialization

	List<int> selectedIndices;

	CustomSpriteButtonScript mulliganHandButtonScript;
	
	void Awake () 
	{
		selectedIndices = new List<int>();
		cards = new List<Card>();
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("MulliganMessage"))
			{
				messageText = comp;
			}
			
		}

		
		GameObject g5 = Instantiate (Resources.Load ("ZoomCard")) as GameObject; 
		g5.name = "ZoomCard";
		g5.transform.parent = transform;
		g5.transform.localPosition = new Vector3 (0,0,0);
		zoomCardScript = g5.GetComponent<ZoomCardScript>();

		GameObject g = Instantiate (Resources.Load ("HandPrefab")) as GameObject;
		g.transform.parent = transform;
		g.transform.localPosition = new Vector3 (0,0,0);
		handViewScript = g.GetComponent<HandViewScript>();
		handViewScript.addCardContainerListener(this);
		CancelButtonScript[] buttons = GetComponentsInChildren<CancelButtonScript>();
		foreach (CancelButtonScript button in buttons)
		{
			button.addListener(this);
		}

		ConfirmButtonScript[] buttons2 = GetComponentsInChildren<ConfirmButtonScript>();
		foreach (ConfirmButtonScript button in buttons2)
		{
			button.addListener(this);
		}

		CustomSpriteButtonScript[] customButtons = GetComponentsInChildren<CustomSpriteButtonScript>();
		foreach (CustomSpriteButtonScript button in customButtons)
		{
			if (button.name.Equals("MulliganHandButton"))
			{
				mulliganHandButtonScript = button;
				button.addListener(this);
			}
		}

	}

	void Start()
	{


		zoomCardScript.setSortingLayerID(this.transform.GetComponent<Renderer>().sortingLayerID);
		handViewScript.setSortingLayerID(this.transform.GetComponent<Renderer>().sortingLayerID);
		Vector3 pos = zoomCardScript.gameObject.transform.localPosition;
		pos.x = 5.0f;
		pos.y = -3.0f;
		zoomCardScript.gameObject.transform.localPosition = pos;
		
		mulliganHandButtonScript.setTag("ShouldIUseTags");
		mulliganHandButtonScript.setMessage("Mulligan Hand");
		mulliganHandButtonScript.resize(128,128);
		messageText.Text = "Replace up to " + Constants.MAX_MULLIGAN_CAP + " cards or your whole hand.";

	}

	public void setMainBoardVC(MainBoardVC vc)
	{
		mainBoardVCScript = vc;
	}

	public void setCards (List<Card> cards)
	{
		this.cards = cards;
		handViewScript.setCards(cards);
	}

	public void cardMouseOver(CardContainerScript scr, Card c)
	{
		zoomCardScript.setCard(c);
	}
	
	public void cardMouseExit(CardContainerScript scr, Card c)
	{
		zoomCardScript.setCard(null);
	}

	public void cardClicked(CardContainerScript scr, Card c)
	{
		int index = cards.IndexOf(c);
		MyLogger.log("CLICK FOR MUlL " + index);
		List<Card> hlCards = new List<Card>();
		hlCards.Add(c);

		if (selectedIndices.Contains(index))
		{
			selectedIndices.Remove(index);

			scr.popHighlights(hlCards, HighlightReason.MULLIGAN);
		}
		else
		{
			selectedIndices.Add(index);
			scr.setHighlights(hlCards, HighlightReason.MULLIGAN, ReplaceMode.NONE);
		}
		//TODO
		//mainBoardVCScript.graveViewCardClicked(this, c);
		//mainBoardVCScript.boardRowClicked (rowType, c);
	}

	//ISpriteButtonListener
	public void buttonClicked(SpriteButtonScript scr)
	{
		MyLogger.log("BUTTON CLICKED");
		//TODO
		if (scr is CancelButtonScript)
		{
			//cancelButtonClicked();
			mainBoardVCScript.confirmMulligan(new List<int>());
		}
		else if (scr is ConfirmButtonScript)
		{
			mainBoardVCScript.confirmMulligan(selectedIndices);
			//requestConfirmButton();
		}
		else if (scr == mulliganHandButtonScript)
		{
			Debug.Log("MULLLLKIGAN HAND");
			mainBoardVCScript.confirmMulliganHand();
			//requestConfirmButton();
		}
	}
	

/*
	

	//HIGHLIGHTS
	public void restoreOriginalHighlights()
	{
		for (int i = 0; i < cards.Count; i++)
		{
			CardViewScript script = viewScripts[i];
			script.popHighlights (); //MAYBE POP INSTEAD?
		}

	}


	public void setHighlights (Card c, int reason, int mode)
	{
		List<Card> cards = new List<Card>();
		cards.Add(c);
		setHighlights(cards, reason, mode);
	}



	public void setHighlights (List<Card> highlights, int reason, int mode)
	{
		Color color = HighlightReason.getColorForReason(reason);

		if (mode == ReplaceMode.REPLACE)
		{
			restoreOriginalHighlights ();
			commitHighlights (highlights, color);
			//pushHighlights (highlights);
		}
		else if (mode == ReplaceMode.NONE)
		{
			commitHighlights (highlights, color);
		}
		else if (mode == ReplaceMode.FULL_LAYER)
		{
			fullLayerHighlight(highlights, color);
		}
		else
		{
			MyLogger.log("BoardRowScript -- PANIC NO REPLACEMODE EXIST");
		}
	}


	private void commitHighlights(List<Card> highlights, Color color)
	{

		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);
		
			if (index >= 0)
			{
				CardViewScript script = viewScripts[i];
				script.pushHighlight(color);
			}
		}
		


	}

	private void fullLayerHighlight(List<Card> highlights, Color color)
	{
		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				CardViewScript script = viewScripts[i];
				script.pushHighlight(color);
				//MyLogger.log("PUSHED RED OR WAHTEVER");
			}
			else
			{
				CardViewScript script = viewScripts[i];
				script.pushDefaultHighlight();
				//MyLogger.log("PUSHED HIWTe");
			}
		}
	
	}

	void setColor(CardViewScript scr, Color color)
	{
		scr.pushHighlight (color);
	}

	public void popHighlights (Card c, int reason)
	{
		List<Card> cardss = new List<Card>();
		cardss.Add(c);
		popHighlights(cardss, HighlightReason.getColorForReason(reason) );
	}


	public void popHighlights (List<Card> cards,int reason)
	{
		popHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popHighlights (List<Card> cards, Color color)
	{
		foreach (Card c in cards)
		{
			int index = cards.IndexOf(c);
			if (index != -1)
			{
				CardViewScript script = viewScripts[index];
				script.removeLastColor(color);
			}		
		}
	}

	public void popLayerHighlights (List<Card> cards,int reason)
	{
		popLayerHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popLayerHighlights (List<Card> highlights, Color color)
	{
		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				CardViewScript script = viewScripts[i];
				script.removeLastColor(color);
			}
			else
			{
				CardViewScript script = viewScripts[i];
				script.removeLastDefaultColor();
			}
		}
		
		
	}

	//BOARDROW
	public void popAllHighlights(int reason)
	{
		popHighlights(cards, reason);
	}*/
}
