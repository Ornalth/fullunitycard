using UnityEngine;
using System.Collections;

public class DarkenViewWindowScript : MonoBehaviour
{
	TypogenicText title;

	void Awake()
	{
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			//MyLogger.log(comp.name);
			//MyLogger.log(comp.Text);
			if (comp.name.Equals("TitleText"))
			{
				title = comp;
				title.Text = "Text";
				//MyLogger.log("Found Title");
			}
		}
	}

	void setText(string text)
	{
		title.Text = text;
	}
}