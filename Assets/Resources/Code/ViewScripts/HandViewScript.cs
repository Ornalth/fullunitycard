using UnityEngine;
using System.Collections.Generic;


public class HandViewScript : CardContainerScript, CardViewScript.CardViewController {

	


	float width ;//= renderer.bounds.size.x;
	float height;//= renderer.bounds.size.y;
	float cardWidth = -1;
	float cardHeight = -1;
	static float gapMultiplier = 1.5f; 
	public bool shouldHide = false;

	private static int numViews = 10;

	void Awake()
	{
		
		cards = new List<Card>();
		views = new List<GameObject>();
		viewsScripts = new List<CardViewScript> ();
		
		
		
		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("GenericCard")) as GameObject;
			views.Add (g);

			
			g.transform.parent = transform;
			CardViewScript script = g.GetComponent<CardViewScript> ();
			viewsScripts.Add (script);
			script.setController (this);
			
			if (cardWidth < 0 || cardHeight < 0) 
			{
				SpriteRenderer renderer = g.GetComponent<SpriteRenderer> ();
				cardWidth = renderer.bounds.size.x * gapMultiplier; //
				cardHeight= renderer.bounds.size.y;
			}
			g.transform.localScale = new Vector3(1.2f,1.2f,1f);
			ViewHelper.hide (g);
		}
		
	}

	// Use this for initialization
	void Start () {
		GameObject parent = transform.parent.gameObject;
		SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer> ();
		width = renderer.bounds.size.x;
		height= renderer.bounds.size.y;

		//MyLogger.log("WIDTH" + width + "HEIGHT " + height);

	}

	override public void setSortingLayerID(int id)
	{
		base.setSortingLayerID(id);
		foreach (CardViewScript scr in viewsScripts)
		{
			scr.setSortingLayerID(id);
		}

	}

	void reorderCards()
	{
		int numCards = cards.Count;

		for (int i = 0; i < numCards; i++)
		{
			GameObject view = views[i];


			float xBuffer =  cardWidth / 2;
			float xPos = ((float)(-((float)numCards)/2.0 + i)) * cardWidth + xBuffer;
			Transform trans = view.transform;
			trans.localPosition = new Vector3(xPos, 0, -3f);
			CardViewScript scr = viewsScripts[i];
			scr.setHidden(shouldHide);
			scr.setCard(cards[i]);

		}
	}


	public void setCards(List<Card> newCards)
	{
		cards = newCards;
		int i = 0;
		for (i = 0; i < cards.Count; i++)
		{
			
			//Card c = cards[i];
			GameObject g = views[i];
			//CardViewScript script = viewsScripts[i];
			//script.setCard (c);
			ViewHelper.show (g);
		
		}
		
		for (; i < numViews; i++)
		{
			GameObject g = views[i];
			CardViewScript vc = viewsScripts[i];
			vc.clearAllHighlights();
			ViewHelper.hide (g);
		}
		reorderCards ();
	}

	public void cardClicked(Card c)
	{
		foreach (ICardContainerListener listener in listeners)
		{
			listener.cardClicked(this, c);
		}
		//MyLogger.log("Hand received Card clicked!");
		//script.handCardClicked (c);
	}


	public void mouseOver(Card c)
	{
		foreach (ICardContainerListener listener in listeners)
		{
			listener.cardMouseOver(this, c);
		}
		//MyLogger.log("mouseOver");
		//script.cardMouseOver (c);
	}
	
	
	public void mouseExit(Card c)
	{
		foreach (ICardContainerListener listener in listeners)
		{
			listener.cardMouseExit(this, c);
		}
		//MyLogger.log("board mouseExit!");
		//script.cardMouseExit (c);
	}
/*
	void OnMouseDown () {
		MyLogger.log("BLANK HAND");
		///script.clickedEmptyHand ();
		
	}
*/
	
	
}
