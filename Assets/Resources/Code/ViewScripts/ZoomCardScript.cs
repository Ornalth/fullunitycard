using UnityEngine;
using System.Collections.Generic;

public class ZoomCardScript : MonoBehaviour {


	TypogenicText title;
	TypogenicText stats;
	TypogenicText cost;
	TypogenicText desc;
	TypogenicText sac;
	TypogenicText type;

	//SpriteRenderer highlightRenderer;
	Card card;
	SpriteRenderer render;
	SpriteRenderer dizzyRenderer;
	SpriteRenderer imageRenderer;
	SpriteRenderer baseRenderer;
	SpriteRenderer setRenderer;
	
	StatusRowViewScript statusScript;
	CounterRowViewScript counterScript;

	List<SpriteRenderer> savedSprites;

	// Use this for initialization
	void Awake()
	{


		//card = CardLibrary.getRandomCard ();
		//MyLogger.log("Card Type " + CardType.convertToString (card.getCardType ()) + card.getName ());
		
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			//MyLogger.log(comp.name);
			//MyLogger.log(comp.Text);
			if (comp.name.Equals("Name"))
			{
				title = comp;
				//MyLogger.log("Found Title");
			}
			else if (comp.name.Equals("Cost"))
			{
				cost = comp;
				//MyLogger.log("Found Stats");
			}
			else if (comp.name.Equals("Sac"))
			{
				sac = comp;
				//MyLogger.log("Found Stats");
			}
			else if (comp.name.Equals("Stats"))
			{
				stats = comp;
				//MyLogger.log("Found Stats");
			}
			else if (comp.name.Equals("Desc"))
			{
				desc = comp;
				//MyLogger.log("Found Stats");
			}
			else if (comp.name.Equals("Type"))
			{
				type = comp;
				//MyLogger.log("Found Stats");
			}
		
			
		}
		/*SpriteRenderer[] renderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer re in renderers)
		{
			if (re.name.Equals("Counter"))
			{
				counterRenderer = re;
			}
		}*/
		//GameObject high = transform.Find("Highlight").gameObject;
		//highlightRenderer = high.GetComponent<SpriteRenderer> ();
		//highlightRenderer.color = Color.green;
		
		//title.Text = card.getName ();
		savedSprites = new List<SpriteRenderer>();

		render = GetComponent<SpriteRenderer> ();
		

		statusScript = GetComponentInChildren<StatusRowViewScript> ();
		counterScript = GetComponentInChildren<CounterRowViewScript> ();
		GameObject diz = transform.Find("Dizzy").gameObject;
		dizzyRenderer = diz.GetComponent<SpriteRenderer> ();
		GameObject imageR = transform.Find("Image").gameObject;
		imageRenderer = imageR.GetComponent<SpriteRenderer> ();
		GameObject bas = transform.Find("Base").gameObject;
		baseRenderer = bas.GetComponent<SpriteRenderer> ();

		savedSprites.Add(render);
		savedSprites.Add(baseRenderer);
		savedSprites.Add(imageRenderer);
		savedSprites.Add(dizzyRenderer);
		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("Set"))
			{
				savedSprites.Add(sprite);
				setRenderer = sprite;
			}
		}
	}

	public void setSortingLayerID(int id)
	{
		this.transform.GetComponent<Renderer>().sortingLayerID = id;

		MeshRenderer[] renders =  gameObject.GetComponentsInChildren<MeshRenderer> ();

		foreach (MeshRenderer render in renders)
		{
			render.sortingLayerID = id;
			render.sortingLayerName = "GraveView2";
			render.sortingOrder = 50;
		}
		
		foreach (SpriteRenderer obj in savedSprites)
		{
			obj.sortingLayerID = id;
		}

	}

	public void setCard(Card c)
	{
		if (card == c)
			return;
		card = c;
		updateCard ();
		
	}

	public void setDetail(StackDetails detail)
	{
		if (detail == null)
		{
			setRenderererer(false);
			//gameObject.SetActive(false);
			//renderer.enabled = false;
			//title.Text = "";
		}
		else
		{
			setRenderererer(true);
			title.Text = detail.origin.getName();
			desc.Text = detail.ev.getDesc();
			ImageLibrary.setBase(baseRenderer, detail.origin.getFactions());
			imageRenderer.sprite = ImageLibrary.getImageForCard (detail.origin);
			
			cost.Text = "";
			sac.Text = "";
			stats.Text = "";
			dizzyRenderer.sprite = null;
			setRenderer.sprite = null;
			type.Text = "";

		}
	}

	private void updateCard()
	{
		if (card == null)
		{
			setRenderererer(false);
			//gameObject.SetActive(false);
			//renderer.enabled = false;
			//title.Text = "";
		}
		else
		{
			setRenderererer(true);

		
			
			ImageLibrary.setBaseOutlined(baseRenderer, card.getFactions());
			
			if (card is CombatCard)
			{
				CombatCard c = (CombatCard) card;
				stats.Text = "" + c.getTotalAttack() + "/" + c.getTotalHealth();
				//MyLogger.log("BOARD TEXT HERE" + c.getBoardText());

				sac.Text = CardFontHelper.getCostStr(((CombatCard)card).getSalvageValue());
				
			}
			else
			{
				if (card is Spell)
				{

				}
				stats.Text = "";
				sac.Text = "";

			}
			statusScript.setStatuses(card.getStatuses());
				

			dizzyRenderer.sprite = ImageLibrary.getDizzyImage(card.getDizzy().getDizzy());
			cost.Text =  CardFontHelper.getCostStr(card.getCost());
			imageRenderer.sprite = ImageLibrary.getImageForCard (card);
			
			title.Text = card.getName();
			type.Text = card.getNameTypeStr();
			desc.Text = card.getDesc();
			
			List<Counter> counters = card.getCounters();
			counterScript.setCounters(counters);
			Rarity rarity = RarityConverter.convertRarity(card.getRarity());
			CardSet cSet = card.getCardSet();
			setRenderer.sprite = ImageLibrary.getCardSetImage(cSet, rarity);
		}
	}

	private void setRenderererer(bool t)
	{
		Renderer[] rs = gameObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer r in rs)
		{
			r.enabled = t;
		}
	}
	

}