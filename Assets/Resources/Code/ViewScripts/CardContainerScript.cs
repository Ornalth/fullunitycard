using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CardContainerScript : MonoBehaviour {

	protected List<ICardContainerListener> listeners = new List<ICardContainerListener>();
	protected List<SpriteRenderer> savedSprites = new List<SpriteRenderer>();

	protected List<Card> cards;
	protected List<GameObject> views;
	protected List<CardViewScript> viewsScripts;

	public void addCardContainerListener(ICardContainerListener listener)
	{
		listeners.Add(listener);
	}

	public void removeListener(ICardContainerListener listener)
	{
		listeners.Remove(listener);
	}


	public virtual void setSortingLayerID(int id)
	{
		this.transform.GetComponent<Renderer>().sortingLayerID = id;

		MeshRenderer[] renders =  gameObject.GetComponentsInChildren<MeshRenderer> ();

		foreach (MeshRenderer render in renders)
		{
			render.sortingLayerID = id;
			render.sortingOrder = 50;
		}
		
		foreach (SpriteRenderer obj in savedSprites)
		{
			obj.sortingLayerID = id;
		}

	}

	//HIGHLIGHTS
	public void restoreOriginalHighlights()
	{
		for (int i = 0; i < cards.Count; i++)
		{
			CardViewScript script = viewsScripts[i];
			script.popHighlights (); //MAYBE POP INSTEAD?
		}

	}


	public void setHighlights (Card c, int reason, int mode)
	{
		List<Card> cards = new List<Card>();
		cards.Add(c);
		setHighlights(cards, reason, mode);
	}



	public void setHighlights (List<Card> highlights, int reason, int mode)
	{
		Color color = HighlightReason.getColorForReason(reason);

		if (mode == ReplaceMode.REPLACE)
		{
			restoreOriginalHighlights ();
			commitHighlights (highlights, color);
			//pushHighlights (highlights);
		}
		else if (mode == ReplaceMode.NONE)
		{
			commitHighlights (highlights, color);
		}
		else if (mode == ReplaceMode.FULL_LAYER)
		{
			fullLayerHighlight(highlights, color);
		}
		else
		{
			MyLogger.log("BoardRowScript -- PANIC NO REPLACEMODE EXIST");
		}
	}


	private void commitHighlights(List<Card> highlights, Color color)
	{

		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);
		
			if (index >= 0)
			{
				CardViewScript script = viewsScripts[i];
				script.pushHighlight(color);
			}
		}
		


	}

	private void fullLayerHighlight(List<Card> highlights, Color color)
	{
		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				CardViewScript script = viewsScripts[i];
				script.pushHighlight(color);
				//MyLogger.log("PUSHED RED OR WAHTEVER");
			}
			else
			{
				CardViewScript script = viewsScripts[i];
				script.pushDefaultHighlight();
				//MyLogger.log("PUSHED HIWTe");
			}
		}
	
	}

	void setColor(CardViewScript scr, Color color)
	{
		scr.pushHighlight (color);
	}

	public void popHighlights (Card c, int reason)
	{
		List<Card> cardss = new List<Card>();
		cardss.Add(c);
		popHighlights(cardss, HighlightReason.getColorForReason(reason) );
	}


	public void popHighlights (List<Card> cards,int reason)
	{
		popHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popHighlights (List<Card> cards, Color color)
	{
		foreach (Card c in cards)
		{
			int index = this.cards.IndexOf(c);
//			MyLogger.log("BAD INDEX IS " + index + " " + c.toString());
			if (index > -1)
			{
				CardViewScript script = viewsScripts[index];
				script.removeLastColor(color);
			}		
		}
	}

	public void popLayerHighlights (List<Card> cards,int reason)
	{
		popLayerHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popLayerHighlights (List<Card> highlights, Color color)
	{
		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				CardViewScript script = viewsScripts[i];
				script.removeLastColor(color);
			}
			else
			{
				CardViewScript script = viewsScripts[i];
				script.removeLastDefaultColor();
			}
		}
		
		
	}

	//BOARDROW
	public void popAllHighlights(int reason)
	{
		popHighlights(cards, reason);
	}
}
