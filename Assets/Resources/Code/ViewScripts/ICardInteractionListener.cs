﻿using UnityEngine;
using System.Collections;


public interface ICardContainerListener
{
	void cardClicked(CardContainerScript scr, Card c);
	void cardMouseOver(CardContainerScript scr, Card c);
	void cardMouseExit(CardContainerScript scr, Card c);
}

