using UnityEngine;
using System.Collections.Generic;
using System;

public class TimerClock : MonoBehaviour
{
	float time;
	float reserveTime;
	TypogenicText mainTimeView;
	TypogenicText reserveTimeView;
	bool paused;
	int team = -1;

	private static readonly float SECONDS_TO_MILLIS = 1000;

	List<TimerClockListener> listeners;

	public interface TimerClockListener {
		void mainTimeComplete(TimerClock scr);
		void reserveTimeComplete(TimerClock scr);
		void allTimeComplete(TimerClock scr);
	}

	void Awake()
	{
		paused = true;
		time = 5000000;
		reserveTime = 1000000000;

		TypogenicText[] texts = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText text in texts)
		{
			if (text.name.Equals("MainTime"))
			{
				mainTimeView = text;
			}
			else if (text.name.Equals("ReserveTime"))
			{
				reserveTimeView = text;
			}	
		}

		mainTimeView = this.GetComponentInChildren<TypogenicText>();
		listeners = new List<TimerClockListener>();
		updateTimeText();
		//yeee... connect with main board...
		//yee.. connect with timeView...
	}

	public void setTeam(int team)
	{
		this.team = team;
	}

	public int getTeam()
	{
		return team;
	}

	public PlayerTimer getPlayerTimerFormatted()
	{
		PlayerTimer timer = new PlayerTimer(time, reserveTime);
		return timer;
	}

	public float getReserveTime()
	{
		return reserveTime;
	}

	public float getTime()
	{
		return time;
	}

	void Update()
	{
		if (paused)
		{
			return;
		}

		float deltaTime = Time.deltaTime * SECONDS_TO_MILLIS;

		if (time <= 0)
		{
			reserveTime -= deltaTime;
			if (reserveTime <= 0)
			{
				reserveTime = 0;
				notifyReserveTimeComplete();
			}
		}
		else
		{
			time -= deltaTime;
			if (time <= 0)
			{
				time = 0;
				notifyMainTimeComplete();
			}
		}

		updateTimeText();
		if (time <= 0 && reserveTime <= 0)
		{
			paused = true;
			notifyAllTimeComplete();
		}
	}

	private void updateTimeText()
	{
		TimeSpan mainTimeSpan = TimeSpan.FromMilliseconds(time);
		TimeSpan reserveTimeSpan = TimeSpan.FromMilliseconds(reserveTime);
		mainTimeView.Text = string.Format("{0:00}:{1:00}", mainTimeSpan.Minutes, mainTimeSpan.Seconds); //mainTimeSpan.ToString(@"hh\h\:mm\m\:ss\s\:fff\m\s");
		reserveTimeView.Text = string.Format("{0:00}:{1:00}", reserveTimeSpan.Minutes, reserveTimeSpan.Seconds);//reserveTimeSpan.ToString(@"hh\h\:mm\m\:ss\s\:fff\m\s");
	}

	public void setTime(float time)
	{
		this.time = time;
		updateTimeText();
	}

	public void addTime(float time)
	{
		this.time += time;
		updateTimeText();
	}

	public void setReserveTime(float time)
	{
		this.reserveTime = time;
		updateTimeText();
	}

	public void addReserveTime(float time)
	{
		this.reserveTime += time;
		updateTimeText();
	}

	public void pause()
	{
		paused = true;
	}

	public void resume()
	{
		paused = false;
	}

	private void notifyAllTimeComplete()
	{
		foreach (TimerClockListener listener in listeners)
		{
			listener.allTimeComplete(this);
		}
	}

	private void notifyMainTimeComplete()
	{
		foreach (TimerClockListener listener in listeners)
		{
			listener.mainTimeComplete(this);
		}
	}

	private void notifyReserveTimeComplete()
	{
		foreach (TimerClockListener listener in listeners)
		{
			listener.reserveTimeComplete(this);
		}
	}



	public void subscribe(TimerClockListener listener)
	{
		listeners.Add(listener);
	}

	public void unsubscribe(TimerClockListener listener)
	{
		listeners.Remove(listener);
	}
}