using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilitySelectView : MonoBehaviour {
	public interface AbilitySelectController {
		void abilityClicked(ActiveAbility abil);
	}


	TypogenicText nameText;
	TypogenicText desc;
	SpriteRenderer baseRenderer;
	SpriteRenderer imageRenderer;
	//SpriteRenderer highlightRenderer;
	bool selected = false;

	bool shouldHide = false;
	
	AbilitySelectController controller = null;
	
	private Color defaultColor;
	// Use this for initialization
	static Dictionary<int,Sprite> dizzyImages;
	ActiveAbility ability;

	void Awake()
	{
	
		defaultColor = Color.white;
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("Desc"))
			{
				desc = comp;
			}
			else if (comp.name.Equals("Name"))
			{
				nameText = comp;
			}
		}

		
		SpriteRenderer[] renderers = gameObject.GetComponentsInChildren<SpriteRenderer> ();
		foreach (SpriteRenderer renderer in renderers)
		{
			if (renderer.name.Equals("Base"))
			{
				baseRenderer = renderer;
			}
			else if (renderer.name.Equals("Image"))
			{
				imageRenderer = renderer;
			}

		}
	}

	void Start () 
	{
		//GameObject high = transform.Find("Highlight").gameObject;
		//highlightRenderer = high.GetComponent<SpriteRenderer> ();
	}
	

	
	void OnMouseDown ()
	{
		MyLogger.log("Mouse down on ability");
		controller.abilityClicked (this.ability);
	}


	public void setController(AbilitySelectController h)
	{
		controller = h;
	}
	
	public void setHighlight(Color c)
	{
		//highlightRenderer.color = c;
	}
	public void setDefaultHighlight()
	{
		//highlightRenderer.color = defaultColor;
	}

	// Setting data
	public void setAbility(ActiveAbility abil, Card origin)
	{
	
		this.ability = abil;
		desc.Text = ability.getDesc();
		nameText.Text = origin.getName();
		ImageLibrary.setBase(baseRenderer, origin.getFactions());
		imageRenderer.sprite = ImageLibrary.getImageForCard (origin);



		
	}
}