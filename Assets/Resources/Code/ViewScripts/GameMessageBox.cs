using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;
public class GameMessageBox : MonoBehaviour {

	public interface MessageBoxListener
	{
		void messageAdded(GameMessageBox box, string message);
	}
	List<string> messages = new List<string>();
	Text text;
	StringBuilder builder;
	Scrollbar scrollbar;
	CanvasGroup canvasGroup;
	public InputField textInputField;
	List<MessageBoxListener> listeners = new List<MessageBoxListener>();
	void Awake()
	{
		canvasGroup = gameObject.GetComponentInChildren<CanvasGroup>();
		scrollbar = gameObject.GetComponentInChildren<Scrollbar>();
		text = gameObject.GetComponentInChildren<Text>();
		reset();
		text.text = "";
		/*Canvas canvas = gameObject.GetComponentInChildren<Canvas>();

		         RectTransform canvasWidth = canvas.gameObject.GetComponent<RectTransform> ();
         MyLogger.log("width: " + objectRectTransform.rect.width + ", height: " + objectRectTransform.rect.height);
		textInputField.gameObject.GetCom
		width = canvas.gameObject.width;
	*/
	}
	public void addListener(MessageBoxListener listener)
	{
		listeners.Add(listener);
	}
	/*void Start()
	{
		ViewHelper.registerMessageDialog(this);
	}*/

	public void reset()
	{
		builder = new StringBuilder();
		messages = new List<string>();
	}

	public List<string> getMessages()
	{
		return messages;
	}

	public void addMessage(string message)
	{
		messages.Add(message);
		builder.Append("\n" + message);
		updateDisplay();

		foreach (MessageBoxListener listener in listeners)
		{
			listener.messageAdded(this, message);
		}
	}

	private void updateDisplay()
	{
		//TODO
		text.text = builder.ToString();
		//scrollbar.value = 1f;
		scrollbar.value = 0;
		//StartCoroutine(wat());
	}
/*
	public IEnumerator wat()
    {
        MyLogger.log("Now its called");
        yield return null; // Waiting just one frame is probably good enough, yield return null does that
       // bar = GetComponentInChildren<Scrollbar>();
        scrollbar.value = 0;
        MyLogger.log("Now its setted");
    }
*/
	public CanvasGroup getCanvasGroup()
	{
		return canvasGroup;
	}

	public void gainFocus()
	{
		//EventSystem.current.SetSelectedGameObject(textInputField.gameObject, null);
		//textInputField.OnPointerClick(new PointerEventData(EventSystem.current));
		textInputField.text = "";
	}

	 public void Update()
     {
         if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown("enter") &&  EventSystem.current.currentSelectedGameObject == textInputField.gameObject)
         {
 	
             string text = textInputField.text;
             text = text.Trim();
             if (text.Length > 0)
             	addMessage(text);
             textInputField.text = "";
             textInputField.ActivateInputField();
    		 textInputField.Select();

            // textInputField.ActivateInputField();
         }
     }
}