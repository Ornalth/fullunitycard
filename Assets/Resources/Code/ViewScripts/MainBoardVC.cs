using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UI;

using Holoville.HOTween; 
using Holoville.HOTween.Plugins;

public class MainBoardVC: NetworkBehaviour, ISpriteButtonListener, GameMessageBox.MessageBoxListener, TimerClock.TimerClockListener, ICardContainerListener {

	public int team = 0;
	PlayerViewScript playerView0;
	PlayerViewScript playerView1;
	BoardRowScript boardRow0;
	BoardRowScript boardRow1;
	HandViewScript handRow0;

	TypogenicText phaseMsg;
	ZoomCardScript zoomCard;
	TypogenicText landMsg;
	
	Renderer requestOKButtonRender;
	Renderer requestCancelButtonRender;
	TypogenicText spellText;
	MainBoardModel model;


	Transform arrowTransform = null;


	GameObject abilView;
	AbilitySelectorScript abilViewScript;

	GameObject stackView;
	StackViewScript stackViewScript;
	List<GameObject> arrows = new List<GameObject>();

	GameObject graveView;
	GraveViewScript graveViewScript;

	GameObject gameOverView;
	GameOverViewScript gameOverScript;

	GameObject messageBox;
	GameMessageBox messageBoxScript;
	bool msgBoxShown = true;
//TODO
	GameObject libraryView;
	DarkRequestViewScript libraryViewScript;

	GameObject oppTimer;
	GameObject ownTimer;
	TimerClock oppTimerScript;
	TimerClock ownTimerScript;

	GameObject mulliganView;
	MulliganViewScript mulliganViewScript;

	SpriteButtonScript concedeButtonScript;

	private void init()
	{
			

		libraryView = Instantiate (Resources.Load("RequestView")) as GameObject;
		libraryView.name = "LibraryView";
		libraryView.transform.parent = transform;
		libraryViewScript = libraryView.GetComponent<DarkRequestViewScript>();
		libraryViewScript.setMainBoardVC(this);
		libraryViewScript.hideCloseButtons();
		
		GameObject graveObj = Instantiate (Resources.Load("GraveView")) as GameObject;
		graveView = graveObj;
		graveView.transform.parent = transform;
		graveViewScript = graveView.GetComponent<GraveViewScript>();
		graveViewScript.setMainBoardVC(this);
		graveViewScript.hideOKCancelButtons();
		

				//Init graveview
		gameOverView = Instantiate (Resources.Load("GameOverScreen")) as GameObject;
		gameOverView.transform.parent = transform;
		gameOverScript = gameOverView.GetComponent<GameOverViewScript>();

		CancelButtonScript[] buttons = GetComponentsInChildren<CancelButtonScript>();
		foreach (CancelButtonScript button in buttons)
		{
			button.addListener(this);
		}

		ConfirmButtonScript[] buttons2 = GetComponentsInChildren<ConfirmButtonScript>();
		foreach (ConfirmButtonScript button in buttons2)
		{
			button.addListener(this);
		}

		CloseButtonScript[] buttons3 = GetComponentsInChildren<CloseButtonScript>();
		foreach (CloseButtonScript button in buttons3)
		{
			button.addListener(this);
		}

		CustomSpriteButtonScript[] buttons4 = GetComponentsInChildren<CustomSpriteButtonScript>();
		foreach (CustomSpriteButtonScript button in buttons4)
		{
			if (button.name.Equals("ConcedeButton"))
			{
				concedeButtonScript = button;
				button.addListener(this);
			}
		}


		mulliganView = Instantiate (Resources.Load("MullPrefab")) as GameObject;
		mulliganView.name = "MulliganView";
		mulliganView.transform.parent = transform;
		mulliganViewScript = mulliganView.GetComponent<MulliganViewScript>();
		mulliganViewScript.setMainBoardVC(this);

		ViewHelper.hide(gameOverView);
		ViewHelper.hide(libraryView);
		ViewHelper.hide(graveView);

		messageBox = Instantiate (Resources.Load("ScrollingTextBoxObject")) as GameObject;
		messageBox.transform.parent = transform;
		messageBoxScript = messageBox.GetComponent<GameMessageBox>();
		messageBoxScript.addListener(this);
		//?????
		messageBox.transform.localPosition = new Vector3 (0,0,0);
		msgBoxShown = false;
		ViewHelper.hide(messageBox);

		//Init abilView
		GameObject g = Instantiate (Resources.Load ("DarkenView")) as GameObject;
		
		abilView = g;
		abilView.name = "AbilityViewSelector";
		g.transform.parent = transform;
		abilViewScript = g.GetComponent<AbilitySelectorScript> ();
		ViewHelper.hide (abilView);

		//Inivt stackView
		GameObject g2 = Instantiate (Resources.Load ("StackView")) as GameObject; 
		stackView = g2;
		g2.transform.parent = transform;
		g2.transform.localPosition = new Vector3 (1, 1, -1); // Needs to be draggable.
		stackViewScript = stackView.GetComponent<StackViewScript>();
		ViewHelper.hide (stackView);

		GameObject g5 = Instantiate (Resources.Load ("ZoomCard")) as GameObject; 
		g5.name = "ZoomCard";
		g5.transform.parent = transform;
		g5.transform.localPosition = new Vector3 (0,0,0);

		GameObject g3 = Instantiate (Resources.Load ("PlayerView")) as GameObject; 
		g3.name = "PlayerViewAlly";
		
		g3.transform.parent = transform;
		g3.transform.localPosition = new Vector3 (-6.25f,-2f,0);

		GameObject g4 = Instantiate (Resources.Load ("PlayerView")) as GameObject; 
		g4.name = "PlayerViewOpp";

		g4.transform.parent = transform;
		g4.transform.localPosition = new Vector3 (6.25f,3.75f,0);
		//stackView = g2;
	}
	//[Command]
	public void initVC(int abc)
	{
		this.team = abc;
		RpcInitVC(abc);
		RpcSetTeam(abc);
		
	}

	[ClientRpc]
	public void RpcInitVC(int abc)
	{
		//MyLogger.log("PRESET TEAM IS" + abc);
		

		if (isLocalPlayer)
		{
			MyLogger.log("SUBBED!");
			GameObject mainBoardModelObject = GameObject.FindWithTag("Model");
			MainBoardModel model = mainBoardModelObject.GetComponent<MainBoardModel>();
			this.model = model;
			model.suscribeToModel(this);

			transform.parent = mainBoardModelObject.transform;

		}
	}

	[ClientRpc]
	private void RpcSetTeam(int abc)
	{
		//MyLogger.log("TEAM IS" + abc);
		this.team = abc;
	}

	void Start()
	{


		
		if (!isLocalPlayer)
		{
			MyLogger.log("NOT LOCAL PLAYER start");
			GetComponent<CloseChildrenScript>().closeChildren();
			return;
		}
		else
		{
		}


		
	}

	public void gameStartReady()
	{
		MyLogger.log("TOLD GAME HAS STARTED");
		ownTimer = Instantiate (Resources.Load("Timer")) as GameObject;
		ownTimer.name = "ownTimer";
		ownTimer.transform.parent = transform;
		ownTimer.transform.localPosition = new Vector3 (-7f,1.0f,0);
		ownTimerScript = ownTimer.GetComponentInChildren<TimerClock>();
		ownTimerScript.subscribe(this);

		oppTimer = Instantiate (Resources.Load("Timer")) as GameObject;
		oppTimer.name = "oppTimer";
		oppTimer.transform.parent = transform;
		oppTimer.transform.localPosition = new Vector3 (5.5f,2.3f,0);
		oppTimerScript = oppTimer.GetComponentInChildren<TimerClock>();
		oppTimerScript.subscribe(this);

		init();
		libraryViewScript.setTeam(team);
		foreach (Transform child in transform)
		{
			if (child.name.Equals("BoardRowOpp"))
			{
				boardRow1 = child.gameObject.GetComponent<BoardRowScript>();
			}
			else if (child.name.Equals ("BoardRowAlly"))
			{
				boardRow0 = child.gameObject.GetComponent<BoardRowScript>();
			}
			else if (child.name.Equals ("PlayerViewOpp"))
			{
				playerView1 = child.gameObject.GetComponent<PlayerViewScript>();
				playerView1.playerNum = 1;
			}
			else if (child.name.Equals ("PlayerViewAlly"))
			{
				playerView0 = child.gameObject.GetComponent<PlayerViewScript>();
				playerView0.playerNum = 0;
			}
			else if (child.name.Equals ("Next"))
			{
			}
			else if (child.name.Equals ("Pass"))
			{
			}
			else if (child.name.Equals ("HandAreaAlly"))
			{
				handRow0 = child.gameObject.GetComponent<HandViewScript>();
				handRow0.addCardContainerListener(this);
				MyLogger.log("ADDED HAND LISTENER");
			}
			else if (child.name.Equals("PhaseMsg"))
			{
				phaseMsg = child.gameObject.GetComponent<TypogenicText>();
			}
			else if (child.name.Equals("LandMsg"))
			{
				landMsg = child.gameObject.GetComponent<TypogenicText>();
			}
			else if (child.name.Equals("ZoomCard"))
			{
				zoomCard = child.gameObject.GetComponent<ZoomCardScript>();
			}
			else if (child.name.Equals("OK"))
			{
				requestOKButtonRender = child.gameObject.GetComponent<SpriteRenderer>();
			}
			else if (child.name.Equals("Cancel"))
			{
				requestCancelButtonRender = child.gameObject.GetComponent<SpriteRenderer>();
			}
			else if (child.name.Equals("SpellText"))
			{
				spellText = child.gameObject.GetComponent<TypogenicText>();
			}
			else if (child.name.Equals("Arrow"))
			{
				arrowTransform = child;
			}
			else if (child.name.Equals("Ability"))
			{
			}
		}

		updateTimers();
		hideRequestDialog ();
		lazyUpdate();

		mulliganViewScript.setCards(model.getPlayer(team).getHand());
		GameObject gameOptionsObject = GameObject.FindWithTag("GameOptions");
		Destroy(gameOptionsObject);
	}

	public void updateTimers()
	{
		List<PlayerTimer> timers = getModel().getPlayerTimers();
		ownTimerScript.setTime(timers[team].getTime());
		ownTimerScript.setReserveTime(timers[team].getReserveTime());
		oppTimerScript.setTime(timers[team ^ 1].getTime());
		oppTimerScript.setReserveTime(timers[team ^ 1].getReserveTime());
	}

	public void Update()
	{
		if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown("enter")) && !msgBoxShown)
		{
			showMessageBox();
		}	
		else if (Input.GetKeyDown("f"))
		{
			fadeMessageBox();
		}
	}

	public void showMessageBox()
	{
		CanvasGroup group = messageBoxScript.getCanvasGroup();
		HOTween.Kill(group);
		msgBoxShown = true;
		
		//Color myColor = messageBox.renderer.material.color;
		//myColor.a = 1;
		//HOTween.To(messageBox.renderer.material, 1, "color", myColor);
		ViewHelper.show(messageBox);
		HOTween.To(group, 0, "alpha", 1);
		messageBoxScript.gainFocus();
		
	}

	public void fadeMessageBox()
	{
		//if (msgBoxShown)
		//{
			CanvasGroup group = messageBoxScript.getCanvasGroup();
			HOTween.Kill(group);
			msgBoxShown = false;
			
			HOTween.To(group, 1, "alpha", 0);

			//Color myColor = messageBox.renderer.material.color;
			//myColor.a = 0;
			//HOTween.To(messageBox.renderer.material, 1, "color", myColor); // color (with alpha) changes as expected
 
			//messageBox.renderer.material.color.a = alpha;
			//ViewHelper.hide(messageBox);
	//	}
	}

	public void postMessage(string message)
	{
		messageBoxScript.addMessage(message);
	}

	

	
	public void hasFinishedMulliganUpdate()
	{
		ViewHelper.hide(mulliganView);
		lazyUpdate();
	}


	public void lazyUpdate()
	{

		int bottom = team;
		int top = team ^ 1;
		if (model == null)
		{
			MyLogger.log("WHAT IS HAPPENING");
			try{
				throw new SystemException();
			}
			catch (SystemException err)
			{
				MyLogger.log(err.Message);
				MyLogger.log(err.StackTrace);
				return;
			}
		}


		playerView0.setPlayer (model.getPlayer (bottom));
		handRow0.setCards (model.getPlayer (bottom).getHand ());
		boardRow0.setCreatures (model.getPlayer (bottom).getCreatures ());
		boardRow0.setStructures (model.getPlayer (bottom).getStructures ());

		playerView1.setPlayer (model.getPlayer (top));

		boardRow1.setCreatures( model.getPlayer (top).getCreatures ());
		boardRow1.setStructures( model.getPlayer (top).getStructures ());
		
		phaseMsg.Text = model.phase.toString();
		
		landMsg.Text = model.land.getDesc ();

		if (model.getPriority() == team)
		{
			arrowTransform.localEulerAngles = new Vector3(0,0, 180);

		}
		else
		{
			arrowTransform.localEulerAngles = new Vector3(0,0, 0);
		}

		
	}

	public void lazyUpdateCombat()
	{
			List<CombatPair> combatArray = model.combatArray;

			int index = 0;
			foreach (CombatPair pair in combatArray)
			{
				Card attacker = pair.attacker;
				Card blocker = pair.blocker;
				Card attacked = pair.attacked;


				Vector3 srcVector = initCombatVector3(attacker);
				Vector3 desVector = Vector3.zero;


				if (srcVector == Vector3.zero)
				{
					MyLogger.log("ATTACKER NOT FOUND???");
					continue;
				}

//TODO MAKE OBJECT POOLER
				if (blocker != null)
				{
					desVector = initCombatVector3(blocker);
					if (index >= arrows.Count)
					{
						GameObject arrow =  Instantiate (Resources.Load ("DynamicArrow")) as GameObject;
						arrow.transform.parent = transform;
						arrow.transform.position = desVector;
						DynamicArrow arrowScript = arrow.GetComponentInChildren<DynamicArrow>();
						arrowScript.setTargetPosition(srcVector);
						arrowScript.setColor(Color.green);	
						arrows.Add(arrow);
					}
					else
					{
						GameObject arrow = arrows[index];
						ViewHelper.show(arrow);
						arrow.transform.position = desVector;
						DynamicArrow arrowScript = arrow.GetComponentInChildren<DynamicArrow>();
						arrowScript.setTargetPosition(srcVector);
						arrowScript.setColor(Color.green);
					}
					index++;
				}

				if (attacked != null)
				{
					desVector = initCombatVector3(attacked);
					if (index >= arrows.Count)
					{
						GameObject arrow =  Instantiate (Resources.Load ("DynamicArrow")) as GameObject;
						arrow.transform.parent = transform;
						arrow.transform.position = srcVector;
						DynamicArrow arrowScript = arrow.GetComponentInChildren<DynamicArrow>();
						arrowScript.setTargetPosition(desVector);
						arrowScript.setColor(Color.black);
						arrows.Add(arrow);
					}
					else
					{
						GameObject arrow = arrows[index];
						ViewHelper.show(arrow);
						arrow.transform.position = srcVector;
						DynamicArrow arrowScript = arrow.GetComponentInChildren<DynamicArrow>();
						arrowScript.setTargetPosition(desVector);
						arrowScript.setColor(Color.black);
					}
					index++;
				}

			}
	
			for (; index < arrows.Count; index++)
			{
				ViewHelper.hide(arrows[index]);

			}


			boardRow0.clearAllHighlights(HighlightReason.ATTACK);
			boardRow1.clearAllHighlights(HighlightReason.ATTACK);

			if (model.boardSelectedCard1 != null && model.boardSelectedCard1.getTeam() == team)
			{
				if( model.boardSelectedCard1.getTeam() == team)
					boardRow0.setHighlights(model.boardSelectedCard1, HighlightReason.ATTACK, ReplaceMode.NONE);
				else 
					boardRow1.setHighlights(model.boardSelectedCard1, HighlightReason.ATTACK, ReplaceMode.NONE);
			}
		
		
		
	}

	private Vector3 initCombatVector3(Card card)
	{
		Vector3 ar = Vector3.zero;
		if (card is Player)
		{
			//TODO GET FROM PLAYER
			if (card.Equals(playerView0.getPlayer()))
			{
				ar = playerView0.getCardPosition();
			}
			else if (card.Equals(playerView1.getPlayer()))
			{
				ar = playerView1.getCardPosition();
			}
			else
			{
				MyLogger.log("TODO PANIC NO PLAYER MATCHES PLAYER THAT WAS MENTIONED");
			}
		}
		else
		{
			ar = boardRow0.getCardTopPosition(card);
			if (ar ==  Vector3.zero)
			{
				ar = boardRow1.getCardBottomPosition(card);
			}
		} 
		return ar;
	}

	public void requestBegin()
	{
		//TODO
		//VIEW SHOW REQUEST OPTIONS.
		MyLogger.log("REQUEST BEGIN");
		if (model.request.getControllingTeam() == team)
		{
			Request request = model.request;

			requestOKButtonRender.enabled = true;
			requestCancelButtonRender.enabled = true;
			String reason = request.getReason();
			spellText.Text = model.request.getDetailsStr();
			List<Card> targets = model.request.getTargets();
			//MyLogger.log("REQUEST MODE " + request.getMode());
	

	/*
			if (request.getMode() == Request.MODE_SEARCH_LIBRARY || (request.getMode() == Request.MODE_TARGET && request.getTargetType().containsState(CardState.ANY_NOT_BOARD)))
			{
				MyLogger.log("SHOWED LIBRARY VIEW");

				ViewHelper.show(libraryView);
				stackViewScript.setColliderState(false);


				libraryViewScript.setCards(targets, model.getCurrentExecutingStackDetail());
				libraryViewScript.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
			}
			else
			{
				MyLogger.log("JUST BOARD VIEW");
				handRow0.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
				boardRow0.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
				boardRow1.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);

				
			}

			*/

		
			MyLogger.log("SHOWED LIBRARY VIEW");

			ViewHelper.show(libraryView);
			stackViewScript.setColliderState(false);
			libraryViewScript.setCards(targets, model.getCurrentExecutingStackDetail());
			libraryViewScript.setReason(reason);
			libraryViewScript.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
			
		}

	
		
		
	}

	public void cancelRequest()
	{
		hideRequestDialog ();
		Player player = model.request.getPlayer();

		MyLogger.log("CANCEL REQUEST");
		if (player != null && player.getTeam() == team)
		{
			List<Card> selectedTargets = model.request.getSelectedTargets();
			handRow0.popHighlights (selectedTargets, HighlightReason.ABILITY_TARGET);
			boardRow0.popHighlights (selectedTargets, HighlightReason.ABILITY_TARGET);
			boardRow1.popHighlights (selectedTargets, HighlightReason.ABILITY_TARGET);

			List<Card> targets = model.request.getTargets();
			handRow0.popHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET);
			boardRow0.popHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET);
			boardRow1.popHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET);

			ViewHelper.hide(libraryView);
			stackViewScript.setColliderState(true);
		}
	}

	public void updateGameEnded()
	{
		Player player = model.getWinner();
		//TODO
		ViewHelper.show(gameOverView);
		gameOverScript.setMessage("Player " + player.getTeam() + " wins.");

		ownTimerScript.pause();
		oppTimerScript.pause();
		
	}
	


	public void buttonClicked(SpriteButtonScript scr)
	{
		if (scr is CancelButtonScript)
		{
			cancelButtonClicked();
		}
		else if (scr is ConfirmButtonScript)
		{
			requestConfirmButton();
		}
		else if (scr is CloseButtonScript)
		{
			ViewHelper.hide(graveView);
			stackViewScript.setColliderState(true);
		}
		else if (scr == concedeButtonScript)
		{
			Debug.Log("CONCEDE");
			requestConcede();
		}

	}
	
	
	void hideRequestDialog()
	{
		
		requestOKButtonRender.enabled = false;
		requestCancelButtonRender.enabled = false;
		spellText.Text = "";
		
	}
	

	//TODO
	//Ability stuff.
	//Selecting which card's active to pick
	public void updateAbilityMode()
	{
		ViewHelper.hide(graveView);
		int abilityTeam = model.abilityTeam;
		bool abilityMode = model.abilityMode;
		List<Card> abilityActiveCards = model.abilityActiveCards;

		Card origin = model.abilityCard;

		if (abilityMode)
		{
			if (origin != null)
			{
				boardRow0.setHighlights (origin, HighlightReason.ABILITY_SOURCE, ReplaceMode.REPLACE);
				boardRow1.setHighlights (origin, HighlightReason.ABILITY_SOURCE, ReplaceMode.REPLACE);
			}	
			else
			{
				boardRow0.setHighlights (abilityActiveCards, HighlightReason.ABILITY_SOURCE, ReplaceMode.FULL_LAYER);
				boardRow1.setHighlights (abilityActiveCards, HighlightReason.ABILITY_SOURCE, ReplaceMode.FULL_LAYER);
			}

		}
		else
		{
			boardRow0.popAllHighlights (HighlightReason.ABILITY_SOURCE);
			boardRow1.popAllHighlights (HighlightReason.ABILITY_SOURCE);
		}
	}

	//NEW REQUEST.
	public void updateRequestMode()
	{
		ViewHelper.hide(graveView);
	//POP ALL
		boardRow0.popAllHighlights (HighlightReason.ABILITY_TARGET);
		boardRow1.popAllHighlights (HighlightReason.ABILITY_TARGET);
		handRow0.popAllHighlights (HighlightReason.ABILITY_TARGET);
		handRow0.popAllHighlights (HighlightReason.ABILITY_POSSIBLE_TARGET);
		boardRow0.popAllHighlights (HighlightReason.ABILITY_POSSIBLE_TARGET);
		boardRow1.popAllHighlights (HighlightReason.ABILITY_POSSIBLE_TARGET);
		
		//boardRow0.popAllHighlight (HighlightReason.ABILITY_SOURCE);
		//boardRow1.popAllHighlight (HighlightReason.ABILITY_SOURCE);

		if (model.request.getPlayer().getTeam() == team && model.request.inProgress())
		{
			//MyLogger.log("SHOW REQUEST");
			requestOKButtonRender.enabled = true;
			requestCancelButtonRender.enabled = true;
			spellText.Text = model.request.getDetailsStr();
			List<Card> targets = model.request.getTargets();
			handRow0.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
			boardRow0.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
			boardRow1.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
			libraryViewScript.setHighlights (targets, HighlightReason.ABILITY_POSSIBLE_TARGET, ReplaceMode.NONE);
			List<Card> targettedCards = model.request.getSelectedTargets();
			if (targettedCards != null)
			{
				handRow0.setHighlights (targettedCards, HighlightReason.ABILITY_TARGET, ReplaceMode.NONE);
				boardRow0.setHighlights (targettedCards, HighlightReason.ABILITY_TARGET, ReplaceMode.NONE);
				boardRow1.setHighlights (targettedCards, HighlightReason.ABILITY_TARGET, ReplaceMode.NONE);
				libraryViewScript.setHighlights (targettedCards, HighlightReason.ABILITY_TARGET, ReplaceMode.NONE);
			}

		}
		else	
		{
			//MyLogger.log("CANCEL REQUEST" + model.request.getPlayer().getTeam() + " model.request.inProgress() " + model.request.inProgress());
			requestOKButtonRender.enabled = false;
			requestCancelButtonRender.enabled = false;
			spellText.Text = "";
		}
	}


	//Ability SElection
	public void updateAbilitySelectionMode()
	{
		if (model.abilitySelectingMode)
		{
			//MyLogger.log("ABILITY SELECTIN MODE");
			//ONLY DISPLAY IF WE ARE THE CORRECT TEAM.
			if (model.abilityTeam == this.team)
			{
				ViewHelper.show (abilView);
				List<ActiveAbility> abilities = model.abilityActives;
				//MyLogger.log("ABILITY LENGTH" + abilities.Count);

				Card origin = model.abilityCard;
				//MyLogger.log("ORIGIN IS " + origin);
				abilViewScript.setAbilities (abilities, origin, "SET REASON HERE");
			}
		}
		else
		{
//			MyLogger.log("NOT ABILITY SELECTIN MODE");
			ViewHelper.hide(abilView);
		}
	}



	public void graveViewClicked()
	{
		ViewHelper.show(graveView);
		stackViewScript.setColliderState(false);
		List<Card> graveCards = model.getGrave(team);
		graveViewScript.setCards(graveCards);

	}


	//Stack stuff
	
	public void updateStack()
	{
		List<StackDetails> stack = model.getStack ();
		if (stack != null && stack.Count > 0)
		{
			ViewHelper.show(stackView);
			stackViewScript.setColliderState(true);
			stackViewScript.setStack(stack);
		}
		else
		{
			ViewHelper.hide(stackView);
		}
	}


	//stackview

	StackDetails currentDetails = null;
//TODO HANDLE ORIGIN/TARGETS etc existing in hand/player/etc.
//TODO FIXME EXTRACOST
	public void stackViewMouseOver(StackViewScript s, StackDetails details)
	{
		/*
		if (currentDetails != null && details == currentDetails)
		{
			return;
		}

		currentDetails = details;
		Card origin = details.origin;
		

		List<Card> targettedCards = details.targets;
		MyLogger.log("TARGETTED CARDS????" + targettedCards.Count + " THIS ABILITY DESC " + details.ev.getDesc());

		if (targettedCards != null)
		{
			boardRow0.setHighlights (targettedCards, HighlightReason.ABILITY_TARGET, ReplaceMode.FULL_LAYER);
			boardRow1.setHighlights (targettedCards, HighlightReason.ABILITY_TARGET, ReplaceMode.FULL_LAYER);
		}

		if (origin != null)
		{
			boardRow0.setHighlights(origin, HighlightReason.ABILITY_SOURCE, ReplaceMode.NONE);
			boardRow1.setHighlights(origin, HighlightReason.ABILITY_SOURCE, ReplaceMode.NONE);
		}

		zoomCard.setDetail (details);
*/
	}
//TODO FIXME EXTRACOST
	public void stackViewMouseExit(StackViewScript s, StackDetails details)
	{
		/*
		if (currentDetails != null && currentDetails != details)
		{
			return;
		}

		Card origin = details.origin;
		List<Card> targettedCards = details.targets;

		
		if (targettedCards != null)
		{
			boardRow0.popLayerHighlights (targettedCards, HighlightReason.ABILITY_TARGET);
			boardRow1.popLayerHighlights (targettedCards, HighlightReason.ABILITY_TARGET);
		}

		if (origin != null)
		{
			boardRow0.popHighlights(origin, HighlightReason.ABILITY_SOURCE);
			boardRow1.popHighlights(origin, HighlightReason.ABILITY_SOURCE);
		}

		
		currentDetails = null;


		zoomCard.setDetail (null);
	*/
	}


	//FOR GENERAL USE
	private void pushBoardHighlights (List<Card> targets, int reason, int replace)
	{
		boardRow0.setHighlights(targets, reason, replace);
		boardRow1.setHighlights(targets, reason, replace);
	}

	private void popBoardHighlights (List<Card> targets, int reason)
	{
		boardRow0.popHighlights(targets, reason);
		boardRow1.popHighlights(targets, reason);
	}


	//messageboxlistner
	//CMD LINE TOOLS
	public void messageAdded(GameMessageBox box, string message)
	{
		string[] strings = message.Split(null);
		if (strings.Length > 0)
		{
			string cmd = strings[0];
			if (cmd.Equals("/STACKSIZE"))
			{
				MyLogger.log("STACK SIZE IS" + model.getStack().Count);
			}
			else if(cmd.Equals("/REQUESTTYPE"))
			{
				Request request = model.request;
				MyLogger.log("REQUEST TYPE" + request.getMode());
				MyLogger.log("REQUEST NUM TARGEST " + request.getTargets().Count);
			}
			else if(cmd.Equals("/SUMMON"))
			{
				if (strings.Length > 2)
				{
					int playerNum = 0;
					int cardNum = 0;
					ViewHelper.parseInt(strings[1], out playerNum);
					ViewHelper.parseInt(strings[2], out cardNum);

					model.DEVAddCard(playerNum, cardNum);

				}
			}
		}
	}


	/*************************
	*
	*
	*
	*
		CONTROLLER NETWORK FCNS.
	*
	*
	*
	*
	**************************/


//TODO



	
	public void graveViewCardClicked(GraveViewScript script, Card c)
	{
		int index = getModel().getGrave(team).IndexOf(c);
		CmdGraveViewCardClicked(index, team);
	}

	[Command]
	public void CmdGraveViewCardClicked(int index, int team)
	{
		Card c = getModel().getGrave(team)[index];
		model.graveCardClicked (c, team);
	}

	public void requestViewCardClicked(DarkRequestViewScript script, Card c)
	{
		int index = getModel().request.getTargets().IndexOf(c);
		CmdRequestViewCardClicked(index, team);
	}

	[Command]
	private void CmdRequestViewCardClicked(int index, int team)
	{
		Card c = getModel().request.getTargets()[index];
		model.requestViewCardClicked (c, team);
	}

	//TODO FIX
	public void abilitySelected(ActiveAbility abil)
	{
		//ViewHelper.hide (abilView);
		model.selectedActiveAbility (abil);
	}


	public void cancelAbility()
	{
		//ViewHelper.hide (abilView);
		model.selectedActiveAbility (null);
	}


	public void requestConfirmButton()
	{
		MyLogger.log("SELECTED OK/YES");
		CmdRequestConfirmButton();
	}

	[Command]
	public void CmdRequestConfirmButton()
	{
		model.requestConfirmButton();
	}
	
	public void cancelButtonClicked()
	{
		MyLogger.log("SELECTED OK/CANCEL/NO");
		CmdCancelButtonClicked();
		
	}

	[Command]
	public void CmdCancelButtonClicked()
	{
		model.requestCancelButton ();
		
	}
	

	public void updatePriority()
	{
		int priority = model.getPriority();
		if (priority == team)
		{
			MyLogger.log("RESUME");
			ownTimerScript.resume();
			oppTimerScript.pause();
		}
		else
		{
			MyLogger.log("PAUSE");
			ownTimerScript.pause();
			oppTimerScript.resume();
		}
	}


	private MainBoardModel getModel()
	{
		if (model == null)
		{
			GameObject mainBoardModelObject = GameObject.FindWithTag("Model");
			MainBoardModel scr = mainBoardModelObject.GetComponent<MainBoardModel>();
			model = scr;
		}
		return model;
	}


	public void mainTimeComplete(TimerClock timeScript)
	{
		//coolio
	}
	public void reserveTimeComplete(TimerClock timeScript)
	{
		//coolio
	}
	public void allTimeComplete(TimerClock timeScript)
	{
		if (timeScript == ownTimerScript)
		{
			CmdTimerComplete(team);
		}
		else
		{
			CmdTimerComplete(team ^ 1);
		}
	}

	[Command]
	public void CmdTimerComplete(int team)
	{
		MyLogger.log("TIME COMPLETE!");
		getModel().timerComplete(team);
	}

	public void requestConcede()
	{
		CmdRequestConcede(team);
	}

	[Command]
	public void CmdRequestConcede(int team)
	{
		MyLogger.log("TIME COMPLETE!");
		getModel().requestConcede(team);
	}






	public void requestNextPhase()
	{
		if (!isLocalPlayer)
			return;
		//CmdUpdateModelTimer(team);
		List<float> timerList = new List<float>();

		if (this.team == 0)
		{
			timerList.Add(ownTimerScript.getTime());
			timerList.Add(ownTimerScript.getReserveTime());

			timerList.Add(oppTimerScript.getTime());
			timerList.Add(oppTimerScript.getReserveTime());
		}
		else
		{
			timerList.Add(oppTimerScript.getTime());
			timerList.Add(oppTimerScript.getReserveTime());

			timerList.Add(ownTimerScript.getTime());
			timerList.Add(ownTimerScript.getReserveTime());
		}
		 
		//+ the requester's team
		//How much time does the opponent have left???
		string serializedTimes = SerializationHelper.serializeFloatList(timerList);
		CmdRequestNextPhase(team, serializedTimes);
		//nextPhase ();
	}



	[Command]
	public void CmdRequestNextPhase(int team, string serializedTimes)
	{

		getModel().requestNextPhase(team, serializedTimes);
	}


	public void confirmMulligan(List<int> indices)
	{
		string serializedIndices = SerializationHelper.serializeIntList(indices);
		CmdRequestMulligan(team, serializedIndices);
	}

	[Command]
	public void CmdRequestMulligan(int team, string serializedIndices)
	{
		getModel().requestMulligan(team, serializedIndices);
	}

	public void confirmMulliganHand()
	{
		CmdRequestMulliganHand(team);
	}

	[Command]
	public void CmdRequestMulliganHand(int team)
	{
		getModel().requestMulliganHand(team);
	}

	
	public void stackResolve()
	{
		if (!isLocalPlayer)
			return;
		CmdStackResolve (team);
	}

	[Command]
	public void CmdStackResolve(int team)
	{
		getModel().stackResolve (team);
	}

	public void playerViewClicked (int playerNum)
	{
		if (!isLocalPlayer)
			return;
		CmdPlayerViewClicked (playerNum, team);
	}

	[Command]
	public void CmdPlayerViewClicked (int playerNum, int team)
	{
		if (team == 1)
			playerNum = playerNum ^ 1;
		getModel().playerViewClicked (playerNum, team);
	}
	

	
	public void boardRowClicked(int rowType, Card c)
	{
		if (!isLocalPlayer)
			return;
		if (team == 1)
			rowType = rowType ^ 1;
		List<Card> cards = model.getPlayer(rowType).getBoardCards();
		int index = cards.IndexOf (c);
		CmdBoardRowClicked (rowType, index, team);	
	}

	[Command]
	public void CmdBoardRowClicked(int rowType, int index, int team)
	{
		Card c = model.getPlayer(rowType).getBoardCards()[index];
		getModel().boardRowClicked (rowType, c, team);
		
	}

	public void shouldSetDeck()
	{
		if (isLocalPlayer)
		{
			MyLogger.log("I AM LOCAL PLAYER");
			GameObject gameOptionsObject = GameObject.FindWithTag("GameOptions");
			Dropdown[] dropDowns = gameOptionsObject.GetComponentsInChildren<Dropdown>();
			foreach (Dropdown dropDown in dropDowns)
			{
				if (dropDown.name.Equals("DeckDropDown"))
				{
					int deckIndex = dropDown.value;
					MyLogger.log("SEND RPC TO SERVER WITH DECL IMBEr " + deckIndex);
					CmdSetDeck(this.team, deckIndex);
				}
			}
		}
		else
		{
			MyLogger.log("HOW IS THIS NOT LOCAL PLAYER");
		}
	}

	[Command]
	public void CmdSetDeck(int team, int deckNumber)
	{
		GameObject optionsServer = GameObject.FindWithTag("GameOptionsServer");
		GameOptionsServer optServerScript = optionsServer.GetComponent<GameOptionsServer>();
		optServerScript.setDeckId(team, deckNumber);
	}


	//TODO CONVERT THE OTHERS OVER TO USE CONTAINER SCIRPT
	public void cardMouseOver(Card c)
	{
		if (!isLocalPlayer)
			return;
		zoomCard.setCard(c);
	}
	
	public void cardMouseExit(Card c)
	{
		if (!isLocalPlayer)
			return;
		zoomCard.setCard(null);
	}
	//END TODO

	public void cardMouseOver(CardContainerScript scr, Card c)
	{
		if (!isLocalPlayer)
			return;
		zoomCard.setCard(c);
	}
	
	public void cardMouseExit(CardContainerScript scr, Card c)
	{
		if (!isLocalPlayer)
			return;
		zoomCard.setCard(null);
	}

	public void cardClicked(CardContainerScript scr, Card c)
	{
		MyLogger.log("WHAT THE FK");
		if (!isLocalPlayer)
			return;
			MyLogger.log("CMD CARD");
		if (scr == handRow0)
		{
			int slot = model.getPlayer(team).getHand().IndexOf(c);
			CmdHandCardClicked(slot, team);
		}
	}

	
	[Command]
	public void CmdHandCardClicked(int slot, int team)
	{
		Card c = model.getPlayer (team).getHand()[slot];
		getModel().handCardClicked (c, team);
	}
	
	
	public void clickedEmptyBoard()
	{
		if (!isLocalPlayer)
			return;
		CmdClickedEmptyBoard();
	}

	[Command]
	public void CmdClickedEmptyBoard()
	{
		getModel().clickedEmptyBoard ();
	}
		
	
	public void clickedEmptyHand()
	{
		if (!isLocalPlayer)
			return;
		CmdClickedEmptyHand ();	
	}

	[Command]
	public void CmdClickedEmptyHand()
	{
		getModel().clickedEmptyHand ();
		
	}

	public void abilityButtonClicked()
	{
		if (!isLocalPlayer)
			return;
		CmdAbilityButtonClicked(team);
	}

	[Command]
	public void CmdAbilityButtonClicked(int team)
	{
		getModel().selectActiveAbilities (team);
	}






}