﻿using UnityEngine;
using System.Collections.Generic;

public class StatusRowViewScript : MonoBehaviour {

	//List<Card> cards = new List<Card>();
	List<StatusEffect> statuses;
	List<GameObject> views = new List<GameObject>();
	List<StatusIconScript> viewsScripts;

	private static int numViews = 5; 
	void Awake ()
	{
		views = new List<GameObject> ();
		viewsScripts = new List<StatusIconScript> ();
		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("StatusIcon")) as GameObject;
			views.Add (g);
			ViewHelper.hide (g);
			g.transform.parent = transform;
			g.GetComponent<Renderer>().sortingLayerID = transform.parent.GetComponent<Renderer>().sortingLayerID;
			g.transform.GetComponent<Renderer>().sortingOrder = transform.parent.GetComponent<Renderer>().sortingOrder + 5;
			g.transform.localScale = new Vector3( 1f, 1f, 1.0f);
			StatusIconScript scr = g.GetComponent<StatusIconScript> ();
			viewsScripts.Add (scr);
		}
	}

	public void setStatuses(List<StatusEffect> objs)
	{
		statuses = objs;
		int numStatus = objs.Count;
		int savedViewsCount = views.Count;
		int i;

		Bounds bounds = this.transform.GetComponent<Renderer>().bounds;
		Vector3 min = bounds.size;
		//MyLogger.log(min.ToString());


		for (i = 0; i < numStatus; i++)
		{
			StatusIconScript script = viewsScripts[i];
			script.setStatusEffect(statuses[i]);
			GameObject g = views[i];
			ViewHelper.show (g);
			g.transform.localPosition = new Vector3(-1f + i, 0.2f,0);
		}

		for (; i < numViews; i++)
		{
			GameObject g = views[i];
			ViewHelper.hide (g);
		}
	}



}
