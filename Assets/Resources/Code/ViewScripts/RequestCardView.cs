using UnityEngine;
using System.Collections.Generic;

public class RequestCardView : MonoBehaviour {


	//SpriteRenderer baseRenderer;
	Card card;

	GameObject theCard;
	CardViewScript cardViewScript;

	SpriteRenderer teamBarSprite;
	SpriteRenderer areaTypeSprite;

	int team;
	List<SpriteRenderer> savedSprites = new List<SpriteRenderer>();
	void Awake()
	{
		theCard = Instantiate (Resources.Load ("GenericCard")) as GameObject;
		theCard.transform.parent = transform;
		theCard.transform.localPosition = new Vector3(0,0, -1);
		cardViewScript = theCard.GetComponent<CardViewScript> ();
		//baseRenderer = GetComponent<SpriteRenderer> ();

		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("TopBar"))
			{
				teamBarSprite = sprite;
				savedSprites.Add(sprite);
			}
			else if (sprite.name.Equals("AreaType"))
			{
				areaTypeSprite = sprite;
				savedSprites.Add(sprite);
			}
		}
	}
	
	public void setTeam(int team)
	{
		this.team = team;
	}
	

	
	public void setCard(Card c)
	{
		card = c;
		if (card == null)
			MyLogger.log("the fek");
		updateCard ();
	}
	
	private void updateCard()
	{
		if (card.getTeam() == team)
		{
			teamBarSprite.color = Color.green;
		}
		else
		{
			teamBarSprite.color = Color.red;
		}
		areaTypeSprite.sprite = ImageLibrary.getCardStateImage(card.getState());

		cardViewScript.setCard(card);
		//copies.Text = "x" + numCopies;
	}
	
	public void setController(CardViewScript.CardViewController h)
	{
		cardViewScript.setController(h);
	}

	public void setSortingLayerID(int id)
	{
		this.transform.GetComponent<Renderer>().sortingLayerID = id;
		
		foreach (SpriteRenderer obj in savedSprites)
		{
			obj.sortingLayerID = id;
		}

		cardViewScript.setSortingLayerID(id);
	}




/*
	public void cardClicked(Card c)
	{
		//MyLogger.log("Hand received Card clicked!");
		handController.cardClicked (card);
	}


	public void mouseOver(Card c)
	{
		//MyLogger.log("mouseOver");
		handController.mouseOver (card);
	}
	
	
	public void mouseExit(Card c)
	{
		//MyLogger.log("board mouseExit!");
		handController.mouseExit (card);
	}
	*/

	
	public void setDefaultHighlight()
	{
		cardViewScript.setDefaultHighlight();
	}

	public void popHighlights()
	{
		cardViewScript.popHighlights();
	}

	public void pushHighlight(Color c)
	{
		cardViewScript.pushHighlight(c);
	}

	public void pushDefaultHighlight()
	{
		cardViewScript.pushDefaultHighlight();
	}

	public void removeLastDefaultColor()
	{
		cardViewScript.removeLastDefaultColor();
	}

	public void removeLastColor(Color c)
	{
		cardViewScript.removeLastColor(c);
	}

	public void clearAllHighlights()
	{
		cardViewScript.clearAllHighlights();
	}
}