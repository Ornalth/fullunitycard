﻿using UnityEngine;
using System.Collections.Generic;

public class CounterIconScript : MonoBehaviour {

	TypogenicText counterText = null;
	SpriteRenderer sprite = null;

	// Use this for initialization
	void Awake () {

		sprite = gameObject.GetComponent<SpriteRenderer>();
		counterText = GetComponentInChildren<TypogenicText> ();

	}
	

	public void setCounter (Counter eff)
	{

		sprite.sprite = ImageLibrary.getCounterTokenImage(eff);
		counterText.Text = "" + eff.getCount();

	}

}
