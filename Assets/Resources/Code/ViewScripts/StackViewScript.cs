using UnityEngine;
using System.Collections.Generic;

public class StackViewScript : MonoBehaviour, StackDetailViewScript.StackSelectController {
	List<StackDetails> stack = new List<StackDetails>();
	List<GameObject> views = new List<GameObject>();
	List<StackDetailViewScript> viewsScripts;
	MainBoardVC script;
	//SpriteRenderer bgRenderer;
	GameObject childBg;
	GameObject stackBackground;

	private static int numAbil = 5;
	private static float stackGap = 2.5f;

	private static float testNumber =1f;
	private static float baseCardHeight;
	private static float baseBGHeight;
	private static float bgScaleBufferY = 0.10f;

	void Awake()
	{


		childBg = (GameObject)(transform.Find("TheDarkenBg").gameObject);
		stackBackground = (GameObject)(transform.Find("StackViewBG").gameObject);

		SpriteRenderer childBgRenderer = childBg.GetComponent<SpriteRenderer>();
		baseBGHeight = childBgRenderer.bounds.size.y;
		//bgGrowthPerStack = 
		//bgGrowthPerStack = 0.3f;


		viewsScripts = new List<StackDetailViewScript> ();
		int savedOrder = this.gameObject.GetComponent<Renderer>().sortingOrder;
		for (int i = 0; i < numAbil; i++)
		{
			GameObject g = Instantiate (Resources.Load ("StackAbilityView")) as GameObject;

		

			Renderer[] renderers = g.GetComponentsInChildren<Renderer>();
			foreach (Renderer rendereree in renderers)
			{
				//renderer.sortingLayerID = this.transform.parent.renderer.sortingLayerID;
				rendereree.sortingOrder = rendereree.sortingOrder + i *10;
				
			}
				
			views.Add (g);
			ViewHelper.hide (g);
			g.transform.parent = stackBackground.transform;

			Vector3 pos = g.transform.position;
			pos.z = -1 - i;
			g.transform.position = pos;

			pos = g.transform.localPosition;
			g.transform.localPosition = new Vector3(pos.x, (i+1)*(-stackGap), pos.z);

			baseCardHeight = renderers[0].bounds.size.y;

			StackDetailViewScript scr = g.GetComponent<StackDetailViewScript> ();
			viewsScripts.Add (scr);
			scr.setController (this);
		}
		this.gameObject.GetComponent<Renderer>().sortingOrder = savedOrder;
/*
		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();

		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("TheDarkenBg"))
			{
				bgRenderer = sprite;
			}
		}
*/
		

	}
	// Use this for initialization
	void Start () {
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<MainBoardVC> ();
		
		//MyLogger.log("WIDTH" + width + "HEIGHT " + height);
		
	}



	public void setColliderState(bool state)
	{
		BoxCollider2D[] colliders = gameObject.GetComponentsInChildren<BoxCollider2D>();
		foreach (BoxCollider2D collider in colliders)
		{
			collider.enabled = state;
		}
	}
	

	void reorderStack()
	{
		int numStacks = stack.Count;
		
		//darkenBG
		Vector3 originalChildBG = childBg.transform.localScale;
		float gapPercent = 0.4f;
		float newBGHeight = baseCardHeight + (numStacks - 1) * gapPercent * baseCardHeight;
		float newScale = newBGHeight / baseBGHeight + bgScaleBufferY;
		childBg.transform.localScale = new Vector3(originalChildBG.x, newScale , originalChildBG.z);
		
		//
		float originalPos = childBg.transform.position.y;
		float bgMovePerStack = 1.15f;
		stackBackground.transform.localPosition = new Vector3(0f,(float)(1.55f + numStacks * bgMovePerStack),0f);

		
		int i = 0;
		int secValue = numStacks-1;
		for (; i < numStacks; i++, secValue--)
		{
			StackDetails item = stack[i];
			StackDetailViewScript scr = viewsScripts[secValue];
			scr.setDetail(item);

		}



	}
	
	public void setStack(List<StackDetails> newStack)
	{
		int i;
		for (i = 0; i < newStack.Count; i++)
		{
			GameObject view = views[i];
			ViewHelper.show (view);
		}
	
		for (; i < numAbil; i++)
		{
			GameObject view = views[i];
			ViewHelper.hide (view);
		}
		
		this.stack = newStack;
		reorderStack ();
	}


	//Delegate

	public void stackDetailClicked(StackDetails c)
	{
		MyLogger.log("Clicked a stackdetail");
	}
	public void stackDetailMouseOver(StackDetails c)
	{
		script.stackViewMouseOver(this, c);
	}
	public void stackDetailMouseExit(StackDetails c)
	{
		script.stackViewMouseExit(this,c);
	}
/*
	private bool dragging = false;
	private float distance;
	
	
	void OnMouseEnter()
	{
		//renderer.material.color = mouseOverColor;
	}
	
	void OnMouseExit()
	{
		//renderer.material.color = originalColor;
	}
	
	void OnMouseDown()
	{
		MyLogger.log("HELLO DRAGGI?NG");
		distance = Vector3.Distance(transform.position, Camera.main.transform.position);
		dragging = true;
	}
	
	void OnMouseUp()
	{
		dragging = false;
	}
	
	void Update()
	{
		if (dragging)
		{
			MyLogger.log("HELLO DRAGGI?NG DURING");
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Vector3 rayPoint = ray.GetPoint(distance);
			transform.position = rayPoint;
		}
	}
*/
}


