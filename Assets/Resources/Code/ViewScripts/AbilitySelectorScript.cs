﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilitySelectorScript : MonoBehaviour, AbilitySelectView.AbilitySelectController {

	List<ActiveAbility> abilities;
	List<GameObject> views;
	List<AbilitySelectView> viewsScripts;

	MainBoardVC script;
	TypogenicText messageText;
	// Use this for initialization

	void Awake()
	{
		views = new List<GameObject> ();
		viewsScripts = new List<AbilitySelectView> ();
		for (int i = 0; i < 5; i++)
		{
			GameObject g = Instantiate (Resources.Load ("AbilityView")) as GameObject;
			views.Add (g);
			ViewHelper.hide (g);
			g.transform.parent = transform;
			AbilitySelectView scr = g.GetComponent<AbilitySelectView> ();
			viewsScripts.Add (scr);
			scr.setController (this);
		}

		TypogenicText[] messages = gameObject.GetComponentsInChildren<TypogenicText>();

		foreach (TypogenicText message in messages)
		{
			if (message.name.Equals("Message"))
			{
				messageText = message;
			}
		}
	}

	void Start () 
	{
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<MainBoardVC> ();
	}
	
	void OnMouseDown () 
	{
		MyLogger.log("BLANK HAND");
		script.cancelAbility ();
	}

	public void setAbilities(List<ActiveAbility> abils, Card origin, string reason)
	{
		abilities = abils;
		int index = 0;

		foreach (ActiveAbility abil in abils)
		{
			GameObject g = views[index];
			ViewHelper.show (g);
			AbilitySelectView scr = viewsScripts[index];//g.GetComponent<AbilitySelectView> ();
			scr.setAbility(abil, origin);
			index++;
		}
		messageText.Text = reason;
		reorderAbilities ();
	}

	public void abilityClicked(ActiveAbility abil)
	{
		deleteAbilityView ();
		script.abilitySelected (abil);
	}

	void deleteAbilityView()
	{
		foreach (GameObject g in views)
		{
			ViewHelper.hide (g);
		}
	}

	void reorderAbilities()
	{
		float screenWidth = 16.0f;

		int count = abilities.Count;
		int divs = count * 2 + 1;
		if (count == 0)
			return;
		float divWidth = screenWidth/divs;

		
		for (int index = 0; index < count; index++)
		{
			GameObject obj = views[index];
			Transform trans = obj.transform;
			//float xPosPixel = (index * 2 + 1) * divWidth;

			float xPos = (index * 2 + 1) * divWidth - 6.5f;
			//Debug.Log("XPOS POXIEL " + xPosPixel + " XPOS " + xPos );
			trans.position = new Vector3(xPos, 0, -5);
		}
	}
}
