using UnityEngine;
using System.Collections;

public class PlayerViewScript : MonoBehaviour {

	TypogenicText nameText;
	TypogenicText numHand;
	TypogenicText numDeck;
	TypogenicText numGrave;
	//TypogenicText numMana;
	TypogenicText numGray;
	TypogenicText numRed;
	TypogenicText numBlue;
	TypogenicText numGreen;
	TypogenicText life;
	SpriteRenderer highlightRenderer;

	//GameObject parent;
	MainBoardVC script;
	public int playerNum = 0;
	Player player;

	void Awake()
	{
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("Name"))
			{
				nameText = comp;
			}
			else if (comp.name.Equals("NumHand"))
			{
				numHand = comp;
			}
			else if (comp.name.Equals("NumDeck"))
			{
				numDeck = comp;
			}
			else if (comp.name.Equals("NumGrave"))
			{
				numGrave = comp;
			}
			//else if (comp.name.Equals("NumMana"))
			//{
			//	numMana = comp;
			//}
			else if (comp.name.Equals("NumRed"))
			{
				numRed = comp;
			}
			else if (comp.name.Equals("NumGreen"))
			{
				numGreen = comp;
			}
			else if (comp.name.Equals("NumBlue"))
			{
				numBlue = comp;
			}
			else if (comp.name.Equals("NumGray"))
			{
				numGray = comp;
			}
			else if (comp.name.Equals("Life"))
			{
				life = comp;
			}
			
		}
		
		GameObject high = transform.Find("Highlight").gameObject;
		highlightRenderer = high.GetComponent<SpriteRenderer> ();
	}

	void Start()
	{
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<MainBoardVC> ();
	}

	
	void OnMouseDown () {
		
		MyLogger.log("Mouse down reoslve stack");
		//GameObject obj = 
		script.playerViewClicked (playerNum);
	}

	public void setPlayer(Player p)
	{
		//MyLogger.log("SET PLAYER" + p.getName ());
		//name.Text = p.getName ();
		numHand.Text = "" + p.getHandSize();// + " in hand";
		numDeck.Text = "" + p.getDeckSize();// + " in deck";
		numGrave.Text = "" + p.getGraveSize();// + " in grave";
		int[] rez = p.getResources ();
		/*string s = "Rez:";
		for (int i = 0; i < Constants.NUM_REZ; i++)
		{
			s = s + " " + rez[i];
		}
		numMana.Text = s;*/

		numGray.Text = "" + rez[0];
		numRed.Text = "" + rez[1];
		numGreen.Text = "" + rez[2];
		numBlue.Text = "" + rez[3];
		life.Text = "" + p.getHealth ();
		if (p.getHealth() > 30)
		{
			ViewHelper.setTextColor(life, new Color(0,0,0,1));
		}
		else
		{
			ViewHelper.setTextColor(life, new Color(1,0,0,1));
		}
		player = p;
	}

	public Player getPlayer()
	{
		return player;
	}

//TODO DO RIGHT SIDE;
	public Vector3 getCardPosition()
	{
		return transform.position;
	}
}
