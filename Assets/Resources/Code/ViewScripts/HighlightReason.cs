﻿using UnityEngine;
using System.Collections;

public class HighlightReason
{
	public static readonly int NONE = 0;
	public static readonly int ATTACK = 1;
	public static readonly int BLOCK = 2;
	public static readonly int ABILITY_SOURCE = 3;
	public static readonly int ABILITY_TARGET = 4;
	public static readonly int POP_ABILITY = 5;
	public static readonly int ABILITY_POSSIBLE_TARGET = 6;
	public static readonly int MULLIGAN = 7;

	public static Color getColorForReason(int reason)
	{
		if (reason == HighlightReason.ABILITY_SOURCE)
		{
			return Color.green;
		}
		else if (reason == HighlightReason.ABILITY_TARGET)
		{
			return Color.red;
		}
		else if (reason == HighlightReason.ATTACK)
		{
			//EHH
			return Color.magenta;
		}
		else if (reason == HighlightReason.BLOCK)
		{
			return Color.magenta;
		}
		else if (reason == HighlightReason.ABILITY_POSSIBLE_TARGET)
		{
			Color color = new Color(0.0f, 0.4f, 0.8f, 1f);
			return color;
		}
		else if (reason == POP_ABILITY)
		{
			return Color.white;
		}
		else if (reason == MULLIGAN)
		{
			return Color.red;
		}
		
		return Color.blue;
	}
}
