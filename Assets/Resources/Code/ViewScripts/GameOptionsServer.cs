using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GameOptionsServer : NetworkBehaviour {


	public int[] deckIndex = new int[2];
	public bool[] isSet = new bool[2];
	public bool started = false;
	public bool isAIGame = false;
	public bool isDraft = false;

	public void setDraft(bool isDraft)
	{
		this.isDraft = isDraft;
		RpcSetIsDraft(isDraft);
	}

	[ClientRpc]
	public void RpcSetIsDraft(bool isDraft)
	{
		this.isDraft = isDraft;
	}

	public void setDeckId(int player, int id)
	{
		if (!isServer)
		{
			MyLogger.log("CMD SET DECK");
			//CmdSetDeckId(player,id);
			RpcSetDeckId(player,id);
		}
		else
		{
			MyLogger.log("RPC SET DECK");
			RpcSetDeckId(player,id);
		}
	}

	[Command]
	public void CmdSetDeckId(int player, int id)
	{
		//if (!started)
		RpcSetDeckId(player,id);
		
	}

	[ClientRpc]
	public void RpcSetDeckId(int player, int id)
	{
		MyLogger.log("PLAYER " + player + " HAS SET DECK " + id);
		deckIndex[player] = id;
		isSet[player] = true;
		if (isAllSet())
		{
			GameObject mainBoardModelObject = GameObject.FindWithTag("Model");
			MainBoardModel model = mainBoardModelObject.GetComponent<MainBoardModel>();
			model.startGameWithDecks(deckIndex, isDraft, isAIGame);
			started = true;
			
			//NetworkServer.Destroy(this.gameObject);
		}

	}
	private bool isAllSet()
	{
		for (int i = 0; i < isSet.Length; i++)
		{	
			if (!isSet[i])
				return false;
		}
		return true;
	}
	public int getDeckId(int player, int id)
	{
		return deckIndex[player];
	}

	void Start()
	{
		if (isDraft)
		{
			MyLogger.log("IS DRAFT GAME");
			/*GameObject mainBoardModelObject = GameObject.FindWithTag("Model");
			MainBoardModel model = mainBoardModelObject.GetComponent<MainBoardModel>();
			model.startGameWithDecks(deckIndex, isDraft);
			started = true;
*/	

			deleteBuilder();
		}
		else if (!isAIGame) //&& !isDraft)
		{
			MyLogger.log("NOT AI GAME AND NOT DRAFT");
			MainBoardVC[] vcs = FindObjectsOfType(typeof(MainBoardVC)) as MainBoardVC[];
			foreach (MainBoardVC vc in vcs)
				vc.shouldSetDeck();
		}
	}

	//[ClientRpc]
	public void deleteBuilder()
	{
		GameObject deckBuilder = GameObject.FindWithTag("DeckBuildModel");
		if (deckBuilder != null)
		{
			Destroy(deckBuilder);
		}
	}
}
