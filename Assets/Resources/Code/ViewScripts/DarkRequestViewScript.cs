using UnityEngine;
using System.Collections.Generic;

public class DarkRequestViewScript : MonoBehaviour, CardViewScript.CardViewController, ScrollableAreaScript.ScrollableAreaListener {

	private static int numViews = 30;
	List<GameObject> views; 
	List<RequestCardView> viewScripts;
	MainBoardVC mainBoardVCScript;

	private static int cardsPerRow = 3;
	private static float baseCardHeight = 0f;
	private static float baseCardWidth = 0f;
	float verticalGapPerRow = 120f;
	float horizontalGapPerRow = 100f;
	float initialYBuffer = -50;

	private GameObject scrollableArea;
	private static float scrollableAreaWidth1X;
	private static float scrollableAreaHeight1X;
	private static float scrollableAreaWidthScale;
	private static float scrollableAreaHeightScale;
	ZoomCardScript zoomCardScript;
	ZoomCardScript originalZoomCardScript;
	TypogenicText messageText;

	private Vector3 originalScrollableVector3;
	List<Card> cards;
	// Use this for initialization
	void Awake () 
	{
		cards = new List<Card>();
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("Message"))
			{
				messageText = comp;
			}
			
		}

		GameObject g5 = Instantiate (Resources.Load ("ZoomCard")) as GameObject; 
		g5.name = "ZoomCard";
		g5.transform.parent = transform;
		g5.transform.localPosition = new Vector3 (0,0,0);

		zoomCardScript = g5.GetComponent<ZoomCardScript>();

		GameObject g6 = Instantiate (Resources.Load ("ZoomCard")) as GameObject; 
		g6.name = "OriginalZoomCard";
		g6.transform.parent = transform;
		g6.transform.localPosition = new Vector3 (0,0,0);

		originalZoomCardScript = g6.GetComponent<ZoomCardScript>();

		ScrollableAreaScript scrollScript = gameObject.GetComponentInChildren<ScrollableAreaScript>();
		scrollScript.setListener(this);

		views = new List<GameObject>();
		viewScripts = new List<RequestCardView>();
		float screenWidth = Screen.width;
		scrollableArea = transform.Find("ScrollableAreaBg").gameObject;
  		SpriteRenderer areaRenderer = scrollableArea.GetComponent<SpriteRenderer> ();
	
		scrollableAreaHeight1X = ViewHelper.pixelsToUnits;
		scrollableAreaWidth1X = ViewHelper.pixelsToUnits;

		ViewHelper.resizeSpriteToScreen(scrollableArea, Camera.main,(0.6666f),(0.9f));
		ViewHelper.bindLeft(scrollableArea, Camera.main);
		ViewHelper.bindBottom(scrollableArea, Camera.main);

		scrollableAreaWidthScale = scrollableArea.transform.localScale.x;
		scrollableAreaHeightScale = scrollableArea.transform.localScale.y;
		
		maxScrollTop = scrollableArea.transform.localPosition.y;
		maxScrollBottom = scrollableArea.transform.localPosition.y;
		baseMaxScrollBottom = scrollableArea.transform.localPosition.y;

//		MyLogger.log("MAXCSROLL BOTTOM = " + maxScrollBottom + " TOP " + maxScrollTop);

		GameObject scrollableArea2 = transform.Find("ScrollableArea").gameObject;
		scrollableArea2.transform.localPosition = scrollableArea.transform.localPosition;
		scrollableArea = scrollableArea2;

	

		originalScrollableVector3 =  scrollableArea.transform.localPosition;

		//Vector3 areaScale = scrollableArea.transform.localScale;
		//Vector3 invertedScale = new Vector3(1.0f/areaScale.x, 1.0f/areaScale.y, areaScale.z);

		//scrollableArea.transform.localScale = new Vector3(10f,11f, 1);
		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("RequestCardPrefab")) as GameObject;
			views.Add (g);
		
			g.transform.parent = scrollableArea.transform;
			
			
			RequestCardView script = g.GetComponent<RequestCardView> ();
			viewScripts.Add (script);
			script.setController (this);
			


		//	g.transform.localScale = invertedScale;
			if (baseCardHeight < 1f)
			{
				Renderer[] renderers = g.GetComponentsInChildren<Renderer>();
				baseCardHeight = renderers[0].bounds.size.y * ViewHelper.pixelsToUnits;
				baseCardWidth = renderers[0].bounds.size.x * ViewHelper.pixelsToUnits;
			}

			script.setSortingLayerID(this.transform.GetComponent<Renderer>().sortingLayerID);
			ViewHelper.hide (g);

		}




	}

	void Start()
	{


		zoomCardScript.setSortingLayerID(this.transform.GetComponent<Renderer>().sortingLayerID);
		Vector3 pos = zoomCardScript.gameObject.transform.localPosition;
		pos.x = 5.0f;
		pos.y = -3.0f;
		zoomCardScript.gameObject.transform.localPosition = pos;


		originalZoomCardScript.setSortingLayerID(this.transform.GetComponent<Renderer>().sortingLayerID);
		pos = originalZoomCardScript.gameObject.transform.localPosition;
		pos.x = 5.0f;
		pos.y = 2.5f;
		originalZoomCardScript.gameObject.transform.localPosition = pos;
		/*if (shouldHideButtons)
		{
			hideButtons();
		}*/
	}

	public void setTeam(int team)
	{
		foreach (RequestCardView scr in viewScripts)
			scr.setTeam(team);
	}

	bool shouldHideButtons = false;
	public void hideOKCancelButtons()
	{
		foreach (Transform child in transform)
		{
			if (child.name.Equals("OK"))
			{
				Destroy(child.gameObject);
			}
			else if (child.name.Equals ("Cancel"))
			{
				Destroy(child.gameObject);
			}
		}
		shouldHideButtons = true;
	}

	public void hideCloseButtons()
	{
		foreach (Transform child in transform)
		{
			if (child.name.Equals("Close"))
			{
				Destroy(child.gameObject);
			}
		}
		shouldHideButtons = true;
	}

	public void setMainBoardVC(MainBoardVC vc)
	{
		mainBoardVCScript = vc;
	}

	public void setCards (List<Card> cards)
	{
		setCards(cards, null);
	}

	public void setCards(List<Card> cards, StackDetails details)
	{
		if (details == null)
		{
			ViewHelper.hide(originalZoomCardScript.gameObject);
		}
		else
		{
			ViewHelper.show(originalZoomCardScript.gameObject);
			originalZoomCardScript.setDetail(details);
		}

		this.cards = cards;
		int i = 0;
		for (i = 0; i < cards.Count; i++)
		{

			GameObject g = views[i];
			ViewHelper.show (g);
		}

		for (; i < numViews; i++)
		{
			GameObject g = views[i];
			ViewHelper.hide (g);
		}

		//TODO can do better with only specific struct/creature
		reorderCards ();
	}

	public void setReason(string reason)
	{
		if (reason == null)
			reason = "Terror null reason";
		messageText.Text = reason;
	}

	void reorderCards()
	{
		int numCards = cards.Count;
		float rowHeight = (baseCardHeight + verticalGapPerRow);
		float rowWidth = baseCardWidth + horizontalGapPerRow;
		float scrollHeight = scrollableAreaHeight1X * scrollableAreaHeightScale;
		float scrollWidth = scrollableAreaHeight1X * scrollableAreaWidthScale;
	//	float scrollHeight = rowHeight * Mathf.Ceil((float)numCards / 3.0f);
//		float scrollWidth = 0.666f * Screen.width;
		
		//scrollHeight = resizeScrollView(scrollHeight);
//		MyLogger.log("SCROLL WIDTH" + scrollWidth + " " + scrollHeight);

		//Vector3 areaScale = scrollableArea.transform.localScale;
		//Vector3 invertedScale = new Vector3(1.0f/areaScale.x, 1.0f/areaScale.y, areaScale.z);
		
//xwidth == 1
		for (int i = 0; i < numCards; i++)
		{
			GameObject view = views[i];
			RequestCardView scr = viewScripts[i];
			scr.setCard(cards[i]);
			int row = i / cardsPerRow;
			int col = i % cardsPerRow;
			float xPixelPos = col * rowWidth;
			float yPixelPos = row * rowHeight;


			//float halfWidth = scrollWidth/2.0f;
			float realX = scrollWidth - xPixelPos - rowHeight/2;
			float xScale = realX/scrollWidth * scrollableAreaWidthScale - scrollableAreaWidthScale/2.0f;

			float realY = scrollHeight - yPixelPos - rowWidth/2 + initialYBuffer;
			float yScale = realY/scrollHeight * scrollableAreaHeightScale - scrollableAreaHeightScale/2.0f;
			//MyLogger.log("XPIXEL" + xPixelPos + " YPIXEL" + yPixelPos);
			//MyLogger.log("XSCALE" + xScale + " YCSLAE " + yScale);

			Vector3 loc = new Vector3(xScale, yScale, ViewHelper.GRAVE_CARD_Z_VALUE);
			view.transform.localPosition = loc;	
			maxScrollBottom = -view.transform.localPosition.y;	
		}
//		MyLogger.log("MAXSCROLLBOMOTOM1   " + maxScrollBottom + " base " + baseMaxScrollBottom + " MAX SCROLL TOP " + maxScrollTop);
		maxScrollBottom /= 2;
		//int maxRows = Mathf.ceil((float)numCards/(float)cardsPerRow);
		if (maxScrollBottom < baseMaxScrollBottom)
		{
			maxScrollBottom = baseMaxScrollBottom;
		}

		//MyLogger.log("MAXSCROLLBOMOTOM2   " + maxScrollBottom + " base " + baseMaxScrollBottom);
	}

/*
	private float resizeScrollView(float height)
	{
		int numCards = cards.Count;
		float minHeight = 0.8f * Screen.height;//getBaseHeight();
		if (height <= minHeight)
		{
			ViewHelper.resizeSpriteToScreen(scrollableArea, Camera.main,(0.6666f),(0.8f));
			ViewHelper.bindLeft(scrollableArea, Camera.main);
			ViewHelper.bindBottom(scrollableArea, Camera.main);
			return minHeight;
		}
		else
		{
			ViewHelper.resizePixelHeight(scrollableArea, Camera.main, height);
			return height;
		}
	}
*/

	/**
	 * DELEGATE
	 */

	
	public void cardClicked(Card c)
	{
		mainBoardVCScript.requestViewCardClicked(this, c);
	}

	public void mouseExit(Card c)
	{
		//mainBoardVCScript.cardMouseExit (c);
	}

	public void mouseOver(Card c)
	{
		//mainBoardVCScript.cardMouseOver (c);
		zoomCardScript.setCard(c);
	}


	private float scrollVelo = 0f;
	private float maxScrollTop;
	private float maxScrollBottom;
	private float baseMaxScrollBottom;
	public void scrolledBy(ScrollableAreaScript scr, float deltaX, float deltaY)
	{
		float goalVelo = deltaY/2000.0f;
		if (goalVelo > scrollVelo)
		{
			scrollVelo += goalVelo/10.0f;
			if (scrollVelo > goalVelo)
			{
				scrollVelo = goalVelo;
			}
		}
		else if (goalVelo < scrollVelo)
		{
			scrollVelo += goalVelo/10.0f;
			if (scrollVelo < goalVelo)
			{
				scrollVelo = goalVelo;
			}
		}
		
		MyLogger.log("SCROLL VELO IS" + scrollVelo + " GOAL IS " + goalVelo);
	}

	public void stoppedScroll(ScrollableAreaScript scr)
	{
		scrollVelo = 0f;
	}

	void Update()
	{
		if (scrollVelo == 0f)
		{
			return;
		}
		Vector3 v3 = scrollableArea.transform.localPosition;
		float oldY = v3.y;
		v3.y += scrollVelo;
		if (v3.y < maxScrollTop  )
		{
			v3.y = maxScrollTop;
		}
		else if (v3.y > maxScrollBottom)
		{
			v3.y = maxScrollBottom;
		}
		/*else if (v3.y < maxScrollBottom)
		{
			v3.y = maxScrollBottom;
		}*/
		//MyLogger.log("V3.Y " + v3.y);
		scrollableArea.transform.localPosition = v3;
	}





	////// highlights

	//HIGHLIGHTS
	public void restoreOriginalHighlights()
	{
		for (int i = 0; i < cards.Count; i++)
		{
			RequestCardView script = viewScripts[i];
			script.popHighlights (); //MAYBE POP INSTEAD?
		}

	}


	public void setHighlights (Card c, int reason, int mode)
	{
		List<Card> cards = new List<Card>();
		cards.Add(c);
		setHighlights(cards, reason, mode);
	}



	public void setHighlights (List<Card> highlights, int reason, int mode)
	{
		Color color = HighlightReason.getColorForReason(reason);

		if (mode == ReplaceMode.REPLACE)
		{
			restoreOriginalHighlights ();
			commitHighlights (highlights, color);
			//pushHighlights (highlights);
		}
		else if (mode == ReplaceMode.NONE)
		{
			commitHighlights (highlights, color);
		}
		else if (mode == ReplaceMode.FULL_LAYER)
		{
			fullLayerHighlight(highlights, color);
		}
		else
		{
			MyLogger.log("BoardRowScript -- PANIC NO REPLACEMODE EXIST");
		}
	}


	private void commitHighlights(List<Card> highlights, Color color)
	{

		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);
		
			if (index >= 0)
			{
				RequestCardView script = viewScripts[i];
				script.pushHighlight(color);
			}
		}
		


	}

	private void fullLayerHighlight(List<Card> highlights, Color color)
	{
		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				RequestCardView script = viewScripts[i];
				script.pushHighlight(color);
				//MyLogger.log("PUSHED RED OR WAHTEVER");
			}
			else
			{
				RequestCardView script = viewScripts[i];
				script.pushDefaultHighlight();
				//MyLogger.log("PUSHED HIWTe");
			}
		}
	
	}

	void setColor(RequestCardView scr, Color color)
	{
		scr.pushHighlight (color);
	}

	public void popHighlights (Card c, int reason)
	{
		List<Card> cardss = new List<Card>();
		cardss.Add(c);
		popHighlights(cardss, HighlightReason.getColorForReason(reason) );
	}


	public void popHighlights (List<Card> cards,int reason)
	{
		popHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popHighlights (List<Card> cards, Color color)
	{
		foreach (Card c in cards)
		{
			int index = cards.IndexOf(c);
			if (index != -1)
			{
				RequestCardView script = viewScripts[index];
				script.removeLastColor(color);
			}		
		}
	}

	public void popLayerHighlights (List<Card> cards,int reason)
	{
		popLayerHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popLayerHighlights (List<Card> highlights, Color color)
	{
		for (int i = 0; i < cards.Count; i++)
		{
			Card c = cards[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				RequestCardView script = viewScripts[i];
				script.removeLastColor(color);
			}
			else
			{
				RequestCardView script = viewScripts[i];
				script.removeLastDefaultColor();
			}
		}
		
		
	}

	//BOARDROW
	public void popAllHighlights(int reason)
	{
		popHighlights(cards, reason);
	}
}
