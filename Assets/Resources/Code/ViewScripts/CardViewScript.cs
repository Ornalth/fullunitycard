using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CardViewScript : MonoBehaviour {
	public interface CardViewController {
		void cardClicked(Card c);
		void mouseOver(Card c);
		void mouseExit(Card c);
	}


	
	TypogenicText title;
	TypogenicText stats;
	TypogenicText cost;
	TypogenicText sac;
	//TypogenicText desc;
	SpriteRenderer highlightRenderer;
	SpriteRenderer baseRenderer;
	SpriteRenderer dizzyRenderer;
	SpriteRenderer imageRenderer;
	SpriteRenderer setRenderer;
	Card card;
	bool selected = false;

	public bool invisible = false;

	CardViewController handController = null;
	

	private Color defaultColor;
// Use this for initialization

	List<SpriteRenderer> savedSprites;

	void Awake()
	{
		defaultColor = Color.black;
		
		//baseRenderer = GetComponent<SpriteRenderer> ();
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			//MyLogger.log(comp.name);
			//MyLogger.log(comp.text);
			if (comp.name.Equals("Name"))
			{
				title = comp;
			}
			
			else if (comp.name.Equals("Stats"))
			{
				stats = comp;
			}
			else if (comp.name.Equals("Cost"))
			{
				cost = comp;
			}
			else if (comp.name.Equals("Sac"))
			{
				sac = comp;
			}
			
		}


		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
		savedSprites = new List<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("Dizzy"))
			{
				dizzyRenderer = sprite;
				savedSprites.Add(sprite);
			}
			else if (sprite.name.Equals("Highlight"))
			{
				highlightRenderer = sprite;
				savedSprites.Add(sprite);
			}
			else if (sprite.name.Equals("Image"))
			{
				imageRenderer = sprite;
				savedSprites.Add(sprite);
			}
			else if (sprite.name.Equals("Base"))
			{
				baseRenderer = sprite;
				savedSprites.Add(sprite);
			}	
			else if (sprite.name.Equals("Set"))
			{
				setRenderer = sprite;
				savedSprites.Add(sprite);
			}	
		}


		highlightRenderer.sprite = ImageLibrary.getHighlightImage();
		setHighlight(defaultColor);
	}

	public void setSortingLayerID(int id)
	{
		this.transform.GetComponent<Renderer>().sortingLayerID = id;

		MeshRenderer[] renders =  gameObject.GetComponentsInChildren<MeshRenderer> ();

		foreach (MeshRenderer render in renders)
		{
			render.sortingLayerID = id;
			render.sortingOrder = 50;
		}
		
		foreach (SpriteRenderer obj in savedSprites)
		{
			obj.sortingLayerID = id;
		}




	}


	void hideCard()
	{
		ImageLibrary.setBaseHidden(baseRenderer);
		Renderer[] renderers = GetComponentsInChildren<Renderer> ();
		foreach (Renderer render in renderers)
		{
			render.enabled = false;
		}
		baseRenderer.enabled = true;
	}

	public void setHidden(bool hidden)
	{
		if (hidden != invisible)
		{
			invisible = hidden;
			if (invisible)
			{
				//			hide (this.gameObject);
				//hideCard();
			}
			else
			{
				ViewHelper.show(this.gameObject);
				//			show (this.gameObject);
				//updateCard ();
			}

			//updateCard();
		}
		//

	}

	void OnMouseDown () 
	{
		if (handController != null)
			handController.cardClicked (card);
	}

	public void setCard(Card c)
	{
		card = c;

		updateCard ();
	}

	private void updateCard()
	{
		if (invisible)
		{
			hideCard();
			return;
		}
		
		ImageLibrary.setBase(baseRenderer, card.getFactions());
		//ViewHelper.resolveTextSizeEllipses(title, card.getName (), ViewHelper.getGenericCardWidth() );
		title.Text = card.getName();
		if (card is CombatCard)
		{
			CombatCard c = (CombatCard) card;
			stats.Text = "" + c.getTotalAttack() + "/" + c.getTotalHealth();
			sac.Text = CardFontHelper.getCostStr(((CombatCard)card).getSalvageValue());
			//MyLogger.log("BOARD TEXT HERE" + c.getBoardText());
		}
		else
		{
			if (card is Spell)
			{

			}
			stats.Text = "";
			sac.Text = "";

		}


		dizzyRenderer.sprite = ImageLibrary.getDizzyImage(card.getDizzy().getDizzy());
		cost.Text = CardFontHelper.getCostStr(card.getCost());
		imageRenderer.sprite = ImageLibrary.getImageForCard (card);

		Rarity rarity = RarityConverter.convertRarity(card.getRarity());
		CardSet cSet = card.getCardSet();
		setRenderer.sprite = ImageLibrary.getCardSetImage(cSet, rarity);
	}

	public void setController(CardViewController h)
	{
		handController = h;
		
	}


	void OnMouseOver() {
		if (handController != null)
			handController.mouseOver (card);
	}

	void OnMouseExit() {
		if (handController != null)
			handController.mouseExit (card);
	}

	/**
	*
	*Color
	*
	*/
	List<Color> highlights = new List<Color>();
	
	private void setHighlight(Color c)
	{
		highlightRenderer.color = c;
	}

	public void setDefaultHighlight()
	{
		highlightRenderer.color = defaultColor;
	}

	public void popHighlights()
	{
		if (highlights.Count == 0)
		{
			setDefaultHighlight();
		}
		else
		{
			Color lastColor = highlights[highlights.Count - 1];
			highlights.RemoveAt (highlights.Count - 1);
			setHighlight(lastColor);
		}
	}

	public void pushHighlight(Color c)
	{
		Color color = highlightRenderer.color;
		highlights.Add(color);
		setHighlight(c);
	}

	public void pushDefaultHighlight()
	{
		pushHighlight(defaultColor);
	}

	public void removeLastDefaultColor()
	{
		removeLastColor(defaultColor);
	}

	public void removeLastColor(Color c)
	{
		if (ViewHelper.isColorEqual(highlightRenderer.color, c))
		{
			popHighlights();
		}
		else
		{
			for (int i = highlights.Count - 1; i >= 0; i--)
			{
				Color storedColor = highlights[i];
				if (ViewHelper.isColorEqual(storedColor, c))
				{
					highlights.RemoveAt(i);
					return;
				}
			}
		}
	}

	public void clearAllHighlights()
	{
		highlights.Clear();
		setDefaultHighlight();
	}


}
