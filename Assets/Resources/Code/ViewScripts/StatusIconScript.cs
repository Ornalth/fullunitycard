﻿using UnityEngine;
using System.Collections.Generic;

public class StatusIconScript : MonoBehaviour {

	TypogenicText counterText = null;
	SpriteRenderer sprite = null;

	// Use this for initialization
	void Awake () {

		
		sprite = gameObject.GetComponent<SpriteRenderer>();
		counterText = GetComponentInChildren<TypogenicText> ();


	}
	

	public void setStatusEffect (StatusEffect eff)
	{

		//MyLogger.log("WANT TO DRAW " + Status.statusToString(status) + " IS AT " +  status + "/" + statusIconLibrary.Keys.Count);
		sprite.sprite =  ImageLibrary.getStatusIcon(eff);
		if (eff.getDuration() > 0 && eff.getDuration() < StatusEffect.INFINITE)
		{
			counterText.Text = "" + eff.getDuration();
		}
		else
		{
			counterText.Text = "";
		}

	}

}
