﻿using UnityEngine;
using System.Collections;

public class AbilityButton : MonoBehaviour {

	
	//GameObject parent;
	MainBoardVC script;
	// Use this for initialization
	void Start () 
	{
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<MainBoardVC> ();
	}
	
	void OnMouseDown () {
		script.abilityButtonClicked ();
	}
}
