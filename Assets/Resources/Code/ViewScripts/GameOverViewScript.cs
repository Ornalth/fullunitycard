﻿using UnityEngine;
using System.Collections;

public class GameOverViewScript : MonoBehaviour {

	TypogenicText messageText;
	void Awake()
	{
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("MessageText"))
			{
				messageText = comp;
			}
		}
	}

	public void setMessage(string str)
	{
		messageText.Text = str;
	}
}
