using UnityEngine;
using System.Collections;

public class DynamicArrow : MonoBehaviour {

/*
	void Update()
	{
		if (Input.GetMouseButtonDown(0)) 
		{
			
			//Vector3 pos1 = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane + 0.5f);
			//pos1 = Camera.main.ScreenToWorldPoint(pos1);
			Vector3 pos1 = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
			MyLogger.log("CLCIKED AT " + pos1.ToString());
			pos1 = Camera.main.ScreenToWorldPoint(pos1);
			pos1.z = 0;
			MyLogger.log("CONVERTED TO CLICKED AT " + pos1.ToString());
			setTargetPosition(pos1);
		}
	}
	*/

	SpriteRenderer spriteRenderer;

	void Awake()
	{
		/*
		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();

		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("DynamicArrow"))
			{
				spriteRenderer = sprite;
				MyLogger.log("ARROW IS SET");
			}
		}*/
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}

	public void setColor(Color c)
	{
		spriteRenderer.color = c;
	}

	public void setTargetPosition(Vector3 newPos)
	{



		Vector3 realBeginPos = transform.position;
		realBeginPos.z = 0;
		Vector3 realNewPos = newPos;
		realNewPos.z = 0; 

		Vector2 v1 = new Vector2(1,0);
		Vector2 v2 = new Vector2(realNewPos.x - realBeginPos.x, realNewPos.y - realBeginPos.y);

		//MyLogger.log("SRC POS " + v1 + "DES POS" + v2);

		float Angle = Mathf.Atan2(v2.y,v2.x);
        float AngleInDegrees = Angle * Mathf.Rad2Deg;
       // MyLogger.log("Atan2 Angle in degrees " + AngleInDegrees);



		float angle = AngleInDegrees - 90;

		//MyLogger.log("ANGLE" + angle);

        Vector3 scale = transform.localScale;

        scale.y = v2.magnitude;
        transform.localScale = scale;



		transform.localEulerAngles = new Vector3(0 ,0, angle);
		
	}
	
}