using UnityEngine;
using System.Collections.Generic;

public class StackDetailViewScript : MonoBehaviour {

	public interface StackSelectController {
		void stackDetailClicked(StackDetails c);
		void stackDetailMouseOver(StackDetails c);
		void stackDetailMouseExit(StackDetails c);
	}
	
	
	TypogenicText desc;
	TypogenicText nameText;
	SpriteRenderer highlightRenderer;
	SpriteRenderer baseRenderer;
	SpriteRenderer imageRenderer;
	SpriteRenderer speedRenderer;
	bool selected = false;
	StackSelectController controller;
	
	private Color defaultColor;


	StackDetails detail;

	void Awake () {
		
		
		defaultColor = Color.white;
		
		//card = CardLibrary.getRandomCard ();
		//MyLogger.log("Card Type " + CardType.convertToString (card.getCardType ()) + card.getName ());
		
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("Desc"))
			{
				desc = comp;
			}
			else if (comp.name.Equals("Name"))
			{
				nameText = comp;
			}
			
		}
		GameObject high = transform.Find("Highlight").gameObject;
		highlightRenderer = high.GetComponent<SpriteRenderer> ();

		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();

		foreach (SpriteRenderer renderer in sprites)
		{
			if (renderer.name.Equals("Base"))
			{
				baseRenderer = renderer;
			}	
			else if (renderer.name.Equals("Image"))
			{
				imageRenderer = renderer;
			}
			else if (renderer.name.Equals("Speed"))
			{
				speedRenderer = renderer;
			}
		}


		//highlightRenderer.color = Color.green;
		
		//title.Text = card.getName ();
		
	}

	public void OnMouseDown () {
		
//		MyLogger.log("Mouse down on ability");
		if (controller != null)
		{
			controller.stackDetailClicked(this.detail);
		}
		
	}

	public void OnMouseOver()
	{
//		MyLogger.log("Mouse down on ability");
		//MyLogger.log("mouseOver");
		controller.stackDetailMouseOver (detail);
	}
	
	
	public void OnMouseExit()
	{
//			MyLogger.log("Mouse exit on ability");
		//MyLogger.log("board mouseExit!");
		controller.stackDetailMouseExit (detail);
	}
	

	public void setController(StackSelectController h)
	{
		controller = h;
		
	}
	
	public void setHighlight(Color c)
	{
		highlightRenderer.color = c;
	}
	public void setDefaultHighlight()
	{
		highlightRenderer.color = defaultColor;
	}
	

	
	public void setDetail(StackDetails detail)
	{
			/*
		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer render in renderers)
		{

			MyLogger.log("HELLO sorting");
			//renderer.sortingLayerID = this.transform.parent.renderer.sortingLayerID;
			renderer.sortingOrder = 123;
		}*/
		this.detail = detail;
		nameText.Text = detail.origin.getName();
		imageRenderer.sprite = ImageLibrary.getImageForCard (detail.origin);
		desc.Text = detail.ev.getDesc();
		ImageLibrary.setBase(baseRenderer, detail.origin.getFactions());
		
		speedRenderer.sprite = ImageLibrary.getAbilitySpeedImage(detail.ev.getAbilitySpeed());
	}

}