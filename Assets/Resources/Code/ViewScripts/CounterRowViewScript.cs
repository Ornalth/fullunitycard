﻿using UnityEngine;
using System.Collections.Generic;

public class CounterRowViewScript : MonoBehaviour {

	//List<Card> cards = new List<Card>();
	List<Counter> counters;
	List<GameObject> views = new List<GameObject>();
	List<CounterIconScript> viewsScripts;

	private static int numViews = 5; 
	void Awake ()
	{
		views = new List<GameObject> ();
		viewsScripts = new List<CounterIconScript> ();
		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("CounterIcon")) as GameObject;
			views.Add (g);
			ViewHelper.hide (g);
			g.transform.parent = transform;
			g.GetComponent<Renderer>().sortingLayerID = transform.parent.GetComponent<Renderer>().sortingLayerID;
			g.transform.GetComponent<Renderer>().sortingOrder = transform.parent.GetComponent<Renderer>().sortingOrder + 5;
			g.transform.localScale = new Vector3( 1f, 1f, 1.0f);
			CounterIconScript scr = g.GetComponent<CounterIconScript> ();
			viewsScripts.Add (scr);
		}
	}

	public void setCounters(List<Counter> objs)
	{
		counters = objs;
		int numCounter = objs.Count;
		int savedViewsCount = views.Count;
		int i;
		//MyLogger.log(min.ToString());


		for (i = 0; i < numCounter; i++)
		{
			CounterIconScript script = viewsScripts[i];
			script.setCounter(counters[i]);
			GameObject g = views[i];
			ViewHelper.show (g);
			g.transform.localPosition = new Vector3(-1f + i, 0.2f,0);
		}

		for (; i < numViews; i++)
		{
			GameObject g = views[i];
			ViewHelper.hide (g);
		}
	}



}
