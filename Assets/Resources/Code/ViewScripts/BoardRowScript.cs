using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using Holoville.HOTween; 
using Holoville.HOTween.Plugins;

public class BoardRowScript : MonoBehaviour, CardViewScript.CardViewController {
	
	List<Card> creatures = new List<Card>();
	List<Card> structures = new List<Card>();
	List<GameObject> creatureViews = new List<GameObject>();
	List<GameObject> structureViews = new List<GameObject>();
	List<CardViewScript> creatureViewsScripts;
	List<CardViewScript> structureViewsScripts;
	//public bool needsUpdate = false;
	float cardWidth = -1;
	float cardHeight = -1;
	static float gapMultiplier = 1.3f; 
	MainBoardVC script;

	public int rowType = 0;
	private static float HARD_CODED_BUFFER = 1.5f;
	private static float HEIGHT_BUFFER = 0.1f;
	private float beginX;
	private Color[] combatColors;
	private Color selectedColor = Color.green;
	private static int numViews = 5;

	//TODO MAYBE NEW SET HIGHLIGHTS
	/*
	public void setHighlights(List<Card>highlights, int highlightReason)
{
	
}
*/

	void Awake()
	{
		creatureViewsScripts = new List<CardViewScript> ();
		structureViewsScripts = new List<CardViewScript> ();



		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("GenericCard")) as GameObject;
			creatureViews.Add (g);
		
			g.transform.parent = transform;
			CardViewScript script = g.GetComponent<CardViewScript> ();
			creatureViewsScripts.Add (script);
			script.setController (this);

			ViewHelper.hide (g);
		}

		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("GenericCard")) as GameObject;
			structureViews.Add (g);
	
			g.transform.parent = transform;
			CardViewScript script = g.GetComponent<CardViewScript> ();
			structureViewsScripts.Add (script);
			script.setController (this);

			ViewHelper.hide (g);
		}

	}
	// Use this for initialization
	void Start () 
	{
		SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
		beginX = renderer.bounds.size.x / 2 - HARD_CODED_BUFFER;
		//MyLogger.log("BEGIN X " + beginX);
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<MainBoardVC> ();

		combatColors = new Color[7];
		combatColors [0] = Color.gray;
		combatColors [1] = Color.black;
		combatColors [2] = Color.cyan;
		combatColors [3] = Color.white;
	}

	void reorderCards()
	{
		int numCards = creatures.Count;
		//MyLogger.log("MAN" + cardWidth +" SMGFG" +  cardHeight + creatures.Count + structures.Count);

		for (int i = 0; i < numCards; i++)
		{
			GameObject view = creatureViews[i];

			if (cardWidth < 0 || cardHeight < 0) 
			{
				//MyLogger.log("HELLO");
				SpriteRenderer renderer = view.GetComponent<SpriteRenderer> ();
				cardWidth = renderer.bounds.size.x * gapMultiplier; //
				cardHeight= renderer.bounds.size.y;

			}

			//If odd, we shift over some.
			float xBuffer =  cardWidth / 2;
			float yPos = (0 == rowType ? -cardHeight/2 - HEIGHT_BUFFER : cardHeight/2 + HEIGHT_BUFFER );
			float xPos = ((float)(-((float)numCards)/2.0 + i)) * cardWidth + xBuffer;// + xBuffer;				//i*cardWidth - beginX;
			Transform trans = view.transform;

			//trans.localPosition = new Vector3(xPos, yPos, -1f);
			Vector3 newPosition = new Vector3(xPos, yPos, -1f);
			if (!ViewHelper.V3Equals(trans.localPosition, newPosition))
			{
				if (Constants.SHOULD_ANIMATE)
				{
					HOTween.To(trans, 1, "localPosition", newPosition);
				}
				else
				{
					trans.localPosition = newPosition;
				}
			}
			CardViewScript scr = creatureViewsScripts[i];
			scr.setCard(creatures[i]);
		}

		numCards = structures.Count;
		
		for (int i = 0; i < numCards; i++)
		{
			GameObject view = structureViews[i];

			if (cardWidth < 0 || cardHeight < 0) 
			{
				//MyLogger.log("HELLO");
				SpriteRenderer renderer = view.GetComponent<SpriteRenderer> ();
				cardWidth = renderer.bounds.size.x * gapMultiplier; //
				cardHeight= renderer.bounds.size.y;

			}

			//Slightly move over.
			float xBuffer =  cardWidth / 2;
			float yPos = (1 == rowType ? -cardHeight/2 - HEIGHT_BUFFER  : cardHeight/2 + HEIGHT_BUFFER );
			float xPos = ((float)(-((float)numCards)/2.0 + i)) * cardWidth + xBuffer;
			Transform trans = view.transform;
			//trans.localPosition = new Vector3(xPos, yPos, -1f);
			Vector3 newPosition = new Vector3(xPos, yPos, -1f);
			if (!ViewHelper.V3Equals(trans.localPosition, newPosition))
			{
				if (Constants.SHOULD_ANIMATE)
				{
					HOTween.To(trans, 1, "localPosition", newPosition);
				}
				else
				{
					trans.localPosition = newPosition;
				}
			}
			CardViewScript scr = structureViewsScripts[i];
			scr.setCard(structures[i]);
		}
	}


	public void setCreatures(List<Card> c)
	{
		creatures = c;
		setCards (creatures, creatureViews, creatureViewsScripts);
	}

	public void setStructures(List<Card> c)
	{
		structures = c;
		setCards (structures, structureViews, structureViewsScripts);
	}

	void setCards(List<Card> cards, List<GameObject> views, List<CardViewScript> scrs)
	{

		int i = 0;
		for (i = 0; i < cards.Count; i++)
		{

			//Card c = cards[i];
			GameObject g = views[i];
		
			//CardViewScript script = scrs[i];
			//script.setCard (c);
			//MyLogger.log("SHOW FKER");
			ViewHelper.show (g);
			/*
			if (cardWidth < 0 || cardHeight < 0) 
			{
				SpriteRenderer renderer = g.GetComponent<SpriteRenderer> ();
				cardWidth = renderer.bounds.size.x * gapMultiplier; //
				cardHeight= renderer.bounds.size.y;
			}*/
		}

		for (; i < numViews; i++)
		{
			GameObject g = views[i];
			ViewHelper.hide (g);
		}

		//TODO can do better with only specific struct/creature
		reorderCards ();
	}

	//HIGHLIGHTS
	public void restoreOriginalHighlights()
	{
		for (int i = 0; i < creatures.Count; i++)
		{
			CardViewScript script = creatureViewsScripts[i];
			script.popHighlights (); //MAYBE POP INSTEAD?
		}
		//TODO
		
		for (int i = 0; i < structures.Count; i++)
		{
			CardViewScript script = structureViewsScripts[i];
			script.popHighlights (); //MAYBE POP INSTEAD?
		}
	}


	public void setHighlights (Card c, int reason, int mode)
	{
		List<Card> cards = new List<Card>();
		cards.Add(c);
		setHighlights(cards, reason, mode);
	}



	public void setHighlights (List<Card> highlights, int reason, int mode)
	{
		Color color = HighlightReason.getColorForReason(reason);

		if (mode == ReplaceMode.REPLACE)
		{
			restoreOriginalHighlights ();
			commitHighlights (highlights, color);
			//pushHighlights (highlights);
		}
		else if (mode == ReplaceMode.NONE)
		{
			commitHighlights (highlights, color);
		}
		else if (mode == ReplaceMode.FULL_LAYER)
		{
			fullLayerHighlight(highlights, color);
		}
		else
		{
			MyLogger.log("BoardRowScript -- PANIC NO REPLACEMODE EXIST");
		}
	}


	private void commitHighlights(List<Card> highlights, Color color)
	{

		for (int i = 0; i < creatures.Count; i++)
		{
			Card c = creatures[i];
			int index = highlights.IndexOf(c);
		
			if (index >= 0)
			{
				CardViewScript script = creatureViewsScripts[i];
				script.pushHighlight(color);
			}
		}
		
		for (int i = 0; i < structures.Count; i++)
		{
			
			Card c = structures[i];
			int index = highlights.IndexOf(c);
			if (index >= 0)
			{
				CardViewScript script = structureViewsScripts[i];
				script.pushHighlight(color);
			}
		}

	}

	private void fullLayerHighlight(List<Card> highlights, Color color)
	{
		for (int i = 0; i < creatures.Count; i++)
		{
			Card c = creatures[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				CardViewScript script = creatureViewsScripts[i];
				script.pushHighlight(color);
				//MyLogger.log("PUSHED RED OR WAHTEVER");
			}
			else
			{
				CardViewScript script = creatureViewsScripts[i];
				script.pushDefaultHighlight();
				//MyLogger.log("PUSHED HIWTe");
			}
		}
		
		for (int i = 0; i < structures.Count; i++)
		{
			
			Card c = structures[i];
			int index = highlights.IndexOf(c);
			if (index >= 0)
			{
				CardViewScript script = structureViewsScripts[i];
				script.pushHighlight(color);
			}
			else
			{
				CardViewScript script = structureViewsScripts[i];
				script.pushDefaultHighlight();
			}
		}
	}

	void setColor(CardViewScript scr, Color color)
	{
		scr.pushHighlight (color);
	}

	public void popHighlights (Card c, int reason)
	{
		List<Card> cards = new List<Card>();
		cards.Add(c);
		popHighlights(cards, HighlightReason.getColorForReason(reason) );
	}


	public void popHighlights (List<Card> cards,int reason)
	{
		popHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popHighlights (List<Card> cards, Color color)
	{
		foreach (Card c in cards)
		{
			int index = creatures.IndexOf(c);
			if (index != -1)
			{
				CardViewScript script = creatureViewsScripts[index];
				script.removeLastColor(color);
			}	
			else 
			{
				index = structures.IndexOf(c);
				if (index != -1)
				{
					CardViewScript script = structureViewsScripts[index];
					script.removeLastColor(color);
				}
			}		
		}
	}

	public void popLayerHighlights (List<Card> cards,int reason)
	{
		popLayerHighlights(cards, HighlightReason.getColorForReason(reason) );
	}

	public void popLayerHighlights (List<Card> highlights, Color color)
	{
		for (int i = 0; i < creatures.Count; i++)
		{
			Card c = creatures[i];
			int index = highlights.IndexOf(c);

			if (index >= 0)
			{
				CardViewScript script = creatureViewsScripts[i];
				script.removeLastColor(color);
			}
			else
			{
				CardViewScript script = creatureViewsScripts[i];
				script.removeLastDefaultColor();
			}
		}
		
		for (int i = 0; i < structures.Count; i++)
		{
			
			Card c = structures[i];
			int index = highlights.IndexOf(c);
			if (index >= 0)
			{
				CardViewScript script = structureViewsScripts[i];
				script.removeLastColor(color);
			}
			else
			{
				CardViewScript script = structureViewsScripts[i];
				script.removeLastDefaultColor();
			}
		}
	}

	//BOARDROW
	public void popAllHighlights(int reason)
	{
		List<Card> all = new List<Card>();
		all.AddRange (creatures);
		all.AddRange (structures);
		popHighlights(all, reason);
	}

	/**
	 * DELEGATE
	 */

	
	public void cardClicked(Card c)
	{
		//MyLogger.log("board received Card clicked!");
		script.boardRowClicked (rowType, c);
	}

	public void mouseExit(Card c)
	{
		//MyLogger.log("board mouseExit!");
		script.cardMouseExit (c);
	}

	public void mouseOver(Card c)
	{
		//MyLogger.log("mouseOver");
		script.cardMouseOver (c);
	}



	void OnMouseDown () {
		//MyLogger.log("BLANK BOARD" + (creatureViews.Count + structureViews.Count));
		script.clickedEmptyBoard ();
		
	}


	//TODO currently returns center.
	public Vector3 getCardBottomPosition(Card card)
	{
		int index = creatures.IndexOf(card);
		if ( index != -1)
		{
			return creatureViewsScripts[index].transform.position;
		}
		else
		{
			index = structures.IndexOf(card);
			if (index != -1)
			{
				return  structureViewsScripts[index].transform.position;
			}
		}
		return Vector3.zero;
	}

	//TODO currently returns center.
	public Vector3 getCardTopPosition(Card card)
	{
		int index = creatures.IndexOf(card);
		if ( index != -1)
		{
			return creatureViewsScripts[index].transform.position;
		}
		else
		{
			index = structures.IndexOf(card);
			if (index != -1)
			{
				return  structureViewsScripts[index].transform.position;
			}
		}
		return Vector3.zero;
	}

	public void clearAllHighlights(int reason)
	{

		Color color = HighlightReason.getColorForReason(reason);
		for (int i = 0; i < creatures.Count; i++)
		{
			CardViewScript script = creatureViewsScripts[i];
			script.removeLastColor (color); //MAYBE POP INSTEAD?
		}

		//TODO
		for (int i = 0; i < structures.Count; i++)
		{
			CardViewScript script = structureViewsScripts[i];
			script.removeLastColor (color); //MAYBE POP INSTEAD?
		}

	}


}