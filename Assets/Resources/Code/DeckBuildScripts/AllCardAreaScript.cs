using UnityEngine;
using System.Collections.Generic;

public class AllCardAreaScript : MonoBehaviour, DeckCardViewScript.DeckCardViewController {

	SortedDictionary<int,int> cardDict;
	//List<Card> cards = new List<Card>();
	List<GameObject> views;
	List<DeckCardViewScript> viewsScripts;
	
	
	float parentSizeWidthPixel ;//= renderer.bounds.size.x;
	float parentSizeHeightPixel;//= renderer.bounds.size.y;
	float cardWidth = -1;
	float cardHeight = -1;
	IDeckAreaController script;

	
	private static int numViews = 0;
	int pageNumber = 0;

	TypogenicText pageNumText;
	
	void Awake()
	{
		
		pageNumText = gameObject.GetComponentInChildren<TypogenicText> ();
		setNumViews(8);
		
	}
	void Start()
	{
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<IDeckAreaController> ();
		
	}
	public void setHeight(float f)
	{
		parentSizeHeightPixel = f;
	}
	public void setWidth(float f)
	{
		parentSizeWidthPixel = f;
	}

	public void setNumViews(int nViews)
	{
		views = new List<GameObject>();
		viewsScripts = new List<DeckCardViewScript> ();
		numViews = nViews;

		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("GenericCardDeck")) as GameObject;
			views.Add (g);
			
			g.transform.parent = transform;
			DeckCardViewScript script = g.GetComponent<DeckCardViewScript> ();
			viewsScripts.Add (script);
			script.setController (this);
			if (cardWidth < 0 || cardHeight < 0) 
			{
				Renderer[] renderers = g.GetComponentsInChildren<Renderer>();
				cardHeight = renderers[0].bounds.size.y * ViewHelper.pixelsToUnits;
				cardWidth = renderers[0].bounds.size.x * ViewHelper.pixelsToUnits;
			}
			
			ViewHelper.hide (g);
		}
	}
	
	void reorderCards()
	{
		List<int> keys = new List<int>(cardDict.Keys);
		int numCards = keys.Count;
		int i;
		int numViewsPerRow = numViews/2;
		float sizeScaleWidth = parentSizeWidthPixel/ViewHelper.pixelsToUnits;
		float sizeScaleHeight = parentSizeHeightPixel/ViewHelper.pixelsToUnits;
		
		//int perRow = numViews/2;
		float widthBuffer = (parentSizeWidthPixel - (cardWidth * numViewsPerRow))/(numViewsPerRow+1);

		float sizePerCard = cardWidth + widthBuffer;
	//	MyLogger.log("SIZE PER CARD" + sizePerCard + " "  + widthBuffer);
		float sizeTransformPerCard = sizePerCard/ViewHelper.pixelsToUnits;
		float beginTransformX = -sizeScaleWidth/2;

		
		//float lrBuffer = (sizeScaleWidth - numViewsPerRow * sizeTransformPerCard)/2;
		//if (lrBuffer < 0)
		//{
		//	lrBuffer = 0;
		//}
		float lrBuffer = (widthBuffer + cardWidth/2) / ViewHelper.pixelsToUnits;


		//float scalePerWidth = sizeScaleWidth/perRow;

		for (i = 0; i < numCards && i < numViewsPerRow; i++)
		{
			GameObject view = views[i];
			Transform trans = view.transform;
			
			
			//trans.localPosition = new Vector3((float)((i-2.0f)*(cardWidth + widthBuffer)), 2f, transform.position.z);
			trans.localPosition = new Vector3((float)(beginTransformX + sizeTransformPerCard * i + lrBuffer), 2f, transform.position.z);
			DeckCardViewScript scr = viewsScripts[i];
			//scr.setHidden(shouldHide);
			Card c = CardLibrary.getCardFromID(keys[i]);
			scr.setCard(c, cardDict[keys[i]]);
			
		}

		for (; i < numCards && i < numViews; i++)
		{
			GameObject view = views[i];
			Transform trans = view.transform;
			trans.localPosition = new Vector3((float)(beginTransformX + sizeTransformPerCard * (i - numViewsPerRow)  + lrBuffer), -0.5f, transform.position.z);
			//trans.localPosition = new Vector3((float)((i-2.0f - numViews/2)*(cardWidth + widthBuffer)), -0.5f, transform.position.z);
			DeckCardViewScript scr = viewsScripts[i];
			//scr.setHidden(shouldHide);
			Card c = CardLibrary.getCardFromID(keys[i]);
			scr.setCard(c, cardDict[keys[i]]);
			
		}
	}
	
	//DATA SOURCE
	public void setCards(SortedDictionary<int, int> newCardDict)
	{
		cardDict = newCardDict;
		int i = 0;
		for (i = 0; i < cardDict.Keys.Count; i++)
		{
			
			//Card c = cards[i];
			GameObject g = views[i];
			//CardViewScript script = viewsScripts[i];
			//script.setCard (c);
			ViewHelper.show (g);
			
		}
		
		for (; i < numViews; i++)
		{
			GameObject g = views[i];
			ViewHelper.hide (g);
		}
		reorderCards ();
	}

	public void setPage(int curPage, int maxPage)
	{
		pageNumText.Text = "" + (curPage+1) + "/" + maxPage;
		pageNumber = curPage;
	}

	
	public void cardClicked(Card c)
	{
		//MyLogger.log("Hand received Card clicked!");
		script.cardClicked (this, c);
	}

	public int getPage()
	{
		return pageNumber;
	}

	public int getNumCardsPerPage()
	{
		return 8;
	}
	
	
	public void mouseOver(Card c)
	{
		//MyLogger.log("mouseOver");
		//script.cardMouseOver (c);
	}
	
	
	public void mouseExit(Card c)
	{
		//MyLogger.log("board mouseExit!");
		//script.cardMouseExit (c);
	}
	
	void OnMouseDown () {
		MyLogger.log("BLANK HAND");
		//script.clickedEmptyHand ();
		
	}

}
