using UnityEngine;
using System.Collections;

public class PrevDeckAreaScript : MonoBehaviour {

	IDeckAreaController script;
	// Use this for initialization
	void Start () {
		GameObject parent = transform.parent.gameObject;
		script = parent.GetComponent<IDeckAreaController> ();
	}
	
	void OnMouseDown () {
		
		
		//MyLogger.log("Mouse down next phasde");
		//GameObject obj = 
		script.onPrevDeckButtonClicked ();
	}
}
