using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class DeckListObj : MonoBehaviour, ISpriteButtonListener {
	public interface DeckListController {
		void deckClicked(DeckInfo deck);
	}


	DeckInfo deck;
	TypogenicText deckNameText;
	TypogenicText numCardsText;

	SpriteRenderer imageRenderer;
	SpriteRenderer bgRenderer;
	DeckListController controller;
	

	void Awake()
	{
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("DeckName"))
			{
				deckNameText = comp;
			}
			
			else if (comp.name.Equals("DeckNumCards"))
			{
				numCardsText = comp;
			}
			
		}


		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("DeckImage"))
			{
				imageRenderer = sprite;
			}	
			else if (sprite.name.Equals("DeckBg"))
			{
				bgRenderer = sprite;
			}
		}
		SpriteButtonScript[] buttons = GetComponentsInChildren<SpriteButtonScript>();
		foreach (SpriteButtonScript button in buttons)
		{
			button.addListener(this);
		}
	}

	public void setBGScaleWidth(float width)
	{
		Vector3 bgScale = bgRenderer.transform.localScale;
		bgScale.x = width;
		bgRenderer.transform.localScale = bgScale;
	}

	public void setDeckInfo(DeckInfo deck)
	{
		this.deck = deck;
		updateViews();
	}

	private void updateViews()
	{
		int numCards = deck.getNumCards();
		int factions = deck.getMostCommonFaction();
			//TODO SET TO RGB BASED ON MAX GENERATORS? OR MAX COLOR CARDS
		if (Faction.hasRed(factions))
		{
			bgRenderer.color = Color.red;
		}
		else if (Faction.hasBlue(factions))
		{
			bgRenderer.color = Color.blue;
		}
		else if (Faction.hasGreen(factions))
		{
			bgRenderer.color = Color.green;
		}
		else if (Faction.hasColourless(factions))
		{
			bgRenderer.color = Color.gray;
		}

		

		numCardsText.Text = "" + numCards;
		deckNameText.Text = deck.getName();
		imageRenderer.sprite = ImageLibrary.getImageForCard (9999);
	}

	public void setController(DeckListController h)
	{
		//MyLogger.log("DECK LIST CONTROLLER SET!");
		controller = h;
		
	}

	public void buttonClicked(SpriteButtonScript scr)
	{
		MyLogger.log("HELLO THAR CLICKE DECK!");
		if (controller != null)
			controller.deckClicked (deck);
	}
}
