﻿using UnityEngine;
using System.Collections;


public interface IDeckAreaController
{
	void onNextOwnedButtonClicked();
	void onNextDeckButtonClicked();
	void onPrevDeckButtonClicked();
	void onPrevOwnedButtonClicked();
	void cardClicked(AllCardAreaScript scr, Card c);
}