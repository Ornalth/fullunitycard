using UnityEngine;
using System.Collections.Generic;

public class DeckCardViewScript : MonoBehaviour, CardViewScript.CardViewController {

	public interface DeckCardViewController {
		void cardClicked(Card c);
		void mouseOver(Card c);
		void mouseExit(Card c);
	}
	

	TypogenicText copies;

	SpriteRenderer baseRenderer;
	Card card;

	DeckCardViewController handController = null;
	
	int numCopies;

	GameObject theCard;
	CardViewScript cardViewScript;
	
	void Awake()
	{
		theCard = Instantiate (Resources.Load ("GenericCard")) as GameObject;
		theCard.transform.parent = transform;
		theCard.transform.localPosition = new Vector3(0,0, -1);
		cardViewScript = theCard.GetComponent<CardViewScript> ();
		cardViewScript.setController (this);

		baseRenderer = GetComponent<SpriteRenderer> ();
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("Copies"))
			{
				copies = comp;
			}
		}	
	}
	

	

	
	public void setCard(Card c, int numCopies)
	{
		card = c;
		this.numCopies = numCopies;
		updateCard ();
	}
	
	private void updateCard()
	{
		cardViewScript.setCard(card);
		copies.Text = "x" + numCopies;
	}
	
	public void setController(DeckCardViewController h)
	{
		handController = h;
		
	}





	public void cardClicked(Card c)
	{
		//MyLogger.log("Hand received Card clicked!");
		handController.cardClicked (card);
	}


	public void mouseOver(Card c)
	{
		//MyLogger.log("mouseOver");
		handController.mouseOver (card);
	}
	
	
	public void mouseExit(Card c)
	{
		//MyLogger.log("board mouseExit!");
		handController.mouseExit (card);
	}

	

}