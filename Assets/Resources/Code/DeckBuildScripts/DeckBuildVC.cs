using UnityEngine;
using System.Collections.Generic;

public class DeckBuildVC : MonoBehaviour, DeckListObj.DeckListController, IDeckAreaController {
	
	public interface DeckBuilderListener
	{
		void deckSaved();
	}
	//Card ID, Num owned.
	DeckInfo currentDeckInfo;// = new DeckInfo();
	SortedDictionary<int, int> baseOwnedCards = new SortedDictionary<int, int>();
	SortedDictionary<int, int> ownedCards = new SortedDictionary<int, int>();
	SortedDictionary<int, int> currentDeck = new SortedDictionary<int, int>();

	AllCardAreaScript ownedAreaScr;
	AllCardAreaScript deckAreaScr;
	int currentDeckNum = 0;

	TypogenicText deckTitleText;

	List<GameObject> views = new List<GameObject>();
	List<DeckListObj> viewsScripts = new List<DeckListObj>();
	GameObject sideArea;
	int numViews = 9;

	BaseDeckParser deckParser;
	BaseDeckParser ownedCardsParser;

	//List<CardViewScript> viewsScripts;

	bool isDraft = false;
	int draftTeam = -1;
	List<DeckBuilderListener> listeners = new List<DeckBuilderListener>();

	public void subscribe(DeckBuilderListener listener)
	{
		MyLogger.log("SUBSCRIBE TO DECKBUILDVC");
		listeners.Add(listener);
	}

	void Start()
	{
		initViews();
		init();
		updateAll (0);
	}

	void initViews()
	{
		GameObject diz = transform.Find("AllCardsArea").gameObject;
		ownedAreaScr = diz.GetComponent<AllCardAreaScript> ();
		ownedAreaScr.setHeight(ViewHelper.getScreenHeight() - 100);
		ownedAreaScr.setWidth(ViewHelper.getScreenWidth() - 100);
		GameObject high = transform.Find("CurrentDeckArea").gameObject;
		deckAreaScr =  high.GetComponent<AllCardAreaScript> ();
		deckAreaScr.setHeight(ViewHelper.getScreenHeight() - 100);
		deckAreaScr.setWidth(ViewHelper.getScreenWidth() - 100);

		sideArea = transform.Find("emptyListArea").gameObject;
		SpriteRenderer deckListSprite = sideArea.GetComponent<SpriteRenderer>();

		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("DeckNameTitle"))
			{
				deckTitleText = comp;
			}
		}

		for (int i = 0; i < numViews; i++)
		{
			GameObject g = Instantiate (Resources.Load ("DeckListObjPrefab")) as GameObject;
			views.Add (g);

			
			g.transform.parent = sideArea.transform;
			DeckListObj script = g.GetComponentInChildren<DeckListObj> ();
			viewsScripts.Add(script);
			
			script.setController (this);
			
		
			g.transform.localPosition = new Vector3(0f, 2f - i * 1.3f, -1f);
			script.setBGScaleWidth(deckListSprite.bounds.size.x);
			ViewHelper.hide (g);
		}
	}
	protected virtual void init()
	{
		checkIsDraft();
		updateAllDecks();
		retrieveOwnedCards ();
		retrieveCurrentDeck ();
	}

	private void checkIsDraft()
	{
		GameObject optionsObj = GameObject.FindWithTag("DeckViewOptions");
		if (optionsObj == null || !optionsObj.GetComponent<DeckViewOptions>().isDraft)
		{
			isDraft = false;
			deckParser = DeckParser.getInstance();
			ownedCardsParser = OwnedCardsParser.getInstance();
		}
		else
		{
			isDraft = true;
			deckParser = DraftDeckParser.getInstance();
			deckParser.refreshDecks();
			ownedCardsParser = DraftOwnedParser.getInstance();
			ownedCardsParser.refreshDecks();
		}	
	}

	void verifyAndSubCardsAndDeck()
	{
		//string str = "";
		foreach (int key in currentDeck.Keys)
		{
			MyLogger.log(key);
			int numCopiesLeft = ownedCards[key] - currentDeck[key];

			ownedCards[key] = numCopiesLeft;

			if ( numCopiesLeft < 0)
			{
				MyLogger.log("ERROR!!!!!!");
			}
			else if (numCopiesLeft == 0)
			{
				ownedCards.Remove(key);
			}
			//str = str + " k " + key + " b" + numCopiesLeft; 
		}
		//MyLogger.log(str);
	}
	void retrieveOwnedCards()
	{

		baseOwnedCards = new SortedDictionary<int, int>();
		List<DeckCardData> deckData;

		if (isDraft)
		{
			GameObject optionsObj = GameObject.FindWithTag("DeckViewOptions");
			draftTeam = optionsObj.GetComponent<DeckViewOptions>().team;
			deckData = ownedCardsParser.getDeck(draftTeam).getCards();
			currentDeckNum = draftTeam;
		}
		else
		{
			deckData = ownedCardsParser.getDeck(0).getCards();
		}
		 
		foreach (DeckCardData data in deckData)
		{
			baseOwnedCards[data.cardID] = data.amount;
		}


	}
	
	void retrieveCurrentDeck()
	{
		
		setCurrentDeckInfo(deckParser.getDeck(currentDeckNum));
		
	}

	void resetOwnedCards()
	{
		//string str = "";
		ownedCards = new SortedDictionary<int, int> ();
		foreach (int key in baseOwnedCards.Keys)
		{
			//str = str + " k " + key + " b" + baseOwnedCards[key]; 
 		
			ownedCards[key] = baseOwnedCards[key];
		}
		//MyLogger.log(str);
	}

	void updateAllDecks()
	{
		List<DeckInfo> allDecks = deckParser.getAllDecks();
		
		int i = 0;
		for (; i < allDecks.Count; i++)
		{
			DeckInfo deck = allDecks[i];
			GameObject view = views[i];
			DeckListObj script = viewsScripts[i];
			ViewHelper.show(view);
			script.setDeckInfo(deck);
		}

		for (; i < numViews; i++)
		{
			GameObject view = views[i];
			ViewHelper.hide(view);
		}
	}

	//DELEGATE
	public void deckClicked(DeckInfo deck)
	{
		MyLogger.log("HELLO!!! DECK IS CLICKED !!!!");
		//TODO CHECK DECK SAVES!!!!!!! WILL  NOT SAVE ATM.
		if (true)
		{
			setCurrentDeckInfo(deck);
			updateAll (0);
		}
	}

	void setCurrentDeckInfo(DeckInfo deck)
	{
		resetOwnedCards();
		currentDeckInfo = deck;
		List<DeckCardData> deckData = deck.getCards();
		currentDeck = new SortedDictionary<int, int> ();
		foreach (DeckCardData data in deckData)
		{
			currentDeck[data.cardID] = data.amount;
		}

		deckTitleText.Text = currentDeckInfo.getName();
		verifyAndSubCardsAndDeck();
	}


	public void saveDeck()
	{
		//MyLogger.log("SAVE DECK");
		List<DeckCardData> deckCards = new List<DeckCardData>();
		foreach (int key in currentDeck.Keys)
		{
			DeckCardData data = new DeckCardData();
			data.cardID = key;
			data.amount = currentDeck[key];
			deckCards.Add(data);
		}
		DeckInfo deck = new DeckInfo(currentDeckInfo.getName());
		deck.setCards(deckCards);
		deckParser.writeDeck(currentDeckNum, deck, isDraft);

		DeckListObj script = viewsScripts[currentDeckNum];
		script.setDeckInfo(deck);

		foreach (DeckBuilderListener listener in listeners)
		{
			listener.deckSaved();
		}
	}


	private DraftModel getDraftModel()
	{
		
		GameObject draftModelObject = GameObject.FindWithTag("DraftModel");
		return draftModelObject.GetComponent<DraftModel>();
	}
	
	void addCardToDeck(Card c)
	{
		//MyLogger.log("ADD CARD TO DECK, REMOVE FROM OWNED");
		int numCopies;
		int id = c.getID ();
		if (ownedCards.TryGetValue(id, out numCopies))
		{
		}

		if (numCopies > 0)
		{
			if (currentDeck.ContainsKey(id))
			{	
				currentDeck[id] = currentDeck[id] + 1;
			}
			else
			{
				currentDeck[id] = 1;
			}
			//Remove if possible
			if (numCopies == 1)
			{
				ownedCards.Remove(id);
			}
			else
			{
				ownedCards[id] = numCopies - 1;
			}
		}
		else
		{
			MyLogger.log("ERROR????????? negative copies");
		}
	}

	void removeCardFromDeck(Card c)
	{
		MyLogger.log("REMOVE CARD FROM DECK, ADD TO OWNED");
		int numCopies;
		int id = c.getID ();
		if (currentDeck.TryGetValue(id, out numCopies))
		{
		}
		
		if (numCopies > 0)
		{
			if (ownedCards.ContainsKey(id))
			{	
				ownedCards[id] = ownedCards[id] + 1;
			}
			else
			{
				ownedCards[id] = 1;
			}
			//Remove if possible
			if (numCopies == 1)
			{
				currentDeck.Remove(id);
				MyLogger.log("NUMCOPIES OF " + id + " IN DECK " + 0);
			}
			else
			{
				currentDeck[id] = numCopies - 1;
				MyLogger.log("NUMCOPIES OF " + id + " IN DECK " + currentDeck[id]);
			}
		}
		else
		{
			MyLogger.log("ERROR????????? negative copies");
		}
	}

	void updateOwnedArea()
	{

		int pageOwned = ownedAreaScr.getPage();
		updateOwnedArea(pageOwned);
	}

	void updateDeckArea()
	{

		int pageDeck = deckAreaScr.getPage();
		updateDeckArea(pageDeck);
	}

	void updateOwnedArea(int pageOwned)
	{
		int numOwnedPerPage = ownedAreaScr.getNumCardsPerPage();
		List<int> ownedKeys = new List<int>(ownedCards.Keys);//.ToList();
		int numOwned = ownedKeys.Count;
		int lowRange = pageOwned * numOwnedPerPage;
		//If there are enough, only show max, else show difference
		int range = (numOwned - lowRange >= numOwnedPerPage ? numOwnedPerPage : numOwned - lowRange);
		if (lowRange < 0 || lowRange > numOwned)
		{
			MyLogger.log("CANT UPDATE");
			return;
		}
		List<int> passKeys = ownedKeys.GetRange(lowRange, range);

		if (passKeys.Count == 0)
		{
			pageOwned--;
			if (pageOwned < 0)
				pageOwned = 0;
			lowRange = pageOwned * numOwnedPerPage;
			range = (numOwned - lowRange >= numOwnedPerPage ? numOwnedPerPage : numOwned - lowRange);
			passKeys = ownedKeys.GetRange(lowRange, range);
		}


		SortedDictionary<int,int> dict = new SortedDictionary<int,int > ();
		foreach (int key in passKeys)
		{
			dict[key] = ownedCards[key];
		}

		ownedAreaScr.setCards(dict);
		
		ownedAreaScr.setPage(pageOwned, (int) Mathf.Ceil((float)numOwned/ (float)numOwnedPerPage));
	}

	void updateDeckArea(int pageDeck)
	{
		int numDeckPerPage = deckAreaScr.getNumCardsPerPage();
		List<int> deckKeys = new List<int>(currentDeck.Keys);//.ToList();

		int numDeck = deckKeys.Count;
		int lowRange = pageDeck * numDeckPerPage;
		int range = (numDeck - lowRange >= numDeckPerPage ? numDeckPerPage : numDeck - lowRange);
		
		if (lowRange < 0 || lowRange > numDeck)
		{
			MyLogger.log("CANT UPDATE");
			return;
		}
		List<int> passKeys = deckKeys.GetRange(lowRange, range);

		if (passKeys.Count == 0)
		{
			pageDeck--;
			if (pageDeck < 0)
				pageDeck = 0;
			lowRange = pageDeck * numDeckPerPage;
			range = (numDeck - lowRange >= numDeckPerPage ? numDeckPerPage : numDeck - lowRange);
			passKeys = deckKeys.GetRange(lowRange, range);
		}

		SortedDictionary<int,int> dict = new SortedDictionary<int,int > ();

//		MyLogger.log("KEYS " + deckKeys.ToString () + "range " + range + " lowrange " + lowRange + " numdeck " + numDeck);
		foreach (int key in passKeys)
		{
			dict[key] = currentDeck[key];
		}

		deckAreaScr.setCards(dict);
		deckAreaScr.setPage(pageDeck, (int) Mathf.Ceil((float)numDeck/ (float)numDeckPerPage));
	}

	void updateAll()
	{
		updateDeckArea();
		updateOwnedArea();
	}

	void updateAll(int page)
	{
		updateDeckArea(page);
		updateOwnedArea(page);
	}


	public void updateCards(AllCardAreaScript scr)
	{
		if (scr == ownedAreaScr)
		{
			updateOwnedArea();
		}
		else if (scr == deckAreaScr)
		{
			updateDeckArea();
		}
	}

	//CARD CLICKED
	public void cardClicked(AllCardAreaScript scr, Card c)
	{
		if (scr == ownedAreaScr)
		{
			addCardToDeck (c);
			updateAll();
		}
		else if (scr == deckAreaScr)
		{
			removeCardFromDeck (c);
			updateAll();
		}

	}

	//PAGE CHANGE CLICKED
	public void onNextDeckButtonClicked()
	{
		updateDeckArea(deckAreaScr.getPage() + 1);
	}

	public void onPrevDeckButtonClicked()
	{
		updateDeckArea(deckAreaScr.getPage() - 1);
	}

	public void onNextOwnedButtonClicked()
	{
		updateOwnedArea(ownedAreaScr.getPage() + 1);
	}

	public void onPrevOwnedButtonClicked()
	{
		updateOwnedArea(ownedAreaScr.getPage() - 1);
	}

}

