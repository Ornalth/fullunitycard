using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerDraftController : NetworkBehaviour, IDeckAreaController, TimerClock.TimerClockListener, DeckBuildVC.DeckBuilderListener {


	List<DeckCardData> unmodifiedBoosterPack = null;
	SortedDictionary<int, int> boosterCards = new SortedDictionary<int, int>();
	SortedDictionary<int, int> currentDeck = new SortedDictionary<int, int>();

	AllCardAreaScript boosterAreaScr;
	AllCardAreaScript deckAreaScr;
	int currentDeckNum = 0;

	TypogenicText deckTitleText;

	List<GameObject> views = new List<GameObject>();
	//List<CardViewScript> viewsScripts;

	public int team = -1;
	private int UPDATE_PER_X_SECONDS = 5;
	private DraftModel model;

	GameObject ownTimer;
	TimerClock ownTimerScript;
	
	private DraftModel getModel()
	{
		if (model == null)
		{
			GameObject draftModelObject = GameObject.FindWithTag("DraftModel");
			model = draftModelObject.GetComponent<DraftModel>();
		}
		return model;
	}
	
	public void setTeam(int team)
	{
		MyLogger.log("TEAM IS" + team);
		RpcSetTeam(team);
	}

	[ClientRpc]
	private void RpcSetTeam(int team)
	{
		MyLogger.log("TEAM IS" + team);
		this.team = team;
	}

	private void updateCards()
	{
		if (team == -1)
		{
			return;
		}
		//Dictionary<int, int> currentCards = getModel().getPlayerDraftDeck(this.team);
		if (getModel().hasBoosterChanged(this.team))
		{
//			MyLogger.log("UPDATED BOOSTER");
			List<DeckCardData> boosterPack = getModel().getBoosterPack(this.team);
			unmodifiedBoosterPack = boosterPack;
			boosterCards = new SortedDictionary<int, int>();
			foreach (DeckCardData data in boosterPack)
			{
				boosterCards.Add(data.cardID, 1);
			}
			updateBoosterArea();

			if (boosterPack.Count > 0)
			{
				//MyLogger.log("I SEE " + boosterPack.Count + "cards.");
				updateTimers();
				ownTimerScript.resume();
			}
		}
		else
		{
//			MyLogger.log("NO CHANGE");
		}


		if (getModel().isDraftFinished())
		{
			notifyFinished();
		}
	}


	void Start()
	{
		if (!isLocalPlayer)
		{
			MyLogger.log("NOT LOCAL PLAYER start");
			GetComponent<CloseChildrenScript>().closeChildren();
			return;
		}
		else
		{

		}
		initViews();
		init();
		updateAll (0);
	}

	protected virtual void init()
	{
		getModel().suscribeToModel(this);
		InvokeRepeating("updateCards", 0, UPDATE_PER_X_SECONDS);
		boosterCards = new SortedDictionary<int, int>();
		currentDeck = new SortedDictionary<int, int>();
		
	} 

	private void clearTimer()
	{
		ownTimerScript.pause();
		ownTimerScript.setTime(0);
	}

	private void clearBoosterArea()
	{
		boosterCards = new SortedDictionary<int, int>();
		boosterAreaScr.setPage(0, 0);
		updateBoosterArea();
	}


	void initViews()
	{
		GameObject diz = transform.Find("AllCardsArea").gameObject;
		boosterAreaScr = diz.GetComponent<AllCardAreaScript> ();
		boosterAreaScr.setHeight(ViewHelper.getScreenHeight() - 100);
		boosterAreaScr.setWidth(ViewHelper.getScreenWidth() - 100);
		GameObject high = transform.Find("CurrentDeckArea").gameObject;
		deckAreaScr =  high.GetComponent<AllCardAreaScript> ();
		deckAreaScr.setHeight(ViewHelper.getScreenHeight() - 100);
		deckAreaScr.setWidth(ViewHelper.getScreenWidth() - 100);

		ownTimer = Instantiate (Resources.Load("Timer")) as GameObject;
		ownTimer.name = "ownTimer";
		ownTimer.transform.parent = transform;
		ownTimer.transform.localPosition = new Vector3 (-7f,1.0f,0);
		ownTimerScript = ownTimer.GetComponentInChildren<TimerClock>();
		ownTimerScript.subscribe(this);

		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("DeckNameTitle"))
			{
				deckTitleText = comp;
			}
		}
	}

	public void setCurrentBoosterCards(SortedDictionary<int, int> deck)
	{
		boosterCards = deck;
		updateBoosterArea();
	}

	public void setCurrentDeckInfo(SortedDictionary<int, int> deck)
	{
		currentDeck = deck;
		updateDeckArea();
	}

	void updateBoosterArea()
	{
		int pageBooster = boosterAreaScr.getPage();
		updateBoosterArea(pageBooster);
	}

	void updateDeckArea()
	{
		int pageDeck = deckAreaScr.getPage();
		updateDeckArea(pageDeck);
	}

	void updateBoosterArea(int pageBooster)
	{
		int numBoosterPerPage = boosterAreaScr.getNumCardsPerPage();
		List<int> boosterKeys = new List<int>(boosterCards.Keys);//.ToList();
		int numBooster = boosterKeys.Count;
		int lowRange = pageBooster * numBoosterPerPage;
		//If there are enough, only show max, else show difference
		int range = (numBooster - lowRange >= numBoosterPerPage ? numBoosterPerPage : numBooster - lowRange);
		if (lowRange < 0 || lowRange > numBooster)
		{
			MyLogger.log("LOWRANGE " + lowRange + " booster " + numBooster);
			MyLogger.log("CANT UPDATE");
			return;
		}
		List<int> passKeys = boosterKeys.GetRange(lowRange, range);

		if (passKeys.Count == 0)
		{
			pageBooster--;
			if (pageBooster < 0)
				pageBooster = 0;
			lowRange = pageBooster * numBoosterPerPage;
			range = (numBooster - lowRange >= numBoosterPerPage ? numBoosterPerPage : numBooster - lowRange);
			passKeys = boosterKeys.GetRange(lowRange, range);
		}


		SortedDictionary<int,int> dict = new SortedDictionary<int,int > ();
		foreach (int key in passKeys)
		{
			dict[key] = boosterCards[key];
		}

		boosterAreaScr.setCards(dict);
		
		boosterAreaScr.setPage(pageBooster, (int) Mathf.Ceil((float)numBooster/ (float)numBoosterPerPage));
	}

	void updateDeckArea(int pageDeck)
	{
		int numDeckPerPage = deckAreaScr.getNumCardsPerPage();
		List<int> deckKeys = new List<int>(currentDeck.Keys);//.ToList();

		int numDeck = deckKeys.Count;
		int lowRange = pageDeck * numDeckPerPage;
		int range = (numDeck - lowRange >= numDeckPerPage ? numDeckPerPage : numDeck - lowRange);
		
		if (lowRange < 0 || lowRange > numDeck)
		{
			MyLogger.log("CANT UPDATE");
			return;
		}
		List<int> passKeys = deckKeys.GetRange(lowRange, range);

		if (passKeys.Count == 0)
		{
			pageDeck--;
			if (pageDeck < 0)
				pageDeck = 0;
			lowRange = pageDeck * numDeckPerPage;
			range = (numDeck - lowRange >= numDeckPerPage ? numDeckPerPage : numDeck - lowRange);
			passKeys = deckKeys.GetRange(lowRange, range);
		}

		SortedDictionary<int,int> dict = new SortedDictionary<int,int > ();

//		MyLogger.log("KEYS " + deckKeys.ToString () + "range " + range + " lowrange " + lowRange + " numdeck " + numDeck);
		foreach (int key in passKeys)
		{
			dict[key] = currentDeck[key];
		}

		deckAreaScr.setCards(dict);
		deckAreaScr.setPage(pageDeck, (int) Mathf.Ceil((float)numDeck/ (float)numDeckPerPage));
	}

	void updateAll()
	{
		updateDeckArea();
		updateBoosterArea();
	}

	void updateAll(int page)
	{
		updateDeckArea(page);
		updateBoosterArea(page);
	}


	public void updateCards(AllCardAreaScript scr)
	{
		if (scr == boosterAreaScr)
		{
			updateBoosterArea();
		}
		else if (scr == deckAreaScr)
		{
			updateDeckArea();
		}
	}


	//PAGE CHANGE CLICKED
	public void onNextDeckButtonClicked()
	{
		updateDeckArea(deckAreaScr.getPage() + 1);
	}

	public void onPrevDeckButtonClicked()
	{
		updateDeckArea(deckAreaScr.getPage() - 1);
	}

	public void onNextOwnedButtonClicked()
	{
		updateBoosterArea(boosterAreaScr.getPage() + 1);
	}

	public void onPrevOwnedButtonClicked()
	{
		updateBoosterArea(boosterAreaScr.getPage() - 1);
	}




	//CARD CLICKED
	public void cardClicked(AllCardAreaScript scr, Card c)
	{
		if (scr == boosterAreaScr)
		{
			boosterCardClicked(c);
		}
		else if (scr == deckAreaScr)
		{
			deckCardClicked(c);
		}

	}


	protected virtual void boosterCardClicked(Card c)
	{
//		MyLogger.log("SELECTED CARD!");
		if (!isLocalPlayer)
			return;
		DeckCardData data = new DeckCardData();
		data.cardID = c.getID();
		data.amount = 1;
		int index = unmodifiedBoosterPack.IndexOf(data);
		clearTimer();
		clearBoosterArea();
		addCardToDeck(c);
		CmdBoosterCardClicked(index);
	}

	[Command]
	public void CmdBoosterCardClicked(int index)
	{
		getModel().selectCard(this.team, index);

	}

	private void addCardToDeck(Card c)
	{
//		MyLogger.log("ADD CARD TO DECK");
		int numCopies;
		int id = c.getID ();
		
		if (currentDeck.ContainsKey(id))
		{	
			currentDeck[id] = currentDeck[id] + 1;
		}
		else
		{
			currentDeck[id] = 1;
		}

		updateDeckArea();
	}

	protected virtual void deckCardClicked(Card c)
	{
		MyLogger.log("??? WHAT ARE YOU DOING");
	}

	public void notifyFinished()
	{
		if (isLocalPlayer)
		{
			CancelInvoke("updateCards");
			MyLogger.log("FINISHED?");


			GameObject obj = Instantiate (Resources.Load("DeckViewOptions")) as GameObject;
			DeckViewOptions opts = obj.GetComponent<DeckViewOptions>();
			opts.isDraft = true;
			//opts.team = playerNum;
			opts.team = this.team;
			DontDestroyOnLoad(obj);
			
			GameObject deckBuilderObj = Instantiate (Resources.Load("DeckBuildModel")) as GameObject;
			//ClientScene.RegisterPrefab(deckBuilderObj);
			//NetworkServer.Spawn(deckBuilderObj);
			
			//NetworkServer.Destroy(this.gameObject);
			

			beginDeckBuilding();
		}
	}

	public void beginDeckBuilding()
	{
		GameObject deckModel = GameObject.FindWithTag("DeckBuildModel");
		DeckBuildVC vc = deckModel.GetComponent<DeckBuildVC>();
		vc.subscribe(this);
		GetComponent<CloseChildrenScript>().closeChildren();
	}

	public void deckSaved()
	{
		if (!isLocalPlayer)
			return;
		CmdDeckSaved();
	}

	[Command]
	public void CmdDeckSaved()
	{
		getModel().deckSaved(this.team);
	} 

	public void mainTimeComplete(TimerClock timeScript)
	{
		//coolio
	}
	public void reserveTimeComplete(TimerClock timeScript)
	{
		//coolio
	}
	public void allTimeComplete(TimerClock timeScript)
	{
		if (timeScript == ownTimerScript)
		{
			CmdTimerComplete(team);
		}
		else
		{
			CmdTimerComplete(team);
		}
	}

	[Command]
	public void CmdTimerComplete(int team)
	{
		getModel().timerComplete(team);
	}

	private void updateTimers()
	{
		PlayerTimer timer = model.getPlayerTimer(this.team);
		ownTimerScript.setTime(timer.getTime());
		ownTimerScript.setReserveTime(timer.getReserveTime());
	}


	[ClientRpc]
	public void RpcDecksSubmitted()
	{
		Application.LoadLevel("GameScene");
	}
}
